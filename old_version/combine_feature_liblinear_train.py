__author__ = 'prasha'
import xml.etree.ElementTree as xml
import os
import codecs
import sys
import shutil
import nltk
from pprint import pprint
from collections import Counter


#username can have '_' in it. so, start indexing from the end after splitting by '_'
#to train on just age or gender just change the label in create_features

def char_ngrams0(str, n):
    arr = list(str)
    ngrams = nltk.ngrams(arr, n) #returns tuples
    str_ngrams = []
    for ngram in ngrams:
        str_ngrams.append("".join(ngram))
    return str_ngrams

def tuple_char_ngrams(str, n):
    arr = list(str)
    return nltk.ngrams(arr, n)

def char_ngrams1(str, n):
    str_ngrams = []
    for i in range(0, len(str)-n+1):
        str_ngrams.append(str[i:n+i])
    return str_ngrams

def process_other_features(other_features_file, username_text_dir):
    files = next(os.walk(username_text_dir))[2] #get just the files and not directories
    fname_userid_dict = {}
    for file in files:
        stats = file.split("_")
        userid = stats[-4]
        fname_list = [userid.zfill(8)]
        fname_list.extend(stats[-3:])
        fname = "_".join(fname_list)
        fname_userid_dict[fname] = userid

    userid_features_dict = {}
    for line in open(other_features_file, 'r').readlines():
        all = line.split(' ',1)
        fname = all[0].strip("/")
        if(fname in fname_userid_dict):
            userid = fname_userid_dict[fname]
            userid_features_dict[userid] = all[1].strip()
    return userid_features_dict

# def del_later(username_text_dir):
#     files = next(os.walk(username_text_dir))[2] #get just the files and not directories
#     for file in files:
#         stats = file.split("_")
#         # print file
#         # print stats
#         gender = stats[-1].split(".")[0]
#         age = stats[-2]
#         lang = stats[-3]
#         id = stats[-4]
#         username = "_".join(stats[:-4])
#         label = age + "_" + gender
#
#         fname = [id.zfill(8)]
#         fname.extend(stats[-3:])
#         print "_".join(fname), "_".join(stats[:-4])

class UsernameLibLinear:

    def __init__(self, username_text_dir, output_fHandle, userdetails_fHandle, threshold, start_at=1, userid_otherfeatures_dict={}):
        self.username_text_dir = username_text_dir
        self.output_fHandle = output_fHandle
        self.userdetails_fHandle = userdetails_fHandle
        self.threshold = threshold
        self.start_at = start_at
        self.userid_otherfeatures_dict = userid_otherfeatures_dict
        self.label_instances_map = {} #{key(18-24_female): {userid1: {feature1: val1, feature2: val2}, userid2: {feature1: val1, feature2: val2}, ...], key(18-24_male): {userid1: {feature1: val1, feature2: val2}, userid2: {feature1: val1, feature2: val2}, ...], ...}
        self.user_detail_features = {}
        self.label_list = []
        self.feature_list = []
        self.create_features()
        # self.remove_infrequent_features(threshold)
        self.write_to_file()

    def create_features(self):
        #user detail features
        for line in self.userdetails_fHandle.readlines():
            details = line.split(",")
            id = details[0]
            username = details[1]
            features = details[-40:]
            self.user_detail_features[id] = features

        files = next(os.walk(self.username_text_dir))[2] #get just the files and not directories
        for file in files:
            fHandle = open(os.path.join(self.username_text_dir, file), 'r').read()
            if len(fHandle) < 250:
                continue
            stats = file.split("_")
            # print file
            # print stats
            gender = stats[-1].split(".")[0]
            age = stats[-2]
            lang = stats[-3]
            id = stats[-4]
            username = "_".join(stats[:-4])
            label = age + "_" + gender
            # label = gender

            # self.update_maps(id, {}, label)
#or all#
            username_start_end_marker = "%" + username + "%"
            bigrams = char_ngrams1(username_start_end_marker, 2)
            trigrams = char_ngrams1(username_start_end_marker, 3)
            fourgrams = char_ngrams1(username_start_end_marker, 4)
            fivegrams = char_ngrams1(username_start_end_marker, 5)
            features = bigrams + trigrams + fourgrams + fivegrams
            cfeatures = Counter(features)
            maxm = cfeatures.most_common()[0][1] * 1.0
            for k, v in cfeatures.iteritems():
                 cfeatures[k] /= maxm
            self.update_maps(id, cfeatures, label)

    def update_maps(self, id, features, label):
        self.label_list.append(label)
        self.label_list = list(set(self.label_list))
        self.feature_list = list(set(self.feature_list+features.keys()))
        if label not in self.label_instances_map:
            self.label_instances_map[label] = {id: features}
        else:
            self.label_instances_map[label][id] = features

    def remove_infrequent_features(self, threshold):
        feature_idf_count_map = {key: 0 for key in self.feature_list}
        for label, instances in self.label_instances_map.iteritems(): #instances: all instances/people of certain class(label)
            for instance in instances:  #instance: features of an instance/person
                for feature in sorted(instance):
                    count = instance[feature]
                    feature_idf_count_map[feature] += count
        less_than_threshold = [k for k in feature_idf_count_map.keys() if feature_idf_count_map[k] < threshold]
        print feature_idf_count_map
        print less_than_threshold
        for x in less_than_threshold:
            self.feature_list.remove(x)

    def write_to_file(self):
        self.label_list = sorted(self.label_list)
        self.feature_list = sorted(self.feature_list)
        features_count = len(self.feature_list)
        # print self.label_list
        # print self.feature_list
        # pretty_print(self.feature_list)
        self.write_list_to_file(self.label_list, "labels_features/labels")
        self.write_list_to_file(self.feature_list, "labels_features/features")
        # self.write_list_to_file(self.label_list, "liblinear_op/labels_only_disease")
        # self.write_list_to_file(self.feature_list, "liblinear_op/features_only_disease")
        for label, instances in self.label_instances_map.iteritems(): #instances: all instances/people of certain class(label)
            # idx = str(self.label_list.index(label))
            label_idx = self.label_list.index(label) + 1
            for userid, instance in instances.iteritems():  #instance: features of an instance/person #userid=id + "_" + username
                line = str(label_idx) + " "
                if(userid in self.userid_otherfeatures_dict):
                    line += self.userid_otherfeatures_dict[userid] + " "
                for feature in sorted(instance):
                    count = instance[feature]
                    if(feature in self.feature_list):
                        feature_idx = self.feature_list.index(feature) +1 + self.start_at
                        line += str(feature_idx) + ":" + str(count) + " "
                i = features_count + 1 + self.start_at
                for detail_feature in self.user_detail_features[userid]:
                    if(float(detail_feature) != 0.0):
                        line += str(i) + ":" + detail_feature + " "
                    i += 1
                line = line.strip() + "\n"
                # print line
                self.output_fHandle.write(line)
        self.output_fHandle.flush()


    def write_list_to_file(self, list1, fname):
        fHandle = open(fname, 'w')
        for k,v in enumerate(list1):
            fHandle.write(str(k+1) + "\t" + str(v) + "\n")

        #idx already calculated before, maybe not such a good idea since list(set()) might change it
    def update_maps_idx(self, features, label):
        self.label_list.append(label)
        self.label_list = list(set(self.label_list))
        # self.feature_list.extend(features.keys())
        # self.feature_list = list(set(self.feature_list))
        self.feature_list = list(set(self.feature_list+features.keys()))
        label_idx = self.label_list.index(label)
        features_with_idx = {}
        for f, c in features.iteritems():
            features_with_idx[self.feature_list.index(f)] = c
        if label_idx not in self.label_instances_map:
            self.label_instances_map[label_idx] = [features_with_idx]
        else:
            self.label_instances_map[label_idx].append(features_with_idx)


#extra
def pretty_print(list1):
    for k,v in enumerate(list1):
        print k, ":", v,
    print "\n"

# ./train -s 7 ~/Dropbox/DailyStrength/liblinear_op/username_liblinear_train dailystrength1.model
# ~/Documents/softwares/liblinear-1.94/predict ~/Dropbox/test/username_liblinear_test dailystrength.model dailystrength.out
if __name__ == "__main__":
    threshold = 2

    # username_text_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/small_username_data_pan14_train_test/train"
    # user_details_file = "/home/prasha/Dropbox/DailyStrength/initial_analysis/userid_userdetails_small.txt"

    #username_text_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/processed_data/small_test/files"
    #user_details_file = "/home/prasha/Documents/dailystrength/author_profiling_data/processed_data/small_test/small_userid_userdetails.txt"
    #other_features_file = "/home/prasha/Documents/dailystrength/author_profiling_data/processed_data/small_test/small.vectors"
    #output_file = "/home/prasha/Documents/dailystrength/author_profiling_data/processed_data/small_test/test_username_liblinear_train"

    # username_text_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/username_data_pan14_train_test/train"
    # user_details_file = "/home/prasha/Documents/dailystrength/author_profiling_data/processed_data/features/userid_userdetails.txt"
    # other_features_file = "/home/prasha/Documents/dailystrength/author_profiling_data/processed_data/features/dailystrength_tfidf.vectors"
    # output_file = "/home/prasha/Documents/dailystrength/author_profiling_data/processed_data/output2/combined_features_liblinear_train"


    username_text_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/train"
    user_details_file = "/home/prasha/Documents/dailystrength/author_profiling_data/processed_data/features/userid_userdetails.txt"
    other_features_file = "/home/prasha/Documents/dailystrength/author_profiling_data/processed_data/features/dailystrength_tfidf.vectors"
    output_file = "/home/prasha/Documents/dailystrength/author_profiling_data/processed_data/output2/combined_features_liblinear_train"


    start_at = 10202954
    userid_otherfeatures_dict = process_other_features(other_features_file, username_text_dir)
    ull = UsernameLibLinear(username_text_dir, open(output_file, 'w'), open(user_details_file, 'r'), threshold, start_at, userid_otherfeatures_dict)

    # del_later(username_text_dir)



