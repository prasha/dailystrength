#!/bin/sh

#change liblinear_op_path in both files
#example:
#/Users/shrprasha/Projects/dailystrength/resources/run_liblinear_all.sh dev all_features
liblinear_op_path="/Users/shrprasha/Projects/resources/dailystrength/liblinear/dev_normalized"

echo "running gender"
 /Users/shrprasha/Projects/dailystrength/resources/run_liblinear.sh $2 gender $1
echo "running age"
 /Users/shrprasha/Projects/dailystrength/resources/run_liblinear.sh $2 age $1
echo "running both"
 /Users/shrprasha/Projects/dailystrength/resources/run_liblinear.sh $2 both $1
# echo "get combined accuracy from separate age and gender"
python2.7 /Users/shrprasha/Projects/dailystrength/new_version/get_combined_age_gender_from_separate_run.py "$liblinear_op_path/$2" "$liblinear_op_path/$2/both/$1.txt" 
