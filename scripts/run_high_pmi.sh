#!/bin/sh

#usage:
#/Users/shrprasha/Projects/dailystrength/resources/run_liblinear.sh op_folder_name(usu. feature types) age_gender_both dev_or_test
#example:
#/Users/shrprasha/Projects/dailystrength/resources/run_liblinear.sh all_features both test

#to change to type of features: change in /Users/shrprasha/Projects/dailystrength/new_version/combine_feature_liblinear.py 

#liblinear_op_path="/Users/shrprasha/Projects/resources/dailystrength/liblinear/"
#liblinear_op_path="/Users/shrprasha/Projects/resources/dailystrength/liblinear/dev_normalized"
#scripts_path="/Users/shrprasha/Projects/dailystrength/new_version"
#dataset_path="/Users/shrprasha/Projects/resources/dailystrength"


rootdir="/Users/shrprasha/Projects/resources/dailystrength/features/high_pmi_removed"
output_path="$rootdir/output"

echo "training age"
/Users/shrprasha/Downloads/Softwares/liblinear-1.96/train -s 0 -c 1 "$rootdir/train/age" "$output_path/age/model"
echo "testing on age"
/Users/shrprasha/Downloads/Softwares/liblinear-1.96/predict -b 1 "$rootdir/test/age" "$output_path/age/model" "$output_path/age/out.txt"

echo "training gender"
/Users/shrprasha/Downloads/Softwares/liblinear-1.96/train -s 0 -c 1 "$rootdir/train/gender" "$output_path/gender/model"
echo "testing on age"
/Users/shrprasha/Downloads/Softwares/liblinear-1.96/predict -b 1 "$output_path/test/gender" "$output_path/gender/model" "$output_path/gender/out.txt"
python2.7 /Users/shrprasha/Projects/dailystrength/new_version/get_combined_age_gender_from_separate_run.py "$output_path" "$rootdir/test/both" 


#echo "training age"
#/Users/shrprasha/Downloads/Softwares/liblinear-1.96/train -s 0 -c 1 "$rootdir/train.txt" "$output_path/model"
#echo "testing on age"
#/Users/shrprasha/Downloads/Softwares/liblinear-1.96/predict -b 1 "$output_path/$3.txt" "$output_path/model" "$output_path/out.txt"
#
## echo "training svm"
##/Users/shrprasha/Downloads/Softwares/liblinear-1.96/train -s 2 -c 1 "$output_path/train.txt" "$output_path/model"
## echo "testing on svm"
## /Users/shrprasha/Downloads/Softwares/liblinear-1.96/predict "$output_path/$3.txt" "$output_path/model" "$output_path/out.txt"
#
#echo "getting age & gender accuracy, not relevant for age only or gender only analysis"
#python2.7 "$scripts_path/get_confusionmatrix_accuracy_liblinearop.py" "$output_path/out.txt" "$output_path/$3.txt" $2

