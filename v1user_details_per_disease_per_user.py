__author__ = 'prasha'
import os
from bs4 import BeautifulSoup #pip install beautifulsoup4

import sys
from pprint import pprint


def update_age(age_groups, age):
    if(age.isdigit()):
        for ages in sorted(age_groups):
            start, end = ages
            if(int(age) in range(start, end)):
                age_groups[ages] += 1


def update_gender(genders, gender):
    if(gender in genders):
        genders[gender] += 1

def wiki_print(disease, data):
    tot = 0
    vals = data.values()
    print data
    print "| ", disease,
    maxv = max(vals)
    for v in vals:
        if(v == maxv):
            print "|_. " + str(v),
        else:
            print "| " + str(v),
        tot += v
    print "|", tot, "|"

if __name__ == "__main__":
    id_age_gender_fname = "id_age_gender.txt"
    rootdir = "/mnt/docsig/storage/daily-strength"
    #diseases = ["Acne"]
    diseases = ["Acne", "ADHD", "Alcoholism", "Asthma", "Back-Pain", "Bipolar-Disorder", "Bone-Cancer", "COPD", "Diets-Weight-Maintenance", "Fibromyalgia", "Gastric-Bypass-Surgery", "Immigration-Law", "Infertility", "Loneliness", "Lung-Cancer", "Migraine", "Miscarriage", "Pregnancy", "Rheumatoid-Arthritis", "War-In-Iraq"]
    all_disesase_opfile = open(os.path.join("per_disease", "all_diseases.csv"), 'w')

    lines = open(id_age_gender_fname, 'r').readlines()
    age_gender_for_id = {}
    for line in lines:
        id, age, gender = line.strip().split(",")
        age_gender_for_id[id] = [age, gender]

    for disease in diseases:
        age_groups = {(10,20) : 0, (20,30) : 0, (30,40) : 0, (40,50) : 0, (50,60) : 0, (60,70) : 0, (70, 5000): 0}
        genders = {"Female": 0, "Male": 0}
        user_posts = open(os.path.join(rootdir, disease, "userPostList.txt"), 'r').readlines()
        user_ids = []
        for post in user_posts:
            id = post.split(",")[0]
            user_ids.append(id)
        uniq_user_ids = set(user_ids)
        #print user_ids
###############################33
#        print len(user_ids), len(uniq_user_ids),
#
#        count = 0
#        for id in user_ids:
#            if(id in age_gender_for_id):
#                count += 1
#        print count,

#######################################3
        opfile = open(os.path.join("per_disease", disease + ".csv"), 'w')
        #count = 0
        for id in uniq_user_ids:
            if(id in age_gender_for_id):
                age, gender = age_gender_for_id[id]
                data = id + "," + age +"," + gender + "\n"
                update_age(age_groups, age.strip())
                update_gender(genders, gender.strip())
                opfile.write(data)
                all_disesase_opfile.write(disease + "," + data)
                #count += 1
        opfile.close()
        #print count
        #print "\n\n\n" + disease
        #pprint(age_groups)
        #pprint(genders)
        #wiki_print(disease, age_groups)
        wiki_print(disease, genders)
