__author__ = 'prasha'
import xml.etree.ElementTree as xml
import os
import codecs
import sys
import shutil

if __name__ == "__main__":
    # people_html_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/data_pan14_1"
    # age_gender_file = "/home/prasha/Dropbox/DailyStrength/username_id_age_gender_both_age_gender_train.txt"
    # output_file_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/username_data_pan14_train_test/train"

    # age_gender_file = "/home/prasha/Dropbox/DailyStrength/username_id_age_gender_both_age_gender_test.txt"
    # output_file_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/username_data_pan14_train_test/test"

    files = next(os.walk(people_html_dir))[2] #get just the files and not directories
    lines = open(age_gender_file, 'r').readlines()
    id_fname_map = {}
    for file in files:
        id = file.split("_")[0]
        id_fname_map[id] = file
    for line in lines:
        print line + "\n"
        username, id, age, gender = line.strip().split(",")
        fname = id_fname_map[id.zfill(8)]
        fname_without_zfillid = "_".join(fname.split("_")[1:])
        shutil.copyfile(os.path.join(people_html_dir, fname), os.path.join(output_file_dir, username + "_" + id + "_" + fname_without_zfillid))



