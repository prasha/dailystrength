__author__ = 'prasha'
import xml.etree.ElementTree as xml
import os
import codecs
import sys
import shutil
import nltk
from pprint import pprint
from collections import Counter


#username can have '_' in it. so, start indexing from the end after splitting by '_'

def char_ngrams0(str, n):
    arr = list(str)
    ngrams = nltk.ngrams(arr, n) #returns tuples
    str_ngrams = []
    for ngram in ngrams:
        str_ngrams.append("".join(ngram))
    return str_ngrams

def tuple_char_ngrams(str, n):
    arr = list(str)
    return nltk.ngrams(arr, n)

def char_ngrams1(str, n):
    str_ngrams = []
    for i in range(0, len(str)-n+1):
        str_ngrams.append(str[i:n+i])
    return str_ngrams

class UsernameLibLinear:

    def __init__(self, username_text_dir, output_fHandle, dict_files_dir, userdetails_fHandle, start_at):
        self.username_text_dir = username_text_dir
        self.output_fHandle = output_fHandle
        self.dict_files_dir = dict_files_dir
        self.start_at = start_at
        self.userdetails_fHandle = userdetails_fHandle
        # self.label_list = self.load_labels(os.path.join(self.dict_files_dir, "labels_only_disease"))
        # self.feature_list = self.load_features(os.path.join(self.dict_files_dir, "features_only_disease"))
        self.label_list = self.load_labels(os.path.join(self.dict_files_dir, "labels"))
        self.feature_list = self.load_features(os.path.join(self.dict_files_dir, "features"))
        # print self.label_list
        # print self.feature_list
        self.user_detail_features = {}
        self.label_instances_map = {}
        for label in self.label_list:
            self.label_instances_map[label] = {}
        self.create_features()
        self.write_to_file()

    def load_labels(self, path):
        lines = open(path).readlines()
        list1 = [''] * (len(lines) + 1)
        for line in lines:
            # print line
            idx, val = line.split("\t")
            list1[int(idx)] = val.strip("\n")
        return list1

    def load_features(self, path):
        lines = open(path).readlines()
        list1 = [()] * (len(lines) + 1)
        for line in lines:
            # print line
            idx, val = line.split("\t")
            # list1[int(idx)] = self.str_to_tuple(val)
            list1[int(idx)] = val.strip("\n")
        return list1

    def str_to_tuple(self, str):
        str = str.strip(")(")
        elems = str.split(",")
        tup = ()
        for elem in elems:
            elem = elem.strip().strip("'")
            tup += (elem,)
        return tup

    def create_features(self):
        for line in self.userdetails_fHandle.readlines():
            details = line.split(",")
            id = details[0]
            username = details[1]
            features = details[-40:]
            self.user_detail_features[id + "_" + username] = features
        files = next(os.walk(self.username_text_dir))[2] #get just the files and not directories
        for file in files:
            stats = file.split("_")
            gender = stats[-1].split(".")[0]
            age = stats[-2]
            lang = stats[-3]
            id = stats[-4]
            username = "_".join(stats[:-4])
            label = age + "_" + gender

            # self.update_maps(id + "_" + username, {}, label)

            username_start_end_marker = "%" + username + "%"
            bigrams = char_ngrams1(username_start_end_marker, 2)
            trigrams = char_ngrams1(username_start_end_marker, 3)
            fourgrams = char_ngrams1(username_start_end_marker, 4)
            fivegrams = char_ngrams1(username_start_end_marker, 5)
            features = bigrams + trigrams + fourgrams + fivegrams
            self.update_maps(id + "_" + username, Counter(features), label)

    def update_maps(self, id, features, label):
        if label in self.label_instances_map:
            self.label_instances_map[label][id] = features

    def write_to_file(self):
        features_count = len(self.feature_list)
        for label, instances in self.label_instances_map.iteritems(): #instances: all instances/people of certain class(label)
            label_idx = self.label_list.index(label)
            for userid, instance in instances.iteritems():  #instance: features of an instance/person
                # line = str(label_idx) + " "
                line = str(userid) + "\t" + str(label_idx) + "\t"
                feature_idx_counts = {}
                for feature in sorted(instance):
                    count = instance[feature]
                    if feature in self.feature_list:
                        feature_idx = self.feature_list.index(feature) + start_at
                        line += str(feature_idx) + ":" + str(count) + " "
                #         feature_idx_counts[feature_idx] = count
                # for feature_idx in sorted(feature_idx_counts):
                #     count = feature_idx_counts[feature_idx]
                #     line += str(feature_idx) + ":" + str(count) + " "
                i = features_count + start_at
                for detail_feature in self.user_detail_features[userid]:
                    if(int(detail_feature) != 0):
                        line += str(i) + ":" + detail_feature + " "
                    i += 1
                line = line.strip() + "\n"
                # print line
                self.output_fHandle.write(line)


    def write_list_to_file(self, list1, fname):
        fHandle = open(fname, 'w')
        for k,v in enumerate(list1):
            fHandle.write(str(k) + " " + str(v) + "\n")


#extra
def pretty_print(list1):
    for k,v in enumerate(list1):
        print k+1, ":", v,
    print "\n"

if __name__ == "__main__":
    username_text_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/username_data_pan14_train_test/test"
    user_details_file = "/home/prasha/Dropbox/DailyStrength/userid_userdetails.txt"

    # output_file = "liblinear_op/disease_only_liblinear_test"
    output_file = "liblinear_op/disease_username_liblinear_test"
    dict_files_dir = "liblinear_op"

    start_at = 10202954
    ull = UsernameLibLinear(username_text_dir, open(output_file, 'w'), dict_files_dir, open(user_details_file, 'r'), start_at)






