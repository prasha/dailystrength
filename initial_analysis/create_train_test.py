__author__ = 'shrprasha'

import shutil
import os
import random

# old_data_list_together:  107
# old_data_list_additional:  14248
# old total: 14355
# new_data_list_additional:  19402
# total: 33757
# 20254 6751 6752
# after removing short files:
# 20006 6642 6644 total: 33292

def change_plus_to_xx():
    # from_to_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/all_data/together"
    from_to_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/additional_data"
    files = next(os.walk(from_to_dir))[2] #get just the files and not directories
    for doc in files:
        if "65-plus" in doc:
            id, _, age, gender = doc.split("_")
            os.rename(os.path.join(from_to_dir, doc), os.path.join(from_to_dir, id + "_en_" + "65-xx" + "_" + gender + ".txt"))


def transfer_old_data_wo_username():
    from_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/username_data_pan14_train_test/together"
    to_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/all_data/together"

    files = next(os.walk(from_dir))[2] #get just the files and not directories
    print len(files)
    for doc in files:
        stats = doc.split("_")
        gender = stats[-1].split(".")[0]
        age = stats[-2]
        lang = stats[-3]
        id = stats[-4]
        shutil.copyfile(os.path.join(from_dir, doc), os.path.join(to_dir, id + "_en_" + age + "_" + gender + ".txt"))
    return files

def subtract_lists(l1, l2):
    return list(set(l1).difference(l2))

def get_random_additional_data():
    from_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/new_data"
    train_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/train"
    dev_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/dev"
    test_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/test"

    files = next(os.walk(from_dir))[2] #get just the files and not directories
    train_files = random.sample(files, (33757*60/100) - 14355)
    files = subtract_lists(files, train_files)
    dev_files = random.sample(files, (33757*20/100))
    test_files = subtract_lists(files, dev_files)

    print len(train_files), len(dev_files), len(test_files)

    copy_files(from_dir, train_dir, train_files)
    copy_files(from_dir, dev_dir, dev_files)
    copy_files(from_dir, test_dir, test_files)

def copy_files(from_dir, to_dir, files):
    for doc in files:
        shutil.copyfile(os.path.join(from_dir, doc), os.path.join(to_dir, doc))

def get_old_usernames_ids():
    op_fhandle = open("/Users/shrprasha/Dropbox/DailyStrength/username_id_age_gender_both_age_gender.txt", "w")
    id_age_gender_lines = open("/Users/shrprasha/Projects/resources/dailystrength/old/id_age_gender.txt", "r").readlines()
    id_age_gender = {}
    for line in id_age_gender_lines:
        uid, age, gender = line.strip().split(",")
        id_age_gender[uid] = (age, gender)
    firstname_lines = open("/Users/shrprasha/Projects/resources/dailystrength/old/userid_firstname_with_class", "r").readlines()
    for line in firstname_lines:
        uid, uname, _ = line.strip().split(",", 2)
        if uid in id_age_gender:
            op_fhandle.write(uname + "," + uid + "," + age + "," + gender + "\n")
        else:
            print uid
    op_fhandle.close()


if __name__ == "__main__":
    # # files1 = transfer_old_data_wo_username()
    # get_random_additional_data()
    # # print len(set(files1) & set(files1))
    #
    # # change_plus_to_xx()

    get_old_usernames_ids()