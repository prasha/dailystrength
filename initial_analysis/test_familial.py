from collections import Counter
import os
from dill import load
from user import Post

__author__ = 'shrprasha'
import re

def find_matches(text, regex_list):
    regx_matches = []
    for regxp in regex_list:
        rexp = re.compile(regxp, re.IGNORECASE)
        matches = rexp.findall(text)
        regx_matches.extend(matches)
    return regx_matches

def get_bucket_count_each_token(matches, buckets, each_token):
    print "matches: ", matches
    counts_bucket = {}
    counts_each_token = []
    words = []
    bc = {}
    # init
    for bucket in buckets:
        counts_bucket[bucket] = 0
        ###
        bc[bucket] = []
    #count word in bucket
    for match in matches:
        word = match.lower()
        temp = sum(counts_bucket.values())
        for bucket, bucket_tokens in buckets.iteritems():
            if word in bucket_tokens:
                counts_bucket[bucket] += 1
                ###
                bc[bucket].append(word)
        if temp == sum(counts_bucket.values()):
            print match
            print "error:", word, counts_bucket
        words.append(word)
    words_counter = Counter(words)
    for token in each_token:
        if token in words_counter:
            counts_each_token.append(words_counter[token])
        else:
            counts_each_token.append(0)
    #print bc
    return counts_bucket, counts_each_token

def get_familial_token_feature(text):
    familial_matches = find_matches(text, regex_list_familial)

    counts_bucket, counts_each_token = get_bucket_count_each_token(familial_matches, gender_buckets, each_token)
    #since order is important
    counts_bucket_idx_feature = dict(zip(range(1, len(counts_bucket) + 1), [counts_bucket["male"], counts_bucket["female"], counts_bucket["neutral"]]))
    counts_each_token_idx_feature = dict(zip(range(1, len(counts_each_token) + 1), counts_each_token))
    print counts_bucket_idx_feature, counts_each_token_idx_feature

if __name__ == "__main__":
    user_posts_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps"
    fname = "100926_19-28"
    text = ""
    regex_list_familial = [
        r"\bmy\b\W+\b((?:son|daughter|grandson|granddaughter|father|mother|brother|sister|uncle|aunt|cousin|nephew|niece|family|godson|goddaughter|grandchild|grandmother|grandfather|husband|wife|bf|gf|boyfriend|girlfriend|hubby|baby|babies|child|children|kids|mom|parent))\W*\b",
        r"\b(?:DH|DS|DD|DW)\b"]
    gender_buckets = {"male": [u'wife', u'gf', u'girlfriend', u'dw'],
                      "female": [u'husband', u'bf', u'boyfriend', u'dh', u'hubby'],
                      "neutral": [u'ds', u'dd', u'son', u'daughter', u'grandson', u'granddaughter', u'father',
                                  u'mother', u'brother', u'sister', u'uncle', u'aunt', u'cousin', u'nephew', u'niece',
                                  u'family', u'godson', u'goddaughter', u'grandchild', u'grandmother', u'grandfather',
                                  u'baby', u'babies', u'child', u'children', u'kids', u'mom', u'parent']}
    each_token = [u'son', u'daughter', u'grandson', u'granddaughter', u'father', u'mother', u'brother', u'sister',
                  u'uncle', u'aunt', u'cousin', u'nephew', u'niece', u'family', u'godson', u'goddaughter',
                  u'grandchild', u'grandmother', u'grandfather', u'husband', u'wife', u'bf', u'gf', u'boyfriend',
                  u'girlfriend', u'dh', u'ds', u'dd', u'dw', u'baby', u'babies', u'child', u'children', u'kids', u'mom',
                  u'parent', u'hubby']

    with open(os.path.join(user_posts_dump_rootdir, fname), "r") as fhandle:
        user_posts = load(fhandle)
        for post in user_posts:
            text += post.text + "\n"
    get_familial_token_feature(text)
    all_in_buckets = gender_buckets["male"] + gender_buckets["female"] + gender_buckets["neutral"]
    print len(each_token), len(all_in_buckets)
    print set(each_token) - set(all_in_buckets)
    print set(all_in_buckets) - set(each_token)







