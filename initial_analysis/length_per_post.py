__author__ = 'prasha'
import os
import codecs
import os
import sys
from pprint import pprint
import math
import re
import nltk
import utils
from pprint import pprint


def get_words(sentence):
    return nltk.word_tokenize(sentence)


def get_sentences(document):
    sentences = nltk.sent_tokenize(document)
    return sentences


def no_of_words_in_text(text):
    count = 0
    for sentences in get_sentences(text):
        count += len(get_words(sentences))
    return count


def find_counts_folder_gender(username_folder):
    username_files = next(os.walk(username_folder))[2]
    avg_counts = {"male": 0.0, "female": 0.0}
    tot = {"male": 0, "female": 0}
    for ufile in sorted(username_files):
        # print ufile
        stats = ufile.split("_")
        gender = stats[-1].split(".")[0]
        age = stats[-2]

        lines = codecs.open(os.path.join(username_folder, ufile), 'r', encoding='utf-8').readlines()
        tot_words = 0
        tot_posts_replies_count = len(lines)
        for line in lines:
            tot_words += no_of_words_in_text(line)
        print ufile + "\t" + age + "\t" + gender + "\t" + str(tot_words) + "\t" + str(
            tot_posts_replies_count) + "\t" + str(tot_words * 1.0 / tot_posts_replies_count)
    print avg_counts
    print tot
    print avg_counts["male"] / tot["male"], avg_counts["female"] / tot["female"]


def find_counts_folder_age_gender(username_folder):
    username_files = next(os.walk(username_folder))[2]
    word_counts = {"18-24": {"male": 0, "female": 0}, "25-34": {"male": 0, "female": 0},
                   "35-49": {"male": 0, "female": 0}, "50-64": {"male": 0, "female": 0},
                   "65-xx": {"male": 0, "female": 0}}
    post_counts = {"18-24": {"male": 0, "female": 0}, "25-34": {"male": 0, "female": 0},
                   "35-49": {"male": 0, "female": 0}, "50-64": {"male": 0, "female": 0},
                   "65-xx": {"male": 0, "female": 0}}
    tot_counts = {"18-24": {"male": 0, "female": 0}, "25-34": {"male": 0, "female": 0},
                   "35-49": {"male": 0, "female": 0}, "50-64": {"male": 0, "female": 0},
                   "65-xx": {"male": 0, "female": 0}}
    for ufile in sorted(username_files):
        id, _, age, gender = ufile.split(".")[0].split("_")
        lines = codecs.open(os.path.join(username_folder, ufile), 'r', encoding='utf-8').readlines()
        tot_words = utils.word_count_in_file_wo_urls_lines(lines)
        tot_posts_replies_count = len(lines)
        word_counts[age][gender] += tot_words
        post_counts[age][gender] += tot_posts_replies_count
        tot_counts[age][gender] += 1
        print ufile + "\t" + age + "\t" + gender + "\t" + str(tot_words) + "\t" + str(
            tot_posts_replies_count) + "\t" + str(tot_words * 1.0 / tot_posts_replies_count)
    print word_counts
    print post_counts
    print tot_counts
    pprint(word_counts)
    pprint(post_counts)
    pprint(tot_counts)


# familial
#boyfriend, husband, wife, bf, gf
username_folder = "/home/prasha/Documents/dailystrength/author_profiling_data/train"
# username_folder = "/home/prasha/Documents/dailystrength/author_profiling_data/small_test"
find_counts_folder_age_gender(username_folder)


#ufile = "/home/prasha/Documents/dailystrength/author_profiling_data/username_data_pan14_train_test/test/seed29_1782036_en_25-34_female.txt"
#ufile = "test"
#text = codecs.open(ufile, 'r', encoding='utf-8').read()
#print find_counts(text, regex_list)