__author__ = 'prasha'
import os
import nltk
from nltk.tag.stanford import NERTagger



if __name__ == "__main__":
    n = 10
    username_text_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/username_data_pan14_train_test/together"
    userid_age_gender_file = "/home/prasha/Documents/dailystrength/author_profiling_data/userid_age_gender"
    fHandle = open(userid_age_gender_file, 'w')
    files = next(os.walk(username_text_dir))[2] #get just the files and not directories
    count_having_firstname = 0
    for fname in sorted(files):
        stats = fname.split("_")
        gender = stats[-1].split(".")[0]
        age = stats[-2]
        lang = stats[-3]
        id = stats[-4]
        username = "_".join(stats[:-4])
        label = age + "_" + gender

        fHandle.write(id + "," + username + "," + age + "," + gender + "," + label +  "\n")


