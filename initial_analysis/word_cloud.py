__author__ = 'shrprasha'

import os
from nltk.corpus import stopwords
from collections import Counter
import utils
import operator
# from pytagcloud import create_tag_image, make_tags
# from pytagcloud.lang.counter import get_tag_counts

def get_words_for_each_class():
    files = next(os.walk(people_html_dir))[2] #get just the files and not directories

    age_groups = {"18-24": [],  "25-34": [],  "35-49": [],  "50-64": [],  "65-xx": []}
    genders = {"male": [], "female": []}
    combined = {"18-24_male": [],  "25-34_male": [],  "35-49_male": [],  "50-64_male": [],  "65-xx_male": [], "18-24_female": [],  "25-34_female": [],  "35-49_female": [],  "50-64_female": [],  "65-xx_female": []}
    filter_words = stopwords.words('english')
    filter_words.extend(["&", "quot", "@", "www", "%"])
    filter_words_containing = ["'", "/", "\\", "."]

    for fname in files:
        if "_NA" in fname:
            continue
        print fname
        id, _, age, gender = fname.strip().split(".")[0].split("_")
        gender = gender.lower()
        lines = open(os.path.join(people_html_dir, fname), 'r').readlines()
        words = []
        for line in lines:
            words.extend(get_filtered_words_in_line(line, filter_words, filter_words_containing))
        age_groups[age].extend(words)
        genders[gender].extend(words)
        combined[age+"_"+gender].extend(words)

    write_all_words_to_file(age_groups)
    write_all_words_to_file(genders)
    write_all_words_to_file(combined)

def get_ngrams_for_each_class():
    files = next(os.walk(people_html_dir))[2] #get just the files and not directories

    age_groups = {"18-24": [],  "25-34": [],  "35-49": [],  "50-64": [],  "65-xx": []}
    genders = {"male": [], "female": []}
    combined = {"18-24_male": [],  "25-34_male": [],  "35-49_male": [],  "50-64_male": [],  "65-xx_male": [], "18-24_female": [],  "25-34_female": [],  "35-49_female": [],  "50-64_female": [],  "65-xx_female": []}
    filter_words = stopwords.words('english')
    filter_words.extend(["&", "quot", "@", "www", "%"])
    filter_words_containing = ["'", "/", "\\", "."]

    for fname in files:
        if "_NA" in fname:
            continue
        print fname
        id, _, age, gender = fname.strip().split(".")[0].split("_")
        gender = gender.lower()
        lines = open(os.path.join(people_html_dir, fname), 'r').readlines()
        words = []
        for line in lines:
            words.extend(add_bigrams_trigrams(get_filtered_words_in_line(line, filter_words, filter_words_containing)))
        age_groups[age].extend(words)
        genders[gender].extend(words)
        combined[age+"_"+gender].extend(words)
    return age_groups, genders, combined


def add_bigrams_trigrams(words):
    bigrams = utils.get_create_n_gram_profile(words, 2)
    trigrams = utils.get_create_n_gram_profile(words, 3)
    words.extend(bigrams)
    words.extend(trigrams)
    return words

def write_all_words_to_file(class_maps):
    for cls, words in class_maps.iteritems():
        fhandle = open(os.path.join(opdir, cls + ".txt"), "w")
        fhandle.write(" ".join(words))
        fhandle.close()

def get_filtered_words_in_line(line, filter_words, filter_words_containing):
    words = []
#    line = remove_punct(line)
    sentence_list = utils.get_sentences(utils.remove_punct(line))
    for sentence in sentence_list:
        word_list = utils.get_words(sentence)
        sw_in_sent = [w.lower() for w in word_list] # just changing this to take all words wont change a thing because stopwords have been used throughout the project
        for word in sw_in_sent:
            if word not in filter_words and not contains_char(word, filter_words_containing):
                words.append(word)
    return words

def contains_char(token, char_list):
    contains = False
    for c in char_list:
        if c in token:
            contains = True
            break
    return contains

# def create_word_cloud_from_file():
#     YOUR_TEXT = "A tag cloud is a visual representation for text data, typically\
#     used to depict keyword metadata on websites, or to visualize free form text."
#
#     tags = make_tags(get_tag_counts(YOUR_TEXT), maxsize=120)
#
#     create_tag_image(tags, 'cloud_large.png', size=(900, 600), fontname='Lobster')

def remove_high_pmi_words(counts):
    high_pmi_words = open("/Users/shrprasha/Projects/dailystrength/resources/high_pmi_words.txt", "r").read().strip().split()
    for word in high_pmi_words:
        if word in counts:
            del counts[word]

def remove_least_frequent(l1, drop_below):
    counts = Counter(l1)
    remove_high_pmi_words(counts)
    if drop_below > 1:
        counts = Counter({k: c for k, c in counts.items() if c >= drop_below})
    freq_words = []
    for word, freq in counts.iteritems():
        freq_words.extend([word] * freq)
    print len(freq_words)
    return freq_words

def clean_files(words):
    filter_list = ["&", "quot", "@", "www", "%"]
    words_clean = [w for w in words if w not in filter_list and "'" not in w]
    print len(words), len(words_clean),
    return words_clean

def write_to_file(words, fhandle):
    # fhandle.seek(0)
    fhandle.truncate(0)
    fhandle.seek(0)
    fhandle.write(" ".join(words))

def write_to_file_wordle(words):
    ophandle = open(os.path.join(opdir, "female_wordle.txt"), "w")
    counts = Counter(words)
    for word, freq in counts.iteritems():
        ophandle.write(word + ":" + str(freq) + "\n")
    ophandle.close()

def get_most_common_words(words):
    remove_list = ["&", "quot"]
    counts = Counter(words)
    common = counts.most_common(50)
    for k, v in common:
        if k not in remove_list and "'" not in k:
            print k + ":" + str(v)


def get_tf_frequencies(list_of_maps):
    freqeunt_map = {}
    #not to calculate totals all the time
    totals = {}
    # print list_of_maps
    for cls, word_ctr in list_of_maps.iteritems():  # female, Counter(words_female)
        tfs = {}
        for word, cnt in word_ctr.iteritems():  # word, count for female
            #get total count of the word in all classes
            if word not in totals:
                tot = cnt
                for cls1, word_ctr1 in list_of_maps.iteritems():  # male, Counter(words_male)
                    if cls == cls1:
                        continue
                    else:
                        tot += word_ctr1[word]
                totals[word] = tot
            else:
                tot = totals[word]

            # tfs[word+"_"+str(cnt)] = cnt * 1.0 / tot
            tfs[word] = cnt * 1.0 / tot
        freqeunt_map[cls] = Counter(tfs)
    return freqeunt_map

def write_most_frequent_tf_words(freqeunt_map, no_of_words):
    # print freqeunt_map
    for cls, word_ct in freqeunt_map.iteritems():
        ophandle = open(os.path.join(opdir, cls + "_wordle.txt"), "w")
        for word, cnt in word_ct.most_common(no_of_words):
            ophandle.write(word + ":" + str(cnt) + "\n")
        ophandle.close()

def write_most_frequent_tf_words_frequencies(freqeunt_map_tf, no_of_words, frequent_map_freq):
    # print freqeunt_map
    for cls, word_ct in freqeunt_map_tf.iteritems():
        ophandle = open(os.path.join(opdir, cls + "_wordle.txt"), "w")
        word_freq = frequent_map_freq[cls]
        new_freq_map = {}
        for word, cnt in word_ct.most_common(no_of_words):
            new_freq_map[word] = word_freq[word]

        for word1, cnt1 in sorted(new_freq_map.items(), key=operator.itemgetter(1), reverse=True):
            ophandle.write(word1 + ":" + str(cnt1) + "\n")
        ophandle.close()

# def write_most_frequent_tf_words_gender(freq):
#     fhandle = open(os.path.join(opdir, "female.txt"), "r")
#     mhandle = open(os.path.join(opdir, "male.txt"), "r")
#     words_female = Counter(remove_least_frequent(clean_files(fhandle.read().strip().split()), freq))
#     words_male = Counter(remove_least_frequent(clean_files(mhandle.read().strip().split()), freq))
#     write_most_frequent_tf_words({"female": words_female, "male": words_male})

def get_words_for_class(cls_list, freq):
    cls_words_map = {}
    for cls in cls_list:
        fhandle = open(os.path.join(opdir, cls + ".txt"), "r")
        words = Counter(remove_least_frequent(fhandle.read().strip().split(), freq))
        cls_words_map[cls] = words
        fhandle.close()
    return cls_words_map

def get_wordle_files(cls_list, freq, no_of_words):
    frequent_words = get_words_for_class(cls_list, freq)
    write_most_frequent_tf_words_frequencies(get_tf_frequencies(frequent_words), no_of_words, frequent_words)
    # write_most_frequent_tf_words(get_tf_frequencies(frequent_words), no_of_words)
    # write_most_frequent_tf_words(get_words_for_class(cls_list, freq), no_of_words)

def get_wordle_files_ngrams(map_cls, freq, no_of_words):
    maps = {}
    for cls, words in maps.iteritems():
        maps[cls] = Counter(remove_least_frequent(words, freq))
    write_most_frequent_tf_words(maps, no_of_words)

def get_wordle_ngrams():
    ages, genders, combined = get_ngrams_for_each_class()
    get_wordle_files_ngrams(ages, freq, no_of_words)
    get_wordle_files_ngrams(genders, freq, no_of_words)
    get_wordle_files_ngrams(combined, freq, no_of_words)

if __name__ == "__main__":
    # people_html_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/additional_data"
    people_html_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/all_data/together"
    opdir = "word_clouds"
    # get_words_for_each_class()

    freq = 100
    no_of_words = 1370

    genders = ["male", "female"]
    get_wordle_files(genders, freq, no_of_words)
    # age_groups = ["18-24", "25-34", "35-49", "50-64", "65-xx"]
    # combined = {"18-24_male",  "25-34_male",  "35-49_male",  "50-64_male",  "65-xx_male", "18-24_female",  "25-34_female",  "35-49_female",  "50-64_female",  "65-xx_female"}
    # get_wordle_files(age_groups, freq, no_of_words)
    # get_wordle_files(combined, freq, no_of_words)

    # tests = ["a", "b"]
    # freq = 1
    # get_wordle_files(tests, freq, no_of_words)

    # # create_word_cloud_from_file()
    #
    # fhandle = open(os.path.join(opdir, "female.txt"), "r+")
    # # fhandle = open("test.txt", "r+")
    # words = fhandle.read().strip().split()
    # # write_to_file_wordle(remove_least_frequent(clean_files(words), 10))
    # get_most_common_words(words)
    # fhandle.close()



