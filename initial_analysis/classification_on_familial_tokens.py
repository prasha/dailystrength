from dill import load
import os
from user import User
# import utils.file_io_utils as futils
# from utils import file_io_utils

__author__ = 'shrprasha'

def get_lines_in_file(fpath):
    with open(fpath) as fhandle:
        return fhandle.readlines()

def get_prediction_majority_class(fname):
    idx_feature_val = load_dumps(os.path.join(feature_dumps_rootdir, fname + ".dump"))
    male_count = idx_feature_val.get(1, 0)
    female_count = idx_feature_val.get(2, 0)
    # if idx_feature_val:
    #     print fname, idx_feature_val, male_count, female_count
    if male_count > female_count:
        return "Male"
    elif male_count < female_count:
        return "Female"
    elif male_count == female_count:
        return "Female"
        # if male_count == 0:
        #     return None
        # else:
        #     # print "equal"
        #     return None

def get_prediction(fname):
    idx_feature_val = load_dumps(os.path.join(feature_dumps_rootdir, fname + ".dump"))
    male_count = idx_feature_val.get(1, 0)
    female_count = idx_feature_val.get(2, 0)
    # if idx_feature_val:
    #     print fname, idx_feature_val, male_count, female_count
    if male_count > female_count:
        return "Male"
    elif male_count < female_count:
        return "Female"
    elif male_count == female_count:
        if male_count == 0:
            return None
        else:
            # print "equal"
            return None


def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

def get_word_ngram_predictions():
    word_ngrams_result_path = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/liblinear_files/word_ngram_features_tf_idf/results/gender"
    # lines = futils.get_lines_in_file(word_ngrams_result_path)[1:]
    lines = get_lines_in_file(word_ngrams_result_path)[1:]
    genders = ["Female", "Male"]
    results = [genders[int(line.split()[0])] for line in lines]
    return results


# overlap with word ngrams and cascading results for gender
# rule based correct where word ngram incorrect
if __name__ == "__main__":
    feature_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/familial_gender_bucket_features"
    # feature_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/familial_features"
    users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps"
    dev_users = load_dumps(os.path.join(users_dumps_rootdir, "dev.dump"))

    word_ngram_predictions = get_word_ngram_predictions()
    total_users_having_feature = 0
    correct_predictions = 0
    rule_correct_word_ngram_incorrect = 0
    word_ngram_correct = 0
    for i, fname in enumerate(sorted(dev_users)):
        actual = dev_users[fname].gender
        # prediction = get_prediction(fname)
        prediction = get_prediction_majority_class(fname)
        word_ngram_prediction = word_ngram_predictions[i]

        if actual == word_ngram_prediction:
            word_ngram_correct += 1
        # print i, actual, word_ngram_prediction

        if prediction:
            total_users_having_feature += 1
            if actual == prediction:
                correct_predictions += 1
                if actual != word_ngram_prediction:
                    rule_correct_word_ngram_incorrect += 1
    print "rule based", total_users_having_feature, correct_predictions, correct_predictions * 1. / total_users_having_feature
    print "word ngrams", rule_correct_word_ngram_incorrect, word_ngram_correct, word_ngram_correct * 1. / len(dev_users)
    cascading_correct = rule_correct_word_ngram_incorrect + word_ngram_correct
    print "total correct after cascading: ", cascading_correct, cascading_correct * 1. / len(dev_users)



# acutal: rule based predictions on familial tokens
# if __name__ == "__main__":
#     feature_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/familial_gender_bucket_features"
#     # feature_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/familial_features"
#     users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps"
#     dev_users = load_dumps(os.path.join(users_dumps_rootdir, "dev.dump"))
#
#     total_users_having_feature = 0
#     correct_predictions = 0
#     for fname in sorted(dev_users):
#         actual = dev_users[fname].gender
#         prediction = get_prediction(fname)
#         if prediction:
#             total_users_having_feature += 1
#             print actual
#             if actual == prediction:
#                 correct_predictions += 1
#     print total_users_having_feature, correct_predictions, correct_predictions * 1. / total_users_having_feature


