__author__ = 'shrprasha'

import os
import utils
import shutil

# old_data_list_together:  107
# old_data_list_additional:  14248
# new_data_list_additional:  19402


# keep only 1 file for single user
def remove_user_duplicates():
    fdir = "/home/prasha/Documents/dailystrength/author_profiling_data/additional_data"
    files = next(os.walk(fdir))[2]  #get just the files and not directories
    remove_list = []
    uid_lines = {}
    for doc in sorted(files):
        uid, _, age, gender = doc.split("_")
        no_of_lines = utils.word_count_in_file(os.path.join(fdir, doc))
        if uid not in uid_lines:
            uid_lines[uid] = (doc, no_of_lines)
        else:
            if uid_lines[uid][1] < no_of_lines:
                remove_list.append(uid_lines[uid][0])
                uid_lines[uid] = (doc, no_of_lines)
            else:
                remove_list.append(doc)
    print len(remove_list)
    for doc1 in remove_list:
        os.remove(os.path.join(fdir, doc1))


#remove files from additional_data already present in old files
def keep_only_new_files():
    additional_fdir = "/home/prasha/Documents/dailystrength/author_profiling_data/before_additional_data/additional_data"
    old_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/before_additional_data/all_data/together"
    old_data = "/home/prasha/Documents/dailystrength/author_profiling_data/old_data"
    new_data = "/home/prasha/Documents/dailystrength/author_profiling_data/new_data"

    old_data_list_additional = []
    old_data_list_together = []
    new_data_list_additional = []

    old_files_uids = []
    for doc in sorted(next(os.walk(old_dir))[2]):
        uid = doc.split("_", 1)[0]
        old_files_uids.append(uid)

    old_files_additional_uids = []
    for doc in sorted(next(os.walk(additional_fdir))[2]):
        uid = doc.split("_", 1)[0]
        if uid in old_files_uids:
            old_data_list_additional.append(doc)
            old_files_additional_uids.append(uid)
        else:
            new_data_list_additional.append(doc)

    for doc in sorted(next(os.walk(old_dir))[2]):
        uid = doc.split("_", 1)[0]
        if uid not in old_files_additional_uids:
            old_data_list_together.append(doc)

    print "old_data_list_together: ", len(old_data_list_together)
    print "old_data_list_additional: ", len(old_data_list_additional)
    print "new_data_list_additional: ", len(new_data_list_additional)

    print "\n\n\nold_data_list_together\n\n\n", old_data_list_together
    print "\n\n\nold_data_list_additional\n\n\n", old_data_list_additional
    print "\n\n\nnew_data_list_additional\n\n\n", new_data_list_additional

    for doc in old_data_list_together:
        shutil.copyfile(os.path.join(old_dir, doc), os.path.join(old_data, doc))

    for doc in old_data_list_additional:
        shutil.copyfile(os.path.join(additional_fdir, doc), os.path.join(old_data, doc))

    for doc in new_data_list_additional:
        shutil.copyfile(os.path.join(additional_fdir, doc), os.path.join(new_data, doc))


def get_age_gender_distribution():
    together = "/Users/shrprasha/Projects/resources/dailystrength/together"
    # together = "/home/prasha/Documents/dailystrength/author_profiling_data/dev"
    age_groups = {"18-24": {}, "25-34": {}, "35-49": {}, "50-64": {}, "65-xx": {}}
    docs = next(os.walk(together))[2]
    net_total = len(docs)
    for doc in sorted(docs):
        id, _, age, gender = doc.strip().split(".")[0].split("_")
        if gender in age_groups[age]:
            age_groups[age][gender] += 1
        else:
            age_groups[age][gender] = 1
    tot_male = 0
    tot_female = 0
    for cls in sorted(age_groups):  #first male then female
        gender_counts = age_groups[cls]
        tot_female += gender_counts["female"]
        tot_male += gender_counts["male"]
        total = gender_counts["male"] + gender_counts["female"]
        print cls + " & " + str(gender_counts["male"]) + " (" + str(
            round(gender_counts["male"] * 100.0 / total, 2)) + "\%) " + " & " + str(
            gender_counts["female"]) + " (" + str(
            round(gender_counts["female"] * 100.0 / total, 2)) + "\%) " + " & " + str(total)  + " (" + str(
            round(total * 100.0 / net_total, 2)) + "\%) "
    # total = tot_female + tot_male
    print "Total" + " & " + str(tot_male) + " (" + str(round(tot_male * 100.0 / net_total, 2)) + "\%) " + " & " + str(
        tot_female) + " (" + str(round(tot_female * 100.0 / net_total, 2)) + "\%) " + " & " + str(net_total)


if __name__ == "__main__":
    # remove_user_duplicates()
    # keep_only_new_files()
    #move all in old_data_list_together and old_data_list_additional i.e. old_data to train -> manual
    #also put random count(60% of total) - count(train) in train -> in create_train_test.py

    get_age_gender_distribution()
