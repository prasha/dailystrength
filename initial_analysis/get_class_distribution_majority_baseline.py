from __future__ import division
from pickle import load
from user import User
from pprint import pprint

__author__ = 'shrprasha'

from collections import Counter


def get_distribution_label_names(test_fpath, label_names):
    with open(test_fpath, "r") as fhandle:
        lines = fhandle.readlines()
    labels = []
    for i, line in enumerate(lines):
        label_idx, _ = line.strip().split(" ", 1)
        labels.append(label_names[int(label_idx)])
    ctr = Counter(labels)
    print ctr
    print ctr.most_common(1), max(ctr.values()) * 100.0 / len(labels)


def get_distribution(test_fpath):
    with open(test_fpath, "r") as fhandle:
        lines = fhandle.readlines()
    labels = []
    for i, line in enumerate(lines):
        label, _ = line.strip().split(" ", 1)
        labels.append(label)
    ctr = Counter(labels)
    print ctr
    print max(ctr.values()) * 100.0 / len(labels)


def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

def get_lines_in_file(fpath):
    with open(fpath) as fhandle:
        return fhandle.readlines()

def get_distribution_from_users(users_fpath, remove_users=[]):
    labels = both_labels + age_labels + gender_labels
    dist = dict(zip(labels, [0] * len(labels)))
    users = load_dumps(users_fpath)
    for userid, user in users.iteritems():
        if userid in remove_users:
            continue
        user_gender = user.gender.lower()
        user_label = user.age_group + "_" + user_gender
        dist[user_label] += 1
        dist[user.age_group] += 1
        dist[user_gender] += 1
    display_dist(dist)

def get_distribution_from_users_cont(users_fpath, remove_users=[]):
    labels = both_labels + age_labels + gender_labels
    dist = dict(zip(labels, [0] * len(labels)))

    users = load_dumps(users_fpath)
    for userid, user in users.iteritems():
        # print userid
        userid, age_group = convert_to_cont(userid)
        if userid in remove_users:
            continue
        user_gender = user.gender.lower()
        user_label = age_group + "_" + user_gender
        dist[user_label] += 1
        dist[age_group] += 1
        dist[user_gender] += 1
    display_dist(dist)

def convert_to_cont(userid):
    uid, age_group = userid.split("_")
    lage, uage = age_group.split("-")
    lage = int(lage)
    uage = int(uage)
    if lage == 66:
        new_age_group = "65-xx"
    elif lage == 12:
        new_age_group = "12-17"
    else:
        new_age_group = str(lage-1) + "-" + str(uage+1)
    return uid + "_" + new_age_group, new_age_group

def display_dist(dist):
    total = dist["female"] + dist["male"]
    for age_group in age_labels:
        print "%s & %d & %d & %d(%0.2f\\%%) \\\\" % (
            age_group, dist[age_group + "_" + "female"], dist[age_group + "_" + "male"], dist[age_group],
            dist[age_group] * 100 / total)
        # Total &  435 (94.93\%) & 391 (5.07\%) & 7714 \\ \hline

    print "Total & %d( %0.2f\\%% ) & %d(%0.2f\\%% ) & %d \\\\ \\hline" % (
    dist["female"], dist["female"] * 100 / total, dist["male"], dist["male"] * 100 / total, total)


## here
# if __name__ == "__main__":
# test_fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/liblinear_files/stylistic_and_old_features/dev/gender"
# get_distribution_label_names(test_fpath, ["female", "male"])
#     test_fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/liblinear_files/stylistic_and_old_features/dev/age"
#     get_distribution_label_names(test_fpath, ["12-16", "19-28", "31-48", "51-63", "66-200"])
#     test_fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/liblinear_files/stylistic_and_old_features/dev/both"
#     get_distribution_label_names(test_fpath, ["12-16_female", "19-28_female", "31-48_female", "51-63_female", "66-200_female", "12-16_male", "19-28_male", "31-48_male", "51-63_male", "66-200_male"])

# cluster
# if __name__ == "__main__":
#     test_fpath = "/project/solorio/prasha/ds/processed_data/liblinear_files/word_ngram_features_tf_idf/test/gender"
#     get_distribution_label_names(test_fpath, ["female", "male"])
#     test_fpath = "/project/solorio/prasha/ds/processed_data/liblinear_files/word_ngram_features_tf_idf/test/age"
#     get_distribution_label_names(test_fpath, ["12-16", "19-28", "31-48", "51-63", "66-200"])
#     test_fpath = "/project/solorio/prasha/ds/processed_data/liblinear_files/word_ngram_features_tf_idf/test/both"
#     get_distribution_label_names(test_fpath, ["12-16_female", "19-28_female", "31-48_female", "51-63_female", "66-200_female", "12-16_male", "19-28_male", "31-48_male", "51-63_male", "66-200_male"])
#     test_fpath = "/project/solorio/prasha/ds/processed_data/liblinear_files/word_ngram_features_tf_idf/test/y1984"
#     get_distribution(test_fpath)

# from users file
if __name__ == "__main__":
    # both_labels = ["12-16_female", "19-28_female", "31-48_female", "51-63_female", "66-200_female", "12-16_male",
    #                "19-28_male", "31-48_male", "51-63_male", "66-200_male"]
    # age_labels = ["12-16", "19-28", "31-48", "51-63", "66-200"]
    # gender_labels = ["female", "male"]
    # get_distribution_from_users(
    #     "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps/all_users.dump")


    # continuous
    both_labels = ["12-17_female", "18-29_female", "30-49_female", "50-64_female", "65-xx_female", "12-17_male", "18-29_male", "30-49_male", "50-64_male", "65-xx_male"]
    age_labels = ["12-17", "18-29", "30-49", "50-64", "65-xx"]
    gender_labels = ["female", "male"]

    remove_users_fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/continuous/short.txt"
    remove_users = [x.strip() for x in get_lines_in_file(remove_users_fpath)]
    get_distribution_from_users_cont(
        "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps/all_users.dump", remove_users)







