__author__ = 'prasha'

names_gender_file = "/home/prasha/Documents/dailystrength/author_profiling_data/name_gender_list"
userid_firstname_file = "/home/prasha/Documents/dailystrength/author_profiling_data/userid_firstname_with_class"

lines = open(names_gender_file, 'r').readlines()
female_names = lines[0].strip().split(",")
female_names = [x.lower() for x in female_names]
male_names = lines[1].strip().split(",")
male_names = [x.lower() for x in male_names]

# print female_names
# print male_names
lines = open(userid_firstname_file, 'r').readlines()
for line in lines:
    id, uname, age, gender, label, firstname = line.strip().split(",")
    count_correct = 0
    if firstname:
        # print line
        pred_gender = ""
        if firstname.lower() in female_names:
            pred_gender = "female"
        elif firstname.lower() in male_names:
            pred_gender = "male"
        if pred_gender:
            print id + "," + uname + "," + age + "," + gender + "," + firstname + "," + pred_gender
            if gender.strip() == pred_gender:
                count_correct += 1
print count_correct


