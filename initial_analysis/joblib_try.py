__author__ = 'shrprasha'


from joblib import Parallel, delayed


def f1(x):
    if x>2:
        return called_by_f1(x) * 10
    else:
        return called_by_f1(x)

def called_by_f1(x):
    return x * b[1]


l = [1,2,3]
b = [100,100,100]
print Parallel(n_jobs=-1)(delayed(f1)(i) for i in l)
