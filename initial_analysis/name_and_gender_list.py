__author__ = 'prasha'
import os

male_names = {}
female_names = {}

data_folder = "/home/prasha/Documents/dailystrength/extras/names"

for name_file in sorted(os.listdir(data_folder)):
    if ".txt" not in name_file:
        continue
    # print name_file
    lines = open(os.path.join(data_folder, name_file), 'r').readlines()
    for line in lines:
        name, gender, count = line.strip().split(",")
        count = int(count)
        name = name.lower()
        gender = gender.lower()
        if gender == "f":
            if name in female_names:
                female_names[name] += count
            else:
                female_names[name] = count
        else:
            if name in male_names:
                male_names[name] += count
            else:
                male_names[name] = count


#if present in both lists, take the one with the higher count
cleaned_female_names = {}
removed_count = 0
for name, fcount in female_names.iteritems():
    if name in male_names:
        removed_count += 1
        mcount = male_names[name]
        if fcount >= mcount:
            cleaned_female_names[name] = fcount
            del male_names[name]
    else:
        cleaned_female_names[name] = fcount

both_names = {x: male_names.get(x, 0) + female_names.get(x, 0) for x in set(male_names).union(female_names)}
# print both_names["can"], both_names["the"], both_names["me"], both_names["you"], both_names["or"], both_names["than"], both_names["so"], both_names["said"], both_names["even"], both_names["not"], both_names["do"], both_names["chat"], both_names["pray"]

#remove less frequent
at_least = 3000
final_female_names = []
final_male_names = []
for name, fcount in cleaned_female_names.iteritems():
    if fcount >= at_least:
        final_female_names.append(name)
for name, fcount in male_names.iteritems():
    if fcount >= at_least:
        final_male_names.append(name)

print ",".join(final_female_names)
print ",".join(final_male_names)







