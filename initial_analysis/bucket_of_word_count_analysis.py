__author__ = 'prasha'
import os
import codecs
import os
import sys
from pprint import pprint
import math
import re
import nltk
from collections import Counter

def get_words(sentence):
    return nltk.word_tokenize(sentence)

def get_sentences(document):
    sentences = nltk.sent_tokenize(document)
    return sentences

def no_of_words_in_text(text):
    count = 0
    for sentences in get_sentences(text):
        count += len(get_words(sentences))
    return count

def find_counts(text, regex_list):
    count = 0
    for regxp in regex_list:
        rexp = re.compile(regxp, re.IGNORECASE)
        matches = rexp.findall(text)
        count += len(matches)
    return count

def find_matches(text, regex_list):
    regx_matches = []
    for regxp in regex_list:
        rexp = re.compile(regxp, re.IGNORECASE)
        matches = rexp.findall(text)
        regx_matches.extend(matches)
    return regx_matches

def get_bucket_count(matches, regex_list, buckets):
    counts = {}
    for bucket in buckets:
        counts[bucket] = 0
    for match in matches:
        word = get_words(match)[-1]
        if word in buckets[word]:
            counts[bucket] += 1
    return counts

def get_bucket_count_each_token(matches, buckets):
    counts_bucket = {}
    counts_each_token = []
    words = []
    ###
    bc = {}

    #count word in bucket
    for match in matches:
        word = match.lower()
        temp = sum(counts_bucket.values())
        for bucket, bucket_tokens in buckets.iteritems():
            if word in bucket_tokens:
                counts_bucket[bucket] += 1
                ###
                bc[bucket].append(word)
        if temp == sum(counts_bucket.values()):
            print match
            print "error:", word, counts_bucket
        words.append(word)
    words_counter = Counter(words)
    for token in each_token:
        if token in words_counter:
            counts_each_token.append(words_counter[token])
        else:
            counts_each_token.append(0)
    print bc
    return counts_bucket, counts_each_token

def add_counts_to_extra_features_file(username_files, regex_list):
    familial_counts = {}
    username_files = os.listdir(username_folder)
    for ufile in sorted(username_files):
        stats = ufile.split("_")
        userid = stats[-4]
        text = codecs.open(os.path.join(username_folder, ufile), 'r', encoding='utf-8').read()
        familial_count = find_counts(text, regex_list)
        familial_counts[userid] = familial_count
        # print ufile, userid, familial_count

    user_details_file = "/home/prasha/Documents/dailystrength/author_profiling_data/processed_data/features/userid_userdetails.txt"
    lines = open(user_details_file, 'r').readlines()
    new_lines = ""
    print len(lines), lines[0], lines[-1:]
    for line in lines:
        stats = line.strip().split(",")
        uname = stats[0]
        if uname in familial_counts:
            new_lines += line.strip() + "," + str(familial_counts[uname]) + "\n"
        else:
            new_lines += line.strip() + ",\n"

    print len(new_lines), new_lines[:500], "\n", new_lines[-500:]
    op_fHandle = open(user_details_file, 'w')
    op_fHandle.write(new_lines.strip())

def main():
    #change later
    #pairs_having_familial = "/home/prasha/Documents/dailystrength/author_profiling_data/processed_data/pairs_having_familial"
    #lines = open(pairs_having_familial, 'r').readlines()
    username_files = os.listdir(username_folder)
    age_counts = {"18-24": 0, "25-34": 0, "35-49": 0, "50-64": 0, "65-xx": 0}
    gender_counts = {"male": 0, "female": 0}
    age_counts_has_abbr = {"18-24": 0, "25-34": 0, "35-49": 0, "50-64": 0, "65-xx": 0}
    gender_counts_has_abbr = {"male": 0, "female": 0}

#counts with buckets
    for ufile in sorted(username_files):
        stats = ufile.split("_")
        gender = stats[-1].split(".")[0]
        age = stats[-2]

        text = codecs.open(os.path.join(username_folder, ufile), 'r', encoding='utf-8').read()
        tot_words = no_of_words_in_text(text)
        matches = find_matches(text, regex_list)

        if matches:
            # counts_each_token = get_bucket_count_each_token(matches)
            # print matches
            match_count = len(matches)
            age_counts[age] += match_count
            gender_counts[gender] += match_count
            age_counts_has_abbr[age] += 1
            gender_counts_has_abbr[gender] += 1
            # print "error: ", ufile
            print ufile + "\t" + age + "\t" + gender + "\t" + str(tot_words) + "\t" + str(match_count)
        else:
            print ufile + "\t" + age + "\t" + gender + "\t" + str(tot_words) + "\t0"

    print "total: ", age_counts
    print "total: ", gender_counts
    print "counts: ", age_counts_has_abbr
    print "counts: ", gender_counts_has_abbr


if __name__ == "__main__":
    # username_folder = "/home/prasha/Documents/dailystrength/author_profiling_data/username_data_pan14_train_test/small_test_familial"

    username_folder = "/home/prasha/Documents/dailystrength/author_profiling_data/username_data_pan14_train_test/together"
    # username_folder = "/Users/prasha/Documents/dailystrength/small_test_familial"

    #will catch term\W* eg. son-in-law, son's etc.
    #b/c
    regex_list = [r"\b(?:2ww|AAMOF|ADN|AF|AFAIK|AFK|AKA|BAC|B4|B4N|BBL|BBT|BF|BFN|BFP|BIL|BITGOD|BRB|BTA|BTW|BYAM|BYKT|CMIIW|C|CUL|DD|DF|DH|DP|DS|DTD|DTRT|DW|EBF|EBM|EOL|ESP|F2F|FAQ|FIL|FITB|FOMCL|FTTT|F|4|FWIW|FYI|G|GAFIA|GFY|GMTA|HPT|HTH|HV|IAC|IAE|IC|IDTT|IIRC|IMAO|IMCO|IMHO|IMNSHO|IMO|IMOBO|INPO|IOW|IWBNI|IYSWIM|JIC|JTYWLTK|LO|LOL|LMAO|M/C|MIL|MW|N/M|NP|NRN|N/T|OIC|OM|OMG|OTOH|OTT|OW|PG|PILs|PMSL|PND|PNI|POV|PTB|ROTF|ROTFL|SAHD|SAHM|SIL|SITD|TAFN|TAFT|TBH|TIC|TGIF|TIA|TMI|TNXorTNS|TPTB|TTBOMK|TTC|TTFN|TTL4N|TTUL|TYVM|WFM|WRT|WTF|WYSIWYG)\b"]
    each_token = [u'2WW', u'AAMOF', u'ADN', u'AF', u'AFAIK', u'AFK', u'AKA', u'BAC', u'B4', u'B4N', u'BBL', u'BBT', u'BF', u'BFN', u'BFP', u'BIL', u'BITGOD', u'BRB', u'BTA', u'BTW', u'BYAM', u'BYKT', u'CMIIW', u'CU', u'CUL', u'DD', u'DF', u'DH', u'DP', u'DS', u'DTD', u'DTRT', u'DW', u'EBF', u'EBM', u'EOL', u'ESP', u'F2F', u'FAQ', u'FIL', u'FITB', u'FOMCL', u'FTTT', u'FU', u'4U', u'FWIW', u'FYI', u'G', u'GAFIA', u'GFY', u'GMTA', u'HPT', u'HTH', u'HV', u'IAC', u'IAE', u'IC', u'IDTT', u'IIRC', u'IMAO', u'IMCO', u'IMHO', u'IMNSHO', u'IMO', u'IMOBO', u'INPO', u'IOW', u'IWBNI', u'IYSWIM', u'JIC', u'JTYWLTK', u'LO', u'LOL', u'LMAO', u'M/C', u'MIL', u'MW', u'N/M', u'NP', u'NRN', u'N/T', u'OIC', u'OH', u'OM', u'OMG', u'OTOH', u'OTT', u'OW', u'PG', u'PILs', u'PMSL', u'PND', u'PNI', u'POV', u'PTB', u'ROTF', u'ROTFL', u'SAHD', u'SAHM', u'SIL', u'SITD', u'TAFN', u'TAFT', u'TBH', u'TIC', u'TGIF', u'TIA', u'TMI', u'TNXorTNS', u'TPTB', u'TTBOMK', u'TTC', u'TTFN', u'TTL4N', u'TTUL', u'TTYL', u'TYVM', u'WFM', u'WRT', u'WTF', u'WYSIWYG']
    main()
