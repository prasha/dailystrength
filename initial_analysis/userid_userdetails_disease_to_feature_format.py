__author__ = 'shrprasha'
import os

userid_diseases = "/Users/shrprasha/Projects/dailystrength/resources/userid_diseases.txt"
together = "/Users/shrprasha/Projects/resources/dailystrength/together"
opfile = "/Users/shrprasha/Projects/dailystrength/resources/diseases_feature.vectors"
opfhandle = open(opfile, "w")
opfhandle.write("40\n")

uid_fname_map = {}
for fname in next(os.walk(together))[2]:
    uid = fname.split("_", 1)[0]
    uid_fname_map[uid] = fname

for line in open(userid_diseases, "r").readlines():
    stats = line.split(",")
    uid = stats[0]
    features = [int(x) for x in stats[-40:]]
    if sum(features) == 0:
        print uid
        continue
    line = uid_fname_map[uid]
    for i, feature in enumerate(features):
        if feature > 0:
            line += " " + str(i) + ":" + str(feature)
    line += "\n"
    opfhandle.write(line)

