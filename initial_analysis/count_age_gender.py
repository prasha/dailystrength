__author__ = 'shrprasha'
from pprint import pprint
import os

def count_age_gender0():
    age_groups = [(18, 24), (25, 34), (35, 49), (50, 64), (65, 200)]
    result = [[0,0], [0,0], [0,0], [0,0], [0,0]]
    # lines = open("/Users/shrprasha/Projects/dailystrength/resources/username_id_age_gender_both_age_gender.txt", "r").readlines()
    lines = open("/Users/shrprasha/Projects/dailystrength/resources/userid_user_details_age_gender_both_train.txt", "r").readlines()
    for line in lines:
        uname, id, age, gender = line.strip().split(",")
        age = int(age)
        for i, age_range in enumerate(age_groups):
            if age_range[0] <= age <= age_range[1]:
                if gender.lower() == "male":
                    result[i][0] += 1
                else:
                    result[i][1] += 1
    print result
    for m,f in result:
        print m
        print f

def get_correct_age_group_for_id(traindir):
    correct_age_group_for_id = {}
    files = next(os.walk(traindir))[2] #get just the files and not directories
    for fname in files:
        uid, _, age_group, _ = fname.split(".")[0].split("_")
        min_age, max_age = age_group.split("-")
        min_age = int(min_age)
        if min_age == 65:
            max_age = 200
        else:
            max_age = int(max_age)
        correct_age_group_for_id[uid] = (min_age, max_age)
    return correct_age_group_for_id

def count_age_gender():
    traindir = "/Users/shrprasha/Projects/resources/dailystrength/train"
    correct_age_group_for_id = get_correct_age_group_for_id(traindir)
    age_groups = [(18, 24), (25, 34), (35, 49), (50, 64), (65, 200)]
    result = [[0,0], [0,0], [0,0], [0,0], [0,0]]
    # lines = open("/Users/shrprasha/Projects/dailystrength/resources/username_id_age_gender_both_age_gender.txt", "r").readlines()
    lines = open("/Users/shrprasha/Projects/dailystrength/resources/userid_user_details_age_gender_both_train.txt", "r").readlines()
    for line in lines:
        uname, uid, age, gender = line.strip().split(",")
        age = int(age)
        i = age_groups.index(correct_age_group_for_id[uid])
        if gender.lower() == "male":
            result[i][0] += 1
        else:
            result[i][1] += 1
    print result
    for m,f in result:
        print m
        print f

def count_age_gender_separate():
    traindir = "/Users/shrprasha/Projects/resources/dailystrength/train"
    correct_age_group_for_id = get_correct_age_group_for_id(traindir)
    age_groups = [(18, 24), (25, 34), (35, 49), (50, 64), (65, 200)]
    genders = ["female", "male"]
    age_result = [0, 0, 0, 0, 0]
    gender_result = [0, 0]
    # lines = open("/Users/shrprasha/Projects/dailystrength/resources/username_id_age_gender_both_age_gender.txt", "r").readlines()
    lines = open("/Users/shrprasha/Projects/dailystrength/resources/userid_user_details_age_gender_both_train.txt", "r").readlines()
    for line in lines:
        uname, uid, age, gender = line.strip().split(",")
        age = int(age)
        i = age_groups.index(correct_age_group_for_id[uid])
        age_result[i] += 1
        i = genders.index(gender.lower())
        gender_result[i] += 1
    print age_result, sum(age_result)
    print gender_result, sum(gender_result)
    print_percentages(age_result)
    print_percentages(gender_result)

def print_percentages(result):
    tot = sum(result)
    for s in result:
        print s * 100.0 / tot,

if __name__ == "__main__":

    # count_age_gender()
    count_age_gender_separate()

    # # ap_data = "/Users/shrprasha/Projects/resources/dailystrength/together"
    # # ap_data = "/Users/shrprasha/Projects/resources/dailystrength/train"
    # # ap_data = "/Users/shrprasha/Projects/resources/dailystrength/test"
    # # ap_data = "/Users/shrprasha/Projects/resources/dailystrength/dev"
    # ap_data = "/home/prasha/Documents/dailystrength/author_profiling_data/train"
    # files = next(os.walk(ap_data))[2] #get just the files and not directories
    # for fname in files:
    #     if ".txt.txt" in fname:
    #         fname_wo_txt = fname.split(".")[0]
    #         os.rename(os.path.join(ap_data, fname), os.path.join(ap_data, fname_wo_txt + ".txt"))





