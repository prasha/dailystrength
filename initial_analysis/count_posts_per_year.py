__author__ = 'prasha'
import xml.etree.ElementTree as xml
import os
import codecs
import sys

#decisions:
#did not leave out treatments: although treatments are not indicative of a person's age or gender
#knowledge about what disease this post was under is not preserved

#TODO:
#load people_age_gender
#parse all xml in XML folder to get all <text> and <treatment>, join them by space

#create file with fname: userid_age_gender
#NA when age or gender are not available
#each file contains all the texts written by that user at that age
class CreateDataset:

    def __init__(self, age_gender_file, opdir, age_groups):
        self.opdir = opdir
        self.age_groups = age_groups
        lines = open(age_gender_file).readlines()
        self.age_gender_map = {}
        self.text_by_user_at_age = {} #{user1: {age11: "text11", age12: "text12"}, user2: {age21: "text21", age22: "text22"}}
        for id_age_gender in lines:
            id_age_gender = id_age_gender.strip()
            id, age, gender = id_age_gender.split(",")
            self.age_gender_map[id] = (age, gender)
        #self.createds()
        #stats
        self.stats = {"total": 0, "not_in_map": 0, "no_age": 0, "no_gender": 0}
        self.yearposted_count = {}

    def createds(self, xmldir):
        self.xmldir = xmldir
        self.disease = xmldir.split("/")[-2] #/mnt/docsig/storage/daily-strength/Acne/XML
        files = sorted(os.listdir(self.xmldir))
        for doc_fname in files:
            extension = os.path.splitext(doc_fname)[-1]
            if extension != ".xml":
                continue
            print self.disease, doc_fname
            self.stats["total"] += 1
            self.parse_xml(os.path.join(self.xmldir, doc_fname))
        self.write_to_file()

    def parse_xml(self, xml_file):
        tree = xml.parse(os.path.join(xml_file))
        root = tree.getroot()
        for problem in root.iter('problem'):
            self.store_to_map(problem)
        for reply in root.iter('reply'):
            self.store_to_map(reply)

    def store_to_map(self, xmlobj):
        val = self.parse_problem_or_reply(xmlobj)
        if(val):
            id, date_posted, text = val
            if id in self.age_gender_map:
                year_posted = date_posted.strip().split()[0].split("/")[-1]
                if year_posted in self.yearposted_count:
                    self.yearposted_count[year_posted] += 1
                else:
                    self.yearposted_count[year_posted] = 1
            else:
                self.stats["not_in_map"] += 1


    def parse_problem_or_reply(self, xmlobj):
        if xmlobj.find('person') is not None: #added since /mnt/docsig/storage/daily-strength/Bipolar-Disorder/XML/10056719-taken-advantage.html.xml has no data in problem or replies
            id = xmlobj.find('person').get('id') #'/people/436265'
            id= id.split("/")[-1]
            date_posted = xmlobj.find('date').text
            content = xmlobj.find('content')
            text = ""
            for child in content: #text or treatment
                if(child.text):
                    text += child.text + " "
            return id, date_posted, text
        else:
            return None

    #all data was read on 2013; so the age is the age of the poster in 2013
    def get_actual_age_group(self, age_now, date_posted):
        year_obtained = 13
        if(age_now):
            age = int(age_now)
            year_posted = int(date_posted.split()[0].split("/")[-1])
            actual_age = age - (year_obtained - year_posted)
            return self.get_age_group(actual_age)
        else:
            self.stats["no_age"] += 1
            return "NA"

    #find out the range under which the age falls
    def get_age_group(self, age):
        for min, max in self.age_groups:
            if(min <= age <= max):
                if(min == 65):
                    return "65-plus"
                else:
                    return str(min) + "-" + str(max)
        return "NA"

    def write_to_file(self):
        for year, in self.yearposted_count.iteritems():
            opfile = os.path.join(self.opdir, )
            opfile.write(text)
            opfile.close()

    def get_stats(self):
        return self.stats

if __name__ == "__main__":
    age_gender_file = "/home/prasha/Dropbox/DailyStrength/id_age_gender.txt"
    age_groups = [(18, 24), (25, 34), (35, 49), (50, 64), (65, 200)]
    xmlroot = "/mnt/docsig/storage/daily-strength"
    opdir = "/home/prasha/Documents/dailystrength/author_profiling_data/train_test_division_analysis"
    diseases = ["Acne", "ADHD", "Alcoholism", "Asthma", "Back-Pain", "Bipolar-Disorder", "Bone-Cancer", "COPD", "Diets-Weight-Maintenance", "Fibromyalgia", "Gastric-Bypass-Surgery", "Immigration-Law", "Infertility", "Loneliness", "Lung-Cancer", "Migraine", "Miscarriage", "Pregnancy", "Rheumatoid-Arthritis", "War-In-Iraq"]
    cds = CreateDataset(age_gender_file, opdir, age_groups)
    for disease in diseases:
        xmldir = os.path.join(xmlroot, disease, "XML")
        print xmldir
        cds.createds(xmldir)
    print cds.stats

    #age_gender_file = "/home/prasha/Dropbox/DailyStrength/id_age_gender.txt"
    #age_groups = [(18, 24), (25, 34), (35, 49), (50, 64), (65, 200)]
    #xmldir = "/home/prasha/Documents/dailystrength/author_profiling_data/test/test_ip"
    #opdir = "/home/prasha/Documents/dailystrength/author_profiling_data/test/test_op"
    #cds = CreateDataset(age_gender_file, opdir, age_groups)
    #cds.createds(xmldir)


    #xmldir = sys.argv[1]
    #opdir = sys.argv[2]
    #cd.parse_xml("/mnt/docsig/storage/daily-strength/Acne/XML/9142628-need-some-input-please.html.xml")