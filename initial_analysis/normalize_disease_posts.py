__author__ = 'prasha'

user_details_file0 = "/home/prasha/Dropbox/DailyStrength/liblinear_op/userid_userdetails.txt"
user_details_file1 = "/home/prasha/Dropbox/DailyStrength/output/userid_userdetails.txt"
# 100119,ReverieAuriel,27,Female,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,3,5,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0
op_file = open(user_details_file1, 'w')
for line in open(user_details_file0, 'r').readlines():
    userid, username, age, gender, rest  = line.split(',', 4)
    disease_counts = [int(x) for x in rest.strip().split(",")]
    maxx = max(disease_counts) * 1.0
    disease_counts_norm = [x/maxx for x in disease_counts]
    str_dsn = [str(x)  for x in disease_counts_norm]
    op_file.write(str(userid) + "," + username + "," + age + "," + gender + "," + ",".join(str_dsn) + "\n")


