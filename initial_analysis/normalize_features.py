__author__ = 'shrprasha'

import sys
import os
import math

def get_features_counts(lines, number_of_features):
    label_features = {}
    tot_feature_vals = [0] * number_of_features
    for line in lines:
        label, features = line.strip().split(" ", 1)
        label_features[label] = {}
        for fpair in features.split(" "):
            fidx, fval = fpair.split(":")
            fidx = int(fidx)
            fval = int(fval)
            label_features[label][fidx] = fval
            tot_feature_vals[fidx] = max(fval, tot_feature_vals[fidx])
    return label_features, tot_feature_vals

def write_to_file_tfidf(label_features, number_of_features, opfile):
    opfile.write(str(number_of_features) + "\n")
    idfs = get_idfs(label_features, number_of_features)
    for label, features in label_features.iteritems():
        line = label
        for fidx in sorted(features):
            fval_normalized = tf(features, features[fidx]) * idfs[fidx]
            line +=  " " + str(fidx) + ":" + str(fval_normalized)
        opfile.write(line + "\n")
def tf(features, fval):
    maxi = max(features.values())
    tf = 0.5 + (0.5 * fval) / maxi
    return tf
def get_idfs(label_features, number_of_features):
    idfs = [0] * number_of_features
    for features in label_features.values():
        for fidx in features:
            idfs[fidx] += 1
    print "done getting idfs"
    return idfs
def idf(label_features, fidx):
    count = 0
    for features in label_features.values():
        if fidx in features:
            count += 1
    return math.log10(len(label_features) * 1.0 / count)

def write_to_file_normalized(label_features, tot_feature_vals, number_of_features, opfile):
    opfile.write(str(number_of_features) + "\n")
    for label, features in label_features.iteritems():
        line = label
        for fidx in sorted(features):
            fval_normalized = features[fidx] * 1.0 / tot_feature_vals[fidx]
            line +=  " " + str(fidx) + ":" + str(fval_normalized)
        opfile.write(line + "\n")

# python2.7 /Users/shrprasha/Projects/dailystrength/initial_analysis/normalize_features.py /Users/shrprasha/Projects/resources/dailystrength/features/main_features/diseases_feature.vectors
# python2.7 /Users/shrprasha/Projects/dailystrength/initial_analysis/normalize_features.py /Users/shrprasha/Projects/resources/dailystrength/features/main_features/familial_bucket_counts.vectors
# python2.7 /Users/shrprasha/Projects/dailystrength/initial_analysis/normalize_features.py /Users/shrprasha/Projects/resources/dailystrength/features/main_features/familial_separate_counts.vectors
# python2.7 /Users/shrprasha/Projects/dailystrength/initial_analysis/normalize_features.py /Users/shrprasha/Projects/resources/dailystrength/features/main_features/uname_char_ngram.vectors
# python2.7 /Users/shrprasha/Projects/dailystrength/initial_analysis/normalize_features.py /Users/shrprasha/Projects/resources/dailystrength/features/main_features/non-normalized/uname_char_ngram_counts.vectors
if __name__ == "__main__":
    features_file = sys.argv[1]
    features_fhandle = open(features_file, "r")
    lines = features_fhandle.readlines()
    number_of_features = int(lines[0])
    opfile = open(os.path.join(os.path.dirname(features_file), "normalized", os.path.basename(features_file)), "w")

    label_features, tot_feature_vals = get_features_counts(lines[1:], number_of_features)
    # write_to_file_tf(label_features, tot_feature_vals, number_of_features, opfile)
    write_to_file_tfidf(label_features, number_of_features, opfile) #for username ngrams




