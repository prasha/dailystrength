#!/bin/bash

for f in *65-plus*; do
    a="$(echo $f | sed s/65-plus/65-xx/)"
    mv "$f" "$a"
done
