__author__ = 'prasha'
import os
import utils
from nltk.corpus import stopwords
from collections import Counter

username_folder = "/home/prasha/Documents/dailystrength/author_profiling_data/username_data_pan14_train_test/together"
# username_folder = "/home/prasha/Documents/dailystrength/author_profiling_data/username_data_pan14_train_test/small_test_familial"
# username_folder = "/Users/prasha/Documents/dailystrength/small_test_familial"
username_files = os.listdir(username_folder)
stops = set(stopwords.words('english'))

def non_stopwords(word_list):
    new_list = []
    for w in word_list:
        if w not in stops:
            new_list.append(w)
    return new_list

if __name__ == "__main__":
    all_words = []
    for ufile in sorted(username_files):
        with open(os.path.join(username_folder, ufile)) as ufhandle:
            for line in ufhandle.readlines():
                for sent in utils.get_sentences(line.lower()):
                    all_words.extend(non_stopwords(utils.get_words(utils.remove_punct(sent))))
    all_words_counter = Counter(all_words)
    print all_words_counter
    print all_words_counter.most_common(100)
