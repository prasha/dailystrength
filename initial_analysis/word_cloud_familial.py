__author__ = 'shrprasha'

import os
from nltk.corpus import stopwords
from collections import Counter
import utils


def remove_least_frequent(l1, drop_below):
    counts = Counter(l1)
    if drop_below > 1:
        counts = Counter({k: c for k, c in counts.items() if c >= drop_below})
    freq_words = []
    for word, freq in counts.iteritems():
        freq_words.extend([word] * freq)
    print len(freq_words)
    return freq_words

def clean_files(words):
    filter_list = ["&", "quot"]
    words_clean = [w for w in words if w not in filter_list]
    print len(words), len(words_clean),
    return words_clean

def familial_only(words):
    words_clean = [w for w in words if w in familial_tokens]
    print len(words), len(words_clean),
    return words_clean

def write_to_file(words, fhandle):
    # fhandle.seek(0)
    fhandle.truncate(0)
    fhandle.seek(0)
    fhandle.write(" ".join(words))

def write_to_file_wordle(words):
    ophandle = open(os.path.join(opdir, "female_wordle.txt"), "w")
    counts = Counter(words)
    for word, freq in counts.iteritems():
        ophandle.write(word + ":" + str(freq) + "\n")
    ophandle.close()

def get_most_common_words(words):
    remove_list = ["&", "quot"]
    counts = Counter(words)
    common = counts.most_common(50)
    for k, v in common:
        if k not in remove_list:
            print k + ":" + str(v)


def get_tf_frequencies(list_of_maps):
    freqeunt_map = {}
    #not to calculate totals all the time
    totals = {}
    # print list_of_maps
    for cls, word_ctr in list_of_maps.iteritems():  # female, Counter(words_female)
        tfs = {}
        for word, cnt in word_ctr.iteritems():  # word, count for female
            #get total count of the word in all classes
            if word not in totals:
                tot = cnt
                for cls1, word_ctr1 in list_of_maps.iteritems():  # male, Counter(words_male)
                    if cls == cls1:
                        continue
                    else:
                        tot += word_ctr1[word]
                totals[word] = tot
            else:
                tot = totals[word]

            tfs[word] = cnt * 1.0 / tot
        freqeunt_map[cls] = Counter(tfs)
    return freqeunt_map

def write_most_frequent_tf_words(freqeunt_map, no_of_words):
    # print freqeunt_map
    for cls, word_ct in freqeunt_map.iteritems():
        ophandle = open(os.path.join(opdir, cls + "_familial_wordle.txt"), "w")
        most_common = word_ct.most_common(no_of_words)
        for word, cnt in most_common:
            ophandle.write(word + ":" + str(cnt) + "\n")
        ophandle.close()

def get_words_for_class(cls_list, freq):
    cls_words_map = {}
    for cls in cls_list:
        fhandle = open(os.path.join(opdir, cls + ".txt"), "r")
        words = Counter(familial_only(fhandle.read().strip().split()))
        cls_words_map[cls] = words
        fhandle.close()
    return cls_words_map

def get_wordle_files(cls_list, freq, no_of_words):
    write_most_frequent_tf_words(get_words_for_class(cls_list, freq), no_of_words)


if __name__ == "__main__":
    # people_html_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/additional_data"
    people_html_dir = "/Users/shrprasha/Projects/resources/dailystrength/train"
    opdir = "word_clouds"

    # gender_buckets = {"male": ['wife', 'gf', 'girlfriend', 'dw'], "female": ['husband', 'bf', 'boyfriend', 'dh', 'hubby'], "neutral": ['ds', 'dd', 'son', 'daughter', 'grandson', 'granddaughter', 'father', 'mother', 'brother', 'sister', 'uncle', 'aunt', 'cousin', 'nephew', 'niece', 'family', 'godson', 'goddaughter', 'grandchild', 'grandmother', 'grandfather', 'baby', 'babies', 'child', 'children', 'kids', 'mom', 'parent']}
    familial_tokens = ['son', 'daughter', 'grandson', 'granddaughter', 'father', 'mother', 'brother', 'sister', 'uncle', 'aunt', 'cousin', 'nephew', 'niece', 'family', 'godson', 'goddaughter', 'grandchild', 'grandmother', 'grandfather', 'husband', 'wife', 'bf', 'gf', 'boyfriend', 'girlfriend', 'dh', 'ds', 'dd', 'dw', 'baby', 'babies', 'child', 'children', 'kids', 'mom', 'parent']

    freq = 20
    no_of_words = 100

    genders = ["male", "female"]
    age_groups = ["18-24", "25-34", "35-49", "50-64", "65-plus"]

    get_wordle_files(genders, freq, no_of_words)
    # get_wordle_files(age_groups, freq, no_of_words)
