__author__ = 'prasha'
import os
import re
import codecs

def find_matches(text, regex_list):
    regx_matches = []
    for regxp in regex_list:
        rexp = re.compile(regxp, re.IGNORECASE)
        matches = rexp.findall(text)
        #print matches
        regx_matches.extend(matches)
    return regx_matches

##############get filenames printed out####################
username_folder = "/home/prasha/Documents/dailystrength/author_profiling_data/username_data_pan14_train_test/together"
regex_list = [r"(?:mummy|mommy|mom|mum|momma|woman|lady|momma|girl|pregnant|granny|daughter|vixen|bride|doll|mrs|grandmama|wife|goddess)"]
username_files = os.listdir(username_folder)
total = len(username_files)
has_keyword = 0
is_female = 0
for ufile in sorted(username_files):
    stats = ufile.split("_")
    gender = stats[-1].split(".")[0]
    username = "_".join(stats[:-4])
    #text = codecs.open(os.path.join(username_folder, ufile), 'r', encoding='utf-8').read()
    matches = find_matches(username, regex_list)
    #print matches
    if matches:
        has_keyword += 1
        if gender == "female":
            is_female += 1
        else:
            print username
        #print has_keyword

print total, has_keyword, is_female


