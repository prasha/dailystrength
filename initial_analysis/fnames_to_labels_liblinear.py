__author__ = 'shrprasha'


labels = ["18-24_male", "18-24_female", "25-34_male", "25-34_female", "35-49_male", "35-49_female", "50-64_male", "50-64_female", "65-xx_male", "65-xx_female"]

# /463568_en_25-34_female.txt
def process_feature_file(feature_file_path, oppath):
    lines = open(feature_file_path, 'r').readlines()
    ophandle = open(oppath, "w")
    for line in lines:
        # print line
        fname, features = line.split(' ', 1)
        cls = fname.split("_", 2)[-1].split(".")[0]
        label = labels.index(cls)
        ophandle.write(str(label) + " " + features)

if __name__ == "__main__":
    # feature_file_path = "/Users/shrprasha/Projects/resources/dailystrength/features/liblinear.vectors"
    # oppath = "/Users/shrprasha/Projects/resources/dailystrength/features/liblinear_train.vectors"
    # process_feature_file(feature_file_path, oppath)

    feature_file_path = "/Users/shrprasha/Projects/resources/dailystrength/features/liblinearc100_test.vectors"
    oppath = "/Users/shrprasha/Projects/resources/dailystrength/features/ngrams_test_labels.vectors"
    # feature_file_path = "/Users/shrprasha/Projects/resources/dailystrength/features/old/ds_test.vectors"
    # oppath = "/Users/shrprasha/Projects/resources/dailystrength/features/old/ds_test_labels.vectors"

    process_feature_file(feature_file_path, oppath)
    # feature_file_path = "/Users/shrprasha/Projects/resources/dailystrength/features/liblinear_dev.vectors"
    # oppath = "/Users/shrprasha/Projects/resources/dailystrength/features/liblinear_dev_labels.vectors"
    # process_feature_file(feature_file_path, oppath)
