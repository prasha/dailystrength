__author__ = 'shrprasha'
import os
import sys
import fnmatch
from dill import load, dump


def create_opdir(opdir):
    if not os.path.exists(opdir):
        os.makedirs(opdir)


def normalize(normalized_fpath, non_normalized_fpath, new_normalized_fpath):
    print normalized_fpath
    print non_normalized_fpath
    print new_normalized_fpath
    with open(normalized_fpath, "r") as normed_fhandle, open(non_normalized_fpath, "r") as non_normed_fhandle, open(new_normalized_fpath, "w") as new_normed_fhandle:
        non_normed_lines = non_normed_fhandle.readlines()
        normed_lines = normed_fhandle.readlines()
        for i, normed_line in enumerate(normed_lines):
            non_normed_line = non_normed_lines[i]
            label = non_normed_line.split(" ", 1)[0]
            idx_features = normed_line.split(" ", 1)[1]
            new_normed_fhandle.write(label + " " + idx_features)


#cluster
# usage: /Users/shrprasha/Projects/dailystrength/new_data_processing_scripts/normalize.py age/gender/both
if __name__ == "__main__":
    iprootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/liblinear_files/stylistic_and_old_features"
    norm_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/liblinear_files/normalized_stylistic_and_old_features"

    train_rootdir = os.path.join(iprootdir, "train")
    dev_rootdir = os.path.join(iprootdir, "dev")
    test_rootdir = os.path.join(iprootdir, "test")

    norm_train_rootdir = os.path.join(norm_rootdir, "train")
    dump_max_rootdir = os.path.join(norm_rootdir, "max")
    norm_dev_rootdir = os.path.join(norm_rootdir, "dev")
    norm_test_rootdir = os.path.join(norm_rootdir, "test")

    create_opdir(norm_train_rootdir)
    create_opdir(dump_max_rootdir)
    create_opdir(norm_dev_rootdir)
    create_opdir(norm_test_rootdir)

    feature_file_fnames = ["gender", "both"]
    ftypes = ["train", "dev", "test"]
    for ftype in ftypes:
        normalized_fpath = os.path.join(norm_rootdir, ftype, "age")
        for feature_file_fname in feature_file_fnames:
            non_normalized_fpath = os.path.join(iprootdir, ftype, feature_file_fname)
            new_normalized_fpath = os.path.join(norm_rootdir, ftype, feature_file_fname)
            normalize(normalized_fpath, non_normalized_fpath, new_normalized_fpath)



