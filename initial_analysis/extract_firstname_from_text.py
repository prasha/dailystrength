__author__ = 'prasha'
import os
import nltk
from nltk.tag.stanford import NERTagger
from collections import Counter

clasifier_model = '/home/prasha/Documents/softwares/stanford-ner-2014-08-27/classifiers/english.all.3class.distsim.crf.ser.gz'
stanford_ner_jar = '/home/prasha/Documents/softwares/stanford-ner-2014-08-27/stanford-ner.jar'
st = NERTagger(clasifier_model, stanford_ner_jar)
female_names = []
male_names = []

def get_census_male_female_list():
    names_gender_file = "/home/prasha/Documents/dailystrength/author_profiling_data/name_gender_list"
    lines = open(names_gender_file, 'r').readlines()
    female_names = lines[0].strip().split(",")
    female_names = [x.lower() for x in female_names]
    male_names = lines[1].strip().split(",")
    male_names = [x.lower() for x in male_names]
    return male_names, female_names

#get firstnames from last sentences of all posts by that person: return the most frequent firstname in all posts
#for better performance of NER system do not split sentences. Check if the named entity falls in last n or not in get_person_name_if_exists instead
#atleast_n : keep on getting words starting from last sentence until you get atleast_n
#n: the person name must fall within n
def get_firstnames_gender_in_last_sents(fHandle, atleast_n , n):
    firstnames = []
    posts = fHandle.readlines()
    for post in posts:
        sentences = nltk.sent_tokenize(post)
        words = []
        word_lists = [] #since ner only gives tags for single sentence
        for sentence in reversed(sentences):
            new_words = nltk.word_tokenize(sentence)
            words = new_words + words
            word_lists = [new_words] + word_lists
            if len(words) >= atleast_n:
                break
        # print words
        firstname = get_person_name_if_exists(word_lists, n)
        if firstname: #if firstname found through NER
            firstnames.append(firstname.lower())
        else: #if not check all the last n words to see if it falls in census list
            for word in reversed(words[-atleast_n:]):
                gender = get_gender_of_name(word)
                if gender:
                    print "method2", word, gender
                    firstnames.append(word)
                    break
    # print "3", firstnames
    firstnames_counter = Counter(firstnames)
    # print "4", firstnames_counter
    if len(firstnames_counter) > 1:
        mc = firstnames_counter.most_common()
        firstnames = [x for x, count in mc if count == mc[0][1]]

    #choose random one if there is more than one with the highest count
    if firstnames:
        firstname = firstnames[-1]
    return firstname, get_gender_of_name(firstname)


#returns last word tagged as person
def get_person_name_if_exists(word_lists, n):
    word_tag_list = []
    for word_list in word_lists:
        word_tag_list.extend(st.tag(word_list))
    # print "2", word_tag_list
    word_tag_list = word_tag_list[-n:]
    firstname = ""
    for word, tag in word_tag_list:
        if tag == "PERSON":
            firstname = word
    return firstname


def get_gender_of_name(firstname):
    pred_gender = ""
    if firstname in female_names:
        pred_gender = "female"
    elif firstname in male_names:
        pred_gender = "male"
    return pred_gender

if __name__ == "__main__":
    n = 10
    atleast_n = 5
    username_text_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/username_data_pan14_train_test/together"
    # username_text_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/test_firstname"
    firstname_feature_file = "/home/prasha/Documents/dailystrength/author_profiling_data/userid_firstname"
    male_names, female_names = get_census_male_female_list()
    # print male_names, female_names
    op_fHandle = open(firstname_feature_file, 'w')
    files = next(os.walk(username_text_dir))[2] #get just the files and not directories
    count_having_firstname = 0
    count_having_gender = 0
    count_correctly_predicted = 0
    for fname in sorted(files):
        if ".txt" not in fname[-4:]:
            continue
        print fname
        stats = fname.split("_")
        gender = stats[-1].split(".")[0]
        age = stats[-2]
        lang = stats[-3]
        id = stats[-4]
        uname = "_".join(stats[:-4])
        fHandle = open(os.path.join(username_text_dir, fname), 'r')
        firstname, pred_gender = get_firstnames_gender_in_last_sents(fHandle, atleast_n , n)
        if(firstname):
            count_having_firstname += 1
            print fname, firstname, pred_gender
            if pred_gender:
                count_having_gender += 1
                if gender.strip() == pred_gender:
                    count_correctly_predicted += 1
            op_fHandle.flush()
        op_fHandle.write(fname + "," + id + "," + uname + "," + age + "," + gender + "," + firstname + "," + pred_gender + "\n")
        # print(id + "," + uname + "," + age + "," + gender + "," + firstname + "," + pred_gender)

print "total having firstname: ", count_having_firstname
print "total in gender list: ", count_having_gender
print "total correctly predicted: ", count_correctly_predicted


