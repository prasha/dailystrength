# -*- coding: utf-8 -*-
import nltk
from nltk.tokenize.punkt import PunktWordTokenizer
# import constants
import re
import numpy
import string
from scipy import spatial
from nltk.corpus import stopwords


def get_sentences(document):
    sentences = nltk.sent_tokenize(document)
    return sentences

#used everywhere
def get_words(sentence):
    return nltk.word_tokenize(sentence)
    #return nltk.word_tokenize(remove_punct(sentence)) #do not uncomment this; call remove_punct and give output to get_words

#not used
def get_words_punkt(sentence):
    PunktWordTokenizer().tokenize(remove_punct(sentence))
    word_list = PunktWordTokenizer().tokenize(sentence)
    return word_list
#not used
def get_words_punkt_period_removed(sentence):
    period = re.compile(r'[.]')
    period.sub("", sentence)
    word_list = PunktWordTokenizer().tokenize(sentence)
    return word_list
   
def create_n_gram_profile(words, n):
    return nltk.ngrams(words, n)
    
def remove_punct(s):
    punctuation = re.compile(r'[-.?!,":;()`]')
#    bullet = re.compile(ur'•', re.UNICODE)
    bullet = re.compile(ur'\u2022')
#    bullet = re.compile(r'u\'\u2022\'')
#    bullet = re.compile("•")
    return bullet.sub(" ", punctuation.sub(" ", s))

def remove_punct_from_list(l):
    puncts = string.punctuation + u'\u2022'
    no_puncts = [x for x in l if x not in puncts]
    return no_puncts

def get_words_in_line(line):
    words = []
    line = remove_punct(line)
    sentence_list = get_sentences(line)
    for sentence in sentence_list:
        word_list = get_words(sentence)
        sw_in_sent = [w.lower() for w in word_list] # just changing this to take all words wont change a thing because stopwords have been used throughout the project
        words.extend(sw_in_sent)
    return words

def get_words_in_line_wo_stopwords(line, stop):
    words = []
#    line = remove_punct(line)
    sentence_list = get_sentences(line)
    for sentence in sentence_list:
        word_list = get_words(sentence)
        sw_in_sent = [w.lower() for w in word_list] # just changing this to take all words wont change a thing because stopwords have been used throughout the project
        words.extend(sw_in_sent)
    return [i for i in words if i not in stop]

def get_words_wo_urls_stopwords(fpath):
    filter_words = stopwords.words('english')
    filter_words.extend(["&", "quot", "@", "www", "%"])
    filter_words_containing = ["'", "/", "\\", "."]
    lines = open(fpath, "r").readlines()
    words = []
    for line in lines:
        line = re.sub(r'^https?:\/\/.*[\r\n]*', '', line, flags=re.MULTILINE)
        words.extend(get_filtered_words_in_line(line, filter_words, filter_words_containing))
    return words
def get_filtered_words_in_line(line, filter_words, filter_words_containing):
    words = []
#    line = remove_punct(line)
    sentence_list = get_sentences(remove_punct(line))
    for sentence in sentence_list:
        word_list = get_words(sentence)
        sw_in_sent = [w.lower() for w in word_list] # just changing this to take all words wont change a thing because stopwords have been used throughout the project
        for word in sw_in_sent:
            if word not in filter_words and not contains_char(word, filter_words_containing):
                words.append(word)
    return words
def contains_char(token, char_list):
    contains = False
    for c in char_list:
        if c in token:
            contains = True
            break
    return contains

def common_between_lists(susp_nes, src_nes):
    return set(susp_nes).intersection(set(src_nes))

def compare_tuples(t1, t2, match):
    if(len(set(t1).intersection(set(t2))) >= match):
#        print "yes"
        return True
    else:
#        print "no"
        return False

def merge_lists_remove_duplicates(l1, l2):
#did not work since list is a list of lists returns unhashable type:list
#    return sorted(l1 + list(set(l2) - set(l1)))
    for x in l1:
        if x not in l2:
            l2.append(x) 
    return l2

def allwords(document):
    document.seek(0)
    doc_all = []
    for line in document:
        doc_all.extend(get_words_in_line(line))
    return doc_all

def total_words(document):
    s = 0
    a = 0
#    for line in document:
#        sentence_list = get_sentences(line)
#        for sentence in sentence_list:
#            word_list = get_words(sentence)
#            for w in word_list:
#                if w.lower() in constants.stopwords:
#                    s+=1
#                a+=1
    for line in document:
        a += len(get_words_in_line(line))
#    print a
#    print s

def word_count_in_file(file1):
    cnt = 0
    lines = open(file1, "r").readlines()
    for line in lines:
        cnt += len(get_words_in_line(line))
        print get_words_in_line(line)
    return cnt

def word_count_in_file_wo_urls(file1):
    cnt = 0
    lines = open(file1, "r").readlines()
    for line in lines:
        line = re.sub(r'^https?:\/\/.*[\r\n]*', '', line, flags=re.MULTILINE)
        cnt += len(get_words_in_line(line))
    return cnt

def word_count_in_file_wo_urls_lines(lines):
    cnt = 0
    for line in lines:
        line = re.sub(r'^https?:\/\/.*[\r\n]*', '', line, flags=re.MULTILINE)
        cnt += len(get_words_in_line(line))
    return cnt

#used by get_avg_distance_betn_plag_seg_and_other_segs        
def get_distance_betn_two_vectors(t1, t2):
#    print "t1: ", t1
#    print "t2: ", t2
    fv1 = numpy.array(t1)
    fv2 = numpy.array(t2)
    return numpy.linalg.norm(fv1-fv2)

def get_cosine_distance(t1, t2):
    return spatial.distance.cosine(t1, t2)

def make_normalize(omin, omax):
    def normalize(x):
#        b = 1
#        a = 0
#        return a + (x-omin) * (b - a) / (omin - omax)
        return 1.0 if (omin==omax) else (x-omin) / (omax - omin)
    return normalize
    
def print_lol(list_of_lists):
    print "["
    for list in list_of_lists:
        print list, ","
    print "]"
    
def remove_multiple_whitespaces(passage):
    passage = passage.replace(u'\xa0', u' ')
    return re.sub( '\s{2,}', ' ', passage).strip()

def display_dict_sorted_keys(mydict):
    for key in sorted(mydict.iterkeys()):
        print "%s: %s" % (key, mydict[key])
    print
    
def get_sil_coeff(dist_own, dist_other):
    return float(dist_other - dist_own) / max(dist_other, dist_own)

#input: list of words in a sentence
def get_pos_tags_hunpos(words_in_sent, ht):
    #tagger given as input since it will be slow to load the model each time
    #ht = HunposTagger('/home/prasha/Documents/softwares/hunpos/english.model', encoding='utf-8')
    tags = ht.tag(get_words(words_in_sent))
    return tags
    
#def get_charidx_wordidx_mapping(document):
#    from = 0
#    to = 1
#    word_count = 1
#    while word_count == 1:
#        nltk.word_tokenize(remove_punct(sent))
                    
if __name__ == "__main__":
#    filen = "/home/prasha/Documents/docsig/pan2013-detailed-comparison-training-corpus/src/source-document00658.txt"
#    document = codecs.open(filen, encoding='utf-8')
#    total_words(document)
#    print get_distance_betn_two_vectors([2,2,2], [2,2,2])
#    s = '• • • • • • • • • • • • Writing Competitions • • • Moot Court Competitions'

    #print "aa"+ remove_punct("``").strip() + "aa"

    print get_words(" Oh, can't you? ")

#    s = u'• • • • • • • • • • • • Writing Competitions • • • Moot Court Competitions'
##    s = "what the heck???? dfsd"
#    print s
#    print remove_punct(s)
    
