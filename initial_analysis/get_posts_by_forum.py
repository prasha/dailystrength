# for byron

import os

__author__ = 'shrprasha'

import scripts_sklearn.file_utils as futils
from new_data_processing_scripts.user import User, Post



if __name__ == "__main__":
    # rootdir = "/Users/shrprasha/Dropbox/temp/usr"
    # rootopdir = "/Users/shrprasha/del/pkl"
    rootdir = "/home/ritual/resources/dailystrength/after_sklearn/all_user_posts_separate"
    rootopdir = "/home/ritual/del/ds_posts_by_forum"
    uid_fnames = futils.get_files_in_folder(rootdir)
    posts_by_diseases = {}
    for uid_fname in uid_fnames:
        print uid_fname
        user = futils.load_dumps(os.path.join(rootdir, uid_fname))
        for posts_list in user.posts.values():
            for post in posts_list:
                if post.disease not in posts_by_diseases:
                    posts_by_diseases[post.disease] = [post.text]
                else:
                    posts_by_diseases[post.disease].append(post.text)
    for disease, posts_list in posts_by_diseases.iteritems():
        futils.create_dumps(posts_list, os.path.join(rootopdir, disease + ".pkl"))
