__author__ = 'shrprasha'
import os
import utils

def get_short_files():
    files = next(os.walk(docdir))[2] #get just the files and not directories
    short_files = []
    for fname in files:
        count = utils.word_count_in_file_wo_urls(os.path.join(docdir, fname))
        if count < 10:
            print fname, count
            short_files.append(fname)
    return short_files

def remove_short_files_from_vectors():
    vector_fhandle = open("/Users/shrprasha/Projects/resources/dailystrength/features/liblinear.vectors", "r+")
    vector_lines = vector_fhandle.readlines()
    short_files = ["/" + x.strip() for x in open("/Users/shrprasha/Projects/resources/dailystrength/remove_short_files_only", "r").readlines()]
    vector_fhandle.seek(0)
    for line in vector_lines:
        fname = line.split(" ", 1)[0]
        if fname not in short_files:
            vector_fhandle.write(line)
    vector_fhandle.truncate()
    vector_fhandle.close()

#used after removing short files from docdir
def remove_short_fnames_userid_userdetails(uid_fname_additional, uid_fname_old, userid_userdetails):
    files = next(os.walk(docdir))[2]
    uids = [x.split("_", 1)[0] for x in files]

    additional_lines = open(uid_fname_additional, "r").readlines()
    old_lines = open(uid_fname_old, "r").readlines()
    op_fhandle = open(userid_userdetails, "w")

    additional_map = {}
    old_map = {}

    for line in additional_lines:
        additional_map[line.split(",")[1]] = line

    for line in old_lines:
        old_map[line.split(",")[1]] = line

    count = 0
    for uid in uids:
        if uid in additional_map:
            line = additional_map[uid]
        elif uid in old_map:
            line = old_map[uid]
        else:
            print "not found", uid
            count += 1
        op_fhandle.write(line)
    print "tot: ", count

def get_userid_userdetails_for_separate_dirs(userid_userdetails, docdir, op_path):
    files = next(os.walk(docdir))[2]
    uids = [x.split("_", 1)[0] for x in files]
    uids_map = {}
    lines = open(userid_userdetails, "r").readlines()
    for line in lines:
        uids_map[line.split(",")[1]] = line
    opfhandle = open(os.path.join(op_path, "userid_user_details_age_gender_both_" + os.path.basename(docdir) + ".txt"), "w")
    for uid in uids:
        opfhandle.write(uids_map[uid])
    opfhandle.close()


def get_stats():
    train = "/Users/shrprasha/Projects/resources/dailystrength/train"
    dev = "/Users/shrprasha/Projects/resources/dailystrength/dev"
    test = "/Users/shrprasha/Projects/resources/dailystrength/test"
    short_files = get_short_files()
    train_files = files = next(os.walk(train))[2]
    dev_files = files = next(os.walk(dev))[2]
    test_files = files = next(os.walk(test))[2]
    print len([x for x in short_files if x in train_files])
    print len([x for x in short_files if x in dev_files])
    print len([x for x in short_files if x in test_files])

if __name__ == "__main__":
    docdir = "/Users/shrprasha/Projects/resources/dailystrength/together"
    # remove_short_files_from_vectors()
    # remove_short_fnames_userid_userdetails("/Users/shrprasha/Projects/dailystrength/resources/username_id_age_gender_both_age_gender_short_removed.txt")
    # remove_short_fnames_userid_userdetails("/Users/shrprasha/Projects/dailystrength/resources/username_id_age_gender_both_age_gender.txt")
    # remove_short_fnames_userid_userdetails("/Users/shrprasha/Projects/dailystrength/resources/username_id_age_gender.txt")
    # remove_short_fnames_userid_userdetails("/Users/shrprasha/Projects/dailystrength/resources/userid_userdetails.txt")

    # uid_fname_additional = "/Users/shrprasha/Projects/dailystrength/resources/username_id_age_gender_both_age_gender.txt"
    # uid_fname_old = "/Users/shrprasha/Dropbox/DailyStrength/username_id_age_gender_both_age_gender.txt"
    # userid_userdetails = "/Users/shrprasha/Projects/dailystrength/resources/username_id_age_gender_both_age_gender_combined.txt"
    # remove_short_fnames_userid_userdetails(uid_fname_additional, uid_fname_old, userid_userdetails)

    userid_userdetails = "/Users/shrprasha/Projects/dailystrength/resources/username_id_age_gender_both_age_gender_combined.txt"
    dev = "/Users/shrprasha/Projects/resources/dailystrength/dev"
    train = "/Users/shrprasha/Projects/resources/dailystrength/train"
    test = "/Users/shrprasha/Projects/resources/dailystrength/test"
    op_path = "/Users/shrprasha/Projects/dailystrength/resources"
    get_userid_userdetails_for_separate_dirs(userid_userdetails, dev, op_path)
    get_userid_userdetails_for_separate_dirs(userid_userdetails, train, op_path)
    get_userid_userdetails_for_separate_dirs(userid_userdetails, test, op_path)


# print utils.word_count_in_file_wo_urls("/Users/shrprasha/Projects/resources/dailystrength/train/2248546_en_25-34_male.txt")