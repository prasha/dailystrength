__author__ = 'shrprasha'

import os
import utils
from collections import Counter
import math
import sys

# def get_all_words():
    # all_unique_words = open("word_clouds/male.txt", "r").read().strip().split()
    # all_unique_words.extend(open("word_clouds/female.txt", "r").read().strip().split())
    # all_unique_words = set(all_unique_words)

def get_word_counts_per_disease(textdir):
    files = sorted(next(os.walk(textdir))[2])
    disease_words_counts = Counter()
    for doc_fname in files:
        words = utils.get_words_wo_urls_stopwords(os.path.join(textdir, doc_fname))
        words_counts = Counter(words)
        disease_words_counts += words_counts
    return disease_words_counts

def get_pmi(diseases_counts, opfile):
    opfhandle = open(opfile, "w")
    opfhandle.write("word " + " ".join(diseases) + "\n")
    all_words_counts = Counter()

    for word_counts in diseases_counts.values():
        all_words_counts += word_counts
    total_words = sum(all_words_counts.values())

    #init
    diseases_words_pmi = {}
    for disease in diseases:
        diseases_words_pmi[disease] = {}

    #get pmi for each word and disease
    for word, count in all_words_counts.iteritems():
        p_word = count * 1.0 / total_words
        line = word
        for disease in diseases:
            print disease
            p_word_given_disease = diseases_counts[disease][word] * 1.0 / sum(diseases_counts[disease].values())
            # print p_word, count, diseases_counts[disease][word], p_word_given_disease, p_word
            if p_word_given_disease == 0.0:
                pmi_word_disease = 0
            else:
                pmi_word_disease = math.log(p_word_given_disease / p_word, 2)
            diseases_words_pmi[disease][word] = pmi_word_disease
            line += " " + str(pmi_word_disease)
        opfhandle.write(line + "\n")
    return diseases_words_pmi

def get_pmi_normalized(diseases_counts):
    print diseases_counts
    all_words_counts = Counter()

    for word_counts in diseases_counts.values():
        all_words_counts += word_counts
    total_words = sum(all_words_counts.values())

    #init
    diseases_words_npmi = {}
    for disease in diseases:
        diseases_words_npmi[disease] = {}

    # get pmi for each word and disease
    for disease, words_counts in diseases_counts.iteritems():
        total_words_in_disease = sum(word_counts.values())
        for word, count in words_counts.iteritems():
            # if count == 0.0: # never happens
            p_word = all_words_counts[word] * 1.0 / total_words
            p_word_given_disease = count * 1.0 / total_words_in_disease
            p_word_and_disease = count * 1.0 / total_words
            #  not used here, but just for the concept that the probability of disease will also be in relation to words
            #  p_disease = sum(words_counts.values()) / total_words
            npmi_word_disease = math.log(p_word_given_disease / p_word, 2) / (-math.log(p_word_and_disease, 2))
            diseases_words_npmi[disease][word] = npmi_word_disease
    return diseases_words_npmi

def write_to_file(diseases_words_npmi, opdir):
    for disease, words_npmi in diseases_words_pmi.iteritems():
        opfhandle = open(os.path.join(opdir, disease), "w")
        for word, npmi in words_npmi.iteritems():
            opfhandle.write(word + " " + str(npmi) + "\n")
        opfhandle.close()

def get_topx(diseases_words_pmi):
    for disease, words_pmi in diseases_words_pmi.iteritems():
        counts = Counter(words_pmi)
        print disease,
        print [x for x,y in counts.most_common(200)]

#  python2.7 pmi_diseases_words.py /mnt/docsig/storage/daily-strength /home/prasha/Dropbox/dailystrength/resources/pmi
#  python pmi_diseases_words.py /Users/shrprasha/Projects/resources/dailystrength/small_tests /Users/shrprasha/Projects/resources/dailystrength/small_tests/output/pmi
if __name__ == "__main__":
    # rootdir = "/mnt/docsig/storage/daily-strength"
    # # opdir = "/home/prasha/Documents/dailystrength/author_profiling_data/data_pan14"
    # opfile = "/home/prasha/Dropbox/dailystrength/resources/pmi.txt"
    # # diseases = ["Bone-Cancer", "Immigration-Law"]
    rootdir = sys.argv[1]
    opdir = sys.argv[2]
    diseases = ["Acne", "ADHD", "Alcoholism", "Asthma", "Back-Pain", "Bipolar-Disorder", "Bone-Cancer", "COPD", "Diets-Weight-Maintenance", "Fibromyalgia", "Gastric-Bypass-Surgery", "Immigration-Law", "Infertility", "Loneliness", "Lung-Cancer", "Migraine", "Miscarriage", "Pregnancy", "Rheumatoid-Arthritis", "War-In-Iraq"]
    diseases = ["Acne", "ADHD"]
    diseases_counts = {}
    for disease in diseases:
        textdir = os.path.join(rootdir, disease, "Texts")
        diseases_counts[disease] = get_word_counts_per_disease(textdir)
    diseases_words_pmi = get_pmi_normalized(diseases_counts)
    print diseases_words_pmi
    write_to_file(diseases_words_pmi, opdir)
    get_topx(diseases_words_pmi)

