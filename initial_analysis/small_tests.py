__author__ = 'shrprasha'

import os
import utils
import re

def split_file(fpath, no_of_lines, outdir):
    lines = open(fpath, "r").readlines()
    print len(lines)
    for i in range(0, len(lines), no_of_lines):
        with open(os.path.join(outdir, str(i / no_of_lines) + ".txt"), "w") as opfhandle:
            for opline in lines[i:i+no_of_lines]:
                opfhandle.write(opline)

def regex_split(regex, l1):
    tokens = []
    for token in l1:
        tokens.extend(re.split(regex, token))
    return tokens

def get_firstname_partitions0(username):
    fp = []
    parts = re.split("[_\.-]", username)
    print parts
    if not parts[0] == username:
        fp.extend(parts)
    parts1 = []
    for part in parts:
        p = re.findall('[A-Z][^A-Z]*', part)
        if(len(p) > 0):
            if not p[0] == part:
                parts1.extend(p)
    fp.extend(parts1)
    if not fp:
        fp = [re.sub(r'[0-9]+', "_", username)]
    return [x for x in fp if x]

def get_firstname_partitions(username):
    parts = regex_split(r"[_\.-]", [username])
    print "2",parts
    parts = regex_split('(\d+)', parts)
    print "1",parts
    fp = [x for x in parts]
    for part in parts:
        p = re.findall('[A-Z][^A-Z]*', part)
        fp.extend(p)
        print part, fp
    if not fp:
        fp = [re.sub(r'[0-9]+', "_", username)]
    return set([x for x in fp if x])

if __name__ == "__main__":
    username = "PrincessBabe_Queen11_amy"
    # username = "11"
    # username = "abc"
    print get_firstname_partitions(username)
    # print get_firstname_partitions0(username)

# outdir = "/Users/shrprasha/del/ds_people_lists/all_people_list_divided"
# fpath = "/Users/shrprasha/del/ds_people_lists/all_people_list_unique.txt"
# no_of_lines = 28
# split_file(fpath, no_of_lines, outdir)

# together = "/Users/shrprasha/Projects/resources/dailystrength/together"
# files = next(os.walk(together))[2]
#
# for fname in files:
#     lines = open(os.path.join(together, fname), "r").readlines()
#     for line in lines:
#         words = utils.get_words_in_line(line)
#         if "andrea" in words:
#             print fname
#             print line


# rootdir = "/mnt/docsig/storage/daily-strength"
# # diseases = ["Acne", "ADHD", "Alcoholism", "Asthma", "Back-Pain", "Bipolar-Disorder", "Bone-Cancer", "COPD", "Diets-Weight-Maintenance", "Fibromyalgia", "Gastric-Bypass-Surgery", "Immigration-Law", "Infertility", "Loneliness", "Lung-Cancer", "Migraine", "Miscarriage", "Pregnancy", "Rheumatoid-Arthritis", "War-In-Iraq"]
# diseases = ["Alcoholism", "Asthma", "Back-Pain", "Bipolar-Disorder", "Bone-Cancer", "COPD", "Diets-Weight-Maintenance", "Fibromyalgia", "Gastric-Bypass-Surgery", "Immigration-Law", "Infertility", "Loneliness", "Lung-Cancer", "Migraine", "Miscarriage", "Pregnancy", "Rheumatoid-Arthritis"]
# for disease in diseases:
#     print "cp -r", os.path.join(rootdir, disease, "Texts"), os.path.join("/nethome/students/prasha/ds_data/", disease)
#     # print "mkdir", disease
