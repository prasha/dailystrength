__author__ = 'shrprasha'

import os
from list_all_pages import list_all
from download_web_pages import download
from threads_to_xml import convert_to_xml

def get_lines_in_file(fpath):
    with open(fpath) as fhandle:
        return fhandle.readlines()

def create_opdir(opdir):
    if not os.path.exists(opdir):
        os.makedirs(opdir)

def get_url(disease):
    return "http://www.dailystrength.org/c/%s/forum" % disease

if __name__ == "__main__":
    output_folder="dataset"
    create_opdir(output_folder)
    for line in get_lines_in_file("diseases"):
        disease = line.strip()
        print "downloading for ", disease
        url = get_url(disease)
        current_output_folder = os.path.join(output_folder, disease)
        create_opdir(current_output_folder)
        list_all([get_url(disease), current_output_folder])
        download([current_output_folder, os.path.join(current_output_folder, "FilesLists.txt")])
        convert_to_xml([os.path.join(current_output_folder, "HTML"), current_output_folder, "FilesLists.txt"])

