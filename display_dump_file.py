__author__ = 'shrprasha'

from pickle import load, dump
from new_data_processing_scripts.user import User, Post
# from user import User, Post

def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

def print_obj(obj):
    attrs = vars(obj)
    print ', '.join("%s: %s" % item for item in attrs.items())

def load_and_print_dump(fpath):
    overall_dump = load(open(fpath, "r"))
    if type(overall_dump) is dict:
        print "length:", len(overall_dump)
        for k, v in overall_dump.iteritems():
            print k, v
    elif type(overall_dump) is list:
        print "length:", len(overall_dump)
        for x in sorted(overall_dump):
            print x
    else:
        print overall_dump
    return overall_dump

def load_and_print_dump_len_sample(fpath):
    overall_dump = load(open(fpath, "r"))
    print type(overall_dump)
    if type(overall_dump) is dict:
        print "length:", len(overall_dump)
        for k, v in overall_dump.iteritems():
            print k, v
            break
    elif type(overall_dump) is list:
        print "length:", len(overall_dump)
        for x in overall_dump:
            print x
            # print x.postid
            break

if __name__ == "__main__":
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/char_ngram_features_idx.dump"
    # load_and_print_dump_len_sample(fpath)
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/char_ngram_features/255_31-48.dump"
    # print(load_dumps(fpath))
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps/test.dump"
    # print(load_dumps(fpath))
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/familial_features/93_31-48.dump"
    # print(load_dumps(fpath))

    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/stylistic_features/93_31-48.dump"
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/stylistic_with_feature_names/93_31-48.dump"
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/idf.dump"
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/idf.dump"
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/char_ngram_features_tf_idf/1015612_19-28.dump"
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/username_partition_features/1015612_19-28.dump"
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/username_features_idx.dump"
    # fpath = "/Users/shrprasha/del/Fructose-Intolerance_posts.dump"
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps/test.dump"
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/disease_post_dumps/Fructose-Intolerance_posts.dump"
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps/93_31-48"
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps/93_31-48"
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps/old_all_cont/test.dump"
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps/old_cont_all/train.dump"
    # fpath = "/project/solorio/prasha/ds/processed_data/user_dumps_old_all/train.dump"
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps/all_users.dump"
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps/old_cont_all/dev.dump"
    # load_and_print_dump_len_sample(fpath)
    # fpath = "/home/ritual/resources/dailystrength/after_sklearn/all_users_posts.pkl"
    fpath = "/home/ritual/resources/dailystrength/after_sklearn/all_user_posts_separate/1000004.pkl"
    user = load_and_print_dump(fpath)
    # print user.age_group
    print_obj(user)
    print_obj(user.posts[0])    

    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps/train.dump"

    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/needs_further_processing/word_ngram_features_tf_idf/1000004_31-48.dump"
    # load_and_print_dump(fpath)

    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps/564434_12-16"
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps/99986_51-63"
    # load_and_print_dump_len_sample(fpath)

    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/idf_train_small.dump"
    # load_and_print_dump(fpath)

    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/idf_train1.dump"
    # load_and_print_dump_len_sample(fpath)

    # fpath = "/Users/shrprasha/del/ds_features/needs_further_processing/word_ngram_features_tf_idf/1098.dump"
    # fpath = "/Users/shrprasha/del/127812_31-48.dump"
    # load_and_print_dump(fpath)

    # user = User("1","2","3", "4")
    # print user
