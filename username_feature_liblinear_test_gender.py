__author__ = 'prasha'
import xml.etree.ElementTree as xml
import os
import codecs
import sys
import shutil
import nltk
from pprint import pprint
from collections import Counter
import re

#username can have '_' in it. so, start indexing from the end after splitting by '_'

def char_ngrams(str, n):
    arr = list(str)
    return nltk.ngrams(arr, n)

class UsernameLibLinear:

    def __init__(self, username_text_dir, output_fHandle, dict_files_dir):
        self.username_text_dir = username_text_dir
        self.output_fHandle = output_fHandle
        self.dict_files_dir = dict_files_dir
        self.label_list = self.load_labels(os.path.join(self.dict_files_dir, "labels_gender"))
        self.feature_list = self.load_features(os.path.join(self.dict_files_dir, "features_gender"))
        self.label_instances_map = {}
        for label in self.label_list:
            self.label_instances_map[label] = []
        self.create_features()
        self.write_to_file()

    def load_labels(self, path):
        lines = open(path).readlines()
        list1 = [''] * (len(lines) + 1)
        for line in lines:
            # print line
            idx, val = line.strip().split("\t")
            list1[int(idx)] = val
        return list1

    def load_features(self, path):
        lines = open(path).readlines()
        list1 = [()] * (len(lines) + 1)
        for line in lines:
            # print line
            idx, val = line.strip().split("\t")
            list1[int(idx)] = self.str_to_tuple(val)
        return list1

    def str_to_tuple(self, str):
        str = str.strip(")(")
        elems = str.split(",")
        tup = ()
        for elem in elems:
            elem = elem.strip().strip("'")
            tup += (elem,)
        return tup

    def create_features(self):
        files = next(os.walk(self.username_text_dir))[2] #get just the files and not directories
        for file in files:
            stats = file.split("_")
            gender = stats[-1].split(".")[0]
            age = stats[-2]
            lang = stats[-3]
            id = stats[-4]
            username = "_".join(stats[:-4])
            firstnames = self.get_firstname_partitions(username)
            username_start_end_marker = "%" + username + "%"
            label = gender
            bigrams = char_ngrams(username_start_end_marker, 2)
            trigrams = char_ngrams(username_start_end_marker, 3)
            fourgrams = char_ngrams(username_start_end_marker, 4)
            fivegrams = char_ngrams(username_start_end_marker, 5)
            features = bigrams + trigrams + fourgrams + fivegrams + firstnames
            self.update_maps(Counter(features), label)

    def get_firstname_partitions(self, username):
        fp = []
        parts = re.split("[_\.-]", username)
        if not parts[0] == username:
            fp.extend(parts)
        parts1 = []
        for part in parts:
            p = re.findall('[A-Z][^A-Z]*', part)
            if(len(p) > 0):
                if not p[0] == part:
                    parts1.extend(p)
        fp.extend(parts1)
        return fp

    def update_maps(self, features, label):
        # self.label_list.append(label)
        # self.label_list = list(set(self.label_list))
        # self.feature_list = list(set(self.feature_list+features.keys()))
        if label in self.label_instances_map:
            self.label_instances_map[label].append(features)

    def write_to_file(self):
        for label, instances in self.label_instances_map.iteritems(): #instances: all instances/people of certain class(label)
            label_idx = self.label_list.index(label)
            for instance in instances:  #instance: features of an instance/person
                line = str(label_idx) + " "
                for feature in sorted(instance):
                    count = instance[feature]
                    if feature in self.feature_list:
                        feature_idx = self.feature_list.index(feature)
                        line += str(feature_idx) + ":" + str(count) + " "
                line = line.strip() + "\n"
                self.output_fHandle.write(line)

    def write_list_to_file(self, list1, fname):
        fHandle = open(fname, 'w')
        for k,v in enumerate(list1):
            fHandle.write(str(k) + " " + str(v) + "\n")


#extra
def pretty_print(list1):
    for k,v in enumerate(list1):
        print k+1, ":", v,
    print "\n"

if __name__ == "__main__":
    # age_gender_file = "/home/prasha/Dropbox/DailyStrength/username_id_age_gender_both_age_gender.txt"
    # people_html_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/data_pan14_1"
    username_text_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/username_data_pan14_train_test/test"
    # username_text_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/test_small_username_data_pan14"
    output_file = "liblinear_op/username_liblinear_test_gender"
    dict_files_dir = "liblinear_op"
    ull = UsernameLibLinear(username_text_dir, open(output_file, 'w'), dict_files_dir)






