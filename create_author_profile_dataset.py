__author__ = 'prasha'
import xml.etree.ElementTree as xml
import os
import codecs
import sys

#decisions:
#did not leave out treatments: although treatments are not indicative of a person's age or gender
#knowledge about what disease this post was under is not preserved

#TODO:
#load people_age_gender
#parse all xml in XML folder to get all <text> and <treatment>, join them by space

#create file with fname: userid_age_gender
#NA when age or gender are not available
#each file contains all the texts written by that user at that age
class CreateDataset:

    def __init__(self, age_gender_file, xmldir, opdir):
        self.opdir = opdir
        self.xmldir = xmldir
        lines = open(age_gender_file).readlines()
        self.age_gender_map = {}
        self.text_by_user_at_age = {} #{user1: {age11: "text11", age12: "text12"}, user2: {age21: "text21", age22: "text22"}}
        for id_age_gender in lines:
            id_age_gender = id_age_gender.strip()
            id, age, gender = id_age_gender.split(",")
            self.age_gender_map[id] = (age, gender)
        self.create()

    def create(self):
        files = sorted(os.listdir(self.xmldir))
        for doc_fname in files:
            extension = os.path.splitext(doc_fname)[-1]
            if extension != ".xml":
                continue
            print doc_fname
            self.parse_xml(os.path.join(self.xmldir, doc_fname))
        self.write_to_file()

    def parse_xml(self, xml_file):
        tree = xml.parse(os.path.join(xml_file))
        root = tree.getroot()
        for problem in root.iter('problem'):
            self.store_to_map(problem)
        for reply in root.iter('reply'):
            self.store_to_map(reply)

    def store_to_map(self, xmlobj):
        id, date_posted, text = self.parse_problem_reply(xmlobj)
        if id in self.age_gender_map:
            age_now = self.age_gender_map[id][0]
            print age_now
            actual_age = self.get_actual_age(age_now, date_posted)
            print actual_age
            if id in self.text_by_user_at_age:
                if(actual_age in self.text_by_user_at_age[id]):
                    self.text_by_user_at_age[id][actual_age] += "\n" + text
                else:
                    self.text_by_user_at_age[id][actual_age] = text
            else:
                self.text_by_user_at_age[id] = {actual_age: text}
            print self.text_by_user_at_age
        else:
            print "not in map: ", id

    def parse_problem_reply(self, xmlobj):
        id = xmlobj.find('person').get('id')  #'/people/436265'
        id= id.split("/")[-1]
        date_posted = xmlobj.find('date').text
        content = xmlobj.find('content')
        text = ""
        for child in content: #text or treatment
            text += child.text + " "
        return id, date_posted, text

    def get_actual_age(self, age_now, date_posted):
        year_obtained = 13  #all data was read on 2013; so the age is the age of the poster in 2013
        if(age_now):
            age = int(age_now)
            year_posted = int(date_posted.split()[0].split("/")[-1])
            actual_age = age - (year_obtained - year_posted)
            return str(actual_age)
        else:
            return "NA"

    def write_to_file(self):
        print self.text_by_user_at_age
        for id, age_texts in self.text_by_user_at_age.iteritems():
            gender = self.age_gender_map[id][1]
            if not gender:
                gender = "NA"
            for age, text in age_texts.iteritems():
                print age, text
                opfile = codecs.open(os.path.join(self.opdir, id+"_"+age+"_"+gender+".txt"), "w", "utf-8")
                opfile.write(text)
                opfile.close()

if __name__ == "__main__":
    age_gender_file = "/home/prasha/Dropbox/DailyStrength/id_age_gender.txt"
    xmldir = "/home/prasha/Documents/dailystrength/author_profiling_data/test/test_ip"
    opdir = "/home/prasha/Documents/dailystrength/author_profiling_data/test/test_op"
    CreateDataset(age_gender_file, xmldir, opdir)
    #cd.parse_xml("/mnt/docsig/storage/daily-strength/Acne/XML/9142628-need-some-input-please.html.xml")