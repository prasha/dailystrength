__author__ = 'shrprasha'

def check_if_float(elem):
    try:
        float(elem)
        return True
    except ValueError:
        return False

def check_line(line):
    correct = True
    label, idx_features = line.strip().split(" ", 1)
    if not str.isdigit(label):
        print "not digit: ", label
        correct = False
    prev_idx = 0
    for idx_feature in idx_features.split():
        idx, feature = idx_feature.split(":")
        idx = int(idx)
        if idx <= prev_idx:
            print "idx <= prev_idx", idx, prev_idx
            correct = False
        if not check_if_float(feature):
            print "not float", feature
            correct = False
        prev_idx = idx
    return correct

if __name__ == "__main__":
    fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/liblinear_files/stylistic_and_old_features/train/gender"
    # fpath = "/Users/shrprasha/del/test_feat"
    with open(fpath, "r") as fhandle:
        lines = fhandle.readlines()
    for i, line in enumerate(lines):
        print i+1
        if not check_line(line):
            # print "line: ", i + 1
            break


