from __future__ import division
import os
import utils.file_io_utils as futils
from user import User, Post
from utils.output_to_file import stdout_redirector

# will not work for dev and test because they have duplicates in them
# duplicates will be counted as different authors
# maybe ok for age but not for gender


__author__ = 'shrprasha'


#  all the forums this user has posted in
def get_forums_per_user(user_posts):
    return set(post.disease for post in user_posts)

# per disease stats from all posts of a user
# no need to do per age and gender because age and gender is the same per user;
# at least for train
# op (posts_count and word_count) format:
# {forum1: count, forum2: count, ...}
# @update_stats
def get_stats_per_user(user_posts):
    posts_count = {}
    words_count = {}
    for post in user_posts:
        if post.disease in posts_count:
            posts_count[post.disease] += 1
            words_count[post.disease] += post.number_of_words()
        else:
            posts_count[post.disease] = 1
            words_count[post.disease] = post.number_of_words()
    return posts_count, words_count


# def update_stats(posts_count, words_count, age_group, gender):
#     update_total_user_count_stat(posts_count.keys(), age_group, gender)
#     update_total_post_count_stat(posts_count, age_group, gender)
#     # update_posts_per_user_stat
#     update_length_per_post_stat(words_count, age_group, gender)
#
# def update_total_post_count_stat(posts_count, age_group, gender):
#     pass
#
#
# def update_length_per_post_stat(words_count, age_group, gender):
#     pass
#
#
# def update_total_user_count_stat(forums_per_user, age_group, gender):
#     for forum in forums_per_user:
#         if forum not in total_user_count_stat:
#             # init dict with zero values for age_group and gender
#             total_user_count_stat[forum] = dict(zip(age_groups_genders, [0] * len(age_groups_genders)))
#         total_user_count_stat[forum][age_group] += 1
#         total_user_count_stat[forum][gender] += 1

def update_age_and_gender(stat_dict, forum, age_group, gender, count):
    stat_dict[forum][age_group] += count
    stat_dict[forum][gender] += count

def update_stats(age_group, gender):
    for forum, post_count in posts_count.iteritems():
        word_count = words_count[forum]
        if forum not in total_user_count_stat:
            # init dict with zero values for age_group and gender
            total_user_count_stat[forum] = dict(zip(age_groups_genders, [0] * len(age_groups_genders)))
            total_post_count_stat[forum] = dict(zip(age_groups_genders, [0] * len(age_groups_genders)))
            words_per_post_stat[forum] = dict(zip(age_groups_genders, [0] * len(age_groups_genders)))
        update_age_and_gender(total_user_count_stat, forum, age_group, gender, 1)
        update_age_and_gender(total_post_count_stat, forum, age_group, gender, post_count)
        # only get total words for now, normalized by number of posts later
        update_age_and_gender(words_per_post_stat, forum, age_group, gender, word_count)

def get_posts_per_user_stat():
    for forum, age_gender_stats_dict in total_post_count_stat.iteritems():
        posts_per_user_stat[forum] = {}
        for agg, post_count in age_gender_stats_dict.iteritems():
            total_users = total_user_count_stat[forum][agg]
            posts_per_user_stat[forum][agg] = (post_count / total_users if total_users else 0)

def get_words_per_post_stat():
    for forum, age_gender_stats_dict in words_per_post_stat.iteritems():
        for agg, post_count in age_gender_stats_dict.iteritems():
            total_users = total_post_count_stat[forum][agg]
            if total_users:
                words_per_post_stat[forum][agg] /= total_users


#  {Acne:{"12-16": count, "19-28": count, "31-48": count, "51-63": count, "66-200": count, "female": count, "male": count}, ...}
def display_stats_totals(stats_dict):
    print  "Forum\t" + "\t".join(age_groups_genders) + "\tTotal"
    for forum in sorted(stats_dict):
        age_gender_stats_dict = stats_dict[forum]
        optext = forum
        for agg in age_groups_genders:
            count = age_gender_stats_dict[agg]
            optext += "\t" + str(count)
        total = age_gender_stats_dict["Female"] + age_gender_stats_dict["Male"]
        print optext + "\t" + str(total)

# created because totals are calculated by adding averaging
def display_stats_ratios(stats_dict, denom_totals_stats_dict):
    print  "Forum\t" + "\t".join(age_groups_genders) + "\tTotal"
    for forum in sorted(stats_dict):
        age_gender_stats_dict = stats_dict[forum]
        optext = forum
        for agg in age_groups_genders:
            count = age_gender_stats_dict[agg]
            optext += "\t" + str(count)
        total = (age_gender_stats_dict["Female"]*denom_totals_stats_dict[forum]["Female"]+
                 age_gender_stats_dict["Male"]*denom_totals_stats_dict[forum]["Male"]) / \
                (denom_totals_stats_dict[forum]["Female"] + denom_totals_stats_dict[forum]["Male"])
        print optext + "\t" + str(total)

## cannot take just training data when going from disease dumps
if __name__ == "__main__":
    users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps"
    user_posts_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps"

    opdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_analysis"

    train_users = futils.load_dumps(os.path.join(users_dumps_rootdir, "train.dump"))

    age_groups = ["12-16", "19-28", "31-48", "51-63", "66-200"]
    genders = ["Female", "Male"]
    age_groups_genders = age_groups + genders

    #  {Acne:{"12-16": count, "19-28": count, "31-48": count, "51-63": count, "66-200": count, "female": count, "male": count}, ...}
    total_user_count_stat = {}
    total_post_count_stat = {}
    words_per_post_stat = {}
    posts_per_user_stat = {}

    for fname in sorted(train_users):
        user = train_users[fname]
        print fname
        with open(os.path.join(user_posts_dump_rootdir, fname), "r") as fhandle:
            user_posts = futils.load(fhandle)
        # forums_per_user = get_forums_per_user(user_posts)
        posts_count, words_count = get_stats_per_user(user_posts)
        update_stats(user.age_group, user.gender)

    get_posts_per_user_stat()
    get_words_per_post_stat()

    with open(os.path.join(opdir, "total_user_count_stat.tsv"), "w") as fhandle:
        with stdout_redirector(fhandle):
            display_stats_totals(total_user_count_stat)

    with open(os.path.join(opdir, "total_post_count_stat.tsv"), "w") as fhandle:
        with stdout_redirector(fhandle):
            display_stats_totals(total_post_count_stat)

    with open(os.path.join(opdir, "words_per_post_stat.tsv"), "w") as fhandle:
        with stdout_redirector(fhandle):
            display_stats_ratios(words_per_post_stat, total_post_count_stat)

    with open(os.path.join(opdir, "posts_per_user_stat.tsv"), "w") as fhandle:
        with stdout_redirector(fhandle):
            display_stats_ratios(posts_per_user_stat, total_user_count_stat)





