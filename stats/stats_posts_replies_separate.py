from __future__ import division
import os
import utils.file_io_utils as futils
from user import User, Post
from utils.output_to_file import stdout_redirector


__author__ = 'shrprasha'


def update_age_and_gender(stat_dict, age_group, gender, count):
    stat_dict[age_group] += count
    stat_dict[gender] += count


def get_posts_per_user_stat(total_posts_or_replies_count_stat, posts_or_replies_per_user_stat):
    for agg, post_count  in total_posts_or_replies_count_stat.iteritems():
        total_users = total_user_count_stat[agg]
        posts_or_replies_per_user_stat[agg] = (post_count / total_users if total_users else 0)

def get_words_per_post_stat(words_per_post_or_reply_stat, total_post_or_reply_count_stat):
    for agg, post_count in words_per_post_or_reply_stat.iteritems():
        total_users = total_post_or_reply_count_stat[agg]
        if total_users:
            words_per_post_or_reply_stat[agg] /= total_users


# {"12-16": count, "19-28": count, "31-48": count, "51-63": count, "66-200": count, "female": count, "male": count}
def display_stats_totals(stats_dict):
    optext = ""
    for agg in age_groups_genders:
        count = stats_dict[agg]
        optext += "|" + str(count)
    total = stats_dict["Female"] + stats_dict["Male"]
    print optext + "|" + str(total)

# created because totals are calculated by adding averaging
def display_stats_ratios(stats_dict, denom_totals_stats_dict):
    optext = ""
    for agg in age_groups_genders:
        count = stats_dict[agg]
        optext += "|" + str(count)
    total = (stats_dict["Female"]*denom_totals_stats_dict["Female"] + stats_dict["Male"]*denom_totals_stats_dict["Male"]) / \
            (denom_totals_stats_dict["Female"] + denom_totals_stats_dict["Male"])
    print optext + "|" + str(total)


def get_no_of_posts_replies(user_posts):
    posts_count = replies_count = 0
    for post in user_posts:
        if post.problem_or_reply == "reply":
            replies_count += 1
        else:
            posts_count += 1
    return posts_count, replies_count

def get_posts_and_replies_separately(user_posts):
    posts = []
    replies = []
    for post in user_posts:
        if post.problem_or_reply == "reply":
            replies.append(post)
        else:
            posts.append(post)
    return posts, replies

if __name__ == "__main__":
    users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps"
    user_posts_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps"

    opdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_analysis"

    train_users = futils.load_dumps(os.path.join(users_dumps_rootdir, "train.dump"))

    age_groups = ["12-16", "19-28", "31-48", "51-63", "66-200"]
    genders = ["Female", "Male"]
    age_groups_genders = age_groups + genders

    total_user_count_stat = dict(zip(age_groups_genders, [0] * len(age_groups_genders)))
    total_post_count_stat = dict(zip(age_groups_genders, [0] * len(age_groups_genders)))
    words_per_post_stat = dict(zip(age_groups_genders, [0] * len(age_groups_genders)))
    posts_per_user_stat = dict(zip(age_groups_genders, [0] * len(age_groups_genders)))

    total_replies_count_stat = dict(zip(age_groups_genders, [0] * len(age_groups_genders)))
    words_per_replies_stat = dict(zip(age_groups_genders, [0] * len(age_groups_genders)))
    replies_per_user_stat = dict(zip(age_groups_genders, [0] * len(age_groups_genders)))

    for fname in sorted(train_users):
        user = train_users[fname]
        print fname
        with open(os.path.join(user_posts_dump_rootdir, fname), "r") as fhandle:
            user_posts = futils.load(fhandle)

        posts, replies = get_posts_and_replies_separately(user_posts)
        update_age_and_gender(total_user_count_stat, user.age_group, user.gender, 1)
        update_age_and_gender(total_post_count_stat, user.age_group, user.gender, len(posts))
        update_age_and_gender(total_replies_count_stat, user.age_group, user.gender, len(replies))
        # only get total words for now, normalized by number of posts later
        update_age_and_gender(words_per_post_stat, user.age_group, user.gender, sum([post.number_of_words() for post in posts]))
        update_age_and_gender(words_per_replies_stat, user.age_group, user.gender, sum([post.number_of_words() for post in replies]))

    get_posts_per_user_stat(total_post_count_stat, posts_per_user_stat)
    get_words_per_post_stat(words_per_post_stat, total_post_count_stat)
    get_posts_per_user_stat(total_replies_count_stat, replies_per_user_stat)
    get_words_per_post_stat(words_per_replies_stat, total_replies_count_stat)

    display_stats_totals(total_user_count_stat)

    display_stats_totals(total_post_count_stat)
    display_stats_totals(total_replies_count_stat)

    display_stats_ratios(words_per_post_stat, total_post_count_stat)
    display_stats_ratios(words_per_replies_stat, total_replies_count_stat)

    display_stats_ratios(posts_per_user_stat, total_user_count_stat)
    display_stats_ratios(replies_per_user_stat, total_user_count_stat)



