__author__ = 'shrprasha'

import os
from dill import load, dump
from features import Features
import sys

if __name__ == "__main__":
    # process_no = int(sys.argv[1])
    user_posts_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps"
    features_dumpdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps"
    # user_posts_dump_rootdir = "/project/solorio/prasha/ds/data/output/user_post_dumps"
    # features_dumpdir = "/project/solorio/prasha/ds/data/output/user_feature_dumps"
    all_files = sorted(next(os.walk(user_posts_dump_rootdir))[2])
    user_posts = []
    features = Features(features_dumpdir)
    for fname in all_files:
        fname = fname.strip()
        with open(os.path.join(user_posts_dump_rootdir, fname), "r") as fhandle:
            print fname
            user_posts = load(fhandle)
            features.dump_all_features(fname, user_posts)



