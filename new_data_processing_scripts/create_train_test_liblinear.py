__author__ = 'shrprasha'
from dill import load, dump
import os
from user import User
import sys

def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

def write_features_to_file(users, oprootdir):
    if not os.path.exists(oprootdir):
        os.makedirs(oprootdir)
    with open(os.path.join(oprootdir, "age"), "w") as age_fhandle, open(os.path.join(oprootdir, "gender"), "w") as gender_fhandle, open(os.path.join(oprootdir, "both"), "w") as both_fhandle, open(os.path.join(oprootdir, "y1984"), "w") as y1984_fhandle:
        for fname in sorted(users):
            user = users[fname]
            line = " "
            offset = 0
            for i, feature in enumerate(features):
                offset += offset_by[i]
                try:
                    idx_feature = load_dumps(os.path.join(feature_dumps_rootdir, feature, fname + ".dump"))
                    for idx in sorted(idx_feature):
                        value = idx_feature[idx]
                        line += str(idx + offset) + ":" + str(value) + " "
                except:
                    print "file not present: ", fname
            line += "\n"
            # no features
            if not ":" in line:
                continue
            age_fhandle.write(get_label_age(user.age_group, user.gender) + line)
            gender_fhandle.write(get_label_gender(user.age_group, user.gender) + line)
            both_fhandle.write(get_label_both(user.age_group, user.gender) + line)
            y1984_fhandle.write(get_label_1984(user.age_group, user.gender, user.uid) + line)

def write_features_to_file_del(users, oprootdir):
    # features_idx = ["avg_char_per_word", "avg_syl_per_word", "avg_word_per_sent", "punctuations", "emoticons", "abbreviations", "stopword_freq", "average_word_frequency", "flesch_kincaid", "gunning_fox", "pos_emo", "neg_emo"]
    features_idx = ['abbreviations', 'average_word_frequency', 'avg_char_per_word', 'avg_syl_per_word', 'avg_word_per_sent', 'emoticons', 'flesch_kincaid', 'gunning_fox', 'neg_emo', 'pos_emo', 'punctuations', 'stopword_freq']
    if not os.path.exists(oprootdir):
        os.makedirs(oprootdir)
    with open(os.path.join(oprootdir, "age"), "w") as age_fhandle, open(os.path.join(oprootdir, "gender"), "w") as gender_fhandle, open(os.path.join(oprootdir, "both"), "w") as both_fhandle:
        for fname in sorted(users):
            user = users[fname]
            line = " "
            for i, feature in enumerate(sorted(features)):
                offset = offset_by[i]
                idx_feature = load_dumps(os.path.join(feature_dumps_rootdir, feature, fname + ".dump"))
                # print "create_train_test1:", len(idx_feature), idx_feature
                for idx in sorted(idx_feature):
                    value = idx_feature[idx]

                    idx1 = features_idx.index(idx) + 1
                    line += str(idx1 + offset) + ":" + str(value) + " "
                    # line += str_idx + ":" + val + " "
            line += "\n"
            if not ":" in line:
                continue
            age_fhandle.write(get_label_age(user.age_group, user.gender) + line)
            gender_fhandle.write(get_label_gender(user.age_group, user.gender) + line)
            both_fhandle.write(get_label_both(user.age_group, user.gender) + line)

def update_stylistic_feature_dump_to_idx(fnames):
    features_idx = ['abbreviations', 'average_word_frequency', 'avg_char_per_word', 'avg_syl_per_word', 'avg_word_per_sent', 'emoticons', 'flesch_kincaid', 'gunning_fox', 'neg_emo', 'pos_emo', 'punctuations', 'stopword_freq']
    oprootdir = os.path.join(feature_dumps_rootdir, "stylistic_features")
    if not os.path.exists(oprootdir):
        os.makedirs(oprootdir)
    for fname in sorted(fnames):
        feature_name_features = load_dumps(os.path.join(feature_dumps_rootdir, "stylistic_with_feature_names", fname))
        idx_features = {}
        for feature_name, feature_val in feature_name_features.iteritems():
            idx_features[features_idx.index(feature_name)+1] = feature_val
        with open(os.path.join(oprootdir, fname), "w") as opfhandle:
            dump(idx_features, opfhandle)



# def get_label_age(age, gender):
#     labels = ["12-16", "19-28", "31-48", "51-63", "66-200"]
#     return str(labels.index(age))
# def get_label_gender(age, gender):
#     gender = gender.lower()
#     labels = ["female", "male"]
#     return str(labels.index(gender))
# def get_label_both(age, gender):
#     gender = gender.lower()
#     labels = ["12-16_female", "19-28_female", "31-48_female", "51-63_female", "66-200_female", "12-16_male", "19-28_male", "31-48_male", "51-63_male", "66-200_male"]
#     return str(labels.index(age + "_" + gender))
# def get_label_1984(age, gender, uid):
#     if uid in duplicates_for_1984:
#         age = duplicates_for_1984[uid]
#     labels = ["12-16", "19-28", "31-48", "51-63", "66-200"]
#     age_label = labels.index(age)
#     if age_label < 2:
#         return "1"
#     else:
#         return "-1"

def get_label_age(age, gender):
    labels = ["12-17", "18-29", "30-49", "50-64", "65-xx"]
    return str(labels.index(age))
def get_label_gender(age, gender):
    gender = gender.lower()
    labels = ["female", "male"]
    return str(labels.index(gender))
def get_label_both(age, gender):
    gender = gender.lower()
    labels = ["12-17_female", "18-29_female", "30-49_female", "50-64_female", "65-xx_female", "12-17_male", "18-29_male", "30-49_male", "50-64_male", "65-xx_male"]
    return str(labels.index(age + "_" + gender))
def get_label_1984(age, gender, uid):
    if uid in duplicates_for_1984:
        age = duplicates_for_1984[uid]
    labels = ["12-17", "18-29", "30-49", "50-64", "65-xx"]
    age_label = labels.index(age)
    if age_label < 2:
        return "1"
    else:
        return "-1"

def get_files_in_folder(folder):
    all_files = sorted(next(os.walk(folder))[2])
    if ".DS_Store" in all_files:
        all_files.remove(".DS_Store")
    return all_files

def create_opdir(opdir):
    if not os.path.exists(opdir):
        os.makedirs(opdir)

def process_for_1984(dev_users):
    seen_uids = {}  # {seen_uid: seen_age_group}
    duplicates = {}  #  {duplicate_uid: higher_age_group}
    for fname in dev_users:
        user = dev_users[fname]
        if user.uid in seen_uids:
            print user.uid, seen_uids[user.uid], user.age_group
            if user.age_group > seen_uids[user.uid]:
                duplicates[user.uid] = user.age_group
            else:
                duplicates[user.uid] = seen_uids[user.uid]
        else:
            seen_uids[user.uid] = user.age_group
    return duplicates


if __name__ == "__main__":
    ## here
    # users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps"
    # feature_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps"
    # #opdir
    # # liblinear_files_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/liblinear_files/stylistic_and_old_features"
    # # liblinear_files_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/liblinear_files/character_ngram_features"
    # # liblinear_files_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/liblinear_files/word_ngram_features_tf_idf"
    # liblinear_files_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/liblinear_files/familial_features_all"
    # # liblinear_files_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/liblinear_files/username_partition_features"
    # # liblinear_files_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/liblinear_files/char_ngram_features_tf_idf"

    ## cluster
    # users_dumps_rootdir = "/project/solorio/prasha/ds/processed_data/users_dumps"
    # feature_dumps_rootdir = "/project/solorio/prasha/ds/processed_data/user_feature_dumps"
    # #opdir
    # liblinear_files_rootdir = "/project/solorio/prasha/ds/processed_data/liblinear_files/character_ngram_features"
    # # liblinear_files_rootdir = "/project/solorio/prasha/ds/processed_data/liblinear_files/stylistic_and_old_features_tfidf"

    ## continuous
    users_dumps_rootdir = "/project/solorio/prasha/ds/processed_data/user_dumps_old_all"
    feature_dumps_rootdir = "/project/solorio/prasha/ds/continous_age_groups/user_feature_dumps_cont"
    #opdir
    liblinear_files_rootdir = "/project/solorio/prasha/ds/continous_age_groups/liblinear_files/word_ngram_features_tf_idf"
    # liblinear_files_rootdir = "/project/solorio/prasha/ds/continous_age_groups/liblinear_files/familial_features_all"


    create_opdir(liblinear_files_rootdir)

    # features = ["char_ngram_features"]
    features = ["word_ngram_features_tf_idf"]
    # features = ["familial_features"]
    # features = ["username_partition_features"]
    # features = ["char_ngram_features_tf_idf"]
    offset_by = [0]
    # features = ["diseases_features", "familial_features", "stylistic_features", "word_ngram_features_tf_idf"]
    # offset_by = [0, 104*2, 36, 12]

    #format: {userid_age-group: user (object), ... }  # userid_age-group=fname
    train_users = load_dumps(os.path.join(users_dumps_rootdir, "train.dump"))
    more_train_users = load_dumps(os.path.join(users_dumps_rootdir, "dev.dump"))
    train_users.update(more_train_users)
    # actually test
    dev_users = load_dumps(os.path.join(users_dumps_rootdir, "test.dump"))

    print len(train_users)
    print len(dev_users)

    duplicates_for_1984 = process_for_1984(dev_users)
    write_features_to_file(train_users, os.path.join(liblinear_files_rootdir, "train"))
    write_features_to_file(dev_users, os.path.join(liblinear_files_rootdir, "test"))


    # write_features_to_file(test_users, os.path.join(liblinear_files_rootdir, "test"))

# if __name__ == "__main__":
#     # labels = ["12-16", "19-28", "31-48", "51-63", "66-200"]
#     # # for age in labels:
#     # #     print age, get_label_1984(age, "female")
#     #
#     # for a1 in labels:
#     #     for a2 in labels:
#     #         print a1, a2, a1 < a2
#     #     print
#
#     users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps"
#     dev_users = load_dumps(os.path.join(users_dumps_rootdir, "dev.dump"))
#     duplicates_for_1984 = process_for_1984(dev_users)
#     print duplicates_for_1984
#     print len(duplicates_for_1984)

# if __name__ == "__main__":
#     feature_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps"
#     update_stylistic_feature_dump_to_idx(get_files_in_folder(os.path.join(feature_dumps_rootdir, "stylistic_with_feature_names")))



# if __name__ == "__main__":
    ###TEMP
    # users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps/subset_for_plots"
    # liblinear_files_rootdir = sys.argv[1]

    # users_dumps_rootdir = "/project/solorio/prasha/ds/data/output/users_dumps"
    # feature_dumps_rootdir = "/project/solorio/prasha/ds/data/output/user_feature_dumps"
    # liblinear_files_rootdir = "/project/solorio/prasha/ds/output/liblinear_files"

    # features = ["stylistic_features"]
    # offset_by = [0]
    # features = ["diseases_features", "familial_features", "word_ngram_features"]
    # offset_by = [0, 12, 48]
    ###TEMP
    # write_features_to_file_del(train_users, os.path.join(liblinear_files_rootdir, "train"))
    # write_features_to_file_del(dev_users, os.path.join(liblinear_files_rootdir, "dev"))
    # write_features_to_file_del(dev_users, os.path.join(liblinear_files_rootdir, "test"))






