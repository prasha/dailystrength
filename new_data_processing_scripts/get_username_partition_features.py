__author__ = 'shrprasha'

import os
from dill import load, dump
from features import UsernameFeature
import sys

def get_files_in_folder(folder):
    all_files = sorted(next(os.walk(folder))[2])
    if ".DS_Store" in all_files:
        all_files.remove(".DS_Store")
    return all_files

def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

def create_opdir(opdir):
    if not os.path.exists(opdir):
        os.makedirs(opdir)

def get_features(fname, user):
    fname = fname.strip()
    features.dump_all_features(fname, user)

if __name__ == "__main__":
    user_posts_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps"
    users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps"
    features_dumpdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/username_partition_features"
    feature_name_idx_list_path = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/username_features_idx.dump"

    create_opdir(features_dumpdir)

    ##  SEE: did not do for test right now
    train_users = load_dumps(os.path.join(users_dumps_rootdir, "train.dump"))
    dev_users = load_dumps(os.path.join(users_dumps_rootdir, "dev.dump"))

    user_posts = []
    features = UsernameFeature(features_dumpdir)
    for fname in sorted(train_users):  # usernames
        get_features(fname, train_users[fname])
    features.dump_feature_name_idx_list(feature_name_idx_list_path)
    features.mode = "dev"
    for fname in sorted(dev_users):  # usernames
        get_features(fname, dev_users[fname])



