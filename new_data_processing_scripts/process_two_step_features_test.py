__author__ = 'shrprasha'
import os
from dill import load, dump


def update_features(features_counts):
    user_posts_features = {}
    for feature, count in features_counts.iteritems():
        # if count < 2:
        #     continue
        if feature in all_features_list:
            idx = all_features_list[feature]  # idx of feature in linguistic features
            user_posts_features[idx] = count
    return user_posts_features

def dump_features(dirpath, fname, features):
    print fname
    with open(os.path.join(dirpath, fname), "w") as opfhandle:
        dump(features, opfhandle)

def create_dumps(obj, fpath):
    with open(fpath, "w") as fhandle:
        dump(obj, fhandle)

def process():
    opath = os.path.join(op_rootdir, feature_type)
    create_opdir(opath)
    features_dir = os.path.join(needs_processing_rootdir, feature_type)
    all_files = sorted(next(os.walk(features_dir))[2])
    print len(all_files)
    # all_features_list = {}
    for fname in all_files:
        opfpath = os.path.join(opath, fname)
        if os.path.isfile(opfpath):
            # print fname
            continue
        fname = fname.strip()
        with open(os.path.join(features_dir, fname), "r") as fhandle:
            word_ngram_features = load(fhandle)
        idx_features = update_features(word_ngram_features)
        dump_features(opath, fname, idx_features)
    create_dumps(all_features_list, os.path.join(op_rootdir, feature_type + "_idx.dump"))

def create_opdir(opdir):
    if not os.path.exists(opdir):
        os.makedirs(opdir)

def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

if __name__ == "__main__":
    # feature_type = "word_ngram_features"
    feature_type = "word_ngram_features_tf_idf"
    # needs_processing_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/needs_further_processing"
    # op_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps"

    needs_processing_rootdir = "/project/solorio/prasha/ds/processed_data/user_feature_dumps/needs_further_processing"
    op_rootdir = "/project/solorio/prasha/ds/processed_data/user_feature_dumps"
    all_features_list = load_dumps(os.path.join(op_rootdir, feature_type + "_idx.dump"))
    process()


