__author__ = 'shrprasha'

import os
from dill import load, dump
from features import StylisticFeatures
import sys

if __name__ == "__main__":
    # user_posts_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps"
    # features_dumpdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps"
    user_posts_dump_rootdir = "/project/solorio/prasha/ds/data/output/user_post_dumps"
    features_dumpdir = "/project/solorio/prasha/ds/data/output/user_feature_dumps"
    all_files = sorted(next(os.walk(user_posts_dump_rootdir))[2])
    user_posts = []
    for fname in all_files:
        fname = fname.strip()
        features = StylisticFeatures(features_dumpdir)
        # features = Features(features_dumpdir)
        with open(os.path.join(user_posts_dump_rootdir, fname), "r") as fhandle:
            user_posts = load(fhandle)
            features.dump_all_features(fname, user_posts)



    # all_user_posts = {}  # (uid, age_group): post_list
    # for post in all_posts:
    #     uid = post.uid
    #     if (uid, age_group) in all_user_posts:
    #         all_user_posts[(uid, age_group)].append(post)
    #     else:
    #         all_user_posts[(uid, age_group)] = [post]
    #
    # for uid_age_group, post_list in all_user_posts:
