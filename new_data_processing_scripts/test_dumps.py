__author__ = 'shrprasha'


from dill import load
import sys

def check_dump(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    print obj

if __name__ == "__main__":
    fpath = sys.argv[1]
    check_dump(fpath)