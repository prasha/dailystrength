__author__ = 'shrprasha'

from dill import load, dump
from collections import Counter
import os, random

def get_all_users(dump_fpath):
    with open(dump_fpath, "r") as dump_fhandle:
        all_users = load(dump_fhandle)
    return all_users

def get_single_duplicates(all_users):
    uids = []
    for fname in all_users:
        uid, age_group = fname.strip().split("_")
        uids.append(uid)
    uid_ctr = Counter(uids)
    uids_has_duplicates = [k for k, v in uid_ctr.iteritems() if v > 1]
    uids_single = set(uids) - set(uids_has_duplicates)
    return list(uids_single), uids_has_duplicates

def get_partitions(uids_single, uids_has_duplicates):
    train_users = {}
    dev_users = {}
    test_users = {}

    #  move_duplicates_to_dev_and_test
    random.shuffle(uids_has_duplicates)
    dev_uids_duplicate = uids_has_duplicates[0:len(uids_has_duplicates)/2]
    test_uids_duplicate = uids_has_duplicates[len(uids_has_duplicates)/2:]
    move_to_map(dev_uids_duplicate, dev_users)
    move_to_map(test_uids_duplicate, test_users)

    random.shuffle(uids_single)
    total = len(uids_single) + 2*len(uids_has_duplicates)
    dev_test_share_single = 20 * total / 100 - len(uids_has_duplicates)
    train_share = len(uids_single) - 2 * dev_test_share_single
    move_to_map(uids_single[0:train_share], train_users)
    if dev_test_share_single > 0:
        move_to_map(uids_single[train_share:train_share+dev_test_share_single], dev_users)
        move_to_map(uids_single[train_share+dev_test_share_single:], test_users)

    return train_users, dev_users, test_users


def move_to_map(uid_list, user_map):
    for fname, user in all_users.iteritems():
        uid, age_group = fname.strip().split("_")
        if uid in uid_list:
            user_map[fname] = user

#############################################################################
def get_single_duplicates_with_age_group(all_users):
    uids = []
    uids_ages = {}
    for fname in all_users:
        uid, age_group = fname.strip().split("_")
        uids.append(uid)
        uids_ages[uid] = age_group
    uid_ctr = Counter(uids)
    uids_has_duplicates = [k for k, v in uid_ctr.iteritems() if v > 1]
    uids_single = set(uids) - set(uids_has_duplicates)
    return list(uids_single), uids_has_duplicates, uids_ages

def get_files_in_folder(folder):
    all_files = sorted(next(os.walk(folder))[2])
    if ".DS_Store" in all_files:
        all_files.remove(".DS_Store")
    return all_files

def get_all_users_having_features():
    features_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/stylistic_features"
    all_files = get_files_in_folder(features_rootdir)
    print len(all_files)
    uids, duplicates, uids_ages = get_single_duplicates_with_age_group(all_files)
    print "duplicate_count:", len(duplicates)
    return uids, uids_ages

def get_user_age_groups_distribution():
    age_group_counter = {"12-16": 0, "19-28": 0, "31-48": 0, "51-63": 0, "66-200": 0}
    for uid in small_subset_uids:
        age_group = uids_ages[uid]
        age_group_counter[age_group] += 1
    print age_group_counter
    min_count = min(age_group_counter.values())
    return min_count

def get_user_age_groups_all_uids():
    age_group_counter = {"12-16": [], "19-28": [], "31-48": [], "51-63": [], "66-200": []}
    for uid in small_subset_uids:
        age_group = uids_ages[uid]
        age_group_counter[age_group].append(uid)
    return age_group_counter

def get_balanced_dataset(no_of_users_in_each_group):
    age_group_uids = get_user_age_groups_all_uids()
    # print age_group_uids
    train_count = 80 * no_of_users_in_each_group / 100
    all_train_uids = []
    all_dev_uids = []
    for age_group, uids in age_group_uids.iteritems():
        train_uids = uids[0:train_count]
        dev_uids = uids[train_count:no_of_users_in_each_group]
        all_train_uids.extend(train_uids)
        all_dev_uids.extend(dev_uids)
        print age_group, len(uids), len(all_dev_uids), len(all_dev_uids)

    print "uids length:", len(all_train_uids), len(all_dev_uids)
    train_users = {}
    dev_users = {}
    move_to_map1(all_train_uids, train_users)
    move_to_map1(all_dev_uids, dev_users)
    print len(train_users), len(dev_users)
    return train_users, dev_users

def move_to_map1(uid_list1, user_map):
    # print len(all_users)
    # print uid_list1
    for fname, user in all_users.iteritems():
        # print fname
        # break
        uid, age_group = fname.strip().split("_")
        if uid in uid_list1:
            user_map[fname] = user

if __name__ == "__main__":
    users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps/subset_for_plots"
    all_users_dump_fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps/train.dump"
    all_users = get_all_users(all_users_dump_fpath)
    print "train_users:", len(all_users)#, train_users

    features_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/stylistic_features"
    all_feature_files = get_files_in_folder(features_rootdir)
    print "has features:", len(all_feature_files)

    # print all_users.keys()
    # print all_feature_files

    all_feature_uids = [x.split(".")[0] for x in all_feature_files]
    small_subset_uid_ag = list(set(all_users.keys()).intersection(set(all_feature_uids)))
    print "train has feat:", len(small_subset_uid_ag)#, small_subset_users

    small_subset_uids, duplicates, uids_ages = get_single_duplicates_with_age_group(small_subset_uid_ag)
    print "duplicate_count, must be 0:", len(duplicates)

    train_users, dev_users = get_balanced_dataset(370)


    # uids_single, uids_has_duplicates = get_single_duplicates(all_users)
#     train_users, dev_users, test_users = get_partitions()
#
# #  7714 4630 1542 1542 7714
#     print len(all_users), len(train_users), len(dev_users), len(test_users), len(train_users) + len(dev_users) + len(test_users)
#     if len(all_users) != len(train_users) + len(dev_users) + len(test_users):
#         print "ERRRORR"
#
    with open(os.path.join(users_dumps_rootdir, "train.dump"), "w") as opfhandle:
        dump(train_users, opfhandle)

    with open(os.path.join(users_dumps_rootdir, "dev.dump"), "w") as opfhandle:
        dump(dev_users, opfhandle)
#
#     with open(os.path.join(users_dumps_rootdir, "test.dump"), "w") as opfhandle:
#         dump(test_users, opfhandle)

###TEMP###############################################################################


# if __name__ == "__main__":
#     # users_dumps_rootdir = "/project/solorio/prasha/ds/processed_data/users_dumps"
#     users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps"
#     all_users_dump_fpath = os.path.join(users_dumps_rootdir, "all_users.dump")
#     all_users = get_all_users(all_users_dump_fpath)
#
#     # uids_single, uids_has_duplicates = get_single_duplicates(all_users)
#     train_users, dev_users, test_users = get_partitions(*get_single_duplicates(all_users))
#
# #  7714 4630 1542 1542 7714
#     print len(all_users), len(train_users), len(dev_users), len(test_users), len(train_users) + len(dev_users) + len(test_users)
#     if len(all_users) != len(train_users) + len(dev_users) + len(test_users):
#         print "ERRRORR"
#
#     with open(os.path.join(users_dumps_rootdir, "train.dump"), "w") as opfhandle:
#         dump(train_users, opfhandle)
#
#     with open(os.path.join(users_dumps_rootdir, "dev.dump"), "w") as opfhandle:
#         dump(dev_users, opfhandle)
#
#     with open(os.path.join(users_dumps_rootdir, "test.dump"), "w") as opfhandle:
#         dump(test_users, opfhandle)
#
