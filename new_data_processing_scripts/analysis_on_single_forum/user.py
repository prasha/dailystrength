__author__ = 'shrprasha'
from collections import Counter

class User:

    def __init__(self, uid, uname, gender, age_group):
        self.uid = uid
        self.uname = uname
        self.gender = gender
        self.age_group = age_group

    def __eq__(self, other):
        return self.uid == other.uid and self.age_group == other.age_group


class Post:

    def __init__(self, text, problem_or_reply, disease, age_when_posted, token_tags, uid, postid):
        self.text = text
        self.problem_or_reply = problem_or_reply
        self.disease = disease
        self.age_when_posted = age_when_posted
        self.token_tags = token_tags
        self.uid = uid
        self.postid = postid

    def get_age_group(self):
        age_groups = [(12, 16), (19, 28), (31, 48), (51, 63), (66, 200)]
        for min, max in age_groups:
            if(min <= self.age_when_posted <= max):
                if(min == 65):
                    return "65-xx"
                else:
                    return str(min) + "-" + str(max)
        return None # for borderline ages









