__author__ = 'shrprasha'
import os
from dill import load
from user import User, Post
from linguistic import Linguistic
import utils
import matplotlib.pyplot as plt
import numpy as np

def create_opdir(opdir):
    if not os.path.exists(opdir):
        os.makedirs(opdir)

def get_average(d1):
    no_of_posts = d1["no_of_posts"]
    for feature, age_totals in d1.iteritems():
        if feature == "no_of_posts":
            continue
        for age in age_totals:
            # print age_totals[age],
            age_totals[age] /= (1.0 * no_of_posts[age])
            # print no_of_posts[age],  d1[feature][age]

def plot(d1):
    for feature, age_avgs in d1.iteritems():
        if feature == "no_of_posts":
            continue

def display(d1):
    lines = {}
    print "age", "\t",
    l1 = d1.keys()
    l1.remove("no_of_posts")
    for feature in ["no_of_posts"] + sorted(l1):
        age_avgs = d1[feature]
        print feature, "\t",
        for age, avg in age_avgs.iteritems():
            if age in lines:
                lines[age].append(avg)
            else:
                lines[age] = [avg]
    print
    for age in sorted(lines):
        print age, "\t", '\t'.join(map(str, lines[age]))

def print_obj(obj):
    attrs = vars(obj)
    print ', '.join("%s: %s" % item for item in attrs.items())

def create_all_plots(d1, features, plots_opdir):
    # aggr_stylistic_features = get_average(d1, age_group_counts)
    for feature in features:
        age_group_values_dict = d1[feature]
        means = {}  # {"12-17": 0.23, "19-28": 0.98,....}
        sds = {}
        for age_group in sorted(age_group_values_dict):
            values_list = age_group_values_dict[age_group]
            means[age_group] = np.mean(values_list)
            # print means[age_group], sum(values_list) / len(values_list)
            sds[age_group] = np.std(values_list)
            # sds[age_group] = sd(values_list)
            # print np.std(values_list), sd(values_list)
        print feature, "averages: ", means
        create_plot(feature, means, sds, plots_opdir)

def create_plot(feature_name, means, sds, rootdir):
    fig = plt.figure()
    colors = list("rgbcmyk")
    markers = ["8", "^", "D", "h", "*", "s", ">", "o"]

    x_labels = sorted(means.keys())
    x_labels.pop()
    x_labels.append("65-xx")
    plt.xticks(range(len(means)), x_labels, size='small')
    y = [means[k] for k in sorted(means.keys())]
    e = [sds[k] for k in sorted(means.keys())]
    plt.scatter(range(len(means)), y, marker="o", color="k")
    # plt.ylim(0, max(means.values()) + max(sds.values()) + 0.5)

    plt.errorbar(range(len(means)), y, e, linestyle='None', marker="o", color="k")
    # plt.errorbar(range(len(d1)), y, xerr=0.2, yerr=0.4)
    # plt.errorbar(range(len(d1)), y, fmt='o')
    # plt.errorbar(range(len(means)), y, [1,2,3,4, 5], linestyle='None', marker=markers.pop())
    plt.suptitle(feature_name)
    # plt.show()
    fig.savefig(os.path.join(rootdir, feature_name), dpi=fig.dpi)
    # plt.close()

def create_plot_single_disease(disease):
    fname = disease + "_posts.dump"
    print fname
    plots_opdir = os.path.join(plots_rootopdir, disease)
    with open(os.path.join(disease_dump_rootdir, fname), "r") as fhandle:
        posts = load(fhandle)

    stylistic_by_age = {"no_of_posts": {}}  #{"avg_char_per_word": {18: 0.123, 19: 0.12, ...}, ...}

    # i = 0
    for post in posts:
        age_group = post.get_age_group()
        if not age_group:
            continue

        # stylistic_features = linguistic.get_stylistic_features(*utils.get_token_tags(post.text))
        stylistic_features = linguistic.get_stylistic_features(post.text, post.token_tags)
        if not stylistic_features:
            continue

        if age_group in stylistic_by_age["no_of_posts"]:
            for feature in stylistic_features:
                stylistic_by_age[feature][age_group].append(stylistic_features[feature])
            stylistic_by_age["no_of_posts"][age_group] += 1
        else:
            for feature in stylistic_features:
                if feature not in stylistic_by_age:
                    stylistic_by_age[feature] = {}
                stylistic_by_age[feature][age_group] = [stylistic_features[feature]]
            stylistic_by_age["no_of_posts"][age_group] = 1
    features = ['abbreviations', 'average_word_frequency', 'avg_char_per_word', 'avg_syl_per_word', 'avg_word_per_sent', 'emoticons', 'flesch_kincaid', 'gunning_fox', 'neg_emo', 'pos_emo', 'punctuations', 'stopword_freq']
    # features = ['abbreviations', 'average_word_frequency']
    create_opdir(plots_opdir)
    create_all_plots(stylistic_by_age, features, plots_opdir)


# ## plot mean, sd per disease, age_groups in x-axis
if __name__ == "__main__":
    disease_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/disease_post_dumps/"
    # disease = "Fatty-Acid-Oxidation-Disorders"
    # diseases = ["Fatty-Acid-Oxidation-Disorders", "Color-Blindness"]
    diseases = ["ADHD-ADD", "Acne", "Diabetes-Type-1", "Insomnia", "Fatty-Liver-Disease"]
    linguistic = Linguistic()
    plots_rootopdir = "/Users/shrprasha/Projects/resources/dailystrength/stats/stylistic_plots_per_disease"
    for disease in diseases:
        create_plot_single_disease(disease)

## plot mean, sd per disease, age in x-axis
# if __name__ == "__main__":
#     disease_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/disease_post_dumps/"
#     # disease = "ADHD-ADD"
#     # disease = "Acne"
#     # disease = "Diabetes-Type-1"
#     # disease = "Insomnia"
#     # disease = "Fatty-Acid-Oxidation-Disorders"
#     disease = "Fructose-Intolerance"
#     fname = disease + "_posts.dump"
#     with open(os.path.join(disease_dump_rootdir, fname), "r") as fhandle:
#         posts = load(fhandle)
#
#     linguistic = Linguistic()
#     stylistic_by_age = {"no_of_posts": {}}  #{"avg_char_per_word": {18: 0.123, 19: 0.12, ...}, ...}
#
#     # i = 0
#     for post in posts:
#         print_obj(post)
#         # stylistic_features = linguistic.get_stylistic_features(*utils.get_token_tags(post.text))
#         stylistic_features = linguistic.get_stylistic_features(post.text, post.token_tags)
#         if post.age_when_posted < 10 or post.age_when_posted > 80 or not stylistic_features:
#             continue
#         if post.age_when_posted in stylistic_by_age["no_of_posts"]:
#             for feature in stylistic_features:
#                 stylistic_by_age[feature][post.age_when_posted].append(stylistic_features[feature])
#             stylistic_by_age["no_of_posts"][post.age_when_posted] += 1
#         else:
#             for feature in stylistic_features:
#                 if feature not in stylistic_by_age:
#                     stylistic_by_age[feature] = {}
#                 stylistic_by_age[feature][post.age_when_posted] = [stylistic_features[feature]]
#             stylistic_by_age["no_of_posts"][post.age_when_posted] = 1
#         # if i == 20:
#         #     break
#         # i += 1
#     # print stylistic_by_age
#     print stylistic_by_age["punctuations"]
#     # get_average(stylistic_by_age)
#     # print stylistic_by_age
#     print stylistic_by_age["punctuations"]
#     print stylistic_by_age["no_of_posts"]
#     display(stylistic_by_age)
#     features = ['abbreviations', 'average_word_frequency', 'avg_char_per_word', 'avg_syl_per_word', 'avg_word_per_sent', 'emoticons', 'flesch_kincaid', 'gunning_fox', 'neg_emo', 'pos_emo', 'punctuations', 'stopword_freq']
#     create_all_plots(stylistic_by_age, features)

## print average value of stylistic features; no plots
# if __name__ == "__main__":
#     disease_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/disease_post_dumps/"
#     # disease = "ADHD-ADD"
#     # disease = "Acne"
#     # disease = "Diabetes-Type-1"
#     # disease = "Insomnia"
#     # disease = "Fatty-Acid-Oxidation-Disorders"
#     disease = "Fatty-Liver-Disease"
#     fname = disease + "_posts.dump"
#     linguistic = Linguistic()
#     with open(os.path.join(disease_dump_rootdir, fname), "r") as fhandle:
#         posts = load(fhandle)
#
#     stylistic_by_age = {"no_of_posts": {}}  #{"avg_char_per_word": {18: 0.123, 19: 0.12, ...}, ...}
#
#     # i = 0
#     for post in posts:
#         # print_obj(post)
#         # stylistic_features = linguistic.get_stylistic_features(*utils.get_token_tags(post.text))
#         stylistic_features = linguistic.get_stylistic_features(post.text, post.token_tags)
#         if post.age_when_posted < 10 or post.age_when_posted > 80 or not stylistic_features:
#             continue
#         if post.age_when_posted in stylistic_by_age["no_of_posts"]:
#             for feature in stylistic_features:
#                 stylistic_by_age[feature][post.age_when_posted] += stylistic_features[feature]
#             stylistic_by_age["no_of_posts"][post.age_when_posted] += 1
#         else:
#             for feature in stylistic_features:
#                 if feature not in stylistic_by_age:
#                     stylistic_by_age[feature] = {}
#                 stylistic_by_age[feature][post.age_when_posted] = stylistic_features[feature]
#             stylistic_by_age["no_of_posts"][post.age_when_posted] = 1
#         # if i == 20:
#         #     break
#         # i += 1
#     # print stylistic_by_age
#     print stylistic_by_age["punctuations"]
#     get_average(stylistic_by_age)
#     # print stylistic_by_age
#     print stylistic_by_age["punctuations"]
#     print stylistic_by_age["no_of_posts"]
#     display(stylistic_by_age)
# ##     features = ['abbreviations', 'average_word_frequency', 'avg_char_per_word', 'avg_syl_per_word', 'avg_word_per_sent', 'emoticons', 'flesch_kincaid', 'gunning_fox', 'neg_emo', 'pos_emo', 'punctuations', 'stopword_freq']




