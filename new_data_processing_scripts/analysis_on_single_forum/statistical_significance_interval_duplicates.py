__author__ = 'shrprasha'

import os
from dill import load
from collections import Counter
from scipy import stats


def get_files_in_folder(folder):
    all_files = sorted(next(os.walk(folder))[2])
    if ".DS_Store" in all_files:
        all_files.remove(".DS_Store")
    return all_files


def get_duplicates(all_feature_files):
    duplicates = {}
    uids_ages = {}
    for fname in all_feature_files:
        uid, age_group = fname.strip().split("_")
        age_group = age_group.split(".")[0]
        if uid not in uids_ages:
            uids_ages[uid] = [age_group]
        else:
            uids_ages[uid].append(age_group)
    # print uids_ages
    for uid, ages in uids_ages.iteritems():
        if len(ages) > 1:
            duplicates[uid] = ages
    return duplicates

def flit_dict(d1):
    flipped = {}

    for key, v in d1.items():
        value = tuple(v)
        if value not in flipped:
            flipped[value] = [key]
        else:
            flipped[value].append(key)
    return flipped


def get_feature_val(uid, age_group, feature):
    with open(os.path.join(feature_dumps_rootdir, uid + "_" + age_group + ".dump"), "r") as fhandle:
        feature_vals = load(fhandle)
    return feature_vals[feature] if feature in feature_vals else 0.0

if __name__ == "__main__":
    feature_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/stylistic_features"
    all_feature_files = get_files_in_folder(feature_dumps_rootdir)
    uids_ages = get_duplicates(all_feature_files)
    ages_uids = flit_dict(uids_ages)
    age_groups = ["12-16", "19-28", "31-48", "51-63", "66-200"]
    features = ['abbreviations', 'average_word_frequency', 'avg_char_per_word', 'avg_syl_per_word', 'avg_word_per_sent', 'emoticons', 'flesch_kincaid', 'gunning_fox', 'neg_emo', 'pos_emo', 'punctuations', 'stopword_freq']

    for age_groups, uids in ages_uids.iteritems():
        for feature in features:
            lower_age_group_feats = []
            upper_age_group_feats = []
            for uid in uids:
                lower_age_group_feats.append(get_feature_val(uid, age_groups[0], feature))
                upper_age_group_feats.append(get_feature_val(uid, age_groups[1], feature))
            # print lower_age_group_feats
            # print upper_age_group_feats
            all = stats.stats.ttest_ind(lower_age_group_feats, upper_age_group_feats, equal_var=False)
            print age_groups, feature, all
