# -*- coding: utf-8 -*-

__author__ = 'shrprasha'

import re
from collections import Counter
from nltk.stem.porter import PorterStemmer
from nltk_contrib.readability.textanalyzer import syllables_en
from itertools import groupby
import math
from nltk.corpus import stopwords
import utils
import codecs
from string import punctuation

def initialize_for_linguistic_features():
    return get_word_frequency_counts(), get_syllable_counts()

def get_word_frequency_counts():
    word_frequency_counts = {}
    smh_doc = open("/Users/shrprasha/Projects/AA/initial_analysis/smh")
    the_count = 1530718
    for line in smh_doc:
        word_count = line.split("\t", 2)
        word = word_count[0]
    #     print word
        count = int(word_count[1])
        steinfreq = math.floor(math.log(the_count/count, 2))
        word_frequency_counts[word] = steinfreq
    return word_frequency_counts

def get_syllable_counts():
    syllable_counts = {}
    syl_doc = open("/Users/shrprasha/Projects/AA/initial_analysis/cmudict.0.6-syl")
    for line in syl_doc:
        word = line.split(" ", 1)[0]
        if not "(" in word:
            count = line.count(".") + 1
            syllable_counts[word] = count
    return syllable_counts


# def update_decorate(feature_function):
#     def update_features(*args):
#         self = args[0]
#         features_counts = feature_function(*args)
#         if not features_counts:
#             return
#         for feature, count in features_counts.iteritems():
#             if feature in self.all_features_list:
#                 idx = self.all_features_list[feature]  # idx of feature in linguistic features
#                 self.review_features[idx] = count
#             else:
#                 idx = len(self.all_features_list) + 1  # append the new feature to the end, + 1 added since features start from 1
#                 self.all_features_list[feature] = idx
#                 self.review_features[idx] = count
#     return update_features

count = lambda l1, l2: len(list(filter(lambda c: c in l2, l1)))

class Linguistic:

    def __init__(self):
        self.word_frequency_counts, self.syllable_counts = initialize_for_linguistic_features()
        #  dictionary of features
        self.all_features_list = {}

    def get_stylistic_features(self, text, token_tags):
        self.review_text = text
        self.review_text = self.review_text.lower()
        self.token_tags = token_tags
        stylistic_features = {}
        # self.review_pos = review["review_pos_tags"].split()
        # self.review_chunk = review["review_chunks"].split()
        self.initialize()
        if self.total_words() == 0:
            return None
        self.get_word_unigrams()
        # self.unigram_features = self.review_features.copy()
        # self.get_pos_ngrams_range(1, 3)
        # self.get_char_ngrams_range(1, 3)
        # self.get_chunk_ngrams_range(1, 3)
        stylistic_features.update(self.avg_char_per_word())
        stylistic_features.update(self.avg_syl_per_word())
        stylistic_features.update(self.avg_word_per_sent())
        stylistic_features.update(self.punctuation_count())
        stylistic_features.update(self.emoticon_count())
        stylistic_features.update(self.abbvrs_count())
        stylistic_features.update(self.stopword_freq())
        stylistic_features.update(self.average_word_frequency())
        stylistic_features.update(self.flesch_kincaid())
        stylistic_features.update(self.gunning_fox())
        stylistic_features.update(self.emotion_words())
        # stylistic_features.update(self.yule_k())
        # stylistic_features.update(self.honore_r())
        # self.pos_trigram_diversity()
        # print self.review_text, self.review_features
        return stylistic_features


    #  main entry
    def get_all_linguistic_features(self, review):
        # print "here", review["review_id"], review["review_text"]
        self.review_text = review["review_text"]
        self.review_text = self.review_text.lower()
        self.review_pos = review["review_pos_tags"].split()
        self.review_chunk = review["review_chunks"].split()
        self.initialize()
        self.get_word_unigrams()
        self.unigram_features = self.review_features.copy()
        self.get_pos_ngrams_range(1, 3)
        self.get_char_ngrams_range(1, 3)
        self.get_chunk_ngrams_range(1, 3)
        self.avg_char_per_word()
        self.avg_syl_per_word()
        self.avg_word_per_sent()
        self.flesch_kincaid()
        self.gunning_fox()
        self.yule_k()
        self.honore_r()
        self.pos_trigram_diversity()
        self.stopword_freq()
        self.average_word_frequency()
        # print self.review_text, self.review_features
        return self.review_features, self.unigram_features

    def initialize(self):
        # self.wac = Counter(utils.get_words_in_text(self.review_text))
        #  features for this review
        self.review_features = {}
        self.words, self.numbers, self.puncts, self.emoticons, self.abbvrs = utils.get_words_numbers_puncts_emoticons_abbvfs_from_tagged(self.token_tags)
        self.wac = Counter(self.words)
        self.sentences = utils.get_sentences(self.review_text)
        self.calculate_syllable_counts()

    def count_of_features(self):
        return len(self.all_features_list)

    def punctuation_count(self):
        return {"punctuations": len(self.puncts)}

    def emoticon_count(self):
        return {"emoticons": len(self.emoticons)}

    def abbvrs_count(self):
        return {"abbreviations": len(self.abbvrs)}

    def calculate_syllable_counts(self):
        no_of_syllables_in_review = 0
        complex_words_count = 0
        count_non_zero_syl = 0
        for word, count in self.wac.iteritems():
            no_of_syllables_in_word = self.get_no_of_syllables(word)
            if no_of_syllables_in_word > 0:
                count_non_zero_syl += count
            no_of_syllables_in_review += count * no_of_syllables_in_word
            # print "ling1: ", word, count, no_of_syllables_in_word
            if(self.is_complex(count)):
                complex_words_count += 1
        self.syllables_count = no_of_syllables_in_review
        if count_non_zero_syl > 0:
            self.avg_syl_per_nonzero_word = self.syllables_count * 1.0 / count_non_zero_syl
        else:
            print "0:", self.review_text,
            self.avg_syl_per_nonzero_word = 0
        # print "sysl: ", count_non_zero_syl, self.total_words()
        self.complex_words_count = complex_words_count

    def is_complex(self, no_of_syllables_in_word):
        if no_of_syllables_in_word > 2:
            return True
        return False

    def emotion_words(self):
        emotions = {"pos_emo": 0, "neg_emo": 0}
        positive_emotion_full_words = ['accept', 'freeing', 'accepted', 'freely', 'accepting', 'freeness', 'accepts', 'freer', 'play', 'played', 'fun', 'playing', 'plays', 'gentle', 'agree', 'gentler', 'gentlest', 'pleasing', 'agreed', 'gently', 'agreeing', 'agrees', 'giving', 'glad', 'gladly', 'pretty', 'pride', 'aok', 'glory', 'good', 'goodness', 'grace', 'awesome', 'graced', 'readiness', 'ready', 'beloved', 'graces', 'benefit', 'grand', 'relief', 'benefits', 'respect ', 'great', 'best', 'grin', 'better', 'grins', 'ROFL', 'ha', 'happy', 'save', 'care', 'cared', 'heartfelt', 'carefree', 'share', 'shared', 'cares', 'shares', 'caring', 'sharing', 'casual', 'casually', 'helping', 'silly', 'helps', 'hilarious', 'special', 'hope', 'hoped', 'hopeful', 'hopefully', 'hopefulness', 'sunnier', 'hopes', 'sunniest', 'hoping', 'sunny', 'confidence', 'hug ', 'confident', 'super', 'confidently', 'hugs', 'considerate', 'support', 'supported', 'contentment', 'supporting', 'cool', 'supports', 'improving', 'sweet', 'daring', 'sweetly', 'definite', 'sweets', 'definitely', 'joking', 'tehe', 'kidding', 'thank', 'kind', 'thanked', 'determined', 'kindly', 'thanks', 'laidback', 'like', 'treat', 'easily', 'liked', 'easiness', 'likes', 'true ', 'easing', 'liking', 'trueness', 'truer', 'LMAO', 'truest', 'LOL', 'truly', 'love', 'loved', 'lovely', 'loves', 'value', 'valued', 'values', 'luck', 'valuing', 'lucked', 'fab', 'lucks', 'lucky', 'madly', 'win', 'fine', 'ok', 'wins', 'okay', 'wisdom', 'okays', 'oks', 'won', 'fond', 'fondly', 'openness', 'fondness', 'worthwhile', 'forgave', 'yay', 'free', 'original', 'yays', 'outgoing', 'paradise']
        positive_emotion_prefixes = ['freed', 'partie', 'accepta', 'party', 'passion', 'peace', 'perfect', 'active', 'frees', 'admir', 'friend', 'ador', 'playful', 'advantag', 'funn', 'adventur', 'genero', 'affection', 'pleasant', 'please', 'agreeab', 'pleasur', 'giggl', 'popular', 'agreement', 'giver', 'positiv', 'prais', 'alright', 'precious', 'amaz', 'prettie', 'amor', 'glamor', 'amus', 'glamour', 'glori', 'privileg', 'appreciat', 'prize', 'assur', 'profit', 'attachment', 'promis', 'attract', 'gorgeous', 'proud', 'award', 'radian', 'beaut', 'graceful', 'reassur', 'benefic', 'graci', 'relax', 'grande', 'reliev', 'benefitt', 'gratef', 'resolv', 'benevolen', 'grati', 'benign', 'revigor', 'reward', 'grinn', 'rich', 'bless', 'bold', 'romanc', 'bonus', 'haha', 'romantic', 'brave', 'handsom', 'safe', 'bright', 'happi', 'satisf', 'brillian', 'calm', 'harmless', 'scrumptious', 'harmon', 'secur', 'sentimental', 'heartwarm', 'careful', 'heaven', 'heh', 'helper', 'helpful', 'silli', 'certain', 'sincer', 'challeng', 'hero', 'smart', 'champ', 'smil', 'charit', 'hoho', 'sociab', 'charm', 'honest', 'soulmate', 'cheer', 'honor', 'cherish', 'honour', 'splend', 'chuckl', 'strength', 'clever', 'strong', 'comed', 'succeed', 'comfort', 'success', 'commitment', 'compassion', 'compliment', 'sunshin', 'hugg', 'superior', 'humor', 'contented', 'humour', 'hurra', 'supporter', 'convinc', 'ideal', 'importan', 'supportive', 'courag', 'impress', 'create', 'improve', 'suprem', 'creati', 'sure', 'credit', 'incentive', 'surpris', 'cute', 'innocen', 'cutie', 'inspir', 'sweetheart', 'intell', 'sweetie', 'darlin', 'interest', 'dear', 'invigor', 'sweetness', 'joke', 'talent', 'delectabl', 'joll', 'delicate', 'joy', 'tender', 'delicious', 'keen', 'terrific', 'deligh', 'determina', 'thankf', 'devot', 'kindn', 'digni', 'kiss', 'thoughtful', 'divin', 'thrill', 'dynam', 'laugh', 'toleran', 'eager', 'libert', 'tranquil', 'ease', 'treasur', 'easie', 'likeab', 'triumph', 'easy', 'livel', 'ecsta', 'efficien', 'elegan', 'trust', 'encourag', 'truth', 'energ', 'useful', 'engag', 'lover', 'valuabl', 'enjoy', 'entertain', 'loving', 'enthus', 'loyal', 'excel', 'excit', 'vigor', 'lucki', 'vigour', 'fabulous', 'virtue', 'faith', 'virtuo', 'fantastic', 'vital', 'favor', 'magnific', 'warm', 'favour', 'merit', 'wealth', 'fearless', 'merr', 'welcom', 'festiv', 'neat', 'well', 'fiesta', 'nice', 'nurtur', 'winn', 'flatter', 'flawless', 'flexib', 'wise', 'flirt', 'openminded', 'wonderf', 'worship', 'opportun', 'optimal', 'wow', 'forgiv', 'optimi', 'free', 'freeb', 'painl', 'palatabl']
        negative_emotion_full_words = ['maddening', 'madder', 'sob', 'envious', 'maddest', 'sobbed', 'sobbing', 'aching', 'sobs', 'afraid', 'mess', 'messy', 'sorry', 'fake', 'miss', 'missed', 'stank', 'agony', 'misses', 'fear', 'missing', 'alone', 'feared', 'mock', 'fearing', 'mocked', 'fears', 'strange', 'mocking', 'mocks', 'fiery', 'stunk', 'fired', 'stunned', 'moody', 'stuns', 'fought', 'suck', 'sucked', 'needy', 'sucks', 'sucky', 'fuck', 'suffer', 'awful', 'suffered', 'bad', 'suffering', 'fucks', 'suffers', 'fuming', 'beaten', 'tears', 'fury', 'temper', 'pain', 'tempers', 'pained', 'boring', 'tensing', 'paining', 'broke', 'pains', 'grief', 'terrified', 'terrifies', 'terrify', 'terrifying', 'thief', 'harm', 'ticked', 'crap', 'harmed', 'crappy', 'harming', 'cried', 'harms', 'cries', 'hate', 'critical', 'hated', 'hates', 'trite', 'crushed', 'hating', 'protest', 'cry', 'hatred', 'protested', 'crying', 'protesting', 'turmoil', 'ugh', 'cut', 'hell', 'unattractive', 'hellish', 'raging', 'raping', 'unfriendly', 'unimportant', 'idiot', 'unkind', 'unpleasant', 'impersonal', 'unprotected', 'rotten', 'vain', 'sad', 'vanity', 'sadly', 'sadness', 'vile', 'jaded', 'dislike', 'scaring', 'disliked', 'jerk', 'scary', 'dislikes', 'jerked', 'war', 'disliking', 'jerks', 'warred', 'warring', 'serious', 'wars', 'distraught', 'lazy', 'seriously', 'seriousness', 'lied', 'lies', 'wept', 'shaky', 'whining', 'lose', 'loses', 'shook', 'losing', 'witch', 'lost', 'sin', 'sinister', 'sins', 'worst', 'emotional', 'lying', 'mad']
        negative_emotion_prefixes = ['abandon', 'enrag', 'snob', 'abuse', 'envie', 'abusi', 'ache', 'envy', 'maniac', 'evil', 'masochis', 'advers', 'excruciat', 'melanchol', 'solemn', 'exhaust', 'sorrow', 'aggravat', 'fail', 'aggress', 'miser', 'spite', 'agitat', 'fatal', 'stammer', 'agoniz', 'fatigu', 'fault', 'startl', 'alarm', 'steal', 'mistak', 'stench', 'anger', 'fearful', 'stink', 'angr', 'strain', 'anguish', 'mocker', 'annoy', 'feroc', 'stress', 'antagoni', 'feud', 'struggl', 'anxi', 'molest', 'stubborn', 'apath', 'fight', 'mooch', 'appall', 'moodi', 'apprehens', 'flunk', 'argh', 'foe', 'moron', 'stupid', 'argu', 'fool', 'mourn', 'stutter', 'arrogan', 'forbid', 'murder', 'submissive', 'asham', 'nag', 'assault', 'frantic', 'nast', 'asshole', 'freak', 'sucker', 'attack', 'fright', 'neglect', 'aversi', 'frustrat', 'nerd', 'avoid', 'nervous', 'fucked', 'neurotic', 'awkward', 'fucker', 'numb', 'sufferer', 'fuckin', 'obnoxious', 'bashful', 'obsess', 'bastard', 'fume', 'offence', 'suspicio', 'battl', 'offend', 'tantrum', 'furious', 'offens', 'bitch', 'outrag', 'teas', 'bitter', 'geek', 'overwhelm', 'blam', 'gloom', 'bore', 'goddam', 'tense', 'gossip', 'painf', 'bother', 'grave', 'tension', 'greed', 'terribl', 'brutal', 'panic', 'burden', 'griev', 'paranoi', 'careless', 'grim', 'pathetic', 'cheat', 'gross', 'peculiar', 'complain', 'grouch', 'perver', 'terror', 'confront', 'grr', 'pessimis', 'confus', 'guilt', 'petrif', 'thieve', 'contempt', 'harass', 'pettie', 'threat', 'contradic', 'petty', 'phobi', 'timid', 'harmful', 'piss', 'tortur', 'craz', 'piti', 'tough', 'pity', 'traged', 'poison', 'tragic', 'prejudic', 'trauma', 'critici', 'hateful', 'pressur', 'trembl', 'crude', 'hater', 'prick', 'trick', 'cruel', 'problem', 'trivi', 'troubl', 'heartbreak', 'cunt', 'heartbroke', 'puk', 'heartless', 'punish', 'ugl', 'cynic', 'rage', 'damag', 'uncertain', 'damn', 'helpless', 'rancid', 'uncomfortabl', 'danger', 'hesita', 'rape', 'uncontrol', 'daze', 'homesick', 'uneas', 'decay', 'hopeless', 'rapist', 'unfortunate', 'defeat', 'horr', 'rebel', 'defect', 'hostil', 'reek', 'ungrateful', 'defenc', 'humiliat', 'regret', 'unhapp', 'defens', 'hurt', 'reject', 'degrad', 'reluctan', 'unimpress', 'depress', 'ignor', 'remorse', 'depriv', 'immoral', 'repress', 'unlov', 'despair', 'impatien', 'resent', 'desperat', 'resign', 'despis', 'impolite', 'restless', 'unsavo', 'destroy', 'inadequa', 'revenge', 'unsuccessful', 'destruct', 'indecis', 'ridicul', 'unsure', 'devastat', 'ineffect', 'rigid', 'unwelcom', 'devil', 'inferior', 'risk', 'upset', 'difficult', 'inhib', 'uptight', 'disadvantage', 'insecur', 'rude', 'useless', 'disagree', 'insincer', 'ruin', 'disappoint', 'insult', 'disaster', 'interrup', 'sadde', 'vicious', 'discomfort', 'intimidat', 'victim', 'discourag', 'irrational', 'disgust', 'irrita', 'sarcas', 'villain', 'dishearten', 'isolat', 'savage', 'violat', 'disillusion', 'scare', 'violent', 'jealous', 'vulnerab', 'vulture', 'sceptic', 'scream', 'warfare', 'dismay', 'kill', 'screw', 'dissatisf', 'lame', 'selfish', 'distract', 'lazie', 'weak', 'distress', 'liabilit', 'weapon', 'distrust', 'liar', 'severe', 'weep', 'disturb', 'shake', 'weird', 'domina', 'shaki', 'doom', 'lone', 'whine', 'dork', 'longing', 'shame', 'doubt', 'shit', 'whore', 'dread', 'loser', 'shock', 'wicked', 'dull', 'wimp', 'dumb', 'shy', 'dump', 'loss', 'sicken', 'woe', 'dwell', 'worr', 'egotis', 'lous', 'worse', 'embarrass', 'low', 'luckless', 'skeptic', 'worthless', 'empt', 'ludicrous', 'slut', 'wrong', 'enemie', 'smother', 'yearn', 'enemy', 'smug']
        for word in self.words:
            if word in positive_emotion_full_words:
                emotions["pos_emo"] += 1
            elif word in negative_emotion_full_words:
                emotions["neg_emo"] += 1
            elif self.contains_prefix(positive_emotion_prefixes, word):
                emotions["pos_emo"] += 1
            elif self.contains_prefix(negative_emotion_prefixes, word):
                emotions["neg_emo"] += 1
        return emotions
    def contains_prefix(self, prefix_list, word):
        for prefix in prefix_list:
            if word.startswith(prefix):
                return True

    #########feature functions############

    def get_pos_ngrams_range(self, n_start, n_end):
        return utils.get_ngrams_from_list(self.review_pos, n_start, n_end)

    def get_chunk_ngrams_range(self, n_start, n_end):
        return utils.get_ngrams_from_list(self.review_chunk, n_start, n_end)

    def get_char_trigrams(self):
        n = 3
        ngrams = utils.char_ngrams(self.review_text, n)
        ngrams_counts = Counter(ngrams)
        # self.char_trigrams = ngrams_counts
        return ngrams_counts

    def get_char_ngrams_range(self, n_start, n_end):
        ngrams = utils.char_ngrams_range(self.review_text, n_start, n_end)
        ngrams_counts = Counter(ngrams)
        # self.char_ngrams_range = ngrams_counts
        return ngrams_counts

    def get_word_unigrams(self):
        return self.wac

    def avg_char_per_word(self):
        # print "hrere:", self.word_pos_tag_segment
        # print self.wac
        return {"avg_char_per_word": float(sum(len(w)*c for w, c in self.wac.iteritems())) / self.total_words()}

    def avg_syl_per_word(self):
        # return {"avg_syl_per_word": float(self.syllables_count) / self.total_words()}
        return {"avg_syl_per_word": self.avg_syl_per_nonzero_word}

    def avg_word_per_sent(self):
        return {"avg_word_per_sent": float(self.total_words()) / self.total_sentences()}

    def flesch_kincaid(self):
        fk = 206.835 - 1.015 * (self.total_words() / self.total_sentences()) - 84.6 * (self.total_syllables() / self.total_words())
        return {"flesch_kincaid": fk}

    def gunning_fox(self):
        return {"gunning_fox": 0.4 * ( (self.total_words()/self.total_sentences()) + 100 * (self.complex_words_count/self.total_words()))}

    def yule_k(self):
        stemmer = PorterStemmer()
        d = Counter(stemmer.stem(w).lower() for w in self.words)
        m1 = float(len(d))
        counts = []
        for w,c in self.wac.iteritems():
            counts.append(c)
        m2 = sum([len(list(g))*(freq**2) for freq,g in groupby(sorted(counts))])
        try:
            return {"yule_k": (m2-m1)/(m1*m1)}
        except ZeroDivisionError:
            return None

    def honore_r(self):
        n = self.total_words()
        v = len(self.wac)
        v1 = 0
        for w,c in self.wac.iteritems():
            if(c == 1):
                v1+=1
        #my modification
        if(v1 == v):
            v1 -= 1
        r = math.log(n/(1-(v1/v)))
        return {"honore_r": r}

    def pos_trigram_diversity(self):
        pos_trigrams = utils.get_ngrams_from_list(self.review_pos, 3, 3)
        unique_pos = set(pos_trigrams)
        if(len(pos_trigrams) > 0):
            return {"pos_trigram_diversity": float(len(unique_pos)) / len(pos_trigrams)}
        else:
            return None

    def stopword_freq(self):
        stopword_count = 0
        stop = stopwords.words('english')
        for w,c in self.wac.iteritems():
            if w in stop:
                stopword_count += c
        freq = float(stopword_count)/self.total_words()
        return {"stopword_freq": freq}

    def average_word_frequency(self):
        tot_f = 0
        not_found = 0
        for w,c in self.wac.iteritems():
            freq = self.get_word_freq_count(w)
            if(freq):
                tot_f += freq
            else:
                not_found += 0
        return {"average_word_frequency": tot_f/(self.total_words() - not_found)}
########feature functions############

    #  maintain list and index of features
    # def update_features(self, features_counts):
    #     if not features_counts:
    #         return
    #     for feature, count in features_counts.iteritems():
    #         if feature in self.all_features_list:
    #             idx = self.all_features_list[feature]  # idx of feature in linguistic features
    #             self.review_features[idx] = count
    #         else:
    #             idx = len(self.all_features_list) + 1  # append the new feature to the end, + 1 added since features start from 1
    #             self.all_features_list[feature] = idx
    #             self.review_features[idx] = count


###############helpers##############
    def get_word_freq_count(self, word):
        #is in database
        if(word in self.word_frequency_counts):
            freq = self.word_frequency_counts[word]
        #is not in databse
        else:
            freq = None
        return freq

    #used for syllable counts and gunning fox
    def get_no_of_syllables(self, word):
        word = word.upper()
        regex = re.compile("^[\d,.]+$") #not a number
        if not regex.search(word):
            if word in self.syllable_counts:
                count = self.syllable_counts[word]
                # print "1"
            else:
                count = syllables_en.count(word)
                # print "2"
        else:
            count = 0
        return count

    def total_words(self):
        return len(self.words)

    def total_sentences(self):
        return len(self.sentences)

    def total_syllables(self):
        return self.syllables_count

    def write_features_to_file(self, opfile):
        opfhandle = codecs.open(opfile, "w", encoding='utf-8')
        for key, value in sorted(self.all_features_list.iteritems(), key=lambda (k,v): (v,k)):
            opfhandle.write(str(key) + " " + str(value) + "\n")


if __name__ == "__main__":
    ling = Linguistic()

    # conn = MySQLdb.connect("localhost", "root", "", "amazon_db_software", charset='utf8')
    # cursor = conn.cursor()
    # dict_cursor = conn.cursor(MySQLdb.cursors.DictCursor)
    #
    # query = "SElECT review_id, review_text, review_pos_tags, review_chunks from reviews_selected_500 where review_id='R2URC1LSIFVLOL'"
    # dict_cursor.execute(query)
    # # fixed by LEFT JOIN: len(results) can be < count when r.product_id is not in products
    # review = list(dict_cursor.fetchall())

    # review = {"review_text": "This is is a a a test.", "review_pos_tags": "NN CD VBD RB DT JJ NN", "review_chunks": "B-NP B-VP B-ADJP I-ADJP O B-VP I-VP B-PP B-NP"}
    # review = {"review_id": 123, "review_text": "H&C® Concrete ", "review_pos_tags": "NN CD VBD RB DT JJ NN", "review_chunks": "B-NP B-VP B-ADJP I-ADJP O B-VP I-VP B-PP B-NP"}
    # print ling.get_all_linguistic_features(review[0])
    # print ling.all_features_list

    # review = {"review_text": 'This is an apologetic for the constitution and Declaration of Independence.  Our constitution has been under attack for over 100 years and it\'s heating up rapidly. The progressives would abolish or amend the constitution out of existance.  You have to know what it says and how to defend it and present solid arguments.  '
    #                          'This is a small book, very readable, no academic gibberish, just plain talk! I found myself underlining and highlighting whole paragraphs and rereading them to friends. Once we lose our constitution, it\'s gone forever and it\'s time to take a stand.  These 200 pages are solid gold information and this book is a gem.',
    #                          "review_pos_tags": 'DT VBZ DT JJ IN DT NN CC NNP IN NNP NNP NN VBZ VBN IN NN IN IN CD NNS CC VBD VBG RP RB DT NNS MD VB CC VB DT NN IN IN VBG PRP VBP TO VB WP PRP VBZ CC WRB TO VB PRP CC JJ JJ NN DT VBZ DT JJ NN RB VBD DT JJ NN RB JJ NN PRP VBD PRP VBG CC VBG JJ NNS CC VBG PRP TO DT RB PRP VBP PRP$ NN IN VBN RB CC VBD NN TO VB DT NN DT CD NNS VBP JJ JJ NN CC DT NN VBZ DT NN',
    #                         "review_chunks": 'B-NP B-VP B-NP I-NP B-PP B-NP I-NP O B-NP B-PP B-NP I-NP I-NP B-VP I-VP B-PP B-NP B-PP B-NP I-NP I-NP O B-VP I-VP B-PRT B-NP I-NP I-NP B-VP I-VP I-VP I-VP B-NP I-NP B-PP B-PP B-VP B-NP B-VP I-VP I-VP B-NP B-NP B-VP O B-ADVP B-VP I-VP B-NP O B-NP I-NP I-NP B-NP B-VP B-NP I-NP I-NP B-ADVP B-VP B-NP I-NP I-NP B-NP I-NP I-NP B-NP B-VP B-NP B-VP I-VP I-VP B-NP I-NP '
    #                                          'O B-VP B-NP B-PP B-NP I-NP B-NP B-VP B-NP I-NP B-SBAR B-VP B-ADVP O B-VP B-NP B-VP I-VP B-NP I-NP B-NP I-NP I-NP B-VP B-NP I-NP I-NP O B-NP I-NP B-VP B-NP I-NP'}
    #
    # print ling.get_all_linguistic_features(review)[0]
    # # print ling.get_all_linguistic_features(review)[1]
    #
    # review = {"review_text": "This is is a a a test.", "review_pos_tags": "NN CD VBD RB DT JJ NN", "review_chunks": "B-NP B-VP B-ADJP I-ADJP O B-VP I-VP B-PP B-NP"}
    #
    # print ling.get_all_linguistic_features(review)[0]
    # # print ling.get_all_linguistic_features(review)[1]


    print ling.get_stylistic_features("1 is too much.")


