__author__ = 'shrprasha'
import os
from dill import load, dump
from user import User, Post
from linguistic import Linguistic
import matplotlib.pyplot as plt
import numpy as np
from collections import Counter
from math import sqrt

def plot(d1):
    for feature, age_avgs in d1.iteritems():
        if feature == "no_of_posts":
            continue

def display(d1):
    lines = {}
    print "age", "\t",
    l1 = d1.keys()
    l1.remove("no_of_posts")
    for feature in ["no_of_posts"] + sorted(l1):
        age_avgs = d1[feature]
        print feature, "\t",
        for age, avg in age_avgs.iteritems():
            if age in lines:
                lines[age].append(avg)
            else:
                lines[age] = [avg]
    print
    for age in sorted(lines):
        print age, "\t", '\t'.join(map(str, lines[age]))

# old: {"Acne": {"avg_char_per_word": {18: 0.123, 19: 0.12, ...}, ...},... }
    #format: {'abbreviations': {"12-17": [0.23, 0.34, ...], "19-28": [0.98, ...], ...}, 'average_word_frequency': {}, ...}
def create_all_plots(d1, features):
    # aggr_stylistic_features = get_average(d1, age_group_counts)
    for feature in features:
        age_group_values_dict = d1[feature]
        means = {}  # {"12-17": 0.23, "19-28": 0.98,....}
        sds = {}
        for age_group in sorted(age_group_values_dict):
            values_list = age_group_values_dict[age_group]
            means[age_group] = np.mean(values_list)
            # print means[age_group], sum(values_list) / len(values_list)
            sds[age_group] = np.std(values_list)
            # sds[age_group] = sd(values_list)
            # print np.std(values_list), sd(values_list)
        create_plot(feature, means, sds)

# checking
def sd(l1):
    sd_sum = sd_sum2 = sd_n = 0
    for x in l1:
        sd_sum += x
        sd_sum2 += x*x
        sd_n += 1.0
        sum, sum2, n = sd_sum, sd_sum2, sd_n
    return sqrt(sum2/n - sum*sum/n/n)


#format: {'abbreviations': {"12-17": 0.23, "19-28": 0.98,....}, 'average_word_frequency': {}, ...}
#input: avg_char_per_word = {"Acne": {12:3.4, 13:4.5, ...}, "ADHD": {...}, ...}
def create_plot(feature_name, means, sds):
    colors = list("rgbcmyk")
    markers = ["8", "^", "D", "h", "*", "s", ">", "o"]

    x_labels = sorted(means.keys())
    x_labels.pop()
    x_labels.append("65-xx")
    plt.xticks(range(len(means)), x_labels, size='small')
    y = [means[k] for k in sorted(means.keys())]
    e = [sds[k] for k in sorted(means.keys())]
    plt.scatter(range(len(means)), y, marker="o", color="k")
    # plt.ylim(0, max(means.values()) + max(sds.values()) + 0.5)

    plt.errorbar(range(len(means)), y, e, linestyle='None', marker="o", color="k")
    # plt.errorbar(range(len(d1)), y, xerr=0.2, yerr=0.4)
    # plt.errorbar(range(len(d1)), y, fmt='o')
    # plt.errorbar(range(len(means)), y, [1,2,3,4, 5], linestyle='None', marker=markers.pop())
    plt.suptitle(feature_name)
    plt.show()

def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

def update_stylistic_features_list(stylistic_features_list, stylistic_features, age_group):
    # for feature, value in stylistic_features.iteritems():
    for feature in features:
        if feature in stylistic_features:
            value = stylistic_features[feature]
        else:
            value = 0.0  #  since if feature is 0, linguistic features will have removed it from the dict
        if feature in stylistic_features_list:
            if age_group in stylistic_features_list[feature]:
                stylistic_features_list[feature][age_group].append(value)
            else:
                stylistic_features_list[feature][age_group] = [value]
        else:
            stylistic_features_list[feature] = {age_group: [value]}

def get_feature_values(users):
    feature_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps"
    stylistic_features_list_dict = {}
    for fname in sorted(users):
        age_group = fname.split(".")[0].split("_")[1]
        idx_feature = load_dumps(os.path.join(feature_dumps_rootdir, "stylistic_features", fname + ".dump"))
        update_stylistic_features_list(stylistic_features_list_dict, idx_feature, age_group)
    return stylistic_features_list_dict

if __name__ == "__main__":
    users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps"
    # train_users = load_dumps(os.path.join(users_dumps_rootdir, "train.dump"))
    # train_users = load_dumps(os.path.join(users_dumps_rootdir, "subset_for_plots", "train.dump"))
    train_users = load_dumps(os.path.join(users_dumps_rootdir, "dev.dump"))
    train_uids = train_users.keys()
    features = ['abbreviations', 'average_word_frequency', 'avg_char_per_word', 'avg_syl_per_word', 'avg_word_per_sent', 'emoticons', 'flesch_kincaid', 'gunning_fox', 'neg_emo', 'pos_emo', 'punctuations', 'stopword_freq']
    # features = ['avg_char_per_word']
    #format: {'abbreviations': {"12-17": 0.23, "19-28": 0.98,....}, 'average_word_frequency': {}, ...}
    stylistic_features_list_dict = get_feature_values(train_uids)
    create_all_plots(stylistic_features_list_dict, features)


    # uids = train_users.keys()
    # for uid in sorted(uids):
    #     print uid