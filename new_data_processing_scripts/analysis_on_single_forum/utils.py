# -*- coding: utf-8 -*-
import nltk
import re
import numpy
import string
from scipy import spatial
from collections import Counter
import CMUTweetTagger as ctt


def get_token_tags(text):
    text = re.sub(r"((www\.[^\s]+)|(https?:\/\/[^\s]+))", '', text, flags=re.MULTILINE)
    token_tags = ctt.runtagger_single(text)
    return text, token_tags

def get_words_numbers_puncts_emoticons_abbvfs_from_tagged(token_tags):
    words = []
    numbers = []
    puncts = []
    emoticons = []
    abbrvs = []
    for token_tag in token_tags:
        token, tag = token_tag
        if tag == "$":
            numbers.append(token)
        elif tag == ",":
            puncts.append(token)
        elif tag == "E":
            emoticons.append(token)
        elif tag == "G" or is_known_acronym(token):
            abbrvs.append(token)
        else:
            words.append(token)
    return words, numbers, puncts, emoticons, abbrvs

def get_words_numbers_puncts_emoticons_abbvfs_twokenize(text):
    words = []
    numbers = []
    puncts = []
    emoticons = []
    abbrvs = []
    #remove urls
    text = re.sub(r"((www\.[^\s]+)|(https?:\/\/[^\s]+))", '', text, flags=re.MULTILINE)
    token_tags = ctt.runtagger_single(text)
    for token_tag in token_tags:
        token, tag, _ = token_tag
        if tag == "$":
            numbers.append(token)
        elif tag == ",":
            puncts.append(token)
        elif tag == "E":
            emoticons.append(token)
        elif tag == "G" or is_known_acronym(token):
            abbrvs.append(token)
        else:
            words.append(token)
    return words, numbers, puncts, emoticons, abbrvs
def is_known_acronym(token):
    known_acronyms = [u'2WW', u'AAMOF', u'ADN', u'AF', u'AFAIK', u'AFK', u'AKA', u'BAC', u'B4', u'B4N', u'BBL', u'BBT', u'BF', u'BFN', u'BFP', u'BIL', u'BITGOD', u'BRB', u'BTA', u'BTW', u'BYAM', u'BYKT', u'CMIIW', u'CU', u'CUL', u'DD', u'DF', u'DH', u'DP', u'DS', u'DTD', u'DTRT', u'DW', u'EBF', u'EBM', u'EOL', u'ESP', u'F2F', u'FAQ', u'FIL', u'FITB', u'FOMCL', u'FTTT', u'FU', u'4U', u'FWIW', u'FYI', u'G', u'GAFIA', u'GFY', u'GMTA', u'HPT', u'HTH', u'HV', u'IAC', u'IAE', u'IC', u'IDTT', u'IIRC', u'IMAO', u'IMCO', u'IMHO', u'IMNSHO', u'IMO', u'IMOBO', u'INPO', u'IOW', u'IWBNI', u'IYSWIM', u'JIC', u'JTYWLTK', u'LO', u'LOL', u'LMAO', u'M/C', u'MIL', u'MW', u'N/M', u'NP', u'NRN', u'N/T', u'OIC', u'OH', u'OM', u'OMG', u'OTOH', u'OTT', u'OW', u'PG', u'PILs', u'PMSL', u'PND', u'PNI', u'POV', u'PTB', u'ROTF', u'ROTFL', u'SAHD', u'SAHM', u'SIL', u'SITD', u'TAFN', u'TAFT', u'TBH', u'TIC', u'TGIF', u'TIA', u'TMI', u'TNXorTNS', u'TPTB', u'TTBOMK', u'TTC', u'TTFN', u'TTL4N', u'TTUL', u'TTYL', u'TYVM', u'WFM', u'WRT', u'WTF', u'WYSIWYG']
    if token.upper() in known_acronyms:
        return True

def get_ngrams_from_list(l1, n_start, n_end):
    pos_ngrams = []
    for i in range(n_start, n_end+1):
        pos_ngrams.extend(nltk.ngrams(l1, i))
    return Counter(pos_ngrams)

def char_ngrams(text, n):
    text_ngrams = []
    for i in range(0, len(text) - n + 1):
        text_ngrams.append(text[i:n + i])
    return text_ngrams

def char_ngrams_range(text, n_start, n_end):
    text_ngrams = []
    for n in range(n_start, n_end):
        for i in range(0, len(text) - n + 1):
            text_ngrams.append(text[i:n + i])
    return text_ngrams

def get_words_in_text(text):
    words = []
#    line = remove_punct(text)
    sentence_list = get_sentences(text)
    for sentence in sentence_list:
        word_list = get_words(sentence)
        words.extend(word_list)
    return words


count = lambda l1, l2: list(filter(lambda c: c in l2, l1))


def get_sentences(document):
    space_added_after_period = re.sub("([a-zA-Z]\.)", "\\1 ", document)
    sentences = nltk.sent_tokenize(space_added_after_period)
    return sentences

#used everywhere
def get_words(sentence):
    return nltk.word_tokenize(sentence)
    #return nltk.word_tokenize(remove_punct(sentence)) #do not uncomment this; call remove_punct and give output to get_words

   
def create_n_gram_profile(words, n):
    return nltk.ngrams(words, n)

def create_n_gram_profile_n(words, n):
    return nltk.ngrams(words, n)
    
def remove_punct(s):
    punctuation = re.compile(r'[-.?!,":;()`]')
#    bullet = re.compile(ur'•', re.UNICODE)
    bullet = re.compile(ur'\u2022')
#    bullet = re.compile(r'u\'\u2022\'')
#    bullet = re.compile("•")
    return bullet.sub(" ", punctuation.sub(" ", s))

def remove_punct_from_list(l):
    puncts = string.punctuation + u'\u2022'
    no_puncts = [x for x in l if x not in puncts]
    return no_puncts

def get_words_in_line(line):
    words = []
#    line = remove_punct(line)
    sentence_list = get_sentences(line)
    for sentence in sentence_list:
        word_list = get_words(sentence)
        sw_in_sent = [w.lower() for w in word_list] # just changing this to take all words wont change a thing because stopwords have been used throughout the project
        words.extend(sw_in_sent)
    return words



def common_between_lists(susp_nes, src_nes):
    return set(susp_nes).intersection(set(src_nes))

def compare_tuples(t1, t2, match):
    if(len(set(t1).intersection(set(t2))) >= match):
#        print "yes"
        return True
    else:
#        print "no"
        return False

def merge_lists_remove_duplicates(l1, l2):
#did not work since list is a list of lists returns unhashable type:list
#    return sorted(l1 + list(set(l2) - set(l1)))
    for x in l1:
        if x not in l2:
            l2.append(x) 
    return l2

def allwords(document):
    document.seek(0)
    doc_all = []
    for line in document:
        doc_all.extend(get_words_in_line(line))
    return doc_all

def total_words(document):
    s = 0
    a = 0
#    for line in document:
#        sentence_list = get_sentences(line)
#        for sentence in sentence_list:
#            word_list = get_words(sentence)
#            for w in word_list:
#                if w.lower() in constants.stopwords:
#                    s+=1
#                a+=1
    for line in document:
        a += len(get_words_in_line(line))
#    print a
#    print s

#used by get_avg_distance_betn_plag_seg_and_other_segs        
def get_distance_betn_two_vectors(t1, t2):
#    print "t1: ", t1
#    print "t2: ", t2
    fv1 = numpy.array(t1)
    fv2 = numpy.array(t2)
    return numpy.linalg.norm(fv1-fv2)

def get_cosine_distance(t1, t2):
    return spatial.distance.cosine(t1, t2)

def make_normalize(omin, omax):
    def normalize(x):
#        b = 1
#        a = 0
#        return a + (x-omin) * (b - a) / (omin - omax)
        return 1.0 if (omin==omax) else (x-omin) / (omax - omin)
    return normalize
    
def print_lol(list_of_lists):
    print "["
    for list in list_of_lists:
        print list, ","
    print "]"
    
def remove_multiple_whitespaces(passage):
    passage = passage.replace(u'\xa0', u' ')
    return re.sub( '\s{2,}', ' ', passage).strip()

def display_dict_sorted_keys(mydict):
    for key in sorted(mydict.iterkeys()):
        print "%s: %s" % (key, mydict[key])
    print
    
def get_sil_coeff(dist_own, dist_other):
    return float(dist_other - dist_own) / max(dist_other, dist_own)

#input: list of words in a sentence
def get_pos_tags_hunpos(words_in_sent, ht):
    #tagger given as input since it will be slow to load the model each time
    #ht = HunposTagger('/home/prasha/Documents/softwares/hunpos/english.model', encoding='utf-8')
    tags = ht.tag(get_words(words_in_sent))
    return tags
    
#def get_charidx_wordidx_mapping(document):
#    from = 0
#    to = 1
#    word_count = 1
#    while word_count == 1:
#        nltk.word_tokenize(remove_punct(sent))
                    
if __name__ == "__main__":
#    filen = "/home/prasha/Documents/docsig/pan2013-detailed-comparison-training-corpus/src/source-document00658.txt"
#    document = codecs.open(filen, encoding='utf-8')
#    total_words(document)
#    print get_distance_betn_two_vectors([2,2,2], [2,2,2])
#    s = '• • • • • • • • • • • • Writing Competitions • • • Moot Court Competitions'

    #print "aa"+ remove_punct("``").strip() + "aa"

    # print get_words(" Oh, can't you? ")

#    s = u'• • • • • • • • • • • • Writing Competitions • • • Moot Court Competitions'
##    s = "what the heck???? dfsd"
#    print s
#    print remove_punct(s)
#     print get_sentences("My soon to be ex dtr-in-law thinks her son is just a brat even though his diagnosis of adhd is so evident.She feels the schools will help him learn and that is all that needs to be done.She is verbally and physically abusive to him and since there have been no bruises or broken bones DCS in tennessee will not get involved.My husband and I are currently trying to get temporary custody until our son is able to provide a home for 3 kids.This child screams at the thought of going home.Something is wrong .His teachers have noticed a change in him since they moved out of our home.I worry as much about his emotional status as I do his physical.I am just frustrated that in this state a women can get protection orders for verbal abuse but a child can get no support or protection form the same.Something is terribly wrong with this.I love all my grandchildren but the oldest is my main concern at this time.I should add that everyone who meets him says what a sweet natured child he is.His mom is the only one who thinks different. ")
#     print nltk.sent_tokenize("I am god. Bow down to me.")
#     print nltk.word_tokenize("I am god.Bow down to me.")

    print get_words_numbers_puncts_emoticons_abbvfs_twokenize("this is the link http://www.vitacost.com/planetary-f")
