__author__ = 'shrprasha'

import os
from dill import load
from collections import Counter

def get_files_in_folder(folder):
    all_files = sorted(next(os.walk(folder))[2])
    if ".DS_Store" in all_files:
        all_files.remove(".DS_Store")
    return all_files


def get_duplicates(all_feature_files):
    duplicates = {}
    uids_ages = {}
    for fname in all_feature_files:
        uid, age_group = fname.strip().split("_")
        age_group = age_group.split(".")[0]
        if uid not in uids_ages:
            uids_ages[uid] = [age_group]
        else:
            uids_ages[uid].append(age_group)
    # print uids_ages
    for uid, ages in uids_ages.iteritems():
        if len(ages) > 1:
            duplicates[uid] = ages
    return duplicates


if __name__ == "__main__":
    feature_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/stylistic_features"
    all_feature_files = get_files_in_folder(feature_dumps_rootdir)
    uids_ages = get_duplicates(all_feature_files)
    age_groups = ["12-16", "19-28", "31-48", "51-63", "66-200"]
    features = ['abbreviations', 'average_word_frequency', 'avg_char_per_word', 'avg_syl_per_word', 'avg_word_per_sent', 'emoticons', 'flesch_kincaid', 'gunning_fox', 'neg_emo', 'pos_emo', 'punctuations', 'stopword_freq']
    # print len(uids_ages), uids_ages

    opdir = "/Users/shrprasha/Projects/resources/dailystrength/stats/stylistic_tables_duplicates"
    fhandle_dict = {}
    for feature in features:
        fhandle = open(os.path.join(opdir, feature + ".csv"), "w")
        fhandle.write("user_id,12-16,19-28,31-48,51-63,66-xx\n")
        fhandle_dict[feature] = fhandle

    for uid, ages in uids_ages.iteritems():
        lines = [uid] * len(features)
        feature_names_lines = dict(zip(features, lines))
        for age_group in age_groups:
            if age_group in ages:
                with open(os.path.join(feature_dumps_rootdir, uid + "_" + age_group + ".dump"), "r") as fhandle:
                    feature_vals = load(fhandle)
                for f in features:
                    if f in feature_vals:
                        feature_names_lines[f] += "," + str(feature_vals[f])
                    else:
                        feature_names_lines[f] += ",0.0"
            else:
                for f1 in features:
                    feature_names_lines[f1] += ",-"
        # print feature_names_lines
        for f2, fhandle in fhandle_dict.iteritems():
            fhandle.write(feature_names_lines[f2] + "\n")






