__author__ = 'shrprasha'
import os
from dill import load
from user import User, Post
from collections import Counter

def display_dict_ordered(d1):
    for k in sorted(d1):
        print k, d1[k]
    print "total", sum(d1.values())


def main_all():
    disease_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/disease_post_dumps/disease_post_dumps"
    # disease_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/nicolas_data_processed/disease_post_dumps"
    all_files = sorted(next(os.walk(disease_dump_rootdir))[2])
    for fname in all_files:
        print "\n\n\n", fname
        ages = []
        with open(os.path.join(disease_dump_rootdir, fname), "r") as fhandle:
            disease_posts = load(fhandle)
            for post in disease_posts:
                if post.age_when_posted < 12 or post.age_when_posted > 100:
                    continue
                ages.append(post.age_when_posted)
                # if post.age_when_posted < 1:
                #     print "uid: ", post.uid, post.age_when_posted
                #     return
            display_dict_ordered(Counter(ages))

def main_single():
    disease_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/disease_post_dumps/disease_post_dumps"
    # disease_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/nicolas_data_processed/disease_post_dumps"
    all_files = sorted(next(os.walk(disease_dump_rootdir))[2])
    fname = "Depression_Teen_posts.dump"
    print "\n\n\n", fname
    ages = []
    with open(os.path.join(disease_dump_rootdir, fname), "r") as fhandle:
        disease_posts = load(fhandle)
        for post in disease_posts:
            if post.age_when_posted < 12 or post.age_when_posted > 100:
                continue
            ages.append(post.age_when_posted)
            # if post.age_when_posted < 1:
            #     print "uid: ", post.uid, post.age_when_posted
            #     return
        display_dict_ordered(Counter(ages))

if __name__ == "__main__":
    # main_all()
    main_single()


