__author__ = 'shrprasha'
import os
from dill import load
from user import User, Post
import numpy as np

if __name__ == "__main__":
    disease_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/disease_post_dumps/disease_post_dumps"
    disease_dumps = sorted(next(os.walk(disease_dump_rootdir))[2])
    for disease_dump_fname in disease_dumps:
        ages = []
        with open(os.path.join(disease_dump_rootdir, disease_dump_fname), "r") as fhandle:
            disease_posts = load(fhandle)
            for post in disease_posts:
                if post.age_when_posted < 12 or post.age_when_posted > 100:
                    continue
                ages.append(post.age_when_posted)
        print disease_dump_fname.split("_")[0], np.mean(ages), np.std(ages), len(ages)

