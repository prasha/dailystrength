__author__ = 'shrprasha'
import os
from dill import load, dump
from user import User, Post
from linguistic import Linguistic
import matplotlib.pyplot as plt
import numpy as np

def get_average(d1):
    no_of_posts = d1["no_of_posts"]
    for feature, age_totals in d1.iteritems():
        if feature == "no_of_posts":
            continue
        for age in age_totals:
            # print age_totals[age],
            age_totals[age] /= (1.0 * no_of_posts[age])
            # print no_of_posts[age],  d1[feature][age]

def plot(d1):
    for feature, age_avgs in d1.iteritems():
        if feature == "no_of_posts":
            continue

def display(d1):
    lines = {}
    print "age", "\t",
    l1 = d1.keys()
    l1.remove("no_of_posts")
    for feature in ["no_of_posts"] + sorted(l1):
        age_avgs = d1[feature]
        print feature, "\t",
        for age, avg in age_avgs.iteritems():
            if age in lines:
                lines[age].append(avg)
            else:
                lines[age] = [avg]
    print
    for age in sorted(lines):
        print age, "\t", '\t'.join(map(str, lines[age]))

# {"Acne": {"avg_char_per_word": {18: 0.123, 19: 0.12, ...}, ...},... }
def create_all_plots(d1, features):
    for feature in features:
        plot_d1 = {}
        for disease, feature_age_vals in d1.iteritems():
            plot_d1[disease] = feature_age_vals[feature]
        if len(d1) <= 7:
            create_plot_less(feature, plot_d1)
        else:
            create_plot_more(feature, plot_d1)



#input: avg_char_per_word = {"Acne": {12:3.4, 13:4.5, ...}, "ADHD": {...}, ...}
def create_plot_more(feature_name, d1):
    # colors = list("rgbcmyk")
    colors = np.random.rand(len(d1))
    # colors = random.sample(range(1, 100), 3)len(d1))
    # markers = ["8", "^", "D", "h", "*", "s", ">"]
    # markers = ["8", "^", "D", "h", "*", "s", ">"]

    plt.xticks(np.arange(12, 100, 1))
    # plt.grid(b=True, which='major', color='b', linestyle='-')
    plt.grid(True)
    for i, data_dict in enumerate(d1.values()):

        x = data_dict.keys()
        y = data_dict.values()
        # plt.scatter(x, y, marker="8", color=colors.pop())
        print colors[i]
        plt.scatter(x, y, marker="8", color=[colors[i]]*len(data_dict))
    # plt.legend(d1.keys(), scatterpoints=1,
    #            loc='upper left',
    #            ncol=2,
    #            fontsize=8)
    plt.suptitle(feature_name)
    plt.show()


def create_plot_less(feature_name, d1):
    colors = list("rgbcmyk")
    # colors = np.random.rand(len(d1))
    # colors = random.sample(range(1, 100), 3)len(d1))
    markers = ["8", "^", "D", "h", "*", "s", ">"]
    # markers = ["8", "^", "D", "h", "*", "s", ">"]

    plt.xticks(np.arange(12, 100, 1))
    # plt.grid(b=True, which='major', color='b', linestyle='-')
    plt.grid(True)
    for i, data_dict in enumerate(d1.values()):

        x = data_dict.keys()
        y = data_dict.values()
        plt.scatter(x, y, marker=markers.pop(), color=colors.pop())
        # print colors[i]
        # plt.scatter(x, y, marker="8", color=[colors[i]]*len(data_dict))
    plt.legend(d1.keys(), scatterpoints=1,
               loc='upper left',
               ncol=2,
               fontsize=8)
    plt.suptitle(feature_name)
    plt.show()

    #
    # # first half of both lists is +1 and second half is -1
    # half = len(listx) / 2
    # plt.ylabel("Feature #" + str(labely))
    # plt.xlabel("Feature #" + str(labelx))
    # # colors = [(1,0,0)] * half + [(0,0,1)] * half
    # # colors = [0.80474453] * half + [0.123] * half
    # # colors = np.random.rand(len(listx))
    # plt.axis([0, 1, 0, 1])
    # plt.xticks(np.arange(0, 1, 0.1))
    # pos = plt.(listx[:half], listy[:half], marker="+", c='b')
    # neg = plt.scatter(listx[half:], listy[half:], marker="_", c='r')
    # plt.legend((pos, neg),
    #            ("Background", "Non-Background"),
    #            scatterpoints=1,
    #            loc='upper left',
    #            ncol=2,
    #            fontsize=8)
    # # plt.suptitle(train_or_test + "_" + str(labely) + "_vs_" + str(labelx))
    # # print train_or_test + "_" + str(labely) + "_vs_" + str(labelx) + ".png"
    # plt.show()

# if __name__ == "__main__":
#     dumpdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/stylistic_by_disease"
#     diseases = ["Schizophrenia", "Incest_Survivors", "Irritable_Bowel_Syndrome", "Caregivers", "Dyslexia", "Common_Variable_Immunodeficiency", "Myasthenia_Gravis"]
#     # diseases = ["Schizophrenia", "Dry-Eyes", "Immigration_Law", "Incest_Survivors", "Irritable_Bowel_Syndrome", "War_In_Iraq", "Fructose-Intolerance", "Families-Friends-of-Gays-Lesbians", "Caregivers", "Dyslexia", "Common_Variable_Immunodeficiency", "Color-Blindness", "Myasthenia_Gravis", "Family-Issues", "Anger-Management", "Diabetes-Type-1", "Bone_Cancer", "Loneliness", "Lung_Cancer", "Asthma", "Cerebral-Palsy", "Food-Allergies", "Environmental-Allergies", "Chronic-Fatigue-Syndrome", "Personality_Disorders", "Insomnia", "Agoraphobia-and-Social-Anxiety", "Trichotillomania_Hair_Pulling", "Celiac-Disease", "Obsessive_Compulsive_Disorder", "Coming-Out", "Dizziness-Vertigo", "Asperger_Syndrome", "Financial_Challenges", "Migraine_Headaches", "Migraine", "Crohn_Disease_Ulcerative_Colitis", "Hypothyroidism", "Lesbian_Relationship_Challenges", "Food-Addiction", "Obesity", "Diabetes-Type-2", "Panic_Attacks", "Female-Sexual-Issues", "Alzheimer_Disease", "Fatty-Liver-Disease", "Back-Pain", "Families-and-Friends-Of-Addicts", "Tinnitus", "Atrial_Fibrillation", "Graves_Disease", "Brain_Injury", "Down-Syndrome", "Sex_Pornography_Addiction", "Pulmonary_Embolism", "Lyme_Disease", "Epilepsy-Seizures", "Families_of_Prisoners", "Hidradenitis_Suppurativa", "Codependency", "ADHD-ADD", "Gambling_Addiction_Recovery", "Deep_Vein_Thrombosis", "Parkinson_Disease", "Pancreatitis", "Family_Friends_of_Bipolar", "COPD", "Narcolepsy", "Acne", "Cirrhosis", "HPV", "Pseudotumor_Cerebri", "Fatty-Acid-Oxidation-Disorders", "Endometriosis", "Stillbirth", "Autism", "Body-Modification", "Grandparents_Raising_Children", "Menopause", "Depression_Teen", "Trying_To_Conceive", "Polycystic_Ovarian_Syndrome", "Pregnancy_After_Loss_Infertility", "Gay_Lesbian_Teens"]
#     all_stylistic_by_age_avg = {}
#     for disease in diseases:
#         print "\n", disease, "\n"
#         fname = disease + ".dump"
#         with open(os.path.join(dumpdir, fname), "r") as fhandle:
#             stylistic_by_age = load(fhandle)
#         all_stylistic_by_age_avg[disease] = stylistic_by_age
#     features = ['avg_syl_per_word', 'stopword_freq', 'emoticons', 'gunning_fox', 'flesch_kincaid', 'abbreviations', 'avg_char_per_word', 'average_word_frequency', 'avg_word_per_sent', 'punctuations']
#     create_all_plots(all_stylistic_by_age_avg, features)


if __name__ == "__main__":
    disease_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/disease_post_dumps"
    # diseases = ["Schizophrenia", "Incest_Survivors", "Irritable_Bowel_Syndrome", "Caregivers", "Dyslexia", "Common_Variable_Immunodeficiency", "Myasthenia_Gravis"]
    diseases = ["Fatty-Acid-Oxidation-Disorders", "Color-Blindness"]
    # diseases = ["Fatty-Acid-Oxidation-Disorders"]
    # diseases = ["Schizophrenia", "Dry-Eyes", "Immigration_Law", "Incest_Survivors", "Irritable_Bowel_Syndrome", "War_In_Iraq", "Fructose-Intolerance", "Families-Friends-of-Gays-Lesbians", "Caregivers", "Dyslexia", "Common_Variable_Immunodeficiency", "Color-Blindness", "Myasthenia_Gravis", "Family-Issues", "Anger-Management", "Diabetes-Type-1", "Bone_Cancer", "Loneliness", "Lung_Cancer", "Asthma", "Cerebral-Palsy", "Food-Allergies", "Environmental-Allergies", "Chronic-Fatigue-Syndrome", "Personality_Disorders", "Insomnia", "Agoraphobia-and-Social-Anxiety", "Trichotillomania_Hair_Pulling", "Celiac-Disease", "Obsessive_Compulsive_Disorder", "Coming-Out", "Dizziness-Vertigo", "Asperger_Syndrome", "Financial_Challenges", "Migraine_Headaches", "Migraine", "Crohn_Disease_Ulcerative_Colitis", "Hypothyroidism", "Lesbian_Relationship_Challenges", "Food-Addiction", "Obesity", "Diabetes-Type-2", "Panic_Attacks", "Female-Sexual-Issues", "Alzheimer_Disease", "Fatty-Liver-Disease", "Back-Pain", "Families-and-Friends-Of-Addicts", "Tinnitus", "Atrial_Fibrillation", "Graves_Disease", "Brain_Injury", "Down-Syndrome", "Sex_Pornography_Addiction", "Pulmonary_Embolism", "Lyme_Disease", "Epilepsy-Seizures", "Families_of_Prisoners", "Hidradenitis_Suppurativa", "Codependency", "ADHD-ADD", "Gambling_Addiction_Recovery", "Deep_Vein_Thrombosis", "Parkinson_Disease", "Pancreatitis", "Family_Friends_of_Bipolar", "COPD", "Narcolepsy", "Acne", "Cirrhosis", "HPV", "Pseudotumor_Cerebri", "Fatty-Acid-Oxidation-Disorders", "Endometriosis", "Stillbirth", "Autism", "Body-Modification", "Grandparents_Raising_Children", "Menopause", "Depression_Teen", "Trying_To_Conceive", "Polycystic_Ovarian_Syndrome", "Pregnancy_After_Loss_Infertility", "Gay_Lesbian_Teens"]
    all_stylistic_by_age_avg = {}
    stylistic_features = {}
    dumpdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/stylistic_by_disease"
    for disease in diseases:
        print "\n", disease, "\n"
        fname = disease + "_posts.dump"
        with open(os.path.join(disease_dump_rootdir, fname), "r") as fhandle:
            posts = load(fhandle)
        linguistic = Linguistic()
        stylistic_by_age = {"no_of_posts": {}}  #{"avg_char_per_word": {18: 0.123, 19: 0.12, ...}, ...}

        for post in posts:
            stylistic_features = linguistic.get_stylistic_features(post.text, post.token_tags)
            if post.age_when_posted < 10 or post.age_when_posted > 80 or not stylistic_features:
                continue
            if post.age_when_posted in stylistic_by_age["no_of_posts"]:
                for feature in stylistic_features:
                    stylistic_by_age[feature][post.age_when_posted] += stylistic_features[feature]
                stylistic_by_age["no_of_posts"][post.age_when_posted] += 1
            else:
                for feature in stylistic_features:
                    if feature not in stylistic_by_age:
                        stylistic_by_age[feature] = {}
                    stylistic_by_age[feature][post.age_when_posted] = stylistic_features[feature]
                stylistic_by_age["no_of_posts"][post.age_when_posted] = 1
            # if i == 20:
            #     break
            # i += 1
        # # print stylistic_by_age
        # # print stylistic_by_age["punctuations"]
        get_average(stylistic_by_age)
        # # print stylistic_by_age
        # # print stylistic_by_age["punctuations"]
        # # print stylistic_by_age["no_of_posts"]
        # # display(stylistic_by_age)
        all_stylistic_by_age_avg[disease] = stylistic_by_age
        with open(os.path.join(dumpdir, disease + ".dump"), "w") as dfhandle:
            dump(stylistic_by_age, dfhandle)

    features = stylistic_features.keys()
    print features
    features.append("no_of_posts")
    # create_all_plots(all_stylistic_by_age_avg, features)
    create_all_plots(all_stylistic_by_age_avg, ["no_of_posts"])





        # d1 = {"Acne": {12:3.4, 13:4.5, 15:5.5}, "ADHD": {11:1, 13:2, 14:8}}
        # create_plot("age_per_post", d1)






