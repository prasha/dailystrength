__author__ = 'shrprasha'
import os
from dill import load, dump
from user import User, Post
from linguistic import Linguistic
import matplotlib.pyplot as plt
import numpy as np

def plot(d1):
    for feature, age_avgs in d1.iteritems():
        if feature == "no_of_posts":
            continue

def display(d1):
    lines = {}
    print "age", "\t",
    l1 = d1.keys()
    l1.remove("no_of_posts")
    for feature in ["no_of_posts"] + sorted(l1):
        age_avgs = d1[feature]
        print feature, "\t",
        for age, avg in age_avgs.iteritems():
            if age in lines:
                lines[age].append(avg)
            else:
                lines[age] = [avg]
    print
    for age in sorted(lines):
        print age, "\t", '\t'.join(map(str, lines[age]))

# old: {"Acne": {"avg_char_per_word": {18: 0.123, 19: 0.12, ...}, ...},... }
    #format: {'abbreviations': {"12-17": 0.23, "19-28": 0.98,....}, 'average_word_frequency': {}, ...}
def create_all_plots(d1, features):
    for feature in features:
        create_plot_less(feature, d1[feature])

    #format: {'abbreviations': {"12-17": 0.23, "19-28": 0.98,....}, 'average_word_frequency': {}, ...}

#input: avg_char_per_word = {"Acne": {12:3.4, 13:4.5, ...}, "ADHD": {...}, ...}
def create_plot_more(feature_name, d1):
    # colors = list("rgbcmyk")
    colors = np.random.rand(len(d1))
    # colors = random.sample(range(1, 100), 3)len(d1))
    # markers = ["8", "^", "D", "h", "*", "s", ">"]
    # markers = ["8", "^", "D", "h", "*", "s", ">"]

    plt.xticks(np.arange(12, 100, 1))
    # plt.grid(b=True, which='major', color='b', linestyle='-')
    plt.grid(True)
    for i, data_dict in enumerate(d1.values()):

        x = data_dict.keys()
        y = data_dict.values()
        # plt.scatter(x, y, marker="8", color=colors.pop())
        print colors[i]
        plt.scatter(x, y, marker="8", color=[colors[i]]*len(data_dict))
    # plt.legend(d1.keys(), scatterpoints=1,
    #            loc='upper left',
    #            ncol=2,
    #            fontsize=8)
    plt.suptitle(feature_name)
    plt.show()


def create_plot_less(feature_name, d1):
    colors = list("rgbcmyk")
    markers = ["8", "^", "D", "h", "*", "s", ">", "o"]

    # plt.xticks(np.arange(1, 5, 1), d1.keys(), size='small')
    x_labels = sorted(d1.keys())
    x_labels.pop()
    x_labels.append("65-xx")
    plt.xticks(range(len(d1)), x_labels, size='small')
    # plt.grid(True)
    # x = d1.keys()
    y = [d1[k] for k in sorted(d1.keys())]
    plt.scatter(range(len(d1)), y, marker=markers.pop(), color=colors.pop())
    # print colors[i]
    # plt.scatter(x, y, marker="8", color=[colors[i]]*len(data_dict))
    # plt.legend(d1.keys(), scatterpoints=1,
    #            loc='upper left',
    #            ncol=2,
    #            fontsize=8)
    plt.suptitle(feature_name)
    plt.show()

def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

def update_aggr_stylistic_features(aggr_stylistic_features, stylistic_features, age_group):
    for feature, value in stylistic_features.iteritems():
        if feature in aggr_stylistic_features:
            if age_group in aggr_stylistic_features[feature]:
                aggr_stylistic_features[feature][age_group] += value
            else:
                aggr_stylistic_features[feature][age_group] = value
        else:
            aggr_stylistic_features[feature] = {age_group: value}

def get_average(aggr_stylistic_features, total_users):
    users_per_class = total_users / 5 ###IMP since data is balanced
    for feature, age_group_value in aggr_stylistic_features.iteritems():
        for age_group in age_group_value:
            age_group_value[age_group] /= users_per_class

def get_feature_values(users):
    feature_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps"
    aggr_stylistic_features = {}
    for fname in sorted(users):
        age_group = fname.split(".")[0].split("_")[1]
        idx_feature = load_dumps(os.path.join(feature_dumps_rootdir, "stylistic_features", fname + ".dump"))
        update_aggr_stylistic_features(aggr_stylistic_features, idx_feature, age_group)
    get_average(aggr_stylistic_features, len(users))
    return aggr_stylistic_features

if __name__ == "__main__":
    users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps/subset_for_plots"
    train_users = load_dumps(os.path.join(users_dumps_rootdir, "train.dump"))
    train_uids = train_users.keys()
    #format: {'abbreviations': {"12-17": 0.23, "19-28": 0.98,....}, 'average_word_frequency': {}, ...}
    aggr_stylistic_features = get_feature_values(train_uids)
    features = ['abbreviations', 'average_word_frequency', 'avg_char_per_word', 'avg_syl_per_word', 'avg_word_per_sent', 'emoticons', 'flesch_kincaid', 'gunning_fox', 'neg_emo', 'pos_emo', 'punctuations', 'stopword_freq']
    # features = ['flesch_kincaid', 'gunning_fox']
    create_all_plots(aggr_stylistic_features, features)