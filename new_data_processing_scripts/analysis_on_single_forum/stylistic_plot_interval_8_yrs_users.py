__author__ = 'shrprasha'

import os
from dill import load
from linguistic import Linguistic
import numpy as np
import matplotlib.pyplot as plt
import sys

def get_average(d1):
    no_of_posts = d1["no_of_posts"]
    for feature, age_totals in d1.iteritems():
        if feature == "no_of_posts":
            continue
        for age in age_totals:
            # print age_totals[age],
            age_totals[age] /= (1.0 * no_of_posts[age])
            # print no_of_posts[age],  d1[feature][age]

def get_files_in_folder(folder):
    all_files = sorted(next(os.walk(folder))[2])
    if ".DS_Store" in all_files:
        all_files.remove(".DS_Store")
    return all_files


# since can be in more than 1 age group
def get_all_substring_of_list(l1, str1):
    all_matches = []
    for str2 in l1:
        str2_uid = str2.split("_", 1)[0]
        # print str2
        if str1 == str2_uid:
            all_matches.append(str2)
    return all_matches


def create_all_plots(d1, features, uid):
    if not os.path.exists(os.path.join(opdir, uid)):
        os.makedirs(os.path.join(opdir, uid))
    for feature in features:
        if feature in d1:
            create_plot(feature, d1[feature], uid)

def create_plot(feature_name, d1, uid):
    fig = plt.figure()
    colors = list("rgbcmyk")
    markers = ["8", "^", "D", "h", "*", "s", ">", "o"]

    x = d1.keys()
    y = d1.values()
    plt.xticks(d1.keys())
    plt.scatter(x, y, marker=markers.pop(), color=colors.pop())

    # plt.errorbar(range(len(d1)), y, xerr=0.2, yerr=0.4)

    plt.suptitle(feature_name)
    # plt.show()
    # plt.savefig(os.path.join(opdir, uid, feature_name))
    fig.savefig(os.path.join(opdir, uid, feature_name), dpi=fig.dpi)

if __name__ == "__main__":
    uids_lines = open("/Users/shrprasha/Projects/resources/dailystrength/ds_data/interval_8_yrs_users", "r").readlines()
    user_posts_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps"
    all_feature_files = get_files_in_folder(user_posts_dumps_rootdir)

    linguistic = Linguistic()
    features = ['abbreviations', 'average_word_frequency', 'avg_char_per_word', 'avg_syl_per_word', 'avg_word_per_sent', 'emoticons', 'flesch_kincaid', 'gunning_fox', 'neg_emo', 'pos_emo', 'punctuations', 'stopword_freq']

    opdir = "/Users/shrprasha/Projects/resources/dailystrength/stats/stylistic_plots_per_user_large_interval"

    # for line in uids_lines:
    line = uids_lines[int(sys.argv[1])]
    train_or_dev, uid = line.strip().split("\t")
    print sys.argv[1], uid
    all_fnames = get_all_substring_of_list(all_feature_files, uid)
    # print all_fnames
    all_posts = []
    for fname in all_fnames:
        with open(os.path.join(user_posts_dumps_rootdir, fname), "r") as fhandle:
            posts = load(fhandle)
            all_posts.extend(posts)
    stylistic_by_age = {"no_of_posts": {}}  #{"avg_char_per_word": {18: 0.123, 19: 0.12, ...}, ...}
    for post in all_posts:
        stylistic_features = linguistic.get_stylistic_features(post.text, post.token_tags)
        if post.age_when_posted < 10 or post.age_when_posted > 80 or not stylistic_features:
            continue
        if post.age_when_posted in stylistic_by_age["no_of_posts"]:
            for feature in stylistic_features:
                stylistic_by_age[feature][post.age_when_posted] += stylistic_features[feature]
            stylistic_by_age["no_of_posts"][post.age_when_posted] += 1
        else:
            for feature in stylistic_features:
                if feature not in stylistic_by_age:
                    stylistic_by_age[feature] = {}
                stylistic_by_age[feature][post.age_when_posted] = stylistic_features[feature]
            stylistic_by_age["no_of_posts"][post.age_when_posted] = 1
    get_average(stylistic_by_age)
    # print stylistic_by_age
    create_all_plots(stylistic_by_age, features, uid)



