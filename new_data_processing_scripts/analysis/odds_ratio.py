from __future__ import division
from collections import Counter
from user import User, Post

__author__ = 'shrprasha'

import os
from dill import load, dump
import sys
# from joblib import Parallel, delayed


def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

def create_dumps(obj, fpath):
    with open(fpath, "w") as fhandle:
        dump(obj, fhandle)

# used counts per post
def update_dicts_per_post(fname, user):
    with open(os.path.join(user_posts_dump_rootdir, fname), "r") as fhandle:
        print fname,
        user_posts = load(fhandle)
        for post in user_posts:
            user_gender = user.gender.lower()
            user_label = user.age_group + "_" + user_gender
            post_unique_tokens = set([token.lower() for token, _ in post.token_tags])
            # print post_unique_tokens
            all_counts[user_label].update(post_unique_tokens)
            all_counts[user.age_group].update(post_unique_tokens)
            all_counts[user_gender].update(post_unique_tokens)

            each_class_total_posts[user_label] += 1
            each_class_total_posts[user.age_group] += 1
            each_class_total_posts[user_gender] += 1
    # print each_class_total_posts


# used counts per post
def update_dicts_per_user(fname, user):
    with open(os.path.join(user_posts_dump_rootdir, fname), "r") as fhandle:
        print fname,
        user_posts = load(fhandle)
        user_unique_tokens = []
        user_gender = user.gender.lower()
        user_label = user.age_group + "_" + user_gender
        unique_user_tokens = []
        for post in user_posts:
            user_unique_tokens.extend([token.lower() for token, _ in post.token_tags])
            # print post_unique_tokens
        user_unique_tokens = set(user_unique_tokens)
        all_counts[user_label].update(user_unique_tokens)
        all_counts[user.age_group].update(user_unique_tokens)
        all_counts[user_gender].update(user_unique_tokens)

        each_class_total_posts[user_label] += 1
        each_class_total_posts[user.age_group] += 1
        each_class_total_posts[user_gender] += 1
    # print each_class_total_posts


def display_counter(counter):
    for label in sorted(counter):
        ctr = counter[label]
        print "=" * 30
        print label
        print ctr.most_common(200)
        print "=" * 30

def get_odds_ratio(labels):
    totals = Counter()
    for label in labels:
        totals += all_counts[label]
    print "totals:", totals
    for label in labels:
        for word, class_word in all_counts[label].iteritems():
            class_not_word = each_class_total_posts[label] - class_word
            not_class_word = totals[word] - class_word
            not_class_not_word = total_posts - each_class_total_posts[label] - not_class_word
            # if word == "."  and label == "31-48":
            #     print class_word, class_not_word, not_class_word, not_class_not_word
            try:
                odds_ratio_word = (class_word / class_not_word) / (not_class_word / not_class_not_word)
            except:
                odds_ratio_word = 0
            odds_ratio[label].update({word: odds_ratio_word})
        print "=" * 30
        print "label:", label
        print odds_ratio[label].most_common(200)
        print "=" * 30

if __name__ == "__main__":
    ## here
    # user_posts_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps"
    # features_dumpdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps"
    # idf_fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/idfs"
    # users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps"

    both_labels = ["12-16_female", "19-28_female", "31-48_female", "51-63_female", "66-200_female", "12-16_male", "19-28_male", "31-48_male", "51-63_male", "66-200_male"]
    age_labels = ["12-16", "19-28", "31-48", "51-63", "66-200"]
    gender_labels = ["female", "male"]

    labels = both_labels + age_labels + gender_labels

    all_counts = {}  # {label: {word:count, ...}, ...}
    each_class_total_posts  = {}
    odds_ratio = {}
    for label in labels:
        all_counts[label] = Counter()
        each_class_total_posts[label] = 0
        odds_ratio[label] = Counter()

    ## cluster
    # user_posts_dump_rootdir = "/project/solorio/prasha/ds/processed_data/user_post_dumps"
    # users_dumps_rootdir = "/project/solorio/prasha/ds/processed_data/users_dumps"
    # all_counts_dump_fpath = ""

    ## here
    user_posts_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps"
    users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps"
    # all_counts_dump_fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/all_counts.dump"
    # odds_ratio_dump_fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/odds_ratio.dump"
    all_counts_dump_fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/odds_count/per_user/all_counts.dump"
    odds_ratio_dump_fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/odds_count/per_user/odds_ratio.dump"

    type = "train"
    # type = "train_small"
    all_users = load_dumps(os.path.join(users_dumps_rootdir, type + ".dump"))
    for fname in sorted(all_users):
        user = all_users[fname]
        update_dicts_per_user(fname, user)
    print

    total_posts = each_class_total_posts["female"] + each_class_total_posts["male"]

    # display_counter()
    get_odds_ratio(age_labels)
    get_odds_ratio(gender_labels)
    get_odds_ratio(both_labels)

    create_dumps(all_counts, all_counts_dump_fpath)
    create_dumps(odds_ratio, odds_ratio_dump_fpath)

# if __name__ == "__main__":
#     # odds_ratio_dump_fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/odds_ratio.dump"
#     odds_ratio_dump_fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/odds_count/per_user/odds_ratio.dump"
#     odds_ratio = load_dumps(odds_ratio_dump_fpath)
#     display_counter(odds_ratio)







