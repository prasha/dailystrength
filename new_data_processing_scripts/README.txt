Disease posts here: not complete

user_post_dumps_continuous_age_groups: 90596
after getting users for discontinuous age groups: 78377 (in new_data_processing_scripts/get_user_features_divide_data.py)
    used the same users to run on continuous age groups as well

before and after ( filtering for >1 word)
train 47027 42968
dev 15674 14254
test 15676 14293
total 78377	71515

78316

user.py -> stores classes
0. qsub /project/solorio/prasha/ds/code/for_tests/run_tests_sge.sh
   user-details.py
    /project/solorio/prasha/ds/code/user-details.py
   op: /project/solorio/prasha/ds/data/username_id_age_gender.txt
       /project/solorio/prasha/ds/data/username_id_age_gender_both_age_gender.txt
   for username_id_age_gender_both_age_gender.txt
1. qsub /project/solorio/prasha/ds/code/for_posts/posts_sge.sh
    new_data_processing_scripts/get_posts.py *
    scp /Users/shrprasha/Projects/dailystrength/new_data_processing_scripts/get_posts.py $CUSCO:/project/solorio/prasha/ds/code/for_posts
    op: /project/solorio/prasha/ds/processed_data/disease_post_dumps/[FORUM]_posts.dump
    log: posts_job.log-
    dump all posts of disease
2. qsub /project/solorio/prasha/ds/code/for_posts/user_posts_sge.sh
   new_data_processing_scripts/get_user_posts.py
   scp /Users/shrprasha/Projects/dailystrength/new_data_processing_scripts/get_user_posts.py $CUSCO:/project/solorio/prasha/ds/code/for_posts
   op: /project/solorio/prasha/ds/processed_data/user_post_dumps
   after get_posts; store as user and list of posts of the user for an age group
---------
3. new: new_data_processing_scripts/get_user_features_divide_data.py
    qsub /project/solorio/prasha/ds/code/for_user_features/user_features_divide_data_sge.sh
   scp /Users/shrprasha/Projects/dailystrength/new_data_processing_scripts/get_user_features_divide_data.py $CUSCO:/project/solorio/prasha/ds/code/for_user_features
   op: /project/solorio/prasha/ds/processed_data/user_feature_dumps
   get features in the form of dict/Counter
   LOTS OF CHANGES:
   code to run parts of the list of users in different processes:
   for_user_features/actual_user_features_divide_data_sge.sh
   temp: scp /Users/shrprasha/Projects/dailystrength/new_data_processing_scripts/get_user_features_divide_data.py $CUSCO:/project/solorio/prasha/ds/code/for_user_features/actual_get_user_features_divide_data.py
   remove the actual later and replace
   in this folder, linguistic.py has been changed -> no nltk.contrib, so syllables is wrong; install nltk-contrib and then change
    old: new_data_processing_scripts/get_user_features.py

4. new_data_processing_scripts/process_two_step_features.py
   for features like word_ngram where we do not know the idx of features beforehand
   qsub /project/solorio/prasha/ds/code/for_user_features/two_step_sge.sh
   qsub /project/solorio/prasha/ds/code/for_user_features/two_step_for_test_sge.sh -> after idxs already created
---------
3. new_data_processing_scripts/get_user_features_stylistic.py
    get style features for age_group analysis and plots
    code and op: same format as 3 above; the output from both of them can be combined for step 5
---------
3. new_data_processing_scripts/get_char_ngram_features.py
    no need for process_two_step_features.py; feature idx calculation done in the same script->should make process_two_step_features.py obsolete for others too; created new class, was not necessary, if all feats were to be calculated at the same time, would be better to merge with Features, but for now since it was calculated separately, creating separate class made it easier
    op: /Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/char_ngram_features
        /Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/char_ngram_features_idx.dump
---------

5. new_data_processing_scripts/create_train_test_liblinear.py
    qsub /project/solorio/prasha/ds/code/for_liblinear_features/liblinear_feature_files_sge.sh
    create feature files
    op: /project/solorio/prasha/ds/processed_data/liblinear_files/word_ngram_features_tf_idf/test/gender

6. qsub /project/solorio/prasha/ds/code/for_run_liblinear/run_logistic_sge.sh
for large dataset: age, both, gender, y1984 in 4 processes
qsub /project/solorio/prasha/ds/code/for_run_liblinear/larger_dataset/run_logistic_sge.sh -> c = x set
qsub /project/solorio/prasha/ds/code/for_run_liblinear/larger_dataset1/run_logistic_sge.sh -> c as parameter
-output in log, therefore change log name before running
# here
/Users/shrprasha/Projects/dailystrength/new_data_processing_scripts/scripts/train_test_liblinear.sh path_to_input_and_output age/gender/both/y1984


---------
3. 0. scp $CUSCO:/project/solorio/prasha/ds/processed_data/user_post_dumps.tar.gz /Users/shrprasha/Projects/resources/dailystrength/ds_processed_data
     untar
    new_data_processing_scripts/dump_users.py
   dump of {uid_age-group:User, ... }
   op: /Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/all_users.dump
    scp /Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/all_users.dump $CUSCO:/project/solorio/prasha/ds/processed_data/users_posts
4. new_data_processing_scripts/train_dev_test_partition.py
   60:20:20; all duplicates half and half in dev and test

Patchups
1. patchups/filter_users_less_than_50_words.py
-- removed all users whose total # of words in all posts < 50; some users even have 0 words for example when all their posts are just single urls
-- removed these users only from train, test and dev
-- these users, their posts and features remain in disease_post_dumps, user_post_dumps, user_feature_dumps


duplicates

word 1,2,3-gram tf idf features 39968155
char 3,4-gram features 1043805

Misc:



Added
2.1. old?: new_data_processing_scripts/get_idf.py
new_data_processing_scripts/get_ngram_idf.py
    get idf from train to get word with tf-idf features
/project/solorio/prasha/ds/code/for_idf/idf_sge.sh

familial and cascading
initial_analysis/classification_on_familial_tokens.py


Note: create_sge_file.py in AA/cluster/setup


Analysis:
new_data_processing_scripts/analysis_on_single_forum/stylistic_changes_single.py-> plot mean, sd of stylistic feats per disease
new_data_processing_scripts/analysis_on_single_forum/stylistic_changes_plot.py-> plot mean, sd of stylistic feats of whole dataset

Extras/Stats:
- get_user_posting_interval(): new_data_processing_scripts/get_stats.py
- check_liblinear_feature_file.py -> to test if file is a.t. liblinear feature file standard
- get distribution and majority baseline -> initial_analysis/get_class_distribution_majority_baseline.py
- initial_analysis/classification_on_familial_tokens.py -> predict only on presence or absence of familial token bucket counts
- stats/stats.py -> disease data distribution
- stats/stats_per_forum.py -> per disease data distribution

updated:
new_data_processing_scripts/analysis_on_single_forum/user.py, new_data_processing_scripts/user.py
new_data_processing_scripts/analysis_on_single_forum/utils.py, new_data_processing_scripts/utils.py

scp -r /Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps/old_all_cont $CUSCO:/project/solorio/prasha/ds/processed_data/user_post_dumps_old_all


created:
new_data_processing_scripts/get_user_features_stylistic.py

STATS:
-- has familial
    23716 train
    7951 test


before:
    user_posts_dump_rootdir = "/project/solorio/prasha/ds/processed_data/user_post_dumps"
    users_dumps_rootdir = "/project/solorio/prasha/ds/processed_data/users_dumps"



No gap, continuous age groups

run on all 78377
but only 78316 had enough text (> 1 word)
short ones:

    user_posts_dump_rootdir = "/project/solorio/prasha/ds/continous_age_groups/user_post_dumps_continuous_age_groups"
    users_dumps_rootdir = "/project/solorio/prasha/ds/processed_data/user_dumps_old_all"
    idf_fpath = "/project/solorio/prasha/ds/continous_age_groups/word_idfs_cont.json"
    features_dumpdir = "/project/solorio/prasha/ds/continous_age_groups/user_feature_dumps_cont"
    feature_dumps_rootdir = "/project/solorio/prasha/ds/continous_age_groups/user_feature_dumps_cont"
    disease_dump_rootdir = "/project/solorio/prasha/ds/processed_data/disease_post_dumps_wo_postids"

scp /Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps/old_cont_all/dev.dump $CUSCO:/project/solorio/prasha/ds/processed_data/user_dumps_old_all/dev.dump




