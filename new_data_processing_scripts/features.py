import math

__author__ = 'shrprasha'

from collections import Counter
from dill import load, dump
import os
import utils
import re
from user import Post, User
from linguistic import Linguistic
import json


### all features are stored as dict and not as list since clean_features(self, features) removes the features whose vals are 0.


def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

def load_json(fpath):
    with open(fpath, "r") as fhandle:
        return json.load(fhandle)

def create_dumps(obj, fpath):
    with open(fpath, "w") as fhandle:
        dump(obj, fhandle)

def regex_split(regex, l1):
    tokens = []
    for token in l1:
        tokens.extend(re.split(regex, token))
    return tokens

class UsernameFeature:
    def __init__(self, dumpdir):
        self.uname_dumpdir = dumpdir
        self.all_features_list = {}
        # created so as not to update all_features_list for test;
        # so that will not need to test for username partitions not existing in train
        self.mode = "train"

    @property
    def mode(self):
        return self.mode

    @mode.setter
    def mode(self, mode):
        self.mode = mode

    def dump_all_features(self, fname, user):
        self.fname = fname + ".dump"
        self.get_username_partition_features(user.uname)

    def get_username_partition_features(self, username):
        partitions = self.get_username_partitions(username)
        username_partition_features = {}
        for partition in partitions:
            if self.mode == "train":
                feature_idx = self.get_feature_idx(partition)
            else:
                feature_idx = self.all_features_list.get(partition, 0)
            if feature_idx:
                username_partition_features[feature_idx] = 1
        self.dump_features_non_zero(self.uname_dumpdir, username_partition_features)

    def get_username_partitions(self, username):
        parts = regex_split(r"[_\.-]", [username])
        parts = regex_split('(\d+)', parts)
        fp = [x for x in parts]
        for part in parts:
            pp = re.findall('[A-Z][^A-Z]*', part)
            fp.extend(pp)
        if not fp:
            fp = [re.sub(r'[0-9]+', "_", username)]
        return set([x for x in fp if x])

    def get_feature_idx(self, feature_name):
        if feature_name not in self.all_features_list:
            self.all_features_list[feature_name] = len(self.all_features_list) + 1 #  since features start from 1
        return self.all_features_list[feature_name]

    def dump_features_non_zero(self, dirpath, features): # for feature that do not need cleaning; already only non-zero
        with open(os.path.join(dirpath, self.fname), "w") as opfhandle:
            dump(features, opfhandle)

    def dump_feature_name_idx_list(self, fpath):
        create_dumps(self.all_features_list, fpath)

# char ngram features directly converted to idx
class CharNgramFeatures:
    def __init__(self, dumpdir, idf_path="", total_no_of_train_docs=0):
        self.all_features_list = {}
        self.char_ngram_dumpdir = dumpdir
        if idf_path:
            self.idfs = load_json(idf_path)
        if total_no_of_train_docs:
            self.total_no_of_train_docs = total_no_of_train_docs + 1. # +1 for smoothing

    def dump_features(self, dirpath, features):
        clean_features = self.clean_features(features)
        if not os.path.exists(dirpath):
            os.makedirs(dirpath)
        with open(os.path.join(dirpath, self.fname), "w") as opfhandle:
            dump(clean_features, opfhandle)

    def clean_features(self, features):
        return dict((k, v) for k, v in features.iteritems() if v != 0)

    def dump_all_features(self, fname, posts):
        self.fname = fname + ".dump"
        self.posts = posts
        self.char_ngrams = []
        for post in posts:
            text = " ".join(post.text.split())  #  remove multiple spaces
            if len(text) > 1:
                self.char_ngrams.extend(utils.get_ngrams_from_list(text, 3, 4))
        if len(self.char_ngrams) > 1:
            self.get_char_ngram_features_tf_idf()

    def get_char_ngram_features(self):
        features = Counter(self.char_ngrams)
        char_ngrams_idx_features = {}
        for ngram, feature_val in features.iteritems():
            feature_idx = self.get_feature_idx(ngram)
            char_ngrams_idx_features[feature_idx] = feature_val
        self.dump_features_non_zero(self.char_ngram_dumpdir, char_ngrams_idx_features)

    def get_char_ngram_features_tf_idf(self):
        features = Counter(self.char_ngrams)
        tf_idf_features = {}
        max_tf = max(features.values()) * 1.0
        for ngram, tf in features.iteritems():
            feature_idx = self.get_feature_idx(ngram)
            tf /= max_tf
            idf = math.log(self.total_no_of_train_docs / (1 + self.idfs.get(ngram, 0)), 2)
            tf_idf_features[feature_idx] = tf * idf
        self.dump_features_non_zero(self.char_ngram_dumpdir, tf_idf_features)

    #  return if present, else update
    def get_feature_idx(self, feature_name):
        if feature_name not in self.all_features_list:
            self.all_features_list[feature_name] = len(self.all_features_list) + 1 #  since features start from 1
        return self.all_features_list[feature_name]

    def dump_features_non_zero(self, dirpath, features): # for feature that do not need cleaning; already only non-zero
        with open(os.path.join(dirpath, self.fname), "w") as opfhandle:
            dump(features, opfhandle)

    def dump_feature_name_idx_list(self, fpath):
        create_dumps(self.all_features_list, fpath)



class StylisticFeatures:

    def __init__(self, dumpdir):
        self.all_features_list = {}
        self.dumpdir = dumpdir

    def dump_features(self, dirpath, features):
        clean_features = self.clean_features(features)
        if not os.path.exists(dirpath):
            os.makedirs(dirpath)
        with open(os.path.join(dirpath, self.fname), "w") as opfhandle:
            dump(clean_features, opfhandle)

    def clean_features(self, features):
        return dict((k, v) for k, v in features.iteritems() if v != 0)

    def find_matches(self, text, regex_list):
        regx_matches = []
        for regxp in regex_list:
            rexp = re.compile(regxp, re.IGNORECASE)
            matches = rexp.findall(text)
            regx_matches.extend(matches)
        return regx_matches

    def dump_all_features(self, fname, posts):
        self.fname = fname + ".dump"
        self.posts = posts
        self.text = ""
        self.token_tags = []
        self.aggr_stylistic_features = {}
        linguistic = Linguistic()
        total_non_empty_posts = 0
        for post in posts:
            stylistic_features = linguistic.get_stylistic_features(post.text, post.token_tags)
            if stylistic_features:
                self.update_aggr_stylistic_features(stylistic_features)
                total_non_empty_posts += 1
        self.aggr_stylistic_features.update((x, y*1.0/total_non_empty_posts) for x, y in self.aggr_stylistic_features.iteritems())
        print "feat1: ", len(self.aggr_stylistic_features), self.aggr_stylistic_features
        stylistic_rootdir = os.path.join(self.dumpdir, "stylistic_features")
        stylistic_features_idx = {}
        for idx, feat in enumerate(sorted(self.aggr_stylistic_features.keys())):
            stylistic_features_idx[idx+1] = self.aggr_stylistic_features[feat]
        # self.dump_features(stylistic_rootdir, self.aggr_stylistic_features)
        self.dump_features(stylistic_rootdir, stylistic_features_idx)

    def update_aggr_stylistic_features(self, stylistic_features):
        if self.aggr_stylistic_features:
            for feature, value in stylistic_features.iteritems():
                self.aggr_stylistic_features[feature] += value
        else:
            for feature, value in stylistic_features.iteritems():
                self.aggr_stylistic_features[feature] = value


# word ngram tf idf features not directly converted to idx
class Features:

    def __init__(self, dumpdir, idf_path="", total_no_of_train_docs=0):
        self.all_features_list = {}
        self.dumpdir = dumpdir
        self.dumpdir_needs_further_processing = os.path.join(dumpdir, "needs_further_processing")
        # TEMP: uncomment
        # if not os.path.exists(self.dumpdir_needs_further_processing):
        #     os.makedirs(self.dumpdir_needs_further_processing)

        ### for get_word_ngram_features_tf_idf features
        self.word_ngram_rootdir = os.path.join(self.dumpdir_needs_further_processing, "word_ngram_features_tf_idf")
        if idf_path:  ## only for word_tf_idf features
            ## here
            self.idfs = load_json(idf_path)
            ## cluster
            # self.idfs = load_dumps(idf_path)
            print "done loading"
        if total_no_of_train_docs:
            self.total_no_of_train_docs = total_no_of_train_docs + 1. # +1 for smoothing


    def dump_all_features(self, fname, posts):
        self.fname = fname + ".dump"
        self.posts = posts
        #TEMP: uncomment
        self.text = ""
        self.ngrams = []

        for post in posts:
            #TEMP: uncomment
            self.text += post.text + "\n"
            tokens = [token for token, _ in post.token_tags]
            # print tokens
            post_ngrams = utils.get_ngrams_from_list(tokens, 1, 3)
            self.ngrams.extend(post_ngrams)

        if len(self.ngrams) > 1:
            self.get_word_ngram_features_tf_idf()
            self.get_familial_token_feature()
            self.get_disease_features()
        else:
            print "too small"


    def dump_features(self, dirpath, features):
        clean_features = self.clean_features(features)
        # if not os.path.exists(dirpath):
        #     os.makedirs(dirpath)
        with open(os.path.join(dirpath, self.fname), "w") as opfhandle:
            dump(clean_features, opfhandle)

# no need to clean because will never be zero
    def dump_features_non_zero(self, dirpath, features): # for feature that do not need cleaning; already only non-zero
        with open(os.path.join(dirpath, self.fname), "w") as opfhandle:
            dump(features, opfhandle)

    def clean_features(self, features):
        return dict((k, v) for k, v in features.iteritems() if v != 0)

    def find_matches(self, text, regex_list):
        regx_matches = []
        for regxp in regex_list:
            rexp = re.compile(regxp, re.IGNORECASE)
            matches = rexp.findall(text)
            regx_matches.extend(matches)
        return regx_matches

    def get_bucket_count_each_token(self, matches, buckets):
        counts_bucket = {}
        counts_each_token = []
        words = []
        bc = {}
        # init
        for bucket in buckets:
            counts_bucket[bucket] = 0
            ###
            bc[bucket] = []
        #count word in bucket
        for match in matches:
            word = match.lower()
            temp = sum(counts_bucket.values())
            for bucket, bucket_tokens in buckets.iteritems():
                if word in bucket_tokens:
                    counts_bucket[bucket] += 1
                    ###
                    bc[bucket].append(word)
            if temp == sum(counts_bucket.values()):
                print match
                print "error:", word, counts_bucket
            words.append(word)
        words_counter = Counter(words)
        for token in self.each_token:
            if token in words_counter:
                counts_each_token.append(words_counter[token])
            else:
                counts_each_token.append(0)
        #print bc
        return counts_bucket, counts_each_token

    #TWO STEP FEATURE: aggregate with other users' features for feature number
    def get_word_ngram_feature(self):
        features = Counter(utils.get_ngrams_from_list(self.tokens, 1, 3))
        word_ngram_rootdir = os.path.join(self.dumpdir_needs_further_processing, "word_ngram_features")
        self.dump_features(word_ngram_rootdir, features)


    def get_word_ngram_features_tf_idf(self):
        features = Counter(self.ngrams)
        tf_idf_features = {}
        max_tf = max(features.values()) * 1.0
        for feature, tf in features.iteritems():
            feature = " ".join(feature)
            tf /= max_tf
            idf = math.log(self.total_no_of_train_docs / (1 + self.idfs.get(feature, 0)), 2)
            tf_idf_features[feature] = tf * idf
        self.dump_features_non_zero(self.word_ngram_rootdir, tf_idf_features)


    def get_familial_token_feature(self):
        regex_list_familial = [
            r"\bmy\b\W+\b((?:son|daughter|grandson|granddaughter|father|mother|brother|sister|uncle|aunt|cousin|nephew|niece|family|godson|goddaughter|grandchild|grandmother|grandfather|husband|wife|bf|gf|boyfriend|girlfriend|hubby|baby|babies|child|children|kids|mom|parent))\W*\b",
            r"\b(?:DH|DS|DD|DW)\b"]
        gender_buckets = {"male": [u'wife', u'gf', u'girlfriend', u'dw'],
                          "female": [u'husband', u'bf', u'boyfriend', u'dh', u'hubby'],
                          "neutral": [u'ds', u'dd', u'son', u'daughter', u'grandson', u'granddaughter', u'father',
                                      u'mother', u'brother', u'sister', u'uncle', u'aunt', u'cousin', u'nephew', u'niece',
                                      u'family', u'godson', u'goddaughter', u'grandchild', u'grandmother', u'grandfather',
                                      u'baby', u'babies', u'child', u'children', u'kids', u'mom', u'parent']}
        self.each_token = [u'son', u'daughter', u'grandson', u'granddaughter', u'father', u'mother', u'brother', u'sister',
                      u'uncle', u'aunt', u'cousin', u'nephew', u'niece', u'family', u'godson', u'goddaughter',
                      u'grandchild', u'grandmother', u'grandfather', u'husband', u'wife', u'bf', u'gf', u'boyfriend',
                      u'girlfriend', u'dh', u'ds', u'dd', u'dw', u'baby', u'babies', u'child', u'children', u'kids', u'mom',
                      u'parent', u'hubby']
        familial_matches = self.find_matches(self.text, regex_list_familial)

        counts_bucket, counts_each_token = self.get_bucket_count_each_token(familial_matches, gender_buckets)
        #since order is important
        counts_bucket_idx_feature = dict(zip(range(1, len(counts_bucket) + 1), [counts_bucket["male"], counts_bucket["female"], counts_bucket["neutral"]]))
        counts_each_token_idx_feature = dict(zip(range(1, len(counts_each_token) + 1), counts_each_token))
        each_token_rootdir = os.path.join(self.dumpdir, "familial_features")
        buckets_rootdir = os.path.join(self.dumpdir, "familial_gender_bucket_features")
        self.dump_features(each_token_rootdir, counts_each_token_idx_feature)
        self.dump_features(buckets_rootdir, counts_bucket_idx_feature)

    def get_disease_features(self):
        # diseases = ["Acne", "Fructose_Intolerance", "Infertility", "Miscarriage-Stillbirth", "Physical-Emotional-Abuse", "Widows-Widowers"]
        diseases = ["Acne", "ADHD-ADD", "Agoraphobia-and-Social-Anxiety", "Alzheimer_Disease", "Anger-Management", "Anxiety", "Asperger_Syndrome", "Asthma", "Atrial_Fibrillation", "Autism", "Back-Pain", "Bereavement", "Body-Modification", "Bone_Cancer", "Brain_Injury", "Caregivers", "Celiac-Disease", "Cerebral-Palsy", "Chronic-Fatigue-Syndrome", "Chronic_Pain", "Cirrhosis", "Codependency", "Color-Blindness", "Coming-Out", "Common_Variable_Immunodeficiency", "COPD", "Crohn_Disease_Ulcerative_Colitis", "Deep_Vein_Thrombosis", "Depression_Teen", "Diabetes-Type-1", "Diabetes-Type-2", "Diets_Weight_Maintenance", "Dizziness-Vertigo", "Down-Syndrome", "Dry-Eyes", "Dyslexia", "Endometriosis", "Environmental-Allergies", "Epilepsy-Seizures", "Families-and-Friends-Of-Addicts", "Families-Friends-of-Gays-Lesbians", "Families_of_Prisoners", "Family_Friends_of_Bipolar", "Family-Issues", "Fatty-Acid-Oxidation-Disorders", "Fatty-Liver-Disease", "Female-Sexual-Issues", "Financial_Challenges", "Food-Addiction", "Food-Allergies", "Fructose-Intolerance", "Gambling_Addiction_Recovery", "Gay_Lesbian_Teens", "Grandparents_Raising_Children", "Graves_Disease", "Healthy_Relationships", "Healthy_Sex", "Hepatitis_C", "Hidradenitis_Suppurativa", "HPV", "Hypothyroidism", "Immigration_Law", "Incest_Survivors", "Insomnia", "Irritable_Bowel_Syndrome", "Lesbian_Relationship_Challenges", "Loneliness", "Lung_Cancer", "Lupus", "Lyme_Disease", "Menopause", "Migraine_Headaches", "Migraine", "Miscarriage", "Multiple_Sclerosis", "Myasthenia_Gravis", "Narcolepsy", "Obesity", "Obsessive_Compulsive_Disorder", "Pancreatitis", "Panic_Attacks", "Parkinson_Disease", "Personality_Disorders", "Physical_Emotional_Abuse", "Polycystic_Ovarian_Syndrome", "Post_Traumatic_Stress_Disorder", "Pregnancy_After_Loss_Infertility", "Pregnancy", "Pseudotumor_Cerebri", "Pulmonary_Embolism", "Rape", "Rheumatoid_Arthritis", "Schizophrenia", "Self_Injury", "Sex_Pornography_Addiction", "Sexual_Abuse", "Smoking_Addiction", "Smoking_Addiction_Recovery", "Stillbirth", "Tinnitus", "Trichotillomania_Hair_Pulling", "Trying_To_Conceive", "War_In_Iraq", "Widows_Widowers"]
        disease_problem_reply_counts = [0] * len(diseases) * 2
        for post in self.posts:
            idx = diseases.index(post.disease)
            idx += 0 if post.problem_or_reply == "problem" else len(diseases)
            disease_problem_reply_counts[idx] += 1
        diseases_rootdir = os.path.join(self.dumpdir, "diseases_features")
        disease_problem_reply_features = dict(zip(range(1, len(disease_problem_reply_counts) + 1), disease_problem_reply_counts))
        self.dump_features(diseases_rootdir, disease_problem_reply_features)

##  TEMP
    # def get_word_ngram_tf_idf(self):
    #     print self.ngrams
    #     features = Counter(self.ngrams)
    #     tf_idf_features = {}
    #     max_tf = max(features.values()) * 1.0
    #     for feature, tf in features.iteritems():
    #         feature = " ".join(feature)
    #         print feature, tf, self.idfs.get(feature, 0),
    #         tf /= max_tf
    #         idf = math.log(4. / (1 + self.idfs.get(feature, 0)), 2)
    #         print tf, idf
    #         # print feature, tf, self.idfs.get(feature, 0)
    #         tf_idf_features[feature] = tf * idf
    #     self.dump_features_non_zero(self.word_ngram_rootdir, tf_idf_features)



if __name__ == "__main__":
    # tx = StylisticFeatures("/Users/shrprasha/del/ds_features")
    tx = Features("/Users/shrprasha/del/ds_features")
    text = "fun is my the girlfriend and this the"
    # text = "the the"
    posts = []
    # print tx.clean_features({1: 1, 2: 0, 3: 0})
    text, token_tags = utils.get_token_tags(text)
    p = Post(text, "reply", "Acne", 13, token_tags, "1098", "1234")
    tx.dump_all_features("1098", [p])
    # print "features1", tx.dump_all_features("1098", [p])
