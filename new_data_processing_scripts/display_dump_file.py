__author__ = 'shrprasha'

from pickle import load, dump
from user import User, Post
import os


def print_obj(obj):
    attrs = vars(obj)
    print ', '.join("%s: %s" % item for item in attrs.items())

def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

def load_and_print_dump(fpath):
    overall_dump = load(open(fpath, "r"))
    if type(overall_dump) is dict:
        print "length:", len(overall_dump)
        for k, v in overall_dump.iteritems():
            print k, v
    elif type(overall_dump) is list:
        print "length:", len(overall_dump)
        for x in overall_dump:
            print x
    else:
        print overall_dump

def load_and_print_dump_len_sample(fpath):
    overall_dump = load(open(fpath, "r"))
    print type(overall_dump)
    if type(overall_dump) is dict:
        print "length:", len(overall_dump)
        for k, v in overall_dump.iteritems():
            print k, v
            break
    elif type(overall_dump) is list:
        print "length:", len(overall_dump)
        for x in overall_dump:
            # print x
            print_obj(x)
            break


if __name__ == "__main__":
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/stylistic_features/93_31-48.dump"
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/stylistic_with_feature_names/93_31-48.dump"
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps/train.dump"
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/idf.dump"
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/disease_post_dumps/Fructose-Intolerance_posts.dump"
    # # load_and_print_dump(fpath)

    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/disease_post_dumps_with_rid/Acne_posts.dump"

    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps/all_users.dump"
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps/train.dump"
    # fpath = "/home/ritual/resources/dailystrength/processed_data/users_dumps/all_users.dump"
    # fpath = "/home/ritual/resources/ds_cluster/processed_data/user_dumps_old_all/train.dump"
    # fpath = "/home/ritual/resources/ds_cluster/processed_data/users_dumps/all_users.dump"
    # fpath = "/home/ritual/resources/ds_cluster/processed_data/user_dumps_old_all/old_all_cont/train.dump"
    fpath = "/home/ritual/resources/dailystrength/after_sklearn/all_users_posts.pkl"
    load_and_print_dump_len_sample(fpath)

    # fdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps"
    # users_zero_tokens = ["111873_12-16", "160200_19-28", "1783261_12-16", "2248546_19-28", "2429242_19-28", "272893_12-16", "289374_12-16", "3345207_31-48", "3395813_19-28", "349231_19-28", "356976_19-28", "4166797_19-28", "484817_19-28", "541528_19-28", "563917_31-48", "568223_31-48", "69116_12-16", "81396_19-28", "94776_12-16"]
    # for userid in users_zero_tokens:
    #     user_posts = load_dumps(os.path.join(fdir, userid))
    #     for post in user_posts:
    #         attrs = vars(post)
    #         print ', '.join("%s: %s" % item for item in attrs.items())

    # acne_person = "2248546"
    # fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/disease_post_dumps_with_rid/Acne_posts.dump"
    # acne_posts = load_dumps(fpath)
    # for post in acne_posts:
    #     if post.uid == acne_person:
    #         print_obj(post)


