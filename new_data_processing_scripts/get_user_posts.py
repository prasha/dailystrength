__author__ = 'shrprasha'

import os
from dill import load, dump
from user import User, Post
import utils


def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

def get_age_group(age):
    for min, max in age_groups:
        if(min <= age <= max):
            if(min == 65):
                return "65-xx"
            # elif(min == 0):
            #     return "18-24"
            else:
                return str(min) + "-" + str(max)
    # print "ERROR: no age group", uid, age
    return None


if __name__ == "__main__":
    # age_groups = [(0, 24), (25, 34), (35, 49), (50, 64), (65, 200)]
    age_groups = [(12, 17), (18, 29), (30, 49), (50, 64), (65, 200)]
    # "12-17", "18-29", "30-49", "50-64", "65-200"
    #  not using borderline ages
    # age_groups = [(12, 16), (19, 28), (31, 48), (51, 63), (66, 200)]
    # disease_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/disease_post_dumps"
    # user_posts_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps"
    disease_dump_rootdir = "/project/solorio/prasha/ds/processed_data/disease_post_dumps_wo_postids"
    user_posts_dump_rootdir = "/project/solorio/prasha/ds/processed_data/user_post_dumps_continuous_age_groups"
    # age_gender_map = get_user_details(age_gender_file)
    all_files = sorted(next(os.walk(disease_dump_rootdir))[2])
    all_posts = []
    for fname in all_files:
        with open(os.path.join(disease_dump_rootdir, fname), "r") as fhandle:
            all_posts.extend(load(fhandle))

    all_user_posts = {}  # (uid, age_group): post_list
    for post in all_posts:
        uid = post.uid
        age_group = get_age_group(post.age_when_posted)
        if age_group:
            if (uid, age_group) in all_user_posts:
                all_user_posts[(uid, age_group)].append(post)
            else:
                all_user_posts[(uid, age_group)] = [post]

    user_posts = {}
    uids = []
    for uid_age_group, post_list in all_user_posts.iteritems():
        uid, age_group = uid_age_group
        # uname, gender = age_gender_map[uid]
        # user = User(uid, uname, gender, age_group)
        # user_posts[user] = post_list
        with open(os.path.join(user_posts_dump_rootdir, uid + "_" + age_group), "w") as opfhandle:
            dump(post_list, opfhandle)








