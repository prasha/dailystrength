__author__ = 'shrprasha'
import os
from dill import load
from user import User, Post

# def check_user_features_empty():
#     features_dumpdir = "/project/solorio/prasha/ds/data/output/user_feature_dumps"
#     familial_dumpdir = os.path.join(features_dumpdir, "familial_features")

def get_files_in_folder(folder):
    all_files = sorted(next(os.walk(folder))[2])
    if ".DS_Store" in all_files:
        all_files.remove(".DS_Store")
    return all_files

def get_duplicate_fname_users_list():
    users_dump_fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/all_users.dump"
    all_users = load(open(users_dump_fpath, "r"))
    uids = {}
    duplicates = []
    duplicate_uids = []
    for fname in sorted(all_users):
        uid = fname.split("_")[0]
        if uid in uids:
            duplicates.append([uids[uid], fname])
            duplicate_uids.append(uid)
        else:
            uids[uid] = fname
    print len(duplicate_uids)
    return duplicates, duplicate_uids

def get_allowed_limit(fname, lower_or_upper):
    age_group = fname.split("_")[1]
    lower, upper = age_group.split("-")
    if lower_or_upper == "lower":
        limit = int(lower)
    else:
        if upper == "xx":
            limit = 65 + (35-allowance-25)
        else:
            limit = int(upper)
    return limit


def age_for_duplicates():
    duplicates, duplicate_uids = get_duplicate_fname_users_list()
    users_posts_dumps_dir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps" #  to get the age_groups
    user_post_dump_files = get_files_in_folder(users_posts_dumps_dir)
    for lower_duplicate, upper_duplicate in duplicates:
        with open(os.path.join(users_posts_dumps_dir, lower_duplicate), "r") as lower_fhandle:
            lower_user_posts = load(lower_fhandle)
        with open(os.path.join(users_posts_dumps_dir, upper_duplicate), "r") as upper_fhandle:
            upper_user_posts = load(upper_fhandle)
        lower_limit = get_allowed_limit(lower_duplicate, "lower")
        upper_limit = get_allowed_limit(upper_duplicate, "upper")
        count_lower = 0
        count_upper = 0
        for post in lower_user_posts:
            if lower_limit <= post.age_when_posted <= lower_limit + allowance:
                count_lower += 1
        for post in upper_user_posts:
            if upper_limit - allowance <= post.age_when_posted <= upper_limit:
                count_upper += 1
        print lower_duplicate, count_lower, upper_duplicate, count_upper

def no_familial_features():
    familial_features_dump_dir = "/Users/shrprasha/Projects/resources/dailystrength/user_feature_dumps/familial_features"
    files = get_files_in_folder(familial_features_dump_dir)
    count = 0
    for fname in files:
        with open(os.path.join(familial_features_dump_dir, fname), "r") as fhandle:
            familial_features = load(fhandle)
        if len(familial_features) > 0:
            count += 1
            print fname, len(familial_features)
    print len(files), count


if __name__ == "__main__":
    allowance = 8
    # age_for_duplicates()
    no_familial_features()