__author__ = 'shrprasha'

import os
from dill import load, dump
from features import Features
import sys
# from joblib import Parallel, delayed


def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

def get_features(fname, opfpath):
    if not os.path.isfile(opfpath):
        with open(os.path.join(user_posts_dump_rootdir, fname), "r") as fhandle:
            print "feat", fname
            user_posts = load(fhandle)
            features.dump_all_features(fname, user_posts)

if __name__ == "__main__":
    type = sys.argv[1]
    process_no = int(sys.argv[3])
    total_processes = int(sys.argv[2])

    ## here
    # user_posts_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps"
    # features_dumpdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps"
    # idf_fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/idfs"
    # users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps"

    ## cluster
    # user_posts_dump_rootdir = "/project/solorio/prasha/ds/processed_data/user_post_dumps"
    # features_dumpdir = "/project/solorio/prasha/ds/processed_data/user_feature_dumps"
    # idf_fpath = "/project/solorio/prasha/ds/processed_data/idf_train.dump"
    # users_dumps_rootdir = "/project/solorio/prasha/ds/processed_data/users_dumps"

    ## continuous
    user_posts_dump_rootdir = "/project/solorio/prasha/ds/continous_age_groups/user_post_dumps_continuous_age_groups"
    users_dumps_rootdir = "/project/solorio/prasha/ds/processed_data/user_dumps_old_all"
    idf_fpath = "/project/solorio/prasha/ds/continous_age_groups/word_idfs_cont.json"
    features_dumpdir = "/project/solorio/prasha/ds/continous_age_groups/user_feature_dumps_cont"

    all_users = sorted(load_dumps(os.path.join(users_dumps_rootdir, type + ".dump")).keys())
    parts = len(all_users)/total_processes
    if process_no + 1 == total_processes:  ### last process
        users = all_users[process_no * parts:]
    else:
        users = all_users[process_no * parts:(process_no+1) * parts]

    ## word tf-idf
    # features = Features(features_dumpdir, idf_fpath, 42968)
    # for fname in sorted(users):
    #     get_features(fname, os.path.join(features_dumpdir, "needs_further_processing", "word_ngram_features_tf_idf", fname + ".dump"))

    # disease and familial
    features = Features(features_dumpdir, idf_fpath, 47027)
    for fname in sorted(users):
        get_features(fname, os.path.join(features_dumpdir, "diseases_features", fname + ".dump"))


