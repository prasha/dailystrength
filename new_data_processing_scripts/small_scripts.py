__author__ = 'shrprasha'
import os
import itertools

def join_peopleLists(rootdir):
    diseases = sorted(next(os.walk(rootdir))[1])
    new_people_lists_fpath = os.path.join(rootdir, "peopleLists.txt")
    lines = []
    for disease in diseases:
        people_list_fpath = os.path.join(rootdir, disease, "peopleLists.txt")
        if not os.path.isfile(people_list_fpath):
            continue
        with open(people_list_fpath, "r") as ipfhandle:
            lines.extend(ipfhandle.readlines())
    lines = set(lines)
    with open(new_people_lists_fpath, "w") as fhandle:
        for line in sorted(lines):
            fhandle.write(line)


def sort_emotion_words(words):
    full_words = []
    prefixes = []
    for word in words:
        if "*" in word:
            prefixes.append(word[:-1])
        else:
            full_words.append(word)
    print full_words
    print prefixes


def skip_n_grams(tokens, k, n):
    skip_ngrams = []
    for i in range(0, len(tokens) - n):
        print tokens[i:i+k+n]
        for x in itertools.combinations(tokens[i:i+k+n], n):
            # print x
            skip_ngrams.append(x)
    return list(sorted(set(skip_ngrams)))


if __name__ == "__main__":
    # rootdir = "/project/solorio/prasha/ds/data/new_for_age_range_output"
    # rootdir = "/Users/shrprasha/Projects/resources/dailystrength/nicolas_data"
    # join_peopleLists(rootdir)

    # pos_emotions = ["accept", "freed*", "partie*", "accepta*", "freeing", "party*", "accepted", "freely", "passion*", "accepting", "freeness", "peace*", "accepts", "freer", "perfect*", "active*", "frees*", "play", "admir*", "friend*", "played", "ador*", "fun", "playful*", "advantag*", "funn*", "playing", "adventur*", "genero*", "plays", "affection*", "gentle", "pleasant*", "agree", "gentler", "please*", "agreeab*", "gentlest", "pleasing", "agreed", "gently", "pleasur*", "agreeing", "giggl*", "popular*", "agreement*", "giver*", "positiv*", "agrees", "giving", "prais*", "alright*", "glad", "precious*", "amaz*", "gladly", "prettie*", "amor*", "glamor*", "pretty", "amus*", "glamour*", "pride", "aok", "glori*", "privileg*", "appreciat*", "glory", "prize*", "assur*", "good", "profit*", "attachment*", "goodness", "promis*", "attract*", "gorgeous*", "proud*", "award*", "grace", "radian*", "awesome", "graced", "readiness", "beaut*", "graceful*", "ready", "beloved", "graces", "reassur*", "benefic*", "graci*", "relax*", "benefit", "grand", "relief", "benefits", "grande*", "reliev*", "benefitt*", "gratef*", "resolv*", "benevolen*", "grati*", "respect ", "benign*", "great", "revigor*", "best", "grin", "reward*", "better", "grinn*", "rich*", "bless*", "grins", "ROFL", "bold*", "ha", "romanc*", "bonus*", "haha*", "romantic*", "brave*", "handsom*", "safe*", "bright*", "happi*", "satisf*", "brillian*", "happy", "save", "calm*", "harmless*", "scrumptious*", "care", "harmon*", "secur*", "cared", "heartfelt", "sentimental*", "carefree", "heartwarm*", "share", "careful*", "heaven*", "shared", "cares", "heh*", "shares", "caring", "helper*", "sharing", "casual", "helpful*", "silli*", "casually", "helping", "silly", "certain*", "helps", "sincer*", "challeng*", "hero*", "smart*", "champ*", "hilarious", "smil*", "charit*", "hoho*", "sociab*", "charm*", "honest*", "soulmate*", "cheer*", "honor*", "special", "cherish*", "honour*", "splend*", "chuckl*", "hope", "strength*", "clever*", "hoped", "strong*", "comed*", "hopeful", "succeed*", "comfort*", "hopefully", "success*", "commitment*", "hopefulness", "sunnier", "compassion*", "hopes", "sunniest", "compliment*", "hoping", "sunny", "confidence", "hug ", "sunshin*", "confident", "hugg*", "super", "confidently", "hugs", "superior*", "considerate", "humor*", "support", "contented*", "humour*", "supported", "contentment", "hurra*", "supporter*", "convinc*", "ideal*", "supporting", "cool", "importan*", "supportive*", "courag*", "impress*", "supports", "create*", "improve*", "suprem*", "creati*", "improving", "sure*", "credit*", "incentive*", "surpris*", "cute*", "innocen*", "sweet", "cutie*", "inspir*", "sweetheart*", "daring", "intell*", "sweetie*", "darlin*", "interest*", "sweetly", "dear*", "invigor*", "sweetness*", "definite", "joke*", "sweets", "definitely", "joking", "talent*", "delectabl*", "joll*", "tehe", "delicate*", "joy*", "tender*", "delicious*", "keen*", "terrific*", "deligh*", "kidding", "thank", "determina*", "kind", "thanked", "determined", "kindly", "thankf*", "devot*", "kindn*", "thanks", "digni*", "kiss*", "thoughtful*", "divin*", "laidback", "thrill*", "dynam*", "laugh*", "toleran*", "eager*", "libert*", "tranquil*", "ease*", "like", "treasur*", "easie*", "likeab*", "treat", "easily", "liked", "triumph*", "easiness", "likes", "true ", "easing", "liking", "trueness", "easy*", "livel*", "truer", "ecsta*", "LMAO", "truest", "efficien*", "LOL", "truly", "elegan*", "love", "trust*", "encourag*", "loved", "truth*", "energ*", "lovely", "useful*", "engag*", "lover*", "valuabl*", "enjoy*", "loves", "value", "entertain*", "loving*", "valued", "enthus*", "loyal*", "values", "excel*", "luck", "valuing", "excit*", "lucked", "vigor*", "fab", "lucki*", "vigour*", "fabulous*", "lucks", "virtue*", "faith*", "lucky", "virtuo*", "fantastic*", "madly", "vital*", "favor*", "magnific*", "warm*", "favour*", "merit*", "wealth*", "fearless*", "merr*", "welcom*", "festiv*", "neat*", "well*", "fiesta*", "nice*", "win", "fine", "nurtur*", "winn*", "flatter*", "ok", "wins", "flawless*", "okay", "wisdom", "flexib*", "okays", "wise*", "flirt*", "oks", "won", "fond", "openminded*", "wonderf*", "fondly", "openness", "worship*", "fondness", "opportun*", "worthwhile", "forgave", "optimal*", "wow*", "forgiv*", "optimi*", "yay", "free", "original", "yays", "free*", "outgoing", "freeb*", "painl*", "palatabl*", "paradise"]
    # neg_emotions = ["abandon*", "enrag*", "maddening", "snob*", "abuse*", "envie*", "madder", "sob", "abusi*", "envious", "maddest", "sobbed", "ache*", "envy*", "maniac*", "sobbing", "aching", "evil*", "masochis*", "sobs", "advers*", "excruciat*", "melanchol*", "solemn*", "afraid", "exhaust*", "mess", "sorrow*", "aggravat*", "fail*", "messy", "sorry", "aggress*", "fake", "miser*", "spite*", "agitat*", "fatal*", "miss", "stammer*", "agoniz*", "fatigu*", "missed", "stank", "agony", "fault*", "misses", "startl*", "alarm*", "fear", "missing", "steal*", "alone", "feared", "mistak*", "stench*", "anger*", "fearful*", "mock", "stink*", "angr*", "fearing", "mocked", "strain*", "anguish*", "fears", "mocker*", "strange", "annoy*", "feroc*", "mocking", "stress*", "antagoni*", "feud*", "mocks", "struggl*", "anxi*", "fiery", "molest*", "stubborn*", "apath*", "fight*", "mooch*", "stunk", "appall*", "fired", "moodi*", "stunned", "apprehens*", "flunk*", "moody", "stuns", "argh*", "foe*", "moron*", "stupid*", "argu*", "fool*", "mourn*", "stutter*", "arrogan*", "forbid*", "murder*", "submissive*", "asham*", "fought", "nag*", "suck", "assault*", "frantic*", "nast*", "sucked", "asshole*", "freak*", "needy", "sucker*", "attack*", "fright*", "neglect*", "sucks", "aversi*", "frustrat*", "nerd*", "sucky", "avoid*", "fuck", "nervous*", "suffer", "awful", "fucked*", "neurotic*", "suffered", "awkward*", "fucker*", "numb*", "sufferer*", "bad", "fuckin*", "obnoxious*", "suffering", "bashful*", "fucks", "obsess*", "suffers", "bastard*", "fume*", "offence*", "suspicio*", "battl*", "fuming", "offend*", "tantrum*", "beaten", "furious*", "offens*", "tears", "bitch*", "fury", "outrag*", "teas*", "bitter*", "geek*", "overwhelm*", "temper", "blam*", "gloom*", "pain", "tempers", "bore*", "goddam*", "pained", "tense*", "boring", "gossip*", "painf*", "tensing", "bother*", "grave*", "paining", "tension*", "broke", "greed*", "pains", "terribl*", "brutal*", "grief", "panic*", "terrified", "burden*", "griev*", "paranoi*", "terrifies", "careless*", "grim*", "pathetic*", "terrify", "cheat*", "gross*", "peculiar*", "terrifying", "complain*", "grouch*", "perver*", "terror*", "confront*", "grr*", "pessimis*", "thief", "confus*", "guilt*", "petrif*", "thieve*", "contempt*", "harass*", "pettie*", "threat*", "contradic*", "harm", "petty*", "ticked", "crap", "harmed", "phobi*", "timid*", "crappy", "harmful*", "piss*", "tortur*", "craz*", "harming", "piti*", "tough*", "cried", "harms", "pity*", "traged*", "cries", "hate", "poison*", "tragic*", "critical", "hated", "prejudic*", "trauma*", "critici*", "hateful*", "pressur*", "trembl*", "crude*", "hater*", "prick*", "trick*", "cruel*", "hates", "problem*", "trite", "crushed", "hating", "protest", "trivi*", "cry", "hatred", "protested", "troubl*", "crying", "heartbreak*", "protesting", "turmoil", "cunt*", "heartbroke*", "puk*", "ugh", "cut", "heartless*", "punish*", "ugl*", "cynic*", "hell", "rage*", "unattractive", "damag*", "hellish", "raging", "uncertain*", "damn*", "helpless*", "rancid*", "uncomfortabl*", "danger*", "hesita*", "rape*", "uncontrol*", "daze*", "homesick*", "raping", "uneas*", "decay*", "hopeless*", "rapist*", "unfortunate*", "defeat*", "horr*", "rebel*", "unfriendly", "defect*", "hostil*", "reek*", "ungrateful*", "defenc*", "humiliat*", "regret*", "unhapp*", "defens*", "hurt*", "reject*", "unimportant", "degrad*", "idiot", "reluctan*", "unimpress*", "depress*", "ignor*", "remorse*", "unkind", "depriv*", "immoral*", "repress*", "unlov*", "despair*", "impatien*", "resent*", "unpleasant", "desperat*", "impersonal", "resign*", "unprotected", "despis*", "impolite*", "restless*", "unsavo*", "destroy*", "inadequa*", "revenge*", "unsuccessful*", "destruct*", "indecis*", "ridicul*", "unsure*", "devastat*", "ineffect*", "rigid*", "unwelcom*", "devil*", "inferior*", "risk*", "upset*", "difficult*", "inhib*", "rotten", "uptight*", "disadvantage*", "insecur*", "rude*", "useless*", "disagree*", "insincer*", "ruin*", "vain", "disappoint*", "insult*", "sad", "vanity", "disaster*", "interrup*", "sadde*", "vicious*", "discomfort*", "intimidat*", "sadly", "victim*", "discourag*", "irrational*", "sadness", "vile", "disgust*", "irrita*", "sarcas*", "villain*", "dishearten*", "isolat*", "savage*", "violat*", "disillusion*", "jaded", "scare*", "violent*", "dislike", "jealous*", "scaring", "vulnerab*", "disliked", "jerk", "scary", "vulture*", "dislikes", "jerked", "sceptic*", "war", "disliking", "jerks", "scream*", "warfare*", "dismay*", "kill*", "screw*", "warred", "dissatisf*", "lame*", "selfish*", "warring", "distract*", "lazie*", "serious", "wars", "distraught", "lazy", "seriously", "weak*", "distress*", "liabilit*", "seriousness", "weapon*", "distrust*", "liar*", "severe*", "weep*", "disturb*", "lied", "shake*", "weird*", "domina*", "lies", "shaki*", "wept", "doom*", "lone*", "shaky", "whine*", "dork*", "longing*", "shame*", "whining", "doubt*", "lose", "shit*", "whore*", "dread*", "loser*", "shock*", "wicked*", "dull*", "loses", "shook", "wimp*", "dumb*", "losing", "shy*", "witch", "dump*", "loss*", "sicken*", "woe*", "dwell*", "lost", "sin", "worr*", "egotis*", "lous*", "sinister", "worse*", "embarrass*", "low*", "sins", "worst", "emotional", "luckless*", "skeptic*", "worthless*", "empt*", "ludicrous*", "slut*", "wrong*", "enemie*", "lying", "smother*", "yearn*", "enemy*", "mad", "smug*"]    # sort_emotion_words(pos_emotions)
    # sort_emotion_words(neg_emotions)

    # tokens = [1,2,3,4,5]
    tokens = ["Insurgents", "killed", "in", "ongoing", "fighting"]
    # skip_n_grams([1,2,3,4,5,6], 2, 3)
    print skip_n_grams(tokens, 2, 3)




