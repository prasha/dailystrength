__author__ = 'shrprasha'
from collections import Counter
from pprint import pprint
from dill import load
from user import User, Post
import os

# def check_dump(fpath):
#     with open(fpath, "r") as fhandle:
#         obj = load(fhandle)
#     print obj
#
# check_dump("/Users/shrprasha/del/ds_features/needs_further_processing/word_ngram_features/1098.dump")
# check_dump("/Users/shrprasha/del/ds_features/familial_gender_bucket_features/1098.dump")
# check_dump("/Users/shrprasha/del/ds_features/familial_features/1098.dump")
# check_dump("/Users/shrprasha/del/ds_features/diseases_features/1098.dump")

def get_user_stats():
    lines = open("/Users/shrprasha/del/del.txt", "r").readlines()
    uids = []
    age_groups = []
    for line in lines:
        # print line
        uid, age_group = line.strip().split("_")
        age_groups.append(age_group)
        uids.append(uid)
    uu = Counter(uids)

    stats = Counter(uu.values())

    au = Counter(age_groups)

    # print uu
    print stats
    pprint(au)

    print sum(uu.values()), sum(au.values())

def get_user_stats_from_user_dumps():
    dump_fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/all_users.dump"
    all_users = load(open(dump_fpath, "r"))
    output_dict = {}
    for fname, user in all_users.iteritems():
        age_group = user.age_group
        gender = user.gender.lower()
        if age_group in output_dict:
            if gender in output_dict[age_group]:
                output_dict[age_group][gender] += 1
            else:
                output_dict[age_group][gender] = 1
        else:
            output_dict[age_group] = {gender: 1}
    print output_dict
    return output_dict

def pretty_print(output_dict):
    overall = 7714
    overall_totals = {"female": 0, "male": 0}
    for age_group in sorted(output_dict):
        gender_counts = output_dict[age_group]
        print age_group, "&",
        total = 0
        for gender in sorted(gender_counts):
            count = gender_counts[gender]
            total += count
            print count, "&",
            overall_totals[gender] += count
        print total, "(" + str(round(total * 100.0 / overall, 2)) + "\%) \\\\"

# Total & 4307  (12.94\%)  & 28985 (87.06\%)  & 33292\\ \hline
    print "Total & ", overall_totals["female"], "(" + str(round(overall_totals["female"] * 100.0 / overall, 2)) + "\%) &",
    print overall_totals["male"], "(" + str(round(overall_totals["male"] * 100.0 / overall, 2)) + "\%) &", sum(overall_totals.values())

    if sum(overall_totals.values()) != overall:
        print "ERROR: update overall"

# if __name__ == "__main__":
#     # get_user_stats()
#     pretty_print(get_user_stats_from_user_dumps())

def get_files_in_folder(folder):
    all_files = sorted(next(os.walk(folder))[2])
    if ".DS_Store" in all_files:
        all_files.remove(".DS_Store")
    return all_files

#unused
def get_disease_age_gender_distribution_for_post():
    users_dump_fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/all_users.dump"
    users_posts_dumps_dir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps" #  to get the age_groups
    all_users = load(open(users_dump_fpath, "r"))
    user_post_dump_files = get_files_in_folder(users_posts_dumps_dir)
    disease_distribution_totals = {}
    #  initialize
    diseases = ["Acne", "Fructose_Intolerance", "Infertility", "Miscarriage-Stillbirth", "Physical-Emotional-Abuse", "Widows-Widowers"]
    labels = ["18-24", "25-34", "35-49", "50-64", "65-xx", "female", "male"]
    for disease in diseases:
        disease_distribution_totals[disease] = {}
        for label in labels:
            disease_distribution_totals[disease][label] = 0

    for fname in user_post_dump_files:
        user = all_users[fname]
        age_group = user.age_group
        gender = user.gender.lower()
        with open(os.path.join(users_posts_dumps_dir, fname), "r") as fhandle:
            user_posts = load(fhandle)
        for post in user_posts:
            disease = post.disease
            disease_distribution_totals[disease][age_group] += 1
            disease_distribution_totals[disease][gender] += 1
    print disease_distribution_totals

    for disease in sorted(disease_distribution_totals):
        line = disease
        distribution = disease_distribution_totals[disease]
        total = sum(distribution.values())
        for label in sorted(distribution):
            count = distribution[label]
            line += "\t" + str(round(count * 100.0 / total, 2))
        print line

# def combine_duplicate_user_posts


def get_disease_age_gender_distribution():
    users_dump_fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/all_users.dump"
    users_posts_dumps_dir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps" #  to get the age_groups
    all_users = load(open(users_dump_fpath, "r"))
    user_post_dump_files = get_files_in_folder(users_posts_dumps_dir)
    disease_distribution_totals = {}
    #  initialize
    diseases = ["Acne", "Fructose_Intolerance", "Infertility", "Miscarriage-Stillbirth", "Physical-Emotional-Abuse", "Widows-Widowers"]
    labels = ["18-24", "25-34", "35-49", "50-64", "65-xx", "female", "male"]
    age_labels = ["18-24", "25-34", "35-49", "50-64", "65-xx"]
    gender_labels = ["female", "male"]
    for disease in diseases:
        disease_distribution_totals[disease] = {}
        for label in labels:
            disease_distribution_totals[disease][label] = 0

    for fname in user_post_dump_files:
        user = all_users[fname]
        age_group = user.age_group
        gender = user.gender.lower()
        with open(os.path.join(users_posts_dumps_dir, fname), "r") as fhandle:
            user_posts = load(fhandle)
        user_diseases = []
        for post in user_posts:
            disease = post.disease
            user_diseases.append(disease)
        unq_user_diseases = set(user_diseases)
        # print unq_user_diseases
        for user_disease in unq_user_diseases:
            disease_distribution_totals[user_disease][age_group] += 1
            disease_distribution_totals[user_disease][gender] += 1

            if user_disease == "Fructose_Intolerance":
                print fname

    print disease_distribution_totals

    for disease in sorted(disease_distribution_totals):
        line = disease
        distribution = disease_distribution_totals[disease]
        age_total = distribution["18-24"] + distribution["25-34"] + distribution["35-49"] + distribution["50-64"] + distribution["65-xx"]
        gender_total = distribution["male"] + distribution["female"]

        if age_total != gender_total:
            print "ERROR: ", disease, age_total, gender_total
        # print disease, total
        for label in sorted(distribution):
            count = distribution[label]
            if label in age_labels:
                line += "\t" + str(round(count * 100.0 / age_total, 2))
            elif label in gender_labels:
                line += "\t" + str(round(count * 100.0 / gender_total, 2))
        line += "\t" + str(age_total)
        print line

def get_user_posting_interval_cluster():
    # users_posts_dumps_dir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps" #  to get the age_groups
    users_posts_dumps_dir = "/project/solorio/prasha/ds/data/output/user_post_dumps" #  to get the age_groups
    user_post_dump_files = get_files_in_folder(users_posts_dumps_dir)
    uid_min_max_dict = {}
    for fname in user_post_dump_files:
        uid = fname.split("_")[0]
        if uid not in uid_min_max_dict:
            uid_min_max_dict[uid] = [200, 0]
        with open(os.path.join(users_posts_dumps_dir, fname), "r") as fhandle:
            user_posts = load(fhandle)
        for post in user_posts:
            if post.age_when_posted < uid_min_max_dict[uid][0]:
                uid_min_max_dict[uid][0] = post.age_when_posted
            if post.age_when_posted > uid_min_max_dict[uid][1]:
                uid_min_max_dict[uid][1] = post.age_when_posted
    for fname, min_max in uid_min_max_dict.iteritems():
        min, max = min_max
        print fname, min, max, max-min


def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

#  changed to do it this way so that we do not do it for all users; leave out those in test
def get_user_posting_interval_uids(uids, users_posts_dumps_dir):
    uid_min_max_dict = {}
    for fname in sorted(uids):
        print fname
        uid = fname.split("_")[0]
        if uid not in uid_min_max_dict:
            uid_min_max_dict[uid] = [200, 0]
        with open(os.path.join(users_posts_dumps_dir, fname), "r") as fhandle:
            user_posts = load(fhandle)
        for post in user_posts:
            if post.age_when_posted < uid_min_max_dict[uid][0]:
                uid_min_max_dict[uid][0] = post.age_when_posted
            if post.age_when_posted > uid_min_max_dict[uid][1]:
                uid_min_max_dict[uid][1] = post.age_when_posted
    return uid_min_max_dict

def display(uid_min_max_dict, automatic):
    for fname, min_max in uid_min_max_dict.iteritems():
        min, max = min_max
        print automatic, fname, min, max, max-min+1

def get_user_posting_interval_here():
    users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps"
    train_users = load_dumps(os.path.join(users_dumps_rootdir, "train.dump"))
    dev_users = load_dumps(os.path.join(users_dumps_rootdir, "dev.dump"))
    users_posts_dumps_dir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps" #  to get the age_groups

    uids = train_users.keys()
    uid_min_max_dict = get_user_posting_interval_uids(uids, users_posts_dumps_dir)
    display(uid_min_max_dict, "train")

    uids = dev_users.keys()
    uid_min_max_dict = get_user_posting_interval_uids(uids, users_posts_dumps_dir)
    display(uid_min_max_dict, "dev")

    #small_test
    # uids = load_dumps(os.path.join(users_dumps_rootdir, "subset_for_plots", "train.dump")).keys()
    # uid_min_max_dict = get_user_posting_interval_uids(uids, users_posts_dumps_dir)
    # display(uid_min_max_dict, "dev")


if __name__ == "__main__":
    # get_disease_age_gender_distribution()
    get_user_posting_interval_here()