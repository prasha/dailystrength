__author__ = 'shrprasha'

import os
from pickle import load, dump
from user import Post, User

def get_user_details(age_gender_file):
    lines = open(age_gender_file).readlines()
    age_gender_map = {}
    for uid_age_gender in lines:
        uid_age_gender = uid_age_gender.strip()
        uname, uid, age, gender = uid_age_gender.split(",")
        age_gender_map[uid] = [uname, gender]
    return age_gender_map



if __name__ == "__main__":
    # age_gender_file = "/Users/shrprasha/Projects/resources/dailystrength/ds_data/username_id_age_gender_both_age_gender.txt"
    age_gender_file = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/username_id_age_gender_both_age_gender.txt"
    users_posts_dumps = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps" #  to get the age_groups
    #output
    users_dump_fpath = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps/all_users.dump"
    # age_gender_file = "/project/solorio/prasha/ds/data/username_id_age_gender_both_age_gender.txt"
    # users_dump_fpath = "/project/solorio/prasha/ds/data/output/users_dumps/all_users.dump"
    # users_posts_dumps = "/project/solorio/prasha/ds/data/output/user_post_dumps" #  to get the age_groups

    age_gender_map = get_user_details(age_gender_file)

    all_files = sorted(next(os.walk(users_posts_dumps))[2])

    all_users = {}
    for fname in all_files:
        fname = fname.strip()
        uid, age_group = fname.split("_")
        uname, gender = age_gender_map[uid]
        user = User(uid, uname, gender, age_group)
        all_users[fname] = user

    with open(users_dump_fpath, "w") as opfhandle:
        dump(all_users, opfhandle)

