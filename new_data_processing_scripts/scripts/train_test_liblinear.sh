#!/bin/sh

#$2=age/gender/both
#$3=name appended to models and results folder; since svm needs separate models and results folder
#eg: /Users/shrprasha/Projects/dailystrength/new_data_processing_scripts/scripts/train_test_liblinear.sh /Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/liblinear_files/char_ngram_features_tf_idf/ both
liblinear_op_path=$1  # path to input and output

mkdir -p $liblinear_op_path/models
mkdir -p $liblinear_op_path/results

liblinear_path="/Users/shrprasha/Downloads/Softwares/liblinear-1.96"
echo "training liblinear $2"
$liblinear_path/train -s 0 -c 2 "$liblinear_op_path/train/$2" "$liblinear_op_path/models/$2"
echo "testing on liblinear $2"
$liblinear_path/predict -b 1 "$liblinear_op_path/dev/$2" "$liblinear_op_path/models/$2" "$liblinear_op_path/results/$2"

#libsvm_path="/Users/shrprasha/Downloads/Softwares/libsvm-3.20"
#echo "training svm $2"
#$libsvm_path/train -s 0 -c 1 -t 0 -b 1 -h 0 "$liblinear_op_path/train/$2" "$liblinear_op_path/models$3/$2"
#echo "testing on svm $2"
#$libsvm_path/predict -b 1 "$liblinear_op_path/dev/$2" "$liblinear_op_path/models$3/$2" "$liblinear_op_path/results$3/$2"
