from collections import Counter
import os, sys
import xml.etree.ElementTree as xml
from dill import load, dump

__author__ = 'shrprasha'



def get_files_in_folder(folder):
    all_files = sorted(next(os.walk(folder))[2])
    if ".DS_Store" in all_files:
        all_files.remove(".DS_Store")
    return all_files


def get_treatments(xml_file):
    tree = xml.parse(os.path.join(xml_file))
    root = tree.getroot()
    return [treatment.attrib["id"] for treatment in root.iter('treatment')]

def treatment_counts(xmldir):
    try:
        files = get_files_in_folder(xmldir)
    except:
        print "cannot parse"
        return None
    all_treatments = []
    for doc_fname in files:
        extension = os.path.splitext(doc_fname)[-1]
        if extension != ".xml":
            continue
        treatments = get_treatments(os.path.join(xmldir, doc_fname))
        all_treatments.extend(treatments)
    return Counter(all_treatments)

def update_all_treatments(disease, treatment_counts_dict):
    for treatment, count in treatment_counts_dict.iteritems():
        if treatment in all_treatment_counts:
            if disease in all_treatment_counts[treatment]:
                all_treatment_counts[treatment][disease] += count
            else:
                all_treatment_counts[treatment][disease] = count
        else:
            all_treatment_counts[treatment] = {disease: count}

def create_dumps(obj, fpath):
    with open(fpath, "w") as fhandle:
        dump(obj, fhandle)

if __name__ == "__main__":
    # here
    xmlroot = "/Users/shrprasha/Projects/resources/dailystrength/raw_data"
    diseases = sorted(["Acne", "Fructose_Intolerance", "Infertility", "Miscarriage-Stillbirth", "Physical-Emotional-Abuse", "Widows-Widowers"])

    # xmlroot = "/project/solorio/prasha/ds/data"
    # diseases = sorted(["Anxiety", "Bereavement", "Self_Injury", "Diets_Weight_Maintenance", "Physical_Emotional_Abuse", "Sexual_Abuse", "Miscarriage", "ADHD-ADD", "Multiple_Sclerosis", "Panic_Attacks", "Obesity", "Healthy_Relationships", "Chronic_Pain", "Healthy_Sex", "Loneliness", "Depression_Teen", "Codependency", "Pregnancy", "Post_Traumatic_Stress_Disorder", "Polycystic_Ovarian_Syndrome", "Rape", "Widows_Widowers", "Endometriosis", "Families-and-Friends-Of-Addicts", "Obsessive_Compulsive_Disorder", "Smoking_Addiction", "Smoking_Addiction_Recovery", "Family-Issues", "Rheumatoid_Arthritis", "Crohn_Disease_Ulcerative_Colitis", "Lupus", "Hepatitis_C", "Back-Pain", "Migraine", "Migraine_Headaches", "Sex_Pornography_Addiction", "Food-Addiction", "Diabetes-Type-2", "Trichotillomania_Hair_Pulling", "Insomnia", "Personality_Disorders", "Lesbian_Relationship_Challenges", "Chronic-Fatigue-Syndrome", "Anger-Management", "Gay_Lesbian_Teens", "Menopause", "Hypothyroidism", "Autism", "Epilepsy-Seizures", "Hidradenitis_Suppurativa", "Pulmonary_Embolism", "Asperger_Syndrome", "Families_of_Prisoners", "Pregnancy_After_Loss_Infertility", "Agoraphobia-and-Social-Anxiety", "Irritable_Bowel_Syndrome", "Female-Sexual-Issues", "Financial_Challenges", "Stillbirth", "Gambling_Addiction_Recovery", "HPV", "Family_Friends_of_Bipolar", "COPD", "Deep_Vein_Thrombosis", "Coming-Out", "Graves_Disease", "Caregivers", "Trying_To_Conceive", "Myasthenia_Gravis", "Asthma", "Pseudotumor_Cerebri", "Tinnitus", "Narcolepsy", "Incest_Survivors", "Brain_Injury", "Diabetes-Type-1", "Schizophrenia", "Parkinson_Disease", "Pancreatitis", "Acne", "Atrial_Fibrillation", "Lyme_Disease", "Cirrhosis", "Grandparents_Raising_Children", "Dizziness-Vertigo", "Lung_Cancer", "Alzheimer_Disease", "Food-Allergies", "Environmental-Allergies", "Common_Variable_Immunodeficiency", "Dyslexia", "Down-Syndrome", "Families-Friends-of-Gays-Lesbians", "War_In_Iraq", "Body-Modification", "Celiac-Disease", "Fatty-Liver-Disease", "Cerebral-Palsy", "Dry-Eyes", "Bone_Cancer", "Fructose-Intolerance", "Immigration_Law", "Color-Blindness", "Fatty-Acid-Oxidation-Disorders"])

    all_treatment_counts = {}  # {treatment: {disease1: count, ...}, ...}
    for disease in diseases:
        print disease
        xmldir = os.path.join(xmlroot, disease, "XML")
        treatment_counts_dict = treatment_counts(xmldir)
        print treatment_counts_dict
        update_all_treatments(disease, treatment_counts_dict)
    create_dumps(all_treatment_counts, "/project/solorio/prasha/ds/all_treatment_counts.dump")
    # print all_treatment_counts


    # min_max_dates("/Users/shrprasha/Projects/resources/dailystrength/temp/Fructose_Intolerance/XML")
