import os, sys
import xml.etree.ElementTree as xml
import datetime

__author__ = 'shrprasha'



def get_files_in_folder(folder):
    all_files = sorted(next(os.walk(folder))[2])
    if ".DS_Store" in all_files:
        all_files.remove(".DS_Store")
    return all_files

def min_max_dates(xmldir):
    try:
        files = get_files_in_folder(xmldir)
    except:
        print "cannot parse"
        return None
    min_max_files = []
    for doc_fname in files:
        extension = os.path.splitext(doc_fname)[-1]
        if extension != ".xml":
            continue
        min_max = min_max_date_file(os.path.join(xmldir, doc_fname))
        min_max_files.extend(min_max)
    return min(min_max_files), max(min_max_files)

def min_max_date_file(xml_file):
    tree = xml.parse(os.path.join(xml_file))
    root = tree.getroot()
    dates_posted = []
    for problem in root.iter('problem'):
        posted = get_date_posted(problem)
        if posted:
            dates_posted.append(posted)
    for reply in root.iter('reply'):
        posted = get_date_posted(reply)
        if posted:
            dates_posted.append(posted)
    if dates_posted:
        return min(dates_posted), max(dates_posted)
    else:
        return []


def get_date_posted(xmlobj):
    if xmlobj.find('person') is not None: #added since /mnt/docsig/storage/daily-strength/Bipolar-Disorder/XML/10056719-taken-advantage.html.xml has no data in problem or replies
        if uid in age_gender_map:
            date_posted = xmlobj.find('date').text
            # print date_posted
            try:
                return datetime.datetime.strptime(date_posted, "%m/%d/%y %I:%M%p")
            except:
                return None
    return None



if __name__ == "__main__":
    age_gender_file = "/project/solorio/prasha/ds/processed_data/username_id_age_gender_both_age_gender.txt"
    xmlroot = "/project/solorio/prasha/ds/data"
    diseases = sorted(["Anxiety", "Bereavement", "Self_Injury", "Diets_Weight_Maintenance", "Physical_Emotional_Abuse", "Sexual_Abuse", "Miscarriage", "ADHD-ADD", "Multiple_Sclerosis", "Panic_Attacks", "Obesity", "Healthy_Relationships", "Chronic_Pain", "Healthy_Sex", "Loneliness", "Depression_Teen", "Codependency", "Pregnancy", "Post_Traumatic_Stress_Disorder", "Polycystic_Ovarian_Syndrome", "Rape", "Widows_Widowers", "Endometriosis", "Families-and-Friends-Of-Addicts", "Obsessive_Compulsive_Disorder", "Smoking_Addiction", "Smoking_Addiction_Recovery", "Family-Issues", "Rheumatoid_Arthritis", "Crohn_Disease_Ulcerative_Colitis", "Lupus", "Hepatitis_C", "Back-Pain", "Migraine", "Migraine_Headaches", "Sex_Pornography_Addiction", "Food-Addiction", "Diabetes-Type-2", "Trichotillomania_Hair_Pulling", "Insomnia", "Personality_Disorders", "Lesbian_Relationship_Challenges", "Chronic-Fatigue-Syndrome", "Anger-Management", "Gay_Lesbian_Teens", "Menopause", "Hypothyroidism", "Autism", "Epilepsy-Seizures", "Hidradenitis_Suppurativa", "Pulmonary_Embolism", "Asperger_Syndrome", "Families_of_Prisoners", "Pregnancy_After_Loss_Infertility", "Agoraphobia-and-Social-Anxiety", "Irritable_Bowel_Syndrome", "Female-Sexual-Issues", "Financial_Challenges", "Stillbirth", "Gambling_Addiction_Recovery", "HPV", "Family_Friends_of_Bipolar", "COPD", "Deep_Vein_Thrombosis", "Coming-Out", "Graves_Disease", "Caregivers", "Trying_To_Conceive", "Myasthenia_Gravis", "Asthma", "Pseudotumor_Cerebri", "Tinnitus", "Narcolepsy", "Incest_Survivors", "Brain_Injury", "Diabetes-Type-1", "Schizophrenia", "Parkinson_Disease", "Pancreatitis", "Acne", "Atrial_Fibrillation", "Lyme_Disease", "Cirrhosis", "Grandparents_Raising_Children", "Dizziness-Vertigo", "Lung_Cancer", "Alzheimer_Disease", "Food-Allergies", "Environmental-Allergies", "Common_Variable_Immunodeficiency", "Dyslexia", "Down-Syndrome", "Families-Friends-of-Gays-Lesbians", "War_In_Iraq", "Body-Modification", "Celiac-Disease", "Fatty-Liver-Disease", "Cerebral-Palsy", "Dry-Eyes", "Bone_Cancer", "Fructose-Intolerance", "Immigration_Law", "Color-Blindness", "Fatty-Acid-Oxidation-Disorders"])
    age_gender_map = {}
    lines = open(age_gender_file).readlines()

    dates_all = []
    for uid_age_gender in lines:
        uid_age_gender = uid_age_gender.strip()
        uname, uid, age, gender = uid_age_gender.split(",")
        age_gender_map[uid] = (age, gender)
    for disease in diseases:
        print disease
        xmldir = os.path.join(xmlroot, disease, "XML")
        min_max_disease = min_max_dates(xmldir)
        if min_max_disease:
            print min_max_disease
            dates_all.extend(min_max_disease)
        dates_all.extend(min_max_disease)
    print min(dates_all), max(dates_all)



    # min_max_dates("/Users/shrprasha/Projects/resources/dailystrength/temp/Fructose_Intolerance/XML")
