from collections import Counter
import os, sys
import xml.etree.ElementTree as xml
from dill import load, dump

__author__ = 'shrprasha'

def get_files_in_folder(folder):
    all_files = sorted(next(os.walk(folder))[2])
    if ".DS_Store" in all_files:
        all_files.remove(".DS_Store")
    return all_files

def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

def create_dumps(obj, fpath):
    with open(fpath, "w") as fhandle:
        dump(obj, fhandle)

def convert_to_disease_key():
    all_treatments = load_dumps("/Users/shrprasha/Projects/resources/dailystrength/suggestion_project/all_treatment_counts.dump")
    all_disease_treatments = {}
    for treatment, diseases_counts in all_treatments.iteritems():
        for disease, counts in diseases_counts.iteritems():
            if disease in all_disease_treatments:
                all_disease_treatments[disease][treatment] = counts
            else:
                all_disease_treatments[disease] = {treatment: counts}
    # print all_disease_treatments
    create_dumps(all_disease_treatments, "/Users/shrprasha/Projects/resources/dailystrength/suggestion_project/all_disease_treatments.dump")


def display_top_treatments():
    common = 50
    all_disease_treatments = load_dumps(dump_path)
    print_list = [[]] * (common+1)
    # print print_list
    for disease, treatment_counts in all_disease_treatments.iteritems():
        ctr = Counter(treatment_counts)
        common_treatments = [x for x, y in ctr.most_common(common)]
        common_treatments.insert(0, disease)
        common_treatments.extend([""] * ((common+1)-len(common_treatments)))
        # print common_treatments
        print " ".join(common_treatments)


# def display_top_treatments():
#     common = 5
#     all_disease_treatments = load_dumps(dump_path)
#     print_list = [[]] * (common+1)
#     print print_list
#     for disease, treatment_counts in all_disease_treatments.iteritems():
#         ctr = Counter(treatment_counts)
#         common_treatments = [x for x, y in ctr.most_common(common)]
#         common_treatments.insert(0, disease)
#         common_treatments.extend([""] * ((common+1)-len(common_treatments)))
#         print common_treatments
#         for i, treatment in enumerate(common_treatments):
#             print_list[i].append(treatment)
#             # print print_list
#     for x in print_list:
#         print ",".join(x)

def get_sentences_in_treatments():
    all_disease_treatments = load_dumps()

if __name__ == "__main__":
    dump_path = "/Users/shrprasha/Projects/resources/dailystrength/suggestion_project/all_disease_treatments.dump"
    display_top_treatments()
    # # convert_to_disease_key()
    # # # here
    # xmlroot = "/Users/shrprasha/Projects/resources/dailystrength/raw_data"
    # diseases = sorted(["Acne", "Fructose_Intolerance", "Infertility", "Miscarriage-Stillbirth", "Physical-Emotional-Abuse", "Widows-Widowers"])
    # # dump_path = "/project/solorio/prasha/ds/all_disease_treatments.dump"
    # dump_path = "/Users/shrprasha/Projects/resources/dailystrength/suggestion_project/all_disease_treatments.dump"
    #
    # # xmlroot = "/project/solorio/prasha/ds/data"
    # # diseases = sorted(["Anxiety", "Bereavement", "Self_Injury", "Diets_Weight_Maintenance", "Physical_Emotional_Abuse", "Sexual_Abuse", "Miscarriage", "ADHD-ADD", "Multiple_Sclerosis", "Panic_Attacks", "Obesity", "Healthy_Relationships", "Chronic_Pain", "Healthy_Sex", "Loneliness", "Depression_Teen", "Codependency", "Pregnancy", "Post_Traumatic_Stress_Disorder", "Polycystic_Ovarian_Syndrome", "Rape", "Widows_Widowers", "Endometriosis", "Families-and-Friends-Of-Addicts", "Obsessive_Compulsive_Disorder", "Smoking_Addiction", "Smoking_Addiction_Recovery", "Family-Issues", "Rheumatoid_Arthritis", "Crohn_Disease_Ulcerative_Colitis", "Lupus", "Hepatitis_C", "Back-Pain", "Migraine", "Migraine_Headaches", "Sex_Pornography_Addiction", "Food-Addiction", "Diabetes-Type-2", "Trichotillomania_Hair_Pulling", "Insomnia", "Personality_Disorders", "Lesbian_Relationship_Challenges", "Chronic-Fatigue-Syndrome", "Anger-Management", "Gay_Lesbian_Teens", "Menopause", "Hypothyroidism", "Autism", "Epilepsy-Seizures", "Hidradenitis_Suppurativa", "Pulmonary_Embolism", "Asperger_Syndrome", "Families_of_Prisoners", "Pregnancy_After_Loss_Infertility", "Agoraphobia-and-Social-Anxiety", "Irritable_Bowel_Syndrome", "Female-Sexual-Issues", "Financial_Challenges", "Stillbirth", "Gambling_Addiction_Recovery", "HPV", "Family_Friends_of_Bipolar", "COPD", "Deep_Vein_Thrombosis", "Coming-Out", "Graves_Disease", "Caregivers", "Trying_To_Conceive", "Myasthenia_Gravis", "Asthma", "Pseudotumor_Cerebri", "Tinnitus", "Narcolepsy", "Incest_Survivors", "Brain_Injury", "Diabetes-Type-1", "Schizophrenia", "Parkinson_Disease", "Pancreatitis", "Acne", "Atrial_Fibrillation", "Lyme_Disease", "Cirrhosis", "Grandparents_Raising_Children", "Dizziness-Vertigo", "Lung_Cancer", "Alzheimer_Disease", "Food-Allergies", "Environmental-Allergies", "Common_Variable_Immunodeficiency", "Dyslexia", "Down-Syndrome", "Families-Friends-of-Gays-Lesbians", "War_In_Iraq", "Body-Modification", "Celiac-Disease", "Fatty-Liver-Disease", "Cerebral-Palsy", "Dry-Eyes", "Bone_Cancer", "Fructose-Intolerance", "Immigration_Law", "Color-Blindness", "Fatty-Acid-Oxidation-Disorders"])
    #
    # all_treatment_counts = {}  # {treatment: {disease1: count, ...}, ...}
    # for disease in diseases:
    #     print disease
    # # create_dumps(all_treatment_counts, "/project/solorio/prasha/ds/all_treatment_counts.dump")
    # # print all_treatment_counts
    #
    #
    # # min_max_dates("/Users/shrprasha/Projects/resources/dailystrength/temp/Fructose_Intolerance/XML")
