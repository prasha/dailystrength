__author__ = 'shrprasha'


fpath = "/Users/shrprasha/Projects/resources/AA/stats/duplicate_results"

correct_age_group_prediction_count_per_user = {}
gender_prediction_single_user = {}
for line in open(fpath, "r").readlines():
    uid, actual_age_group, pred_age_group, actual_gender, pred_gender = line.strip().split("\t")
    if actual_age_group == pred_age_group:
        if uid in correct_age_group_prediction_count_per_user:
            correct_age_group_prediction_count_per_user[uid] += 1
        else:
            correct_age_group_prediction_count_per_user[uid] = 1

    if uid in gender_prediction_single_user:
        if gender_prediction_single_user[uid] != pred_gender:
            print uid, actual_gender, gender_prediction_single_user[uid], pred_gender
    else:
        gender_prediction_single_user[uid] = pred_gender

print "=" * 10

total_correct_count = 0
for uid in sorted(correct_age_group_prediction_count_per_user):
    correct_count = correct_age_group_prediction_count_per_user[uid]
    if correct_count == 2:
        print uid
        total_correct_count += 1
print "=" * 10

print total_correct_count
