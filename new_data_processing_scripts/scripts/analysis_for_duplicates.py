__author__ = 'shrprasha'

import os
from dill import load
from user import User
from collections import Counter


def get_results(results_fpath):
    with open(results_fpath, "r") as results_fhandle:
        lines = results_fhandle.readlines()[1:]
    result_label_idxs = []
    for line in lines:
        result_label_idxs.append(int(line.split(" ", 1)[0]))
    return result_label_idxs

if __name__ == "__main__":
    users_dumps_rootdir = "/project/solorio/prasha/ds/data/output/users_dumps/"
    age_results_fpath = "/project/solorio/prasha/ds/output/logistic_results/results/age"
    gender_results_fpath = "/project/solorio/prasha/ds/output/logistic_results/results/gender"
    opdir = "/project/solorio/prasha/ds/output/"
    with open(os.path.join(users_dumps_rootdir, "dev.dump"), "r") as devfhandle:
        dev_users = load(devfhandle)

    age_labels = ["18-24", "25-34", "35-49", "50-64", "65-xx"]
    gender_labels = ["female", "male"]
    age_label_idxs = get_results(age_results_fpath)
    gender_label_idxs = get_results(gender_results_fpath)

    if len(age_label_idxs) != len(dev_users) != len(gender_label_idxs):
        print "ERROR!!!", len(age_label_idxs), len(dev_users), len(gender_label_idxs)

    results_just_duplicates = {}
    results_all = {}
    uids = []
    for i, fname in enumerate(sorted(dev_users)):
        uid = fname.split("_")[0]
        uids.append(uid)
    uid_ctr = Counter(uids)
    duplicates = []
    for uid, count in uid_ctr.iteritems():
        if count > 1:
            duplicates.append(uid)

    with open(os.path.join(opdir, "duplicate_analysis.txt"), "w") as duplicate_fhandle, open(os.path.join(opdir, "dev_analysis.txt"), "w") as dev_fhandle:
        for i, fname in enumerate(sorted(dev_users)):
            uid, actual_age_group = fname.split("_")
            user = dev_users[fname]
            actual_gender = user.gender.lower()
            predicted_age_group = age_labels[age_label_idxs[i]]
            predicted_gender = gender_labels[gender_label_idxs[i]]
            dev_fhandle.write(uid + " " + actual_age_group + " " + predicted_age_group + " " + actual_gender + " " + predicted_gender + "\n")
            if uid in duplicates:
                duplicate_fhandle.write(uid + " " + actual_age_group + " " + predicted_age_group + " " + actual_gender + " " + predicted_gender + "\n")




