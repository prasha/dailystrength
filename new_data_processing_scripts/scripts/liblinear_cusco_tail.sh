#!/bin/sh

declare -a types=(age gender both)
type=${types[$1]}
echo $type
liblinear_path="/project/solorio/prasha/softwares/liblinear-1.96"
train_features_path="/project/solorio/prasha/ds/output/liblinear_files/train"
dev_features_path="/project/solorio/prasha/ds/output/liblinear_files/dev"
#test_features_path="/project/solorio/prasha/ds/output/liblinear_files/test"
output_path="/project/solorio/prasha/ds/output/logistic_results"

$liblinear_path/train -s 0 -c 0.1 "$train_features_path/$type" "$output_path/models/$type"
$liblinear_path/predict -b 1 "$dev_features_path/$type" "$output_path/models/$type" "$output_path/results/$type"
#$liblinear_path/predict -b 1 "$test_features_path/$type" "$output_path/models/$type" "$output_path/results/$type"
