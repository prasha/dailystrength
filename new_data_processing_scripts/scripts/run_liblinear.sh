#!/bin/sh

#usage:
#/Users/shrprasha/Projects/dailystrength/new_data_processing_scripts/scripts/run_liblinear.sh op_folder_name(usu. feature types) _svm
#/Users/shrprasha/Projects/dailystrength/new_data_processing_scripts/scripts/run_liblinear.sh op_folder_name(usu. feature types)  _logistic
#example:
#/Users/shrprasha/Projects/dailystrength/new_data_processing_scripts/scripts/run_liblinear.sh normalized_stylistic_and_old_features _logistic

##old usage:
##/Users/shrprasha/Projects/dailystrength/new_data_processing_scripts/scripts/run_liblinear.sh op_folder_name(usu. feature types) age_gender_both dev_or_test
##example:
##/Users/shrprasha/Projects/dailystrength/resources/run_liblinear.sh all_features both test

#to change to type of features: change in /Users/shrprasha/Projects/dailystrength/new_version/combine_feature_liblinear.py 

#liblinear_op_path="/Users/shrprasha/Projects/resources/dailystrength/liblinear/"
liblinear_op_path="/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/liblinear_files/$1"
scripts_path="/Users/shrprasha/Projects/dailystrength/new_data_processing_scripts"
echo "creating train and dev/test files"
#python "$scripts_path/create_train_test_liblinear.py" $liblinear_op_path

mkdir -p "$liblinear_op_path/models$2"
mkdir -p "$liblinear_op_path/results$2"
#$scripts_path/scripts/train_test_liblinear.sh $liblinear_op_path gender $2
$scripts_path/scripts/train_test_liblinear.sh $liblinear_op_path age $2
#$scripts_path/scripts/train_test_liblinear.sh $liblinear_op_path both $2

#echo "getting age & gender accuracy, not relevant for age only or gender only analysis"
#python2.7 "$scripts_path/scripts/get_confusionmatrix_accuracy_liblinearop.py" "$liblinear_op_path/results" "$liblinear_op_path/dev/both"
python2.7 "$scripts_path/scripts/get_combined_age_gender_from_separate_run.py" "$liblinear_op_path/results$2" "$liblinear_op_path/dev/both"

