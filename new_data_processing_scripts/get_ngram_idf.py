__author__ = 'shrprasha'

import os
from dill import load, dump
from collections import Counter
import utils
import sys
# import operator
import json

def get_files_in_folder(folder):
    all_files = sorted(next(os.walk(folder))[2])
    if ".DS_Store" in all_files:
        all_files.remove(".DS_Store")
    return all_files

def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj


def add_counter(d1, ctr):
    for k, v in ctr.iteritems():
        if k in d1:
            d1[k] += v
        else:
            d1[k] = v

def add_set(d1, s1):
    for k in s1:
        if k in d1:
            d1[k] += 1
        else:
            d1[k] = 1

def add_dict(d1, d2):
    for k, v in d2.iteritems():
        if k in d1:
            d1[k] += v
        else:
            d1[k] = v

def get_word_ngrams(post):
    tokens = [token for token, _ in post.token_tags]
    ngrams = utils.get_ngrams_from_list(tokens, 1, 3)
    return ngrams

def get_char_ngrams(post):
    text = " ".join(post.text.split())  #  remove multiple spaces
    return utils.get_ngrams_from_list(text, 3, 4)

# ## actual
# # idf is calculated per post; not per user
if __name__ == "__main__":
    feature_type = sys.argv[1]
    feature_functions = {"word": get_word_ngrams, "char": get_char_ngrams}
    feature_function = feature_functions[feature_type]

    ## cluster
    user_posts_dump_rootdir = "/project/solorio/prasha/ds/processed_data/user_post_dumps_continuous_age_groups"
    users_dumps_rootdir = "/project/solorio/prasha/ds/processed_data/user_dumps_old_all"
    idf_opdir = "/project/solorio/prasha/ds/processed_data"

    ## here
    # user_posts_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps"
    # idf_opdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data"
    # users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps"

    train_users = load_dumps(os.path.join(users_dumps_rootdir, "train.dump"))

    all_files = get_files_in_folder(user_posts_dump_rootdir)
    user_posts = []
    idf_dict = {}
    for fname in sorted(train_users):
        print fname
        user_posts = load_dumps(os.path.join(user_posts_dump_rootdir, fname))
        user_ngrams = []
        for post in user_posts:
            user_ngrams.extend(feature_function(post))
        user_ngrams_set = set(user_ngrams)
        add_set(idf_dict, user_ngrams_set)
    print len(idf_dict)
    # print idf_dict

    # converting ngrams to strings; since ngrams is in the form of tuples and json will not dump them
    idf_string_keys = {}
    for k, v in idf_dict.iteritems():
        ngram = " ".join(k)
        idf_string_keys[ngram] = v


    with open(os.path.join(idf_opdir, feature_type + "_idfs_cont.json"), "w") as idf_fhandle:
        json.dump(idf_string_keys, idf_fhandle)

##test
# if __name__ == "__main__":
#     feature_type = sys.argv[1]
#     idf_opdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data"
#     # with open(os.path.join(idf_opdir, "idfs"), "r") as idf_fhandle:
#     with open(os.path.join(idf_opdir, feature_type + "_idfs.json"), "r") as idf_fhandle:
#         print json.load(idf_fhandle)




    #
    # with open(os.path.join(idf_opdir, "idfs"), "w") as idf_fhandle:
    #     for k, v in sorted(idf_dict.items(), key=operator.itemgetter(1)):
    #         ngram = " ".join(k)
    #         idf_fhandle

## patchup
# if __name__ == "__main__":
#     idf_opdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data"
#
#     idf1 = load_dumps(os.path.join(idf_opdir, "idf_train1.dump"))
#     idf2 = load_dumps(os.path.join(idf_opdir, "idf_train2.dump"))
#     idf3 = load_dumps(os.path.join(idf_opdir, "idf_train3.dump"))
#
#     print "done loading"
#
#     print len(idf1), len(idf2), len(idf3), len(idf1) + len(idf2) + len(idf3), len(set(idf1) - set(idf2)), len(set(idf1) - set(idf3))
#     print len(idf1) + len(set(idf1) - set(idf2)) + len(set(idf1) - set(idf3))
#
#     add_dict(idf1, idf2)
#     add_dict(idf1, idf3)
#
#     print len(idf1)
#
#     with open(os.path.join(idf_opdir, "idf.dump"), "w") as idf_fhandle:
#         dump(idf1, idf_fhandle)

## with list; acutal uses dict
# if __name__ == "__main__":
#     user_posts_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps"
#     idf_opdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data"
#     users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps"
#     train_users = load_dumps(os.path.join(users_dumps_rootdir, "train.dump"))
#
#     # print len(train_users)
#
#     # user_posts_dump_rootdir = "/project/solorio/prasha/ds/data/output/user_post_dumps"
#     # features_dumpdir = "/project/solorio/prasha/ds/data/output/user_feature_dumps"
#     all_files = get_files_in_folder(user_posts_dump_rootdir)
#     user_posts = []
#     idf_list = []
#     for fname in sorted(train_users):
#         print fname
#         fname = fname.strip()
#         user_posts = load_dumps(os.path.join(user_posts_dump_rootdir, fname))
#         user_ngrams = []
#         for post in user_posts:
#             tokens = [token for token, _ in post.token_tags]
#             ngrams = utils.get_ngrams_from_list(tokens, 1, 3)
#             user_ngrams.extend(ngrams)
#         idf_list.extend(set(user_ngrams))
#     idfs = Counter(idf_list)
#     print idfs
#     print len(idfs)
#
#     with open(os.path.join(idf_opdir, "idf.dump"), "w") as idf_fhandle:
#         dump(idfs, idf_fhandle)




