from datetime import datetime

__author__ = 'shrprasha'
from collections import Counter


def append_sorted_by_date(posts, new_post):
    for i, post in enumerate(posts):
        if post.date_posted >= new_post.date_posted:
            break
    posts.insert(i, new_post)

class User(object):

    def __init__(self, uid, uname, gender):
        self.uid = uid
        self.uname = uname
        self.gender = gender
        self._posts = {}  # {age when posted: [posts]}
        self.age_group = ()

    @property
    def posts(self):
        return self._posts

    @posts.setter
    def posts(self, posts):  # appends to the dictionary
        for post in posts:
            if post.age_when_posted not in self._posts:
                self._posts[post.age_when_posted] = [post]
            else:
                append_sorted_by_date(self.posts[post.age_when_posted], post)
        self.age_group = (min(self._posts.keys()), max(self._posts.keys())+ 1)  # final no. not included

    def has_posts_in_age_range(self, age_range):
        overlap = (max(age_range[0], self.age_group[0]), min(age_range[1], self.age_group[1]))
        if overlap[1] - overlap[0] < 1:
            return []
        return overlap

    def get_posts_age_range(self, age_range):
        posts = []
        overlap = self.has_posts_in_age_range(age_range)
        if overlap:
            for i in range(*overlap):  # final no. not included
                posts.extend(self.posts.get(i, []))
        return posts

    def get_posts_sorted(self, reverse=False):
        sorted_posts = []
        for age_group in sorted(self.posts):
            sorted_posts.extend(self.posts[age_group])
        if reverse:
            return list(reversed(sorted_posts))
        else:
            return sorted_posts

    def get_posts_all(self):
        return [post for posts in self.posts.values() for post in posts]

    def post_count(self):
        return sum([len(x) for x in self.posts.values()])

    def __eq__(self, other):
        return self.uid == other.uid and self.age_group == other.age_group

    def __str__(self):
        attrs = vars(self)
        return ', '.join("%s: %s" % item for item in attrs.items())



class Post:

    def __init__(self, text, problem_or_reply, disease, age_when_posted, token_tags, uid, postid, date_posted=None):
        self.text = text
        self.problem_or_reply = problem_or_reply
        self.disease = disease
        self.age_when_posted = age_when_posted
        self.token_tags = token_tags
        self.uid = uid
        self.postid = postid
        self.date_posted = date_posted

    def number_of_words(self):
        return len(self.token_tags)

    def time_elapsed(self):
        return float((datetime.now()-self.date_posted).days)/365


# user = User("10", "mine", "female")
# user.posts = [Post("abc", "problem", "all", 30, "pos", "123", "345")]
# print user









