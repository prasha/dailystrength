__author__ = 'shrprasha'
import os
import sys
import fnmatch
from dill import load, dump

def get_max(feature_rootdir, dump_max_rootdir, feature_file_fname):
    max_dict = {}
    labels_list = []
    line_idx_features_list = []
    feature_file_fpath = os.path.join(feature_rootdir, feature_file_fname)
    # print feature_file_fpath
    with open(feature_file_fpath, "r") as feature_fhandle:
        lines = feature_fhandle.readlines()
        for line in lines:
            line_idx_features = {}
            label, features = line.split(" ", 1)
            idx_feature_list = features.split()
            labels_list.append(label)
            for idx_feature in idx_feature_list:
                idx, feature = idx_feature.split(":")
                idx = int(idx)
                feature = float(feature)
                if idx in max_dict:
                    max_dict[idx] = max(feature, max_dict[idx])
                else:
                    max_dict[idx] = feature
                line_idx_features[idx] = feature
            line_idx_features_list.append(line_idx_features)
    dump(max_dict, open(os.path.join(dump_max_rootdir, feature_file_fname), "w"))
    # print max_dict
    return max_dict, line_idx_features_list, labels_list

#  for rest
def get_just_idx_features(rootdir, feature_file_fname):
    labels_list = []
    line_idx_features_list = []
    print rootdir, feature_file_fname
    with open(os.path.join(rootdir, feature_file_fname), "r") as feature_fhandle:
        lines = feature_fhandle.readlines()
        try:
            for line in lines:
                line_idx_features = {}
                label, features = line.split(" ", 1)
                idx_feature_list = features.split()
                labels_list.append(label)
                for idx_feature in idx_feature_list:
                    idx, feature = idx_feature.split(":")
                    idx = int(idx)
                    feature = float(feature)
                    line_idx_features[idx] = feature
                line_idx_features_list.append(line_idx_features)
        except:
            print " ERROR : ", line, idx, feature, feature_file_fname
    return line_idx_features_list, labels_list

def normalize(max_dict, line_idx_features_list, labels_list, fname, normed_rootdir):
    with open(os.path.join(normed_rootdir, fname), "w") as normed_fhandle:
        for i, line_idx_features in enumerate(line_idx_features_list):
            line = str(labels_list[i])
            for idx in sorted(line_idx_features):
                feature = line_idx_features[idx]
                # print idx, feature
                if idx in max_dict:
                    if max_dict[idx] > 1:
                        try:
                            feature /= max_dict[idx]
                        except: #divide by zero exception
                            feature = 0
                            print idx, i
                            # print max_dict[idx]
                    line += " " + str(idx) + ":" + str(feature)
            normed_fhandle.write(line + "\n")

def create_opdir(opdir):
    if not os.path.exists(opdir):
        os.makedirs(opdir)

#cluster
# usage: /Users/shrprasha/Projects/dailystrength/new_data_processing_scripts/normalize.py age/gender/both
if __name__ == "__main__":
    iprootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/liblinear_files/stylistic_and_old_features"
    norm_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/liblinear_files/normalized_stylistic_and_old_features"

    train_rootdir = os.path.join(iprootdir, "train")
    dev_rootdir = os.path.join(iprootdir, "dev")
    test_rootdir = os.path.join(iprootdir, "test")

    norm_train_rootdir = os.path.join(norm_rootdir, "train")
    dump_max_rootdir = os.path.join(norm_rootdir, "max")
    norm_dev_rootdir = os.path.join(norm_rootdir, "dev")
    norm_test_rootdir = os.path.join(norm_rootdir, "test")

    create_opdir(norm_train_rootdir)
    create_opdir(dump_max_rootdir)
    create_opdir(norm_dev_rootdir)
    create_opdir(norm_test_rootdir)


    #old
    # feature_file_fname = sys.argv[1]
    # max_dict, line_idx_features_list, labels_list = get_max(train_rootdir, dump_max_rootdir, feature_file_fname)
    # normalize(max_dict, line_idx_features_list, labels_list, feature_file_fname, norm_train_rootdir)
    # line_idx_features_list, labels_list = get_just_idx_features(dev_rootdir, feature_file_fname)
    # normalize(max_dict, line_idx_features_list, labels_list, feature_file_fname, norm_dev_rootdir)
    # line_idx_features_list, labels_list = get_just_idx_features(test_rootdir, feature_file_fname)
    # normalize(max_dict, line_idx_features_list, labels_list, feature_file_fname, norm_test_rootdir)


    #actual
    # feature_file_fnames = ["age", "gender", "both"]
    # # all have same max dict since only label is the different part
    # max_dict, line_idx_features_list, labels_list = get_max(train_rootdir, dump_max_rootdir, feature_file_fnames[0])
    # for i, feature_file_fname in enumerate(feature_file_fnames):
    #     if i > 0:
    #       line_idx_features_list, labels_list = get_just_idx_features(train_rootdir, feature_file_fname)
    #     normalize(max_dict, line_idx_features_list, labels_list, feature_file_fname, norm_train_rootdir)
    #     line_idx_features_list, labels_list = get_just_idx_features(dev_rootdir, feature_file_fname)
    #     normalize(max_dict, line_idx_features_list, labels_list, feature_file_fname, norm_dev_rootdir)
    #     line_idx_features_list, labels_list = get_just_idx_features(test_rootdir, feature_file_fname)
    #     normalize(max_dict, line_idx_features_list, labels_list, feature_file_fname, norm_test_rootdir)

    #patched up
    feature_file_fnames = ["gender", "both"]
    # all have same max dict since only label is the different part
    with open(os.path.join(dump_max_rootdir, "age"), "r") as fhandle:
        max_dict = load(fhandle)
    for feature_file_fname in feature_file_fnames:
        line_idx_features_list, labels_list = get_just_idx_features(train_rootdir, feature_file_fname)
        normalize(max_dict, line_idx_features_list, labels_list, feature_file_fname, norm_train_rootdir)
        line_idx_features_list, labels_list = get_just_idx_features(dev_rootdir, feature_file_fname)
        normalize(max_dict, line_idx_features_list, labels_list, feature_file_fname, norm_dev_rootdir)
        line_idx_features_list, labels_list = get_just_idx_features(test_rootdir, feature_file_fname)
        normalize(max_dict, line_idx_features_list, labels_list, feature_file_fname, norm_test_rootdir)






