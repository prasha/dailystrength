__author__ = 'shrprasha'

import os
from dill import load, dump
from features import CharNgramFeatures
import sys

def get_files_in_folder(folder):
    all_files = sorted(next(os.walk(folder))[2])
    if ".DS_Store" in all_files:
        all_files.remove(".DS_Store")
    return all_files

def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

def create_opdir(opdir):
    if not os.path.exists(opdir):
        os.makedirs(opdir)

if __name__ == "__main__":
    user_posts_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps"
    users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps"
    # features_dumpdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/char_ngram_features"
    features_dumpdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/char_ngram_features_tf_idf"
    feature_name_idx_list_path = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps/char_ngram_features_idx.dump"
    char_ngram_idf_path = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/char_idfs.json"

    # user_posts_dump_rootdir = "/project/solorio/prasha/ds/data/output/user_post_dumps"
    # users_dumps_rootdir = "/project/solorio/prasha/ds/processed_data/users_dumps"
    # features_dumpdir = "/project/solorio/prasha/ds/data/output/user_feature_dumps/char_ngram_features"
    # feature_name_idx_list_path = "/project/solorio/prasha/ds/data/output/user_feature_dumps/char_ngram_feature_idx.dump"

    create_opdir(features_dumpdir)

    ##  SEE: did not do for test right now
    train_fnames = load_dumps(os.path.join(users_dumps_rootdir, "train.dump")).keys()
    dev_fnames = load_dumps(os.path.join(users_dumps_rootdir, "dev.dump")).keys()
    all_files = train_fnames + dev_fnames
    no_of_train_authors = 42968

    user_posts = []
    # features = CharNgramFeatures(features_dumpdir)
    features = CharNgramFeatures(features_dumpdir, char_ngram_idf_path, no_of_train_authors)
    for fname in sorted(all_files):  # usernames
        fname = fname.strip()
        print   fname
        with open(os.path.join(user_posts_dump_rootdir, fname), "r") as fhandle:
            user_posts = load(fhandle)
            features.dump_all_features(fname, user_posts)

    features.dump_feature_name_idx_list(feature_name_idx_list_path)


