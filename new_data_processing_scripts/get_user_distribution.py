__author__ = 'shrprasha'

from collections import Counter

dev_file = "/project/solorio/prasha/ds/output/liblinear_files/dev/both"


labels = []
lines = open(dev_file, "r").readlines()
for line in lines:
    label = line.split(" ", 1)[0]
    labels.append(int(label))

counts_labels = Counter(labels)
both_labels = ["18-24_female", "18-24_male", "25-34_female", "25-34_male", "35-49_female", "35-49_male", "50-64_female", "50-64_male", "65-xx_female", "65-xx_male"]
for label_ids in sorted(counts_labels):
    print both_labels[label_ids], counts_labels[label_ids]
