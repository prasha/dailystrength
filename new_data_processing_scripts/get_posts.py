import random

__author__ = 'prasha'
import xml.etree.ElementTree as xml
import os
import codecs
import sys
from user import Post
from pickle import dump
import utils
from datetime import datetime
import sys
reload(sys)
sys.setdefaultencoding('utf-8')

#decisions:
#did not leave out treatments: although treatments are not indicative of a person's age or gender
#knowledge about what disease this post was under is not preserved

#TODO:
#load people_age_gender
#parse all xml in XML folder to get all <text> and <treatment>, join them by space

#create file with fname: userid_age_gender
#NA when age or gender are not available
#each file contains all the texts written by that user at that age

# no use of age groups


def get_files_in_folder(folder):
    all_files = sorted(next(os.walk(folder))[2])
    if ".DS_Store" in all_files:
        all_files.remove(".DS_Store")
    return all_files

class CreateDataset:

    def __init__(self, age_gender_file, opdir, age_groups, disease):
        self.opdir = opdir
        self.age_groups = age_groups
        lines = open(age_gender_file).readlines()
        self.age_gender_map = {}
        self.disease = disease
        # self.text_by_user_at_age = {} #{user1: {age11: "text11", age12: "text12"}, user2: {age21: "text21", age22: "text22"}}
        self.posts = []
        for uid_age_gender in lines:
            uid_age_gender = uid_age_gender.strip()
            uname, uid, age, gender = uid_age_gender.split(",")
            self.age_gender_map[uid] = (age, gender)
        #self.createds()
        #stats
        self.stats = {"total": 0, "not_in_map": 0, "no_age": 0, "no_gender": 0}

    def createds(self, xmldir):
        self.xmldir = xmldir
        self.disease = xmldir.split("/")[-2] #/mnt/docsig/storage/daily-strength/Acne/XML
        files = get_files_in_folder(self.xmldir)
        for doc_fname in files:
            # doc_fname = "20292587-adhd-family.html.xml"
            extension = os.path.splitext(doc_fname)[-1]
            if extension != ".xml":
                continue
            print self.disease, doc_fname
            self.postid = doc_fname.split("-", 1)[0]
            self.parse_xml(os.path.join(self.xmldir, doc_fname))
        #self.write_to_file()

    def parse_xml(self, xml_file):
        try:
            tree = xml.parse(os.path.join(xml_file))
        except:
            print "empty file"
            return
        root = tree.getroot()
        for problem in root.iter('problem'):
            self.store_to_map(problem, "problem")
        for reply in root.iter('reply'):
            self.store_to_map(reply, "reply")
        self.stats["total"] += 1
        print

    def store_to_map(self, xmlobj, problem_or_reply):
        val = self.parse_problem_or_reply(xmlobj)
        if(val):
            uid, date_posted, text = val
            age_now = self.age_gender_map[uid][0]
            age_when_posted, age_group = self.get_actual_age_group(age_now, date_posted)
            text, pos_tags = utils.get_token_tags(text)
            date_posted = date_posted.replace("\xc2\xa0", " ")
            ddt = datetime.strptime(pad_time(date_posted), "%m/%d/%y %I:%M%p")
            # print ddt
            post = Post(text, problem_or_reply, self.disease, age_when_posted, pos_tags, uid,
                        self.postid, ddt)
            self.posts.append(post)

    def parse_problem_or_reply(self, xmlobj):
        if xmlobj.find('person') is not None: #added since /mnt/docsig/storage/daily-strength/Bipolar-Disorder/XML/10056719-taken-advantage.html.xml has no data in problem or replies
            uid = xmlobj.find('person').get('id') #'/people/436265'
            print uid,
            uid = uid.split("/")[-1]
            if uid in self.age_gender_map:
                date_posted = xmlobj.find('date').text
                content = xmlobj.find('content')
                text = ""
                for child in content: #text or treatment
                    if(child.text):
                        text += child.text + " "
                return uid, date_posted, text
        return None

    #all data was read on 2013; so the age is the age of the poster in 2013
    def get_actual_age_group(self, age_now, date_posted):
        year_obtained = 15
        if(age_now):
            age = int(age_now)
            year_posted = int(date_posted.split()[0].split("/")[-1])
            actual_age = age - (year_obtained - year_posted)
            # if actual_age > 2015:
            #     print age, year_obtained, year_posted
            return actual_age, self.get_age_group(actual_age)
        else:
            self.stats["no_age"] += 1
            return "NA"

    #find out the range under which the age falls
    def get_age_group(self, age):
        for min, max in self.age_groups:
            if(min <= age <= max):
                if(min == 65):
                    return "65-xx"
                else:
                    return str(min) + "-" + str(max)
        return "NA"

    def write_to_file(self, opath):
        # print "total posts: ", len(self.posts)
        with open(opath, "w") as posts_dump:
            dump(self.posts, posts_dump)

    def get_stats(self):
        return self.stats

def pad_time(date_posted):
    dd, dt = date_posted.split()
    if len(dt.split(":")[0]) < 2:
        return dd + " 0" + dt
    else:
        return date_posted

def posts_per_disease(disease):
    if not os.path.isfile(os.path.join(opdir, disease + "_posts.dump")):
        cds = CreateDataset(age_gender_file, opdir, age_groups, disease)
        xmldir = os.path.join(xmlroot, disease, "XML")
        print xmldir
        cds.createds(xmldir)
        if not os.path.isfile(os.path.join(opdir, disease + "_posts.dump")):
            cds.write_to_file(os.path.join(opdir, disease + "_posts.dump"))

if __name__ == "__main__":
    # age_gender_file = "/project/solorio/prasha/ds/processed_data/username_id_age_gender_both_age_gender.txt"
    # xmlroot = "/project/solorio/prasha/ds/data"
    # opdir = "/project/solorio/prasha/ds/processed_data/disease_post_dumps"
    ###TEMP uncomment below; comment above
    age_gender_file = "/home/ritual/resources/dailystrength/processed_data/username_id_age_gender_both_age_gender.txt"
    xmlroot = "/home/ritual/resources/dailystrength/data"
    opdir = "/home/ritual/resources/dailystrength/processed_data/disease_post_dumps_w_date_posted"
    # age_groups = [(18, 24), (25, 34), (35, 49), (50, 64), (65, 200)]
    age_groups = []

    diseases = next(os.walk(os.path.join(xmlroot)))[1]
    diseases.remove("PeopleHTML")
    random.shuffle(diseases)
    try:
        # disease = diseases[int(sys.argv[1])]
        disease = sys.argv[1]
        print disease
        posts_per_disease(disease)
    except:
        for disease in diseases:
            print disease
            posts_per_disease(disease)

# if __name__ == "__main__":
#     # age_gender_file = "/project/solorio/prasha/ds/processed_data/username_id_age_gender_both_age_gender.txt"
#     # xmlroot = "/project/solorio/prasha/ds/data"
#     # opdir = "/project/solorio/prasha/ds/processed_data/disease_post_dumps"
#     ###TEMP uncomment below; comment above
#     age_gender_file = "/home/ritual/resources/dailystrength/processed_data/username_id_age_gender_both_age_gender.txt"
#     xmlroot = "/home/ritual/resources/dailystrength/data"
#     opdir = "/home/ritual/resources/dailystrength/processed_data/disease_post_dumps_w_date_posted"
#     # age_groups = [(18, 24), (25, 34), (35, 49), (50, 64), (65, 200)]
#     age_groups = []
#
#     diseases = sorted(next(os.walk(os.path.join(xmlroot)))[1])
#     diseases.remove("PeopleHTML")
#     try:
#         disease = diseases[int(sys.argv[1])]
#         print disease
#         posts_per_disease(disease)
#     except:
#         for disease in diseases:
#             print disease
#             posts_per_disease(disease)
#






