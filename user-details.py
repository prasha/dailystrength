__author__ = 'prasha'
import os
from bs4 import BeautifulSoup #pip install beautifulsoup4

def update_age(age_groups, age):
    if(age.isdigit()):
        for ages in sorted(age_groups):
            start, end = ages
            if(int(age) in range(start, end)):
                age_groups[ages] += 1


def update_gender(genders, gender):
    if(gender in genders):
        genders[gender] += 1

def get_user_details(fname):
    user_details = None
    username = None
    html = open(os.path.join(people_html_dir, fname)).read()
    soup = BeautifulSoup(html)
    #extrat the div that contains the user_details
    user_details_div = soup.find("div", {"id": "user-details"})
    if (user_details_div):
        username_div = user_details_div.findNext('div', {"class": 'username'})
        if(username_div):
            username = username_div.text
        else:
            print fname
        more_details_div = user_details_div.findNext('div', {"class": 'more-details'})
        if (more_details_div):
            user_details = more_details_div.text
    return username, user_details


def get_uname_from_uids():
    uids = ["1016025", "103715", "1065694", "111835", "1140343", "116586", "119548", "1200570", "124675", "133126", "1382909", "146694", "148375", "150576", "1531425", "155557", "1580719", "166758", "16819", "16940", "1775465", "1839776", "1847865", "185966", "190535", "1956255", "1970417", "198893", "1989980", "2025010", "2037519", "2101063", "210299", "2120017", "2148", "220131", "225303", "2394126", "2452748", "247304", "249070", "2500778", "260144", "2619722", "262068", "2658460", "26637", "2672350", "26943", "284653", "294146", "2959266", "3027364", "3053398", "3060276", "306523", "3089650", "311892", "3184357", "3235879", "3278905", "3284209", "3351473", "3386499", "3403705", "3416449", "3423159", "355986", "357985", "367525", "423414", "43358", "435051", "44622", "449967", "458025", "470613", "480806", "481694", "484924", "487771", "49246", "492566", "504539", "5058", "50737", "522730", "527917", "553559", "556031", "558228", "559537", "562329", "565904", "573823", "579358", "580256", "58768", "589746", "600003", "616660", "62103", "675466", "711465", "87753", "898270", "92216"]
    for uid in uids:
        html = open(os.path.join(people_html_dir, uid + ".html")).read()
        soup = BeautifulSoup(html)
        user_details_div = soup.find("div", {"id": "user-details"})
        if (user_details_div):
            username_div = user_details_div.findNext('div', {"class": 'username'})
            if(username_div):
                username = username_div.text.strip()
            else:
                print "ERROR!!!!:", uid
            print uid, username

if __name__ == "__main__":
    result = open("/project/solorio/prasha/ds/processed_data/username_id_age_gender.txt", "w")
    result_age_gender = open("/project/solorio/prasha/ds/processed_data/username_id_age_gender_both_age_gender.txt", "w")
    people_html_dir = "/project/solorio/prasha/ds/data/PeopleHTML"

    # result = open("/Users/shrprasha/Projects/resources/dailystrength/ds_data/username_id_age_gender.txt", "w")
    # result_age_gender = open("/Users/shrprasha/Projects/resources/dailystrength/ds_data/username_id_age_gender_both_age_gender.txt", "w")
    #fname = "/nethome/students/prasha/Documents/dailystrength/PeopleHTML/383150.html"
    #fname = "/nethome/students/prasha/Documents/dailystrength/PeopleHTML/339642.html"
    # people_html_dir = "/Users/shrprasha/Projects/resources/dailystrength/ds_raw_data/PeopleHTML"
    files = next(os.walk(people_html_dir))[2] #get just the files and not directories
    user_age_gender = {}
    user_only_age = {}
    user_only_gender = {}
    age_groups = {(10,20) : 0, (20,30) : 0, (30,40) : 0, (40,50) : 0, (50,60) : 0, (60,70) : 0, (70, 5000): 0}
    genders = {"Female": 0, "Male": 0}

    for fname in sorted(files):
        username, user_details = get_user_details(fname)
        if (user_details):
            username = username.strip()
            user_id, _ = fname.split(".")
            age_gender = user_details.split(",")
            if (len(age_gender) > 0):
                data = "" + username + ","
                if (age_gender[0].strip().isdigit()):
                    user_only_age[user_id] = age_gender[0].strip()
                    data += user_id + "," + age_gender[0].strip() + ","
                elif (age_gender[0].strip() in ["Female", "Male"]):
                    if (len(age_gender) > 1 and age_gender[1].strip().isdigit()):
                        user_age_gender[user_id] = [age_gender[1].strip(), age_gender[0].strip()]
                        data += user_id + "," + age_gender[1].strip() + "," + age_gender[0].strip()
                        result_age_gender.write(data + "\n")
                        result_age_gender.flush()
                    else:
                        user_only_gender[user_id] = age_gender[0].strip()
                        data += user_id + ",," + age_gender[0].strip()
                if(data):
                    result.write(data + "\n")
                    result.flush()
                #update_age(age_groups, age_gender[0].strip())
                #update_gender(genders, age_gender[1].strip())

    print age_groups
    print genders

    print len(user_age_gender), user_age_gender
    print len(user_only_age), user_only_age
    print len(user_only_gender), user_only_gender


    # get_uname_from_uids()

