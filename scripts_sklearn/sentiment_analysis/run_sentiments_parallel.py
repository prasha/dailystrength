from __future__ import division
from collections import Counter
import os
import numpy as np
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.cross_validation import train_test_split
import scripts_sklearn.file_utils as futils
from new_data_processing_scripts.user import User, Post
from scripts_sklearn.features import PostsVectorizer
from scripts_sklearn.sentiment_analysis.sentiments import SentiWordNetFeature
from operator import add
from joblib import Parallel, delayed

__author__ = 'shrprasha'

def get_aggregate(sentiments):
    aggr_acc = [[0]*4, [0]*4, [0]*4, [0]*4, [0]*4, [0]*4]
    for user_sentiments in sentiments: # len 6
        aggr_acc = [map(add, a, u) for a, u in zip(aggr_acc, user_sentiments)]
    return [[float(j)/len(sentiments) for j in i] for i in aggr_acc]

# for users having > 10 posts+replies,
# get sentiment for first 3 and last 3 posts/replies

def sentiment_analysis(rootdir):
    # if os.path.isfile(os.path.join(opdir, "user_details.pkl")):
    if os.path.isfile(os.path.join(opdir, "sentiments.pkl")):
        return futils.load_dumps(os.path.join(opdir, "user_details.pkl")), futils.load_dumps(os.path.join(opdir, "sentiments.pkl"))
    uid_fnames = futils.get_files_in_folder(rootdir)
    user_details_sentiments = Parallel(n_jobs=6, verbose=3)(delayed(get_user_details_sentiments)(uid_fname) for uid_fname in uid_fnames)
    user_details = []
    sentiments = []
    for x in user_details_sentiments:
        if x:
            user_details.append(x[0])
            sentiments.append(x[1])
    futils.create_dumps(user_details, os.path.join(opdir, "user_details.pkl"))
    futils.create_dumps(sentiments, os.path.join(opdir, "sentiments.pkl"))

    return user_details, sentiments

def get_user_details_sentiments(uid_fname):
    user = futils.load_dumps(os.path.join(rootdir, uid_fname))
    sorted_posts = user.get_posts_sorted()
    if sorted_posts and len(sorted_posts) >= min_posts_count:
        user_sentiments = get_valid_post_sentiments(sorted_posts, 3) +\
                          list(reversed(get_valid_post_sentiments(reversed(sorted_posts), 3)))   # list reversed for correct order
        return (user.uid, user.gender, user.age_group), user_sentiments

def get_valid_post_sentiments(sorted_posts, n=3):
    sentiment_list = []
    for post in sorted_posts:
        try:
            sents = senti.get_sentiments(post.text, polarity_only=True)
            sentiment_list.append(sents)
        except:
            pass
        if len(sentiment_list) == n:
            return sentiment_list

def get_sentiments_from_base_15(new_min_posts_count=25):
    opdir = os.path.join("/home/ritual/resources/dailystrength/after_sklearn/results/sentiments_parallel", "min_posts_" + str(new_min_posts_count))
    if os.path.isfile(os.path.join(opdir, "sentiments_polarity.pkl")):
        # print "here"
        return futils.load_dumps(os.path.join(opdir, "user_details.pkl")), futils.load_dumps(os.path.join(opdir, "sentiments_dum.pkl"))
    futils.create_opdir(opdir)
    min_15_opdir = "/home/ritual/resources/dailystrength/after_sklearn/results/sentiments_parallel/min_posts_15_unnormalized"
    user_details = futils.load_dumps(os.path.join(min_15_opdir, "user_details.pkl"))
    sentiments = futils.load_dumps(os.path.join(min_15_opdir, "sentiments.pkl"))
    op_sentiments = []
    op_user_details = []
    for d, s in zip(user_details, sentiments):
        uid = d[0]
        user = futils.load_dumps(os.path.join(rootdir, str(uid) + ".pkl"))
        if user.post_count() >= new_min_posts_count:
            print uid
            op_sentiments.append(s)
            op_user_details.append(d)
    futils.create_dumps(user_details, os.path.join(opdir, "user_details.pkl"))
    futils.create_dumps(sentiments, os.path.join(opdir, "sentiments.pkl"))
    return op_user_details, op_sentiments

def get_first_last_bins(sentiments, which=3, bins=20):  # which 0-> 1-neg-pos, neg, pos, polarity
    firsts = []
    lasts = []
    for user_sentiment in sentiments:
        # print user_sentiment
        firsts.extend([x[which] for x in user_sentiment[:3]])
        lasts.extend([x[which] for x in user_sentiment[3:]])
    bins_counts = np.histogram(firsts, bins=bins)
    return zip(bins_counts[1], bins_counts[0], np.histogram(lasts, bins=bins)[0])

def polarity_flip_users(sentiments, which=3):
    flips = {"np": 0, "pn": 0, "nn": 0, "pp":0}
    for user_sentiment in sentiments:
        firsts = sum([x[which] for x in user_sentiment[:3]])
        lasts = sum([x[which] for x in user_sentiment[3:]])
        if firsts < 0:
            if lasts > 0:
                flips["np"] += 1
            elif lasts < 0:
                flips["nn"] += 1
        elif firsts > 0:
            if lasts > 0:
                flips["pp"] += 1
            elif lasts < 0:
                flips["pn"] += 1
    return flips


if __name__ == "__main__":
    rootdir = "/home/ritual/resources/dailystrength/after_sklearn/all_user_posts_separate"

    # min_posts_count = 25
    # opdir = os.path.join("/home/ritual/resources/dailystrength/after_sklearn/results/sentiments_parallel", "min_posts_" + str(min_posts_count))
    # senti = SentiWordNetFeature()
    # user_details, sentiments = sentiment_analysis(rootdir)

    _, sentiments = get_sentiments_from_base_15()
    # print "there"

    print polarity_flip_users(sentiments)

    # bins = np.arange(-1, 1.2, 0.1)
    # for interval, firsts_count, lasts_count in get_first_last_bins(sentiments, bins=bins):
    # # for interval, firsts_count, lasts_count in get_first_last_bins(sentiments, which=0):
    #     print interval, firsts_count, lasts_count

    # # print get_aggregate(sentiments)



