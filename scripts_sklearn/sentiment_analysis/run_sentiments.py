from __future__ import division
from collections import Counter
import os
import numpy as np
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.cross_validation import train_test_split
import scripts_sklearn.file_utils as futils
from new_data_processing_scripts.user import User, Post
from scripts_sklearn.features import PostsVectorizer
from scripts_sklearn.sentiment_analysis.sentiments import SentiWordNetFeature
from operator import add

__author__ = 'shrprasha'

def get_aggregate(sentiments):
    aggr_acc = [[0]*4, [0]*4, [0]*4, [0]*4, [0]*4, [0]*4]
    for user_sentiments in sentiments: # len 6
        aggr_acc = [map(add, a, u) for a, u in zip(aggr_acc, user_sentiments)]
    return [[float(j)/len(sentiments) for j in i] for i in aggr_acc]

# for users having > 10 posts+replies,
# get sentiment for first 3 and last 3 posts/replies
def sentiment_analysis(rootdir, n=3):
    uid_fnames = futils.get_files_in_folder(rootdir)
    user_details = []
    sentiments = []  #[ [ [aggr_pos_neg, pos, neg, pol]*6 ]per_user ]
    for uid_fname in uid_fnames:
        print uid_fname
        user = futils.load_dumps(os.path.join(rootdir, uid_fname))
        sorted_posts = user.get_posts_sorted()
        if sorted_posts and len(sorted_posts) > 15:
            user_details.append((user.uid, user.gender, user.age_group))
            user_sentiments = get_valid_post_sentiments(sorted_posts, n) +\
                              list(reversed(get_valid_post_sentiments(reversed(sorted_posts), n)))   # list reversed for correct order
            sentiments.append(user_sentiments)
        # if len(sentiments) > 10:
        #     break
    return user_details, sentiments

def get_valid_post_sentiments(sorted_posts, n=3):
    sentiment_list = []
    for post in sorted_posts:
        try:
            sents = senti.get_sentiments(post.text)
            sentiment_list.append(sents)
        except:
            pass
        if len(sentiment_list) == n:
            return sentiment_list

if __name__ == "__main__":
    rootdir = "/home/ritual/resources/dailystrength/after_sklearn/all_user_posts_separate"
    opdir = "/home/ritual/resources/dailystrength/after_sklearn/results/sentiments"
    age_ranges = [(12,17), (18,29), (30,49), (50,64), (65,110)]
    senti = SentiWordNetFeature()

    user_details, sentiments = sentiment_analysis(rootdir)
    futils.create_dumps(user_details, os.path.join(opdir, "user_details.pkl"))
    futils.create_dumps(sentiments, os.path.join(opdir, "sentiments.pkl"))

    print get_aggregate(sentiments)


