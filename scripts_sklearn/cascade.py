from __future__ import division
from collections import Counter
import os
from sklearn import metrics
import file_utils as futils
import run_expts


__author__ = 'shrprasha'


def get_cascade_accuracy_gender_total(expt_type, subtype=""):
    print expt_type, subtype

    results_opdir = os.path.join(results_rootopdir, expt_type, subtype)
    # familial_idxs_correctness = futils.load_dumps(os.path.join(results_rootopdir, expt_type, subtype.split("_", 1)[0],
    familial_idxs_correctness = futils.load_dumps(os.path.join(results_rootopdir, expt_type, subtype,
                                                               "familial_gender_id_correctness.pkl"))

    predicted = futils.load_dumps(os.path.join(results_opdir, "word_ngrams_combined_predictions.pkl"))
    predicted_gender = futils.load_dumps(os.path.join(results_opdir, "word_ngrams_gender_predictions.pkl"))
    predicted_age = futils.load_dumps(os.path.join(results_opdir, "word_ngrams_age_predictions.pkl"))

    y_test = futils.load_dumps(os.path.join(results_opdir, "y_test.pkl"))  # 13648 # unbalanced
    y_test_gender = [y.split("_")[0] for y in y_test]  # 4320 # balanced
    y_test_age = [y.split("_")[1] for y in y_test]

    cascaded_predictions_gender = [""] * len(y_test)
    for idx in range(len(y_test)):
        if idx in familial_idxs_correctness:
            correct = familial_idxs_correctness[idx]
            if correct:
                cascaded_predictions_gender[idx] = y_test_gender[idx]
            else:
                cascaded_predictions_gender[idx] = "Female" if len(y_test_gender[idx]) < 5 else "Male"
        else:
            cascaded_predictions_gender[idx] = predicted_gender[idx]
    # acc = run_expts.accuracy_score_percent(metrics.accuracy_score(y_test_gender, cascaded_predictions_gender))

    cascaded_predictions = [g+"_"+a for g,a in zip(cascaded_predictions_gender, predicted_age)]
    acc = run_expts.accuracy_score_percent(metrics.accuracy_score(y_test, cascaded_predictions))
    # print "5 class overall: ", acc

    print "before cascading: ", run_expts.accuracy_score_percent(metrics.accuracy_score(y_test_age, predicted_age)), \
        run_expts.accuracy_score_percent(metrics.accuracy_score(y_test_gender, predicted_gender)), \
        run_expts.accuracy_score_percent(metrics.accuracy_score(y_test, predicted))
    print "after cascading:", run_expts.accuracy_score_percent(metrics.accuracy_score(y_test_age, predicted_age)), \
        run_expts.accuracy_score_percent(metrics.accuracy_score(y_test_gender, cascaded_predictions_gender)), \
        run_expts.accuracy_score_percent(metrics.accuracy_score(y_test, cascaded_predictions))

def get_familial_accuracy(expt_type, subtype):
    familial_gender_id_correctness = futils.load_dumps(os.path.join("/Users/shrprasha/Projects/resources/after_sklearn/familial_results",
                             expt_type, subtype, "familial_gender_id_correctness.pkl"))
    # print familial_gender_id_correctness
    correct = sum([1 for x in familial_gender_id_correctness.values() if x])
    print expt_type, subtype, correct, len(familial_gender_id_correctness), correct / len(familial_gender_id_correctness)


if __name__ == "__main__":
    results_rootopdir = "/home/ritual/resources/dailystrength/after_sklearn/results"
    dumps_opdir = "/home/ritual/resources/dailystrength/after_sklearn/dumps"
    # # expt_type = "5_age_groups"
    # expt_type = "3_age_groups"
    # # subtype = "unbalanced"
    # # subtype = "unbalanced_word_char"
    # subtype = "balanced_word_char"
    # get_cascade_accuracy_gender_total(expt_type, subtype)

    expt_types = ["3_age_groups", "5_age_groups"]
    subtypes = ["balanced_word_char", "unbalanced_word_char"]
    for expt_type in expt_types:
        for subtype in subtypes:
            get_cascade_accuracy_gender_total(expt_type, subtype)


    # expt_types = ["3_age_groups", "5_age_groups"]
    # subtypes = ["balanced", "unbalanced"]
    # for expt_type in expt_types:
    #     for subtype in subtypes:
    #         get_familial_accuracy(expt_type, subtype)

