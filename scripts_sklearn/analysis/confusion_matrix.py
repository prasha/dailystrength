import os
from sklearn.metrics import classification_report

__author__ = 'shrprasha'
import file_utils as futils


def get_confusion_matrix(expt_type, subtype):
    results_opdir = os.path.join(results_rootopdir, expt_type, subtype)
    predicted_age = futils.load_dumps(os.path.join(results_opdir, "word_ngrams_age_predictions.pkl"))
    y_test = futils.load_dumps(os.path.join(results_opdir, "y_test.pkl"))
    classification_report(y_test, predicted_age)



if __name__ == "__main__":
    results_rootopdir = "/home/ritual/resources/dailystrength/after_sklearn/results"
    dumps_opdir = "/home/ritual/resources/dailystrength/after_sklearn/dumps"
    expt_type = "5_age_groups"
    subtype = "unbalanced_word_char"
    get_confusion_matrix(expt_type, subtype)

