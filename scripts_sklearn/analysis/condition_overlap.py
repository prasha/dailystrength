from collections import Counter
import itertools
import os
from new_data_processing_scripts.user import User, Post
import scripts_sklearn.file_utils as futils

__author__ = 'shrprasha'


def get_all_combinations(l1):
    combinations = []  # because itertools also returns tuple
    for i in range(2, len(l1)+1):
        combinations.extend(list(itertools.combinations(l1, i)))
    return combinations

def update_users_per_disease_per_demographic(user, diseases_per_user):
    for disease in diseases_per_user:
        if disease not in users_per_disease_count:
            users_per_disease_count[disease] = dict(zip(age_ranges, [0] * len(age_ranges)) + zip(genders, [0]*2))
        users_per_disease_count[disease][user.gender] += 1
    for age_range in age_ranges:
        if user.has_posts_in_age_range(age_range):
            for disease in diseases_per_user:
                users_per_disease_count[disease][age_range] += 1

def write_users_per_disease_count():
    with open(os.path.join(results_rootopdir, "users_per_disease_count.tsv"), "w") as fhandle:
        fhandle.write("Disease\t(12,17)\t(18,29)\t(30,49)\t(50,64)\t(65,110)\tMale\tFemale\n")
        for disease in sorted(users_per_disease_count):
            fhandle.write(disease)
            for age_gender in age_ranges + genders:
                fhandle.write("\t" + str(users_per_disease_count[disease][age_gender]))
            fhandle.write("\n")

def write_conditions_overlap():
    with open(os.path.join(results_rootopdir, "condition_overlap.tsv"), "w") as fhandle:
        for disease_combination, user_count in disese_combinations_counter.most_common(1000):
            fhandle.write(",".join(disease_combination) + "\t" + str(len(disease_combination)) + "\t" + str(user_count) + "\n")


def write_diseases_count():
    with open(os.path.join(results_rootopdir, "users_count_per_combination.tsv"), "w") as fhandle:
        for count, no_of_users in Counter(diseases_count).most_common():
            fhandle.write(str(count) + "\t" + str(no_of_users) + "\n")


if __name__ == "__main__":
    rootdir = "/home/ritual/resources/dailystrength/after_sklearn/all_user_posts_separate"
    results_rootopdir = "/home/ritual/resources/dailystrength/after_sklearn/results/analysis"

    users_per_disease_count = {}  #{condition: {age_group/gender: count} }
    age_ranges = [(12,17), (18,29), (30,49), (50,64), (65,110)]
    genders = ["Male", "Female"]

    all_common_disease_combinations = []
    # all_users = get_user_posts(rootdir)

    diseases_count = []
    uid_fnames = futils.get_files_in_folder(rootdir)
    for uid_fname in uid_fnames:
        user = futils.load_dumps(os.path.join(rootdir, uid_fname))
        print user.uid,
        diseases_per_user = set([post.disease for posts in user.posts.values() for post in posts])
        no_of_diseases = len(diseases_per_user)
        print no_of_diseases

        diseases_count.append(no_of_diseases)
        if no_of_diseases < 2 or no_of_diseases > 15:
            continue
        all_common_disease_combinations.extend(get_all_combinations(diseases_per_user))
        update_users_per_disease_per_demographic(user, diseases_per_user)
    disese_combinations_counter = Counter(all_common_disease_combinations)
    write_conditions_overlap()
    write_users_per_disease_count()
    write_diseases_count()




    # # l1 = set(["A", "B", "c", "d", "A"])
    # l1 = set(["A"])
    # print Counter(get_all_combinations(l1)), sum(Counter(get_all_combinations(l1)).values())