from __future__ import division
from collections import Counter
import os
import numpy as np
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.cross_validation import train_test_split
import scripts_sklearn.file_utils as futils
from new_data_processing_scripts.user import User, Post
from scripts_sklearn.features import PostsVectorizer
import scripts_sklearn.run_expts as run_expts


__author__ = 'shrprasha'

def get_tfidf(posts, labels):
    post_vectorizer = PostsVectorizer(analyzer='word', ngram_range=(1, 3), stop_words='english')
    tf_idf_per_post = post_vectorizer.fit_transform(posts)
    ngrams = post_vectorizer.get_feature_names()

    tf_idf_per_post = tf_idf_per_post.toarray()

    for i, val in enumerate(tf_idf_per_post):
        tf_idf_per_class = Counter(dict(zip(ngrams, list(val))))
        with open((os.path.join(results_rootopdir, labels[i] + '.tsv')), "w") as fhandle:
            for ngram, tfidf in tf_idf_per_class.most_common(1000):
                fhandle.write(ngram + "\t" + str(tfidf) + "\n")

if __name__ == "__main__":
    rootdir = "/home/ritual/resources/dailystrength/after_sklearn/all_user_posts_separate"
    results_rootopdir = "/home/ritual/resources/dailystrength/after_sklearn/results/analysis/tfidf_keywords"
    # results_rootopdir = "/Users/shrprasha/del/analysis"
    dumps_opdir = "/home/ritual/resources/dailystrength/after_sklearn/dumps"    # expt_type = "3_age_groups"
    expt_type = "5_age_groups"
    subtype = "unbalanced"
    age_ranges = [(12,17), (18,29), (30,49), (50,64), (65,110)]
    typedir = os.path.join(dumps_opdir, expt_type)
    all_users_posts, _, user_age_groups, user_gender_age, _ = run_expts.get_data(rootdir, typedir, age_ranges)

    # all_users_posts = all_users_posts[:10]
    # user_age_groups = user_age_groups[:10]
    # user_gender_age = user_gender_age[:10]

    age_groups = sorted(set(user_age_groups))
    gender_ages = sorted(set(user_gender_age))
    print age_groups
    print gender_ages

    posts_a_t_age_groups = [[]] * len(age_groups)
    posts_a_t_gender_age = [[]] * len(gender_ages)

    for posts_list, age_group, gender_age in zip(all_users_posts, user_age_groups, user_gender_age):
        posts_a_t_age_groups[age_groups.index(age_group)].extend(posts_list)
        posts_a_t_gender_age[gender_ages.index(gender_age)].extend(posts_list)

    get_tfidf(posts_a_t_age_groups, age_groups)
    get_tfidf(posts_a_t_gender_age, gender_ages)


    # # posts = [["this is a test", "this is the next post. the second."], ["another age group"]]
    # posts = [["person1post1", "person1post2"], ["person2post1"]]
    # get_tfidf(posts, ['p1', 'p2'])


