import os
from pickle import load
from new_data_processing_scripts.dump_users import get_user_details
from new_data_processing_scripts.user import User, Post
import file_utils as futils

__author__ = 'ritual'

# currently, posts of a user are under uid_age-group
# merge all posts of a single user
# add gender to fname
# will make User class and user files obsolete
# new fnames: uid_gender
# file will contain all the posts of a user, regardless of age group
# did not do: filter out the users that do not fall under disjoint classes total 78316; since: total was not the same as 78316 but was 71515 as in readme!??

def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

# unused
def non_filtered_posts():
    users_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps"
    train_users = load_dumps(os.path.join(users_rootdir, "train.dump"))
    dev_users = load_dumps(os.path.join(users_rootdir, "dev.dump"))
    test_users = load_dumps(os.path.join(users_rootdir, "test.dump"))


    all_users = train_users.values() + dev_users.values() + test_users.values()
    # get unique uids and their genders
    uid_gender = {}
    for user in all_users:
        if user.uid not in uid_gender:
            uid_gender[user.uid] = user.gender
    print len(all_users), len(uid_gender)


if __name__ == "__main__":
    # user_posts_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps"
    # opdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps_combined"
    # age_gender_file = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/username_id_age_gender_both_age_gender.txt"

    user_posts_rootdir = "/home/ritual/resources/ds_cluster/continous_age_groups/user_post_dumps_continuous_age_groups"
    # opdir = "/home/ritual/resources/ds_cluster/user_post_dumps"
    age_gender_file = "/home/ritual/resources/ds_cluster/processed_data/username_id_age_gender_both_age_gender.txt"
    user_details = get_user_details(age_gender_file)

    fnames = futils.get_files_in_folder(user_posts_rootdir)
    uids = list(set([f.split("_")[0] for f in fnames]))
    print len(fnames), len(uids)

    uid_posts = {}
    for fname in fnames:
        print fname
        uid = fname.split("_")[0]
        if uid not in uid_posts:
            uname, gender = user_details[uid]
            user = User(uid, uname, gender)
            uid_posts[uid] = user
        uid_posts[uid].posts = futils.load_dumps(os.path.join(user_posts_rootdir, fname))
    print len(uid_posts)
    # futils.create_dumps(uid_posts, "/home/ritual/resources/dailystrength/after_sklearn/all_users_posts.pkl")

    for uid, user in uid_posts.iteritems():
        print uid
        futils.create_dumps(user, os.path.join("/home/ritual/resources/dailystrength/after_sklearn/all_user_posts_separate", uid + ".pkl"))















