from __future__ import division
from collections import Counter
import os
import numpy as np
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.cross_validation import train_test_split
from textblob import TextBlob
import scripts_sklearn.file_utils as futils
from new_data_processing_scripts.user import User, Post
from scripts_sklearn.features import PostsVectorizer
from scripts_sklearn.sentiment_analysis.sentiments import SentiWordNetFeature
from operator import add
from joblib import Parallel, delayed

__author__ = 'shrprasha'


######## not complete



def get_valid_post_sentiments(sorted_posts, n=3):
    sentiment_list = []
    for post in sorted_posts:
        try:
            sents = senti.get_sentiments(post.text)
            sentiment_list.append(sents)
        except:
            pass
        if len(sentiment_list) == n:
            return sentiment_list

def update_sentiments_base_15():
    min_15_opdir = "/home/ritual/resources/dailystrength/after_sklearn/results/sentiments_parallel/min_posts_15_unnormalized"
    opdir = "/home/ritual/resources/dailystrength/after_sklearn/results/sentiments_parallel/min_posts_15"
    user_details = futils.load_dumps(os.path.join(min_15_opdir, "user_details.pkl"))
    sentiments = futils.load_dumps(os.path.join(min_15_opdir, "sentiments.pkl"))
    op_sentiments = []
    op_user_details = []
    for d, s in zip(user_details, sentiments):
        uid = d[0]
        user = futils.load_dumps(os.path.join(rootdir, str(uid) + ".pkl"))
        sorted_posts = user.get_posts_sorted()
        for post in sorted_posts:
            try:
                blob = TextBlob(post.text)
                len(blob.sentences)
            except:
                pass
    futils.create_dumps(sentiments, os.path.join(opdir, "sentiments.pkl"))
    return op_user_details, op_sentiments


def normalize_post_polarity(sorted_posts, n=3):
    sentiment_list = []
    for post in sorted_posts:
        try:
            sents = senti.get_sentiments(post.text)
            sentiment_list.append(sents)
        except:
            pass
        if len(sentiment_list) == n:
            return sentiment_list


def get_first_last_bins(sentiments, which=3, bins=20):  # which 0-> 1-neg-pos, neg, pos, polarity
    firsts = []
    lasts = []
    for user_sentiment in sentiments:
        firsts.extend([x[which] for x in user_sentiment[:3]])
        lasts.extend([x[which] for x in user_sentiment[3:]])
    bins_counts = np.histogram(firsts, bins=bins)
    return zip(bins_counts[1], bins_counts[0], np.histogram(lasts, bins=bins)[0])


def add_dummy_pos_neg():
    sentiments = futils.load_dumps("/home/ritual/resources/dailystrength/after_sklearn/results/sentiments_parallel/min_posts_25/sentiments_polarity.pkl")
    op_sentiments = []
    for user_sentiments in sentiments:
        op_sentiments.append([[0, 0, 0, x] for x in user_sentiments])
    futils.create_dumps(op_sentiments, "/home/ritual/resources/dailystrength/after_sklearn/results/sentiments_parallel/min_posts_25/sentiments.pkl")


def combine():
    sentiments = futils.load_dumps("/home/ritual/resources/dailystrength/after_sklearn/results/sentiments_parallel/min_posts_25/sentiments.pkl")
    sentiments_polarity = futils.load_dumps("/home/ritual/resources/dailystrength/after_sklearn/results/sentiments_parallel/min_posts_25/sentiments_polarity.pkl")
    op_sentiments = []
    for sent, sentp in zip(sentiments, sentiments_polarity):
        op_sentiments.append([[s[0], s[1], s[2], sp] for s, sp in zip(sent, sentp)])
    futils.create_dumps(op_sentiments, "/home/ritual/resources/dailystrength/after_sklearn/results/sentiments_parallel/min_posts_25/sentiments.pkl")



if __name__ == "__main__":
    rootdir = "/home/ritual/resources/dailystrength/after_sklearn/all_user_posts_separate"

    combine()
    # add_dummy_pos_neg()
    # update_sentiments_base_15()


