__author__ = 'shrprasha'

import os
from pickle import load, dump
from new_data_processing_scripts.dump_users import get_user_details
from new_data_processing_scripts.user import User, Post
import file_utils as futils


def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

if __name__ == "__main__":
    disease_dump_rootdir = "/home/ritual/resources/dailystrength/processed_data/disease_post_dumps_w_date_posted"
    user_posts_dump_rootdir = "/home/ritual/resources/dailystrength/after_sklearn/user_post_dumps_w_date_posted"
    age_gender_file = "/home/ritual/resources/ds_cluster/processed_data/username_id_age_gender_both_age_gender.txt"
    user_details = get_user_details(age_gender_file)

    all_files = sorted(next(os.walk(disease_dump_rootdir))[2])
    all_posts = []
    for fname in all_files:
        with open(os.path.join(disease_dump_rootdir, fname), "r") as fhandle:
            all_posts.extend(load(fhandle))

    uid_posts = {}  # uid: post_list
    for post in all_posts:
        uid = post.uid
        if uid not in uid_posts:
            uname, gender = user_details[uid]
            user = User(uid, uname, gender)
            uid_posts[uid] = user
        uid_posts[uid].posts = [post]

    for uid, user in uid_posts.iteritems():
        print uid
        futils.create_dumps(user, os.path.join(user_posts_dump_rootdir, uid + ".pkl"))





