from __future__ import division
from collections import Counter
import os
import numpy as np
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.cross_validation import train_test_split
import scripts_sklearn.file_utils as futils
from new_data_processing_scripts.user import User, Post
from scripts_sklearn.features import PostsVectorizer
from scripts_sklearn.sentiment_analysis.sentiments import SentiWordNetFeature
from operator import add
from joblib import Parallel, delayed

__author__ = 'shrprasha'

def get_aggregate(sentiments):
    aggr_acc = [[0]*4, [0]*4, [0]*4, [0]*4, [0]*4, [0]*4]
    for user_sentiments in sentiments: # len 6
        aggr_acc = [map(add, a, u) for a, u in zip(aggr_acc, user_sentiments)]
    return [[float(j)/len(sentiments) for j in i] for i in aggr_acc]

# for users having > 10 posts+replies,
# get sentiment for first 3 and last 3 posts/replies

def sentiment_analysis(rootdir):
    if os.path.isfile(os.path.join(opdir, "user_details.pkl")):
        return futils.load_dumps(os.path.join(opdir, "user_details.pkl")), futils.load_dumps(os.path.join(opdir, "sentiments.pkl"))
    uid_fnames = futils.get_files_in_folder(rootdir)
    user_details_sentiments = Parallel(n_jobs=6, verbose=4)(delayed(get_user_details_sentiments)(uid_fname) for uid_fname in uid_fnames)
    user_details = []
    sentiments = []
    for x in user_details_sentiments:
        if x:
            user_details.append(x[0])
            sentiments.append(x[1])
    futils.create_dumps(user_details, os.path.join(opdir, "user_details.pkl"))
    futils.create_dumps(sentiments, os.path.join(opdir, "sentiments.pkl"))

    return user_details, sentiments

def get_user_details_sentiments(uid_fname):
    user = futils.load_dumps(os.path.join(rootdir, uid_fname))
    sorted_posts = user.get_posts_sorted()
    if sorted_posts and len(sorted_posts) >= min_posts_count:
        user_sentiments = get_valid_post_sentiments(sorted_posts, 3) +\
                          list(reversed(get_valid_post_sentiments(reversed(sorted_posts), 3)))   # list reversed for correct order
        return (user.uid, user.gender, user.age_group), user_sentiments

def get_valid_post_sentiments(sorted_posts, n=3):
    sentiment_list = []
    for post in sorted_posts:
        try:
            sents = senti.get_sentiments(post.text)
            sentiment_list.append(sents)
        except:
            pass
        if len(sentiment_list) == n:
            return sentiment_list

def get_sentiments_from_base_15(min_posts_count):
    min_15_opdir = "/home/ritual/resources/dailystrength/after_sklearn/results/sentiments_parallel/min_posts_15"
    user_details = futils.load_dumps(os.path.join(opdir, "user_details.pkl"))
    sentiments = futils.load_dumps(os.path.join(opdir, "sentiments.pkl"))
    for d, s in zip(user_details, sentiments):
        uid = d[0]
        user = futils.load_dumps(os.path.join(rootdir, str(uid) + ".pkl"))
        if user.post_count() >= 25:


if __name__ == "__main__":
    rootdir = "/home/ritual/resources/dailystrength/after_sklearn/all_user_posts_separate"

