from __future__ import division
import os
from scripts_sklearn import run_expts

__author__ = 'ritual'
import file_utils as futils
from collections import Counter


def get_majority_baseline(y_test_fpath):
    y_test = futils.load_dumps(y_test_fpath)
    y_test_gender = [y.split("_")[0] for y in y_test]
    y_test_age = [y.split("_")[1] for y in y_test]

    print len(y_test)
    print round(max(Counter(y_test_age).values()) * 100 / len(y_test), 2)
    print round(max(Counter(y_test_gender).values()) * 100 / len(y_test), 2)
    print round(max(Counter(y_test).values()) * 100 / len(y_test), 2)


def get_stats():
    dumps_opdir = "/home/ritual/resources/dailystrength/after_sklearn/dumps"
    # IMP: expt_type and age ranges are connected
    expt_type = "5_age_groups"
    age_ranges = [(12,17), (18,29), (30,49), (50,64), (65,110)]
    typedir = os.path.join(dumps_opdir, expt_type)
    _, user_genders, user_age_groups, user_gender_age, _ = run_expts.get_data(typedir, age_ranges)
    # print len(user_gender_age), user_gender_age[1]
    ctr = Counter(user_gender_age)
    fcount_tot = 0
    mcount_tot = 0
    for age_range in age_ranges:
        fcount = ctr["Female" + "_" + str(age_range)]
        mcount = ctr["Male" + "_" + str(age_range)]
        total = fcount + mcount
        fcount_tot += fcount
        mcount_tot += mcount
        print str(age_range[0]) + "-" + str(age_range[1]), "&",\
              fcount, "&", \
              mcount, "&", \
              total, "(" + str(round(total*100/len(user_gender_age), 2)) + "\\%) \\\\"
    print "Total", "&", \
        fcount_tot, "(" + str(round(fcount_tot*100/len(user_gender_age), 2)) + "\\%) &",\
        mcount_tot, "(" + str(round(mcount_tot*100/len(user_gender_age), 2)) + "\\%) &",\
        len(user_gender_age)




if __name__ == "__main__":
    # results_rootopdir = "/home/ritual/resources/dailystrength/after_sklearn/results"
    # dumps_opdir = "/home/ritual/resources/dailystrength/after_sklearn/dumps"
    # expt_type = "5_age_groups"
    # # subtype = "unbalanced"
    # subtype = "balanced"
    # y_test_fpath = os.path.join(results_rootopdir, expt_type, subtype, "y_test.pkl")
    # # y_test_fpath = "/home/ritual/resources/dailystrength/after_sklearn/results0/5_age_groups/unbalanced_5_age_groups/y_test.pkl"
    # # y_test_fpath = "/home/ritual/resources/dailystrength/after_sklearn/results0/5_age_groups/balanced_5_age_groups/y_test.pkl"
    # # y_test_fpath = "/home/ritual/resources/dailystrength/after_sklearn/results0/3_age_groups/unbalanced_3_age_groups/y_test.pkl"
    # # y_test_fpath = "/home/ritual/resources/dailystrength/after_sklearn/results0/3_age_groups/balanced_3_age_groups/y_test.pkl"
    # get_majority_baseline(y_test_fpath)


    get_stats()


