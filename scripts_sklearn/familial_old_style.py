from __future__ import division
import run_expts
import re
from collections import Counter
import os
import numpy as np
from sklearn import metrics
from sklearn.cross_validation import train_test_split
import file_utils as futils
from new_data_processing_scripts.user import User, Post

__author__ = 'shrprasha'


class Features:

    def __init__(self, dumpdir=""):
        self.dumpdir = dumpdir
        futils.create_opdir(dumpdir)


    def get_bucket_count_each_token(self, matches, buckets):
        counts_bucket = {}
        counts_each_token = []
        words = []
        bc = {}
        # init
        for bucket in buckets:
            counts_bucket[bucket] = 0
            ###
            bc[bucket] = []
        #count word in bucket
        for match in matches:
            word = match.lower()
            temp = sum(counts_bucket.values())
            for bucket, bucket_tokens in buckets.iteritems():
                if word in bucket_tokens:
                    counts_bucket[bucket] += 1
                    ###
                    bc[bucket].append(word)
            if temp == sum(counts_bucket.values()):
                print match
                print "error:", word, counts_bucket
            words.append(word)
        words_counter = Counter(words)
        for token in self.each_token:
            if token in words_counter:
                counts_each_token.append(words_counter[token])
            else:
                counts_each_token.append(0)
        #print bc
        return counts_bucket, counts_each_token

    def find_matches(self, text, regex_list):
        regx_matches = []
        for regxp in regex_list:
            rexp = re.compile(regxp, re.IGNORECASE)
            matches = rexp.findall(text)
            regx_matches.extend(matches)
        return regx_matches

    def get_familial_token_feature(self, posts):
        self.text = ""
        for post in posts:
            self.text += post + "\n"

        regex_list_familial = [
            r"\bmy\b\W+\b((?:son|daughter|grandson|granddaughter|father|mother|brother|sister|uncle|aunt|cousin|nephew|niece|family|godson|goddaughter|grandchild|grandmother|grandfather|husband|wife|bf|gf|boyfriend|girlfriend|hubby|baby|babies|child|children|kids|mom|parent))\W*\b",
            r"\b(?:DH|DS|DD|DW)\b"]
        gender_buckets = {"male": [u'wife', u'gf', u'girlfriend', u'dw'],
                          "female": [u'husband', u'bf', u'boyfriend', u'dh', u'hubby'],
                          "neutral": [u'ds', u'dd', u'son', u'daughter', u'grandson', u'granddaughter', u'father',
                                      u'mother', u'brother', u'sister', u'uncle', u'aunt', u'cousin', u'nephew', u'niece',
                                      u'family', u'godson', u'goddaughter', u'grandchild', u'grandmother', u'grandfather',
                                      u'baby', u'babies', u'child', u'children', u'kids', u'mom', u'parent']}
        self.each_token = [u'son', u'daughter', u'grandson', u'granddaughter', u'father', u'mother', u'brother', u'sister',
                      u'uncle', u'aunt', u'cousin', u'nephew', u'niece', u'family', u'godson', u'goddaughter',
                      u'grandchild', u'grandmother', u'grandfather', u'husband', u'wife', u'bf', u'gf', u'boyfriend',
                      u'girlfriend', u'dh', u'ds', u'dd', u'dw', u'baby', u'babies', u'child', u'children', u'kids', u'mom',
                      u'parent', u'hubby']
        familial_matches = self.find_matches(self.text, regex_list_familial)
        if not familial_matches:
            return None
        counts_bucket, counts_each_token = self.get_bucket_count_each_token(familial_matches, gender_buckets)
        #since order is important
        # counts_bucket_idx_feature = dict(zip(range(1, len(counts_bucket) + 1), [counts_bucket["male"], counts_bucket["female"], counts_bucket["neutral"]]))
        # counts_each_token_idx_feature = dict(zip(range(1, len(counts_each_token) + 1), counts_each_token))
        # each_token_rootdir = os.path.join(self.dumpdir, "familial_features", uid)
        # buckets_rootdir = os.path.join(self.dumpdir, "familial_gender_bucket_features")
        # futils.create_dumps(counts_each_token, each_token_rootdir)
        # futils.create_dumps(counts_bucket, buckets_rootdir)
        if counts_bucket["male"] > counts_bucket["female"]:
            return "Male"
        elif counts_bucket["female"] > counts_bucket["male"]:
            return "Female"
        else:
            return None

def perform_prediction(results_opdir):
    print "expt: ", results_opdir
    y_test_gender = [y.split("_")[0] for y in y_test]
    # print y_test_gender
    predictions = []
    y_actual = []
    idxs = {}   # {idx in result list for those having familial tokens: familial correct or not}
    for i, posts, actual_Y in zip(range(len(y_test_gender)), X_test, y_test_gender):
        pred = features.get_familial_token_feature(posts)
        if pred:
            predictions.append(pred)
            y_actual.append(actual_Y)
            idxs[i] = (pred==actual_Y)
    futils.create_dumps(idxs, os.path.join(results_opdir, "familial_gender_id_correctness.pkl"))
    print "accuracy: ", run_expts.accuracy_score_percent(metrics.accuracy_score(y_actual, predictions)), "total having familial", len(idxs), "total in test", len(y_test)

def perform_prediction_train(results_opdir):
    print "expt: ", results_opdir
    y_train_gender = [y.split("_")[0] for y in y_train]
    # print y_train_gender
    predictions = []
    y_actual = []
    idxs = {}   # {idx in result list for those having familial tokens: familial correct or not}
    for i, posts, actual_Y in zip(range(len(y_train_gender)), X_train, y_train_gender):
        pred = features.get_familial_token_feature(posts)
        if pred:
            predictions.append(pred)
            y_actual.append(actual_Y)
            idxs[i] = (pred==actual_Y)
    print "Train accuracy: ", run_expts.accuracy_score_percent(metrics.accuracy_score(y_actual, predictions)), "total having familial", len(idxs), "total in train", len(y_train)

if __name__ == "__main__":
    rootdir = "/home/ritual/resources/dailystrength/after_sklearn/all_user_posts_separate"
    results_rootopdir = "/home/ritual/resources/dailystrength/after_sklearn/results"
    dumps_opdir = "/home/ritual/resources/dailystrength/after_sklearn/dumps"
    # IMP: expt_type and age ranges are connected
    expt_type = "5_age_groups"
    age_ranges = [(12,17), (18,29), (30,49), (50,64), (65,110)]
    # expt_type = "3_age_groups"
    # age_ranges = [(13,27), (23,27), (33,47)]
    typedir = os.path.join(dumps_opdir, expt_type)
    all_users_posts, user_genders, user_age_groups, user_gender_age, uids = run_expts.get_data(rootdir, typedir, age_ranges)
    features = Features(results_rootopdir)
    X_train, X_test, y_train, y_test = train_test_split(all_users_posts, user_gender_age, test_size=0.20, random_state=42)
    # perform_prediction_train(os.path.join(results_opdir, expt_type, "unbalanced"))
    futils.create_opdir(os.path.join(results_rootopdir, expt_type, "unbalanced_word_char"))
    perform_prediction(os.path.join(results_rootopdir, expt_type, "unbalanced_word_char"))
    X_train, X_test, y_train, y_test = run_expts.train_test_split_balanced(all_users_posts, user_gender_age, stratify=user_genders, test_size=0.20, random_state=42)
    # perform_prediction_train(os.path.join(results_opdir, expt_type, "balanced"))
    futils.create_opdir(os.path.join(results_rootopdir, expt_type, "balanced_word_char"))
    perform_prediction(os.path.join(results_rootopdir, expt_type, "balanced_word_char"))

