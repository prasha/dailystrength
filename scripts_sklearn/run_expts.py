from __future__ import division
from collections import Counter
import os
import numpy as np
from sklearn import metrics
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import FeatureUnion, Pipeline
from sklearn.cross_validation import train_test_split
import file_utils as futils
from new_data_processing_scripts.user import User, Post
from scripts_sklearn.features import PostsVectorizer


__author__ = 'shrprasha'

def accuracy_score_percent(accuracy_score):
    return round(accuracy_score * 100, 2)

def train_test_split_balanced(x, y, stratify, test_size=0.20, random_state=42):
    counts = Counter(stratify)
    minority_count = min(counts.values())
    x_d = dict_by_category(x, stratify)
    y_d = dict_by_category(y, stratify)
    balanced_x = []
    balanced_y = []
    balanced_category = []
    rng =  np.random.RandomState(random_state)
    for cat in sorted(x_d):
        permutation = rng.permutation(len(x_d[cat]))
        balanced_x.extend([x_d[cat][idx] for idx in permutation[:minority_count]])
        balanced_y.extend([y_d[cat][idx] for idx in permutation[:minority_count]])
        balanced_category.extend([cat] * minority_count)
    print minority_count, len(balanced_category)
    return train_test_split(balanced_x, balanced_y, stratify=balanced_category, test_size=test_size, random_state=random_state)

def dict_by_category(l, category):
    d = {}
    for x, cat in zip(l, category):
        if cat not in d:
            d[cat] = [x]
        else:
            d[cat].append(x)
    return d

def perform_prediction(results_opdir, X_train, y_train, X_test, y_test):
    print "train:", len(X_train), "test", len(X_test)

    y_train_gender = [y.split("_")[0] for y in y_train]
    y_train_age = [y.split("_")[1] for y in y_train]
    y_test_gender = [y.split("_")[0] for y in y_test]
    y_test_age = [y.split("_")[1] for y in y_test]

    futils.create_dumps(y_test, os.path.join(results_opdir, "y_test.pkl"))

    word_feats = ( 'word', PostsVectorizer(analyzer='word', ngram_range=(1, 3)) )
    char_feats = ( 'char', PostsVectorizer(analyzer='char', ngram_range=(3, 3)) )
    union_features1 = ( 'features', FeatureUnion([word_feats, char_feats]) )

    classifier = ('logit', LogisticRegression(C=100))
    text_clf = Pipeline([
                        union_features1,
                        classifier
    ])
    print text_clf
    text_clf.fit(X_train, y_train_gender)
    predicted_5_gender = text_clf.predict(X_test)
    acc = accuracy_score_percent(metrics.accuracy_score(y_test_gender, predicted_5_gender))
    print "Gender: ", acc

    classifier = ('logit', LogisticRegression(C=10))
    text_clf = Pipeline([
                        union_features1,
                        classifier
    ])
    print text_clf
    text_clf.fit(X_train, y_train_age)
    predicted_5_age = text_clf.predict(X_test)
    acc = accuracy_score_percent(metrics.accuracy_score(y_test_age, predicted_5_age))
    print "Age: ", acc

    predicted_5 = [g+"_"+a for g,a in zip(predicted_5_gender, predicted_5_age)]
    futils.create_dumps(predicted_5, os.path.join(results_opdir, "word_ngrams_combined_predictions.pkl"))
    acc = accuracy_score_percent(metrics.accuracy_score(y_test, predicted_5))
    print "5 class overall: ", acc

    futils.create_dumps(predicted_5_gender, os.path.join(results_opdir, "word_ngrams_gender_predictions.pkl"))
    futils.create_dumps(predicted_5_age, os.path.join(results_opdir, "word_ngrams_age_predictions.pkl"))

    # text_clf.fit(X_train, y_train)
    # predicted_10 = text_clf.predict(X_test)
    # acc = accuracy_score_percent(metrics.accuracy_score(y_test, predicted_10))
    # print "Overall: ", acc
    # futils.create_dumps(predicted_10, os.path.join(results_opdir, "word_ngrams_overall_predictions.pkl"))

def perform_parameter_tuning(results_opdir, X_train, y_train, X_test, y_test):
    print "train:", len(X_train), "test", len(X_test)

    y_train_gender = [y.split("_")[0] for y in y_train]
    y_train_age = [y.split("_")[1] for y in y_train]
    y_test_gender = [y.split("_")[0] for y in y_test]
    y_test_age = [y.split("_")[1] for y in y_test]

    futils.create_dumps(y_test, os.path.join(results_opdir, "y_test.pkl"))

    word_feats = ( 'word', PostsVectorizer(analyzer='word', ngram_range=(1, 3)) )
    char_feats = ( 'char', PostsVectorizer(analyzer='char', ngram_range=(3, 3)) )
    union_features1 = ( 'features', FeatureUnion([word_feats, char_feats]) )
    classifier = ('logit', LogisticRegression(C=1))
    text_clf = Pipeline([
                        # char_feats,
                        # word_feats,
                        union_features1,
                        classifier
    ])

    print text_clf

    text_clf.fit(X_train, y_train_gender)
    predicted_5_gender = text_clf.predict(X_test)
    acc = accuracy_score_percent(metrics.accuracy_score(y_test_gender, predicted_5_gender))
    print "Gender: ", acc

    text_clf.fit(X_train, y_train_age)
    predicted_5_age = text_clf.predict(X_test)
    acc = accuracy_score_percent(metrics.accuracy_score(y_test_age, predicted_5_age))
    print "Age: ", acc

    predicted_5 = [g+"_"+a for g,a in zip(predicted_5_gender, predicted_5_age)]
    futils.create_dumps(predicted_5, os.path.join(results_opdir, "word_ngrams_combined_predictions.pkl"))
    acc = accuracy_score_percent(metrics.accuracy_score(y_test, predicted_5))
    print "5 class overall: ", acc

    futils.create_dumps(predicted_5_gender, os.path.join(results_opdir, "word_ngrams_gender_predictions.pkl"))
    futils.create_dumps(predicted_5_age, os.path.join(results_opdir, "word_ngrams_age_predictions.pkl"))

    # text_clf.fit(X_train, y_train)
    # predicted_10 = text_clf.predict(X_test)
    # acc = accuracy_score_percent(metrics.accuracy_score(y_test, predicted_10))
    # print "Overall: ", acc
    # futils.create_dumps(predicted_10, os.path.join(results_opdir, "word_ngrams_overall_predictions.pkl"))


def get_user_posts_a_t_age_ranges(rootdir, age_ranges):
    uid_fnames = futils.get_files_in_folder(rootdir)
    all_users_posts = []  # [[u1p1, u1p2, ...], []]
    user_genders = []
    user_age_groups = []
    user_gender_age = []  # necessary to divide into train test since order is lost
    uids = []
    for uid_fname in uid_fnames:
        print uid_fname
        user = futils.load_dumps(os.path.join(rootdir, uid_fname))
        for age_range in age_ranges:
            posts = user.get_posts_age_range(age_range)
            if posts:
                all_users_posts.append([post.text for post in posts])
                user_genders.append(user.gender)
                user_age_groups.append(str(age_range))  # works better as a label
                user_gender_age.append((user.gender + "_" + str(age_range))) # works better as a label
                uids.append(uid_fname)
        # if len(all_users_posts) >= 100:
        #     # print len(user_genders), user_genders
        #     # print len(user_age_groups), user_age_groups
        #     break
    return all_users_posts, user_genders, user_age_groups, user_gender_age, uids

def get_data(rootdir, typedir, age_ranges):
    if not os.path.exists(os.path.join(typedir, "all_users_posts.pkl")):
        all_users_posts, user_genders, user_age_groups, user_gender_age, uids = get_user_posts_a_t_age_ranges(rootdir, age_ranges)
        futils.create_dumps(all_users_posts, os.path.join(typedir, "all_users_posts.pkl"))
        futils.create_dumps(user_genders, os.path.join(typedir, "user_genders.pkl"))
        futils.create_dumps(user_age_groups, os.path.join(typedir, "user_age_groups.pkl"))
        futils.create_dumps(user_gender_age, os.path.join(typedir, "user_gender_age.pkl"))
        futils.create_dumps(uids, os.path.join(typedir, "uids.pkl"))
    else:
        all_users_posts = futils.load_dumps(os.path.join(typedir, "all_users_posts.pkl"))
        user_genders = futils.load_dumps(os.path.join(typedir, "user_genders.pkl"))
        user_age_groups = futils.load_dumps(os.path.join(typedir, "user_age_groups.pkl"))
        user_gender_age = futils.load_dumps(os.path.join(typedir, "user_gender_age.pkl"))
        uids = futils.load_dumps(os.path.join(typedir, "uids.pkl"))
    return all_users_posts, user_genders, user_age_groups, user_gender_age, uids

if __name__ == "__main__":
    rootdir = "/home/ritual/resources/dailystrength/after_sklearn/all_user_posts_separate"
    results_rootopdir = "/home/ritual/resources/dailystrength/after_sklearn/results"
    dumps_opdir = "/home/ritual/resources/dailystrength/after_sklearn/dumps"    # expt_type = "3_age_groups"
    # age_ranges = [(13,27), (23,27), (33,47)]
    # IMP: expt_type and age ranges are connected
    expt_type = "5_age_groups"
    # subtype = "unbalanced"
    subtype = "unbalanced_word_char"
    age_ranges = [(12,17), (18,29), (30,49), (50,64), (65,110)]
    typedir = os.path.join(dumps_opdir, expt_type)
    all_users_posts, user_genders, user_age_groups, user_gender_age, uids = get_data(rootdir, typedir, age_ranges)

    print "total users: ", len(user_genders)

    X_train, X_test, y_train, y_test = train_test_split(all_users_posts, user_gender_age, test_size=0.20, random_state=42)
    # X_train_small, X_dev, y_train_small, y_dev = train_test_split(X_train, y_train, test_size=0.20, random_state=42)

    futils.create_opdir(os.path.join(results_rootopdir, expt_type, subtype))
    perform_prediction(os.path.join(results_rootopdir, expt_type, subtype), X_train, y_train, X_test, y_test)
    # perform_prediction(os.path.join(results_rootopdir, expt_type, subtype), X_train_small, y_train_small, X_dev, y_dev)


    # X_train, X_test, y_train, y_test = train_test_split_balanced(all_users_posts, user_gender_age, stratify=user_genders, test_size=0.20, random_state=42)
    # futils.create_opdir(os.path.join(results_rootopdir, expt_type, "balanced"))
    # perform_prediction(os.path.join(results_rootopdir, expt_type, "balanced"))


