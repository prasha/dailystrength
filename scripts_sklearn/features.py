from sklearn.feature_extraction.text import TfidfVectorizer
__author__ = 'shrprasha'


class PostsVectorizer(TfidfVectorizer):
    def build_analyzer(self):
        analyzer = super(PostsVectorizer,
                         self).build_analyzer()
        return lambda posts: [w for p in posts for w in analyzer(p)]

if __name__ == '__main__':
    pvect = PostsVectorizer()
    # print pvect.fit_transform(["this", "is an apple", "this is an apple"])
    print pvect.fit_transform([["this", "is"], ["this", "is"]])
    print pvect.get_feature_names()