import os
from new_data_processing_scripts.user import User, Post

__author__ = 'shrprasha'

from scripts_sklearn.run_expts import get_data

if __name__ == "__main__":
    rootdir = "/home/ritual/resources/dailystrength/after_sklearn/all_user_posts_separate"
    results_rootopdir = "/home/ritual/resources/dailystrength/after_sklearn/results"
    dumps_opdir = "/home/ritual/resources/dailystrength/after_sklearn/dumps"    # expt_type = "3_age_groups"
    # age_ranges = [(13,27), (23,27), (33,47)]
    # IMP: expt_type and age ranges are connected
    expt_type = "5_age_groups"
    # subtype = "unbalanced"
    subtype = "unbalanced_word_char"
    age_ranges = [(12,17), (18,29), (30,49), (50,64), (65,110)]
    typedir = os.path.join(dumps_opdir, expt_type)
    all_users_posts, user_genders, user_age_groups, user_gender_age, uids = get_data(rootdir, typedir, age_ranges)
    print len(all_users_posts)
    print len(user_gender_age)
    print len(uids)

