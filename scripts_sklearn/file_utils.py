import os
from pickle import load, dump
import json

__author__ = 'shrprasha'

def get_files_in_folder(folder):
    all_files = sorted(next(os.walk(folder))[2])
    if ".DS_Store" in all_files:
        all_files.remove(".DS_Store")
    return all_files

def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

def create_dumps(obj, fpath):
    with open(fpath, "w") as fhandle:
        dump(obj, fhandle)

def load_dumps_json(fpath):
    with open(fpath, "r") as fhandle:
        obj = json.load(fhandle)
    return obj

def create_dumps_json(obj, fpath):
    with open(fpath, "w") as fhandle:
        json.dump(obj, fhandle)

def create_opdir(opdir):
    if not os.path.exists(opdir):
        os.makedirs(opdir)

def get_lines_in_file(fpath):
    with open(fpath) as fhandle:
        return fhandle.readlines()

def print_obj(obj):
    attrs = vars(obj)
    print ', '.join("%s: %s" % item for item in attrs.items())








