__author__ = 'prasha'
import os
import codecs
import os
import sys
from pprint import pprint
import math
import re
import nltk
from collections import Counter
import utils


def get_words(sentence):
    return nltk.word_tokenize(sentence)


def get_sentences(document):
    sentences = nltk.sent_tokenize(document)
    return sentences


def no_of_words_in_text(text):
    count = 0
    for sentences in get_sentences(text):
        count += len(get_words(sentences))
    return count


def find_counts(text, regex_list):
    count = 0
    for regxp in regex_list:
        rexp = re.compile(regxp, re.IGNORECASE)
        matches = rexp.findall(text)
        count += len(matches)
    return count


def find_matches(text, regex_list):
    regx_matches = []
    for regxp in regex_list:
        rexp = re.compile(regxp, re.IGNORECASE)
        matches = rexp.findall(text)
        regx_matches.extend(matches)
    return regx_matches


def get_bucket_count(matches, buckets):
    counts = {}
    for bucket in buckets:
        counts[bucket] = 0
    for match in matches:
        word = get_words(match)[-1]
        if word in buckets[word]:
            counts[bucket] += 1
    return counts


def get_bucket_count_each_token(matches, buckets):
    counts_bucket = {}
    counts_each_token = []
    words = []

    ###
    bc = {}

    # init
    for bucket in buckets:
        counts_bucket[bucket] = 0
        ###
        bc[bucket] = []
    #count word in bucket
    for match in matches:
        word = match.lower()
        temp = sum(counts_bucket.values())
        for bucket, bucket_tokens in buckets.iteritems():
            if word in bucket_tokens:
                counts_bucket[bucket] += 1
                ###
                bc[bucket].append(word)
        if temp == sum(counts_bucket.values()):
            print match
            print "error:", word, counts_bucket
        words.append(word)
    words_counter = Counter(words)
    for token in each_token:
        if token in words_counter:
            counts_each_token.append(words_counter[token])
        else:
            counts_each_token.append(0)
    #print bc
    return counts_bucket, counts_each_token


def add_counts_to_extra_features_file(username_files, regex_list):
    familial_counts = {}
    username_files = os.listdir(username_folder)
    for ufile in sorted(username_files):
        stats = ufile.split("_")
        userid = stats[-4]
        text = codecs.open(os.path.join(username_folder, ufile), 'r', encoding='utf-8').read()
        familial_count = find_counts(text, regex_list)
        familial_counts[userid] = familial_count
        # print ufile, userid, familial_count

    user_details_file = "/Users/shrprasha/Projects/dailystrength/resources/userid_userdetails.txt"
    # user_details_file = "/home/prasha/Documents/dailystrength/author_profiling_data/processed_data/features/userid_userdetails.txt"
    lines = open(user_details_file, 'r').readlines()
    new_lines = ""
    # print len(lines), lines[0], lines[-1:]
    for line in lines:
        stats = line.strip().split(",")
        uname = stats[0]
        if uname in familial_counts:
            new_lines += line.strip() + "," + str(familial_counts[uname]) + "\n"
        else:
            new_lines += line.strip() + ",\n"

    print len(new_lines), new_lines[:500], "\n", new_lines[-500:]
    op_fHandle = open(user_details_file, 'w')
    op_fHandle.write(new_lines.strip())


def surajkolagi(text):
    familial_matches = find_matches(text, regex_list)
    if familial_matches:
        counts_bucket = {}
        # init
        for bucket in gender_buckets:
            counts_bucket[bucket] = 0
        #count word in bucket
        for match in familial_matches:
            word = match.lower()
            for bucket, bucket_tokens in gender_buckets.iteritems():
                if word in bucket_tokens:
                    counts_bucket[bucket] += 1
    return counts_bucket

def main(opdir):
    # change later
    #pairs_having_familial = "/home/prasha/Documents/dailystrength/author_profiling_data/processed_data/pairs_having_familial"
    #lines = open(pairs_having_familial, 'r').readlines()
    # separate_fhandle = open("/Users/shrprasha/Projects/resources/dailystrength/features/familial_separate_counts.vectors", 'w')
    # bucket_fhandle = open("/Users/shrprasha/Projects/resources/dailystrength/features/familial_bucket_counts.vectors", 'w')
    # separate_fhandle = open("/home/prasha/Documents/dailystrength/author_profiling_data/features/familial_separate_counts.vectors", 'w')
    # bucket_fhandle = open("/home/prasha/Documents/dailystrength/author_profiling_data/features/familial_bucket_counts.vectors", 'w')
    separate_fhandle = open(os.path.join(opdir, "familial_separate_counts.vectors"), 'w')
    bucket_fhandle = open(os.path.join(opdir, "familial_bucket_counts.vectors"), 'w')
    separate_fhandle.write(str(len(each_token)) + "\n")
    bucket_fhandle.write("3\n")
    username_files = os.listdir(username_folder)
    #counts with buckets
    # print "3"
    # print len(each_token)
    count_no_familial_tokens = 0
    for ufile in sorted(username_files):
        stats = ufile.split("_")
        gender = stats[-1].split(".")[0]
        age = stats[-2]

        text = codecs.open(os.path.join(username_folder, ufile), 'r', encoding='utf-8').read()
        familial_matches = find_matches(text, regex_list)

        if familial_matches:
            counts_bucket, counts_each_token = get_bucket_count_each_token(familial_matches, gender_buckets)
            #print familial_matches
            #print counts_bucket["male"]
            #print counts_bucket["female"]
            tot_words = no_of_words_in_text(text)
            if sum(counts_each_token) != len(familial_matches) != sum(counts_bucket.values()):
                print "error: ", ufile
            #print ufile + "\t" + age + "\t" + gender + "\t" + str(tot_words) + "\t" + str(len(familial_matches)) + "\t" + "\t".join(str(x) for x in counts_each_token) + "\t" + str(counts_bucket["male"]) + "\t" + str(counts_bucket["female"]) + "\t" + str(counts_bucket["neutral"])
            #print ufile + "\t" + age + "\t" + gender + "\t" + str(counts_bucket["male"]) + "\t" + str(counts_bucket["female"]) + "\t" + str(counts_bucket["neutral"])

            line = ufile
            for i, counts in enumerate(counts_each_token):
                line += " " + str(i) + ":" + str(counts)
            separate_fhandle.write(line + "\n")

            bucket_fhandle.write(
                ufile + " 0:" + str(counts_bucket["male"]) + " 1:" + str(counts_bucket["female"]) + " 2:" + str(
                    counts_bucket["neutral"]) + "\n")
        else:
            count_no_familial_tokens += 1
        print ufile
    print "does not have familial tokens: ", count_no_familial_tokens

    #normal counts without buckets
    #avg_counts = {"male": 0.0, "female": 0.0}
    #tot = {"male": 0, "female": 0}
    #for ufile in sorted(username_files):
    #    stats = ufile.split("_")
    #    gender = stats[-1].split(".")[0]
    #    age = stats[-2]
    #
    #    text = codecs.open(os.path.join(username_folder, ufile), 'r', encoding='utf-8').read()
    #    familial_count = find_counts(text, regex_list)
    #    tot_words = no_of_words_in_text(text)
    #    avg_counts[gender] += familial_count * 1.0 / tot_words
    #    tot[gender] += 1
    #    print ufile + "\t" + age + "\t" + gender + "\t" + str(tot_words) + "\t" + str(familial_count) + "\t" + str(familial_count * 1.0 / tot_words)
    #print avg_counts
    #print tot
    #print avg_counts["male"] / tot["male"], avg_counts["female"] / tot["female"]

# # python familial_count.py /home/prasha/Documents/dailystrength/author_profiling_data/together /home/prasha/Dropbox/dailystrength/resources
# if __name__ == "__main__":
#     # username_folder = "/home/prasha/Documents/dailystrength/author_profiling_data/username_data_pan14_train_test/small_test_familial"
#     # username_folder = "/home/prasha/Documents/dailystrength/author_profiling_data/username_data_pan14_train_test/together"
#     # username_folder = "/Users/shrprasha/Projects/resources/dailystrength/together"
#     username_folder = sys.argv[1]
#     opdir = sys.argv[2]
#     #will catch term\W* eg. son-in-law, son's etc.
#     regex_list = [
#         r"\bmy\b\W+\b((?:son|daughter|grandson|granddaughter|father|mother|brother|sister|uncle|aunt|cousin|nephew|niece|family|godson|goddaughter|grandchild|grandmother|grandfather|husband|wife|bf|gf|boyfriend|girlfriend|hubby|baby|babies|child|children|kids|mom|parent))\W*\b",
#         r"\b(?:DH|DS|DD|DW)\b"]
#     gender_buckets = {"male": [u'wife', u'gf', u'girlfriend', u'dw'],
#                       "female": [u'husband', u'bf', u'boyfriend', u'dh', u'hubby'],
#                       "neutral": [u'ds', u'dd', u'son', u'daughter', u'grandson', u'granddaughter', u'father',
#                                   u'mother', u'brother', u'sister', u'uncle', u'aunt', u'cousin', u'nephew', u'niece',
#                                   u'family', u'godson', u'goddaughter', u'grandchild', u'grandmother', u'grandfather',
#                                   u'baby', u'babies', u'child', u'children', u'kids', u'mom', u'parent']}
#     #age_buckets = {"18-24": [], "25-34": [], "35-49": [], "50-64": [], "65-xx": []}
#     each_token = [u'son', u'daughter', u'grandson', u'granddaughter', u'father', u'mother', u'brother', u'sister',
#                   u'uncle', u'aunt', u'cousin', u'nephew', u'niece', u'family', u'godson', u'goddaughter',
#                   u'grandchild', u'grandmother', u'grandfather', u'husband', u'wife', u'bf', u'gf', u'boyfriend',
#                   u'girlfriend', u'dh', u'ds', u'dd', u'dw', u'baby', u'babies', u'child', u'children', u'kids', u'mom',
#                   u'parent']
#     #print len(each_token)
#     # main(opdir)
#
#     #print get_words("my son's wife-in-law")
#     #print find_matches("my son-in-law my son's", regex_list)
#
#
#     #add_to_extra_features_file(username_folder, regex_list)
#
#     #ufile = "/home/prasha/Documents/dailystrength/author_profiling_data/username_data_pan14_train_test/test/seed29_1782036_en_25-34_female.txt"
#     #ufile = "test"
#     #text = codecs.open(ufile, 'r', encoding='utf-8').read()
    #print find_counts(text, regex_list)

#  python familial_count.py /home/prasha/Documents/dailystrength/author_profiling_data/together /home/prasha/Dropbox/dailystrength/resources
if __name__ == "__main__":
    #will catch term\W* eg. son-in-law, son's etc.
    regex_list = [
        r"\bmy\b\W+\b((?:son|daughter|grandson|granddaughter|father|mother|brother|sister|uncle|aunt|cousin|nephew|niece|family|godson|goddaughter|grandchild|grandmother|grandfather|husband|wife|bf|gf|boyfriend|girlfriend|hubby|baby|babies|child|children|kids|mom|parent))\W*\b",
        r"\b(?:DH|DS|DD|DW)\b"]
    gender_buckets = {"male": [u'wife', u'gf', u'girlfriend', u'dw'],
                      "female": [u'husband', u'bf', u'boyfriend', u'dh', u'hubby'],
                      "neutral": [u'ds', u'dd', u'son', u'daughter', u'grandson', u'granddaughter', u'father',
                                  u'mother', u'brother', u'sister', u'uncle', u'aunt', u'cousin', u'nephew', u'niece',
                                  u'family', u'godson', u'goddaughter', u'grandchild', u'grandmother', u'grandfather',
                                  u'baby', u'babies', u'child', u'children', u'kids', u'mom', u'parent']}
    #age_buckets = {"18-24": [], "25-34": [], "35-49": [], "50-64": [], "65-xx": []}
    each_token = [u'son', u'daughter', u'grandson', u'granddaughter', u'father', u'mother', u'brother', u'sister',
                  u'uncle', u'aunt', u'cousin', u'nephew', u'niece', u'family', u'godson', u'goddaughter',
                  u'grandchild', u'grandmother', u'grandfather', u'husband', u'wife', u'bf', u'gf', u'boyfriend',
                  u'girlfriend', u'dh', u'ds', u'dd', u'dw', u'baby', u'babies', u'child', u'children', u'kids', u'mom',
                  u'parent']
    #print len(each_token)
    # main(opdir)
    username_folder = ""

    #print get_words("my son's wife-in-law")
    # print find_matches("this is my son-in-law my son's my girlfriend and this", regex_list)
    familial_matches = find_matches("fun is my girlfriend and this", regex_list)

    print len(each_token)
    print get_bucket_count_each_token(familial_matches, gender_buckets)


    #add_to_extra_features_file(username_folder, regex_list)

    #ufile = "/home/prasha/Documents/dailystrength/author_profiling_data/username_data_pan14_train_test/test/seed29_1782036_en_25-34_female.txt"
    #ufile = "test"
    #text = codecs.open(ufile, 'r', encoding='utf-8').read()
    #print find_counts(text, regex_list)