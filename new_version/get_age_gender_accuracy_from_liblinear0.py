__author__ = 'prasha'
import sys

# self.labels = ["18-24_female", "18-24_male", "25-34_female", "25-34_male", "35-49_female", "35-49_male", "50-64_female", "50-64_male", "65-xx_female", "65-xx_male"]
# 0  0 0 18-24_female
# 1  0 1 18-24_male
# 2  1 0 25-34_female
# 3  1 1 25-34_male
# 4  2 0 35-49_female
# 5  2 1 35-49_male
# 6  3 0 50-64_female
# 7  3 1 50-64_male
# 8  4 0 65-xx_female
# 9  4 1 65-xx_male

def same_age(cl1, cl2): #they are different by 1 and greater is odd ; lesser is even
    if cl1 > cl2:
        g = cl1
        l = cl2
    else:
        g = cl2
        l = cl1
    if g-l == 1 and g%2 == 1 and l%2 == 0:
        return True
    return False

def same_gender(cl1, cl2):
    if cl1%2 == 0 and cl2%2 == 0: #both even
        return True
    elif cl1%2 == 1 and cl2%2 == 1: #both odd
        return True
    return False

# python2.7 get_age_gender_accuracy_from_liblinear.py /home/prasha/Documents/dailystrength/author_profiling_data/processed_data/liblinear_train_test/ngram_familial_separate.out /home/prasha/Documents/dailystrength/author_profiling_data/processed_data/liblinear_train_test/ngram_familial_separate_test
if __name__ == "__main__":
    same_age_classes = []
    same_gender_classes = [()]

    outfile = sys.argv[1]
    testfile = sys.argv[2]
    outfile_lines = open(outfile, 'r').readlines()
    testfile_lines = open(testfile, 'r').readlines()
    total = len(outfile_lines)
    both_correct = 0
    age_correct = 0
    gender_correct = 0

    if len(outfile_lines) != len(testfile_lines):
        print "wrong set of files"
    else:
        for i, out_line in enumerate(outfile_lines):
            test_line = testfile_lines[i]
            out_class = int(out_line.strip().split(" ", 1)[0])
            test_class = int(test_line.strip().split(" ", 1)[0])
            if out_class == test_class:
                age_correct += 1
                gender_correct += 1
                both_correct += 1
            else:
                if same_age(out_class, test_class):
                    age_correct += 1
                elif same_gender(out_class, test_class):
                    gender_correct += 1
    print "total accuracy: ", both_correct * 100.0 / total
    print "age accuracy: ", age_correct * 100.0 / total
    print "gender accuracy: ", gender_correct * 100.0 / total
    print("|{0:.2f}|{1:.2f}|{2:.2f}|".format(both_correct * 100.0 / total, age_correct * 100.0 / total, gender_correct * 100.0 / total))
    #print "|" + both_correct * 100.0 / total + "|" + age_correct * 100.0 / total + "|" + gender_correct * 100.0 / total + "|"



