__author__ = 'shrprasha'
import utils
import os
import heapq
from collections import Counter
import math

#  format: [{word1: pmi1, ...}, ....20]
def get_high_pmi_words_per_disease(pmi_counts_file, remove_top_x_percent):
    lines = open(pmi_counts_file).readlines()
    # lines = open(pmi_counts_file).readlines()[:100]  #  TODO: comment
    words_pmi_map_list = []
    for i in range(0, 20):
        words_pmi_map_list.append({})
    for line in lines:
        stats = line.strip().split()
        word = stats[0]
        pmis = stats[1:]
        for i, st in enumerate(pmis):
            words_pmi_map_list[i][word] = float(st)
    high_pmi_words = []
    for words_pmi_map in words_pmi_map_list:
        high_pmi_words_disease = heapq.nlargest(int(remove_top_x_percent * len(words_pmi_map)), words_pmi_map, key=words_pmi_map.get)
        print high_pmi_words_disease
        high_pmi_words.extend(high_pmi_words_disease)
    return high_pmi_words


def get_ngrams_except_high_pmi(author_file, high_pmi_words):
    words = utils.get_words_wo_high_pmi(author_file, high_pmi_words)
    ngrams = utils.get_ngrams_counter_from_list_remove_least_frequent(words, 1, 3, 1)
    return ngrams

def get_idf(idf_counter, no_of_files):
    for word, count in idf_counter.iteritems():
        den = math.log10(count)
        idf_counter[word] = 0 if den == 0 else no_of_files / den
    return idf_counter

#  also creates ngram_idf map
def get_tf_idf(files_ngrams_map, idf_map, dictionary):
    files_ngrams_tfidf_map = {}
    # print files_ngrams_map
    for fname, ngrams_counts in files_ngrams_map.iteritems():
        print fname
        files_ngrams_tfidf_map[fname] = {}
        max_count = max(ngrams_counts.values())
        for ngram, count in ngrams_counts.iteritems():
            tf_idf = tf_idf_value(count, max_count, ngram)
            if tf_idf != 0:
                files_ngrams_tfidf_map[fname][dictionary.index(ngram) + 1] = tf_idf
    return files_ngrams_tfidf_map
def tf_idf_value(count, max_count, ngram):
    tf = 0.5 + 0.5 * count / max_count
    idf = idf_map[ngram]
    tf_idf = tf * idf
    return tf_idf

def get_class_from_fname(fname):
    _, _, age, gender = fname.split(".", 1)[0].split("_")
    return get_label_age(age, gender), get_label_gender(age, gender), get_label_both(age, gender)
def get_label_age(age, gender):
    labels = ["18-24", "25-34", "35-49", "50-64", "65-xx"]
    return labels.index(age)
def get_label_gender(age, gender):
    labels = ["female", "male"]
    return labels.index(gender)
def get_label_both(age, gender):
    labels = ["18-24_female", "18-24_male", "25-34_female", "25-34_male", "35-49_female", "35-49_male", "50-64_female", "50-64_male", "65-xx_female", "65-xx_male"]
    return labels.index(age + "_" + gender)

#  creates dictionary file and writes feature file
def write_to_file(files_ngrams_tfidf_map, opdir):
    age_feature_fhandle = open(os.path.join(opdir, "age"), "w")
    gender_feature_fhandle = open(os.path.join(opdir, "gender"), "w")
    both_feature_fhandle = open(os.path.join(opdir, "both"), "w")
    for fname, idx_tfidf_map in files_ngrams_tfidf_map.iteritems():
        line = ""
        for idx in sorted(idx_tfidf_map):
            tfidf = idx_tfidf_map[idx]
            line += " " + str(idx) + ":" + str(tfidf)
        age_label, gender_label, both_label = get_class_from_fname(fname)
        age_feature_fhandle.write(str(age_label) + line + "\n")
        gender_feature_fhandle.write(str(gender_label) + line + "\n")
        both_feature_fhandle.write(str(both_label) + line + "\n")

def write_to_file_test(files_ngrams_counts_map, opdir):
    age_feature_fhandle = open(os.path.join(opdir, "age"), "w")
    gender_feature_fhandle = open(os.path.join(opdir, "gender"), "w")
    both_feature_fhandle = open(os.path.join(opdir, "both"), "w")
    for fname, ngram_counts_map in files_ngrams_counts_map.iteritems():
        line = ""
        max_count = max(ngram_counts_map.values())
        for ngram in sorted(ngram_counts_map):
            if ngram in dictionary:
                count = ngram_counts_map[ngram]
                tf_idf = tf_idf_value(count, max_count, ngram)
                line += " " + str(dictionary.index(ngram)) + ":" + str(tf_idf)
        age_label, gender_label, both_label = get_class_from_fname(fname)
        age_feature_fhandle.write(str(age_label) + line + "\n")
        gender_feature_fhandle.write(str(gender_label) + line + "\n")
        both_feature_fhandle.write(str(both_label) + line + "\n")

def for_test(test_files_dir):
    author_files = sorted(next(os.walk(test_files_dir))[2])
    files_ngrams_counts_map = {}
    for fname in author_files:
        if "DS_Store" in fname:
            continue
        ngrams = utils.get_ngrams_from_fname(os.path.join(test_files_dir, fname), 1, 3)
        files_ngrams_counts_map[fname] = ngrams
    write_to_file_test(files_ngrams_map, test_opdir)


if __name__ == "__main__":
    train_files_dir = "/Users/shrprasha/Projects/resources/dailystrength/train"
    test_files_dir = "/Users/shrprasha/Projects/resources/dailystrength/test"
    pmi_counts_file = "/Users/shrprasha/Projects/resources/dailystrength/processed_intermediates/pmi.txt"
    train_opdir = "/Users/shrprasha/Projects/resources/dailystrength/features/high_pmi_removed/train"
    test_opdir = "/Users/shrprasha/Projects/resources/dailystrength/features/high_pmi_removed/test"
    high_pmi_words = get_high_pmi_words_per_disease(pmi_counts_file, 0.01)
    author_files = sorted(next(os.walk(train_files_dir))[2])
    files_ngrams_map = {}
    idf_counter = Counter()
    no_of_files = 0
    for fname in author_files:
    # for fname in author_files[:10]: # TODO: comment
        if "DS_Store" in fname:
            continue
        ngrams = get_ngrams_except_high_pmi(os.path.join(train_files_dir, fname), high_pmi_words)
        idf_counter.update(ngrams.keys())
        files_ngrams_map[fname] = ngrams
        no_of_files += 1
    idf_map = get_idf(idf_counter, no_of_files)
    dictionary = sorted(idf_map)
    files_ngrams_tfidf_map = get_tf_idf(files_ngrams_map, idf_map, dictionary)
    write_to_file(files_ngrams_tfidf_map, train_opdir)
    for_test(test_files_dir)


