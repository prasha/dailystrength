__author__ = 'prasha'
import os
import utils
import sys

# create training feature file for liblinear
#by merging many feature files
#feature file :
# (eg. tfidf features from old authorprofiling) ;
# has in each line-> fname feature_idx:feature_val ;
# idx values are updated and then ;
# copied from here to final feature file ;
#some feature files do not have a line for files where that feature is not there, eg: familial counts

#since all these feature files have been created together for train and test,
#the fname is taken by iterating the train and test directories separately
#label is also taken from the fname

#TRICKY: all other features except tfidf start at 0
#liblinear features cannot start at 0
#everything works fine if tfidf features are processes first
#if not, change
#    def __init__(self, rootdir, outdir, out_name, start_at=1):

#username can have '_' in it. so, start indexing from the end after splitting by '_'
#to train on just age or gender change the label in init and use apt process in write_to_file


class UsernameLibLinear:
    def __init__(self, rootdir, outdir, test_or_dev, age_gender_both="both", start_at=1):
        self.traindir = os.path.join(rootdir, "train")
        self.testdir = os.path.join(rootdir, test_or_dev)
        self.train_fhandle = open(os.path.join(outdir, "train.txt"), 'w')
        self.test_fhandle = open(os.path.join(outdir, test_or_dev + ".txt"), 'w')
        self.start_at = start_at
        self.fname_features_dict = {}
        self.label_list = []
        self.feature_list = []
        self.age_gender_both = age_gender_both

        if age_gender_both == "age":
            self.get_label = self.get_label_age
        elif age_gender_both == "gender":
            self.get_label = self.get_label_gender
        elif age_gender_both == "both":
            self.get_label = self.get_label_both

    def get_label_age(self, age, gender):
        labels = ["18-24", "25-34", "35-49", "50-64", "65-xx"]
        return labels.index(age)
    def get_label_gender(self, age, gender):
        labels = ["female", "male"]
        return labels.index(gender)
    def get_label_both(self, age, gender):
        labels = ["18-24_female", "18-24_male", "25-34_female", "25-34_male", "35-49_female", "35-49_male", "50-64_female", "50-64_male", "65-xx_female", "65-xx_male"]
        return labels.index(age + "_" + gender)

    def process_feature_file(self, feature_file_path):
        lines = open(feature_file_path, 'r').readlines()
        features_count = int(lines[0].strip())
        for line in lines[1:]:
            all = line.split(' ', 1)
            fname = all[0].strip("/")
            if fname in self.fname_features_dict:
                self.fname_features_dict[fname] += self.update_idx(all[1].strip())
            else:
                self.fname_features_dict[fname] = self.update_idx(all[1].strip())
        self.start_at += features_count  #TODO

    def update_idx(self, features):  # features-> featureidx:feature_val ....
        updated_features = ""
        for fidx_fval in features.split(" "):
            fidx, fval = fidx_fval.strip().split(":")
            fidx = int(fidx) + self.start_at
            updated_features += " " + str(fidx) + ":" + fval
        return updated_features

    # def add_other_class_as_feature(self):
    #     for fname, features in self.fname_features_dict.iteritems():
    #         id, _, age, gender = fname.split(".")[0].split("_")
    #         label = str(self.get_label(age, gender))



    def write_to_file(self):
        train_files = next(os.walk(self.traindir))[2]
        test_files = next(os.walk(self.testdir))[2]
        # train_files = utils.remove_username_from_fnames(next(os.walk(self.traindir))[2])
        # test_files = utils.remove_username_from_fnames(next(os.walk(self.testdir))[2])
        print "number of features:", len(self.fname_features_dict)

        in_train = []
        for fname, features in self.fname_features_dict.iteritems():
            id, _, age, gender = fname.split(".")[0].split("_")
            label = str(self.get_label(age, gender))

            # # label = str(self.labels.index(age + "_" + gender))
            # # label = str(self.labels.index(gender))
            # label = str(self.labels.index(age))

            #added other class as features
            #################
            if self.age_gender_both == "age":
                other_class = self.get_label_gender(age, gender)
                features += " " + str(self.start_at + 1) + ":" + str(other_class)
            elif self.age_gender_both == "gender":
                other_class = self.get_label_age(age, gender)
                features += " " + str(self.start_at + 1) + ":" + str(other_class)

            #################

            if fname in train_files:
                self.train_fhandle.write(label + features + "\n")
                in_train.append(fname)
            elif fname in test_files:
                self.test_fhandle.write(label + features + "\n")

        error = [x for x in train_files if x not in in_train]
        # print error
        # print len(error)

                # print len(self.fname_features_dict)
                # for fname, features in self.fname_features_dict.iteritems():
                #     print fname, len(features)


# ./train -s 7 ~/Dropbox/DailyStrength/liblinear_op/username_liblinear_train dailystrength1.model
# ~/Documents/softwares/liblinear-1.94/predict ~/Dropbox/test/username_liblinear_test dailystrength.model dailystrength.out

#/home/prasha/Documents/softwares/liblinear-1.94/train -s 0 ngram_familial_bucket_train  ngram_familial_bucket.model
#~/Documents/softwares/liblinear-1.94/predict ngram_familial_bucket_test ngram_familial_bucket.model ngram_familial_bucket.out

#/home/prasha/Documents/softwares/liblinear-1.94/train -s 0 ngram_familial_bucket_gender_train  ngram_familial_bucket_gender.model
#~/Documents/softwares/liblinear-1.94/predict ngram_familial_bucket_gender_test ngram_familial_bucket_gender.model ngram_familial_bucket_gender.out

# python2.7 combine_feature_liblinear.py /home/prasha/Documents/dailystrength/author_profiling_data/username_data_pan14_train_test /home/prasha/Documents/dailystrength/author_profiling_data/processed_data/liblinear_train_test ngram_familial_bucket
# python2.7 combine_feature_liblinear.py /home/prasha/Documents/dailystrength/author_profiling_data /home/prasha/Documents/dailystrength/author_profiling_data/liblinear default
# python combine_feature_liblinear.py /Users/shrprasha/Projects/resources/dailystrength /Users/shrprasha/Projects/resources/dailystrength/liblinear both default
if __name__ == "__main__":

    rootdir = sys.argv[1]
    outdirpath = sys.argv[2]
    outdir_name = sys.argv[4]
    age_gender_both = sys.argv[3]
    test_or_dev = sys.argv[5]

    outdir = os.path.join(outdirpath, outdir_name, age_gender_both)
    if not os.path.exists(outdir):
        os.makedirs(outdir)

    # rootdir = "/home/prasha/Documents/dailystrength/author_profiling_data/username_data_pan14_train_test"
    # outdir = "/home/prasha/Documents/dailystrength/author_profiling_data/processed_data/liblinear_train_test"
    # out_name = "ngram_familial_bucket"

    # feature_files = [
    #                  "/home/prasha/Documents/dailystrength/author_profiling_data/processed_data/features/dailystrength_tfidf_combined.vectors",
    #                  "/home/prasha/Documents/dailystrength/author_profiling_data/processed_data/features/username_char_ngrams.vectors",
    #                  # "/home/prasha/Documents/dailystrength/author_profiling_data/processed_data/features/familial_neutral_male_female_counts.vectors"
    #                  "/home/prasha/Documents/dailystrength/author_profiling_data/processed_data/features/familial_separate_counts.vectors"
    #                  ]

    # feature_files = [
    #                  "/home/prasha/Documents/dailystrength/author_profiling_data/features/ds.vectors",
    #                  # "/home/prasha/Documents/dailystrength/author_profiling_data/features/username_char_ngrams.vectors",
    #                  # "/home/prasha/Documents/dailystrength/author_profiling_data/features/familial_neutral_male_female_counts.vectors"
    #                  # "/home/prasha/Documents/dailystrength/author_profiling_data/features/familial_separate_counts.vectors"
    #                  ]

    feature_files = [
        "/Users/shrprasha/Projects/resources/dailystrength/features/main_features/ngrams_train_dev_test.vectors",
        # "/Users/shrprasha/Projects/dailystrength/resources/diseases_feature.vectors",
        "/Users/shrprasha/Projects/resources/dailystrength/features/main_features/familial_bucket_counts.vectors"#,
        # "/Users/shrprasha/Projects/resources/dailystrength/features/main_features/familial_separate_counts.vectors",
        # "/Users/shrprasha/Projects/resources/dailystrength/features/main_features/non-normalized/normalized/uname_char_ngrams.vectors"
    ]
        # "/Users/shrprasha/Projects/resources/dailystrength/features/main_features/non-normalized/uname_char_ngram.vectors"
        # "/Users/shrprasha/Projects/resources/dailystrength/features/main_features/uname_char_ngram.vectors"

    ull = UsernameLibLinear(rootdir, outdir, test_or_dev, age_gender_both)
    for feature_file_path in feature_files:
        ull.process_feature_file(feature_file_path)
    ull.write_to_file()




