__author__ = 'prasha'
import os
from bs4 import BeautifulSoup  # pip install beautifulsoup4

import sys
from pprint import pprint
import matplotlib.pyplot as plt
import numpy as np
import xml.etree.ElementTree as xml

#counts only posts and not replies


# def update_age(age_groups, age):
#     if (age.isdigit()):
#         for ages in sorted(age_groups):
#             start, end = ages
#             if (int(age) in range(start, end)):
#                 age_groups[ages] += 1

def update_age(age_groups, age):
    for ages in sorted(age_groups):
        min, max = ages
        if (min <= age <= max):
            age_groups[ages] += 1
            return

def update_age_unique_users0(age_groups, age, uid):
    for ages in sorted(age_groups):
        min, max = ages
        if (min <= age <= max):
            age_groups[ages].append(uid)
            return

#update age group only if this is the age group we selected for this user in our data
def update_age_unique_users(age_groups, age, uid):
    ages = correct_age_group_for_id[uid]
    min, max = ages
    if (min <= age <= max):
        age_groups[ages].append(uid)
        return True
    # print age, uid, ages
    return False

#all data was read on 2013; so the age is the age of the poster in 2013
def get_actual_age(age_now, date_posted):
    year_obtained = 13
    if (age_now):
        age = int(age_now)
        year_posted = int(date_posted.split()[0].split("/")[-1])
        actual_age = age - (year_obtained - year_posted)
        return actual_age
    else:
        return "NA"

def update_gender(genders, gender):
    if (gender in genders):
        genders[gender] += 1

def update_gender_unique_users(genders, gender, uid):
    if (gender in genders):
        genders[gender].append(uid)

def get_plots():
    id_age_gender_fname = "id_age_gender.txt"
    rootdir = "/mnt/docsig/storage/daily-strength"
    # diseases = ["Acne"]
    diseases = ["Acne", "ADHD", "Alcoholism", "Asthma", "Back-Pain", "Bipolar-Disorder", "Bone-Cancer", "COPD",
                "Diets-Weight-Maintenance", "Fibromyalgia", "Gastric-Bypass-Surgery", "Immigration-Law", "Infertility",
                "Loneliness", "Lung-Cancer", "Migraine", "Miscarriage", "Pregnancy", "Rheumatoid-Arthritis",
                "War-In-Iraq"]
    #all_disesase_opfile = open(os.path.join("per_disease", "all_diseases.csv"), 'w')

    lines = open(id_age_gender_fname, 'r').readlines()
    age_gender_for_id = {}

    #for plot
    xlabels = ["10-20", "20-30", "30-40", "40-50", "50-60", "60-70", "70+"]
    x = range(7)
    plt.xticks(x, xlabels)
    num_plots = 15
    colormap = plt.cm.hsv
    plt.gca().set_color_cycle([colormap(i) for i in np.linspace(0, 1, num_plots)])
    linestyles = ['-', '-.', '--']  #, ':']

    for line in lines:
        uid, age, gender = line.strip().split(",")
        age_gender_for_id[uid] = [age, gender]

    for disease in diseases:
        age_groups = {(10, 20): 0, (20, 30): 0, (30, 40): 0, (40, 50): 0, (50, 60): 0, (60, 70): 0, (70, 5000): 0}
        genders = {"Female": 0, "Male": 0}
        user_posts = open(os.path.join(rootdir, disease, "userPostList.txt"), 'r').readlines()
        #opfile = open(os.path.join("per_disease", disease + ".csv"), 'w')
        for post in user_posts:
            uid = post.split(",")[0]
            if (uid in age_gender_for_id):
                age, gender = age_gender_for_id[id]
                data = uid + "," + age + "," + gender + "\n"
                update_age(age_groups, age.strip())
                update_gender(genders, gender.strip())
        #        opfile.write(data)
        #        all_disesase_opfile.write(disease + "," + data)
        #opfile.close()
        tot = sum(age_groups.values())
        max_age = max(age_groups.values())
        tot_gender = sum(genders.values())
        max_gender = max(genders.values())
        plt.plot(x, [v * 100 / tot for k, v in sorted(age_groups.iteritems())], linewidth=4,
                 linestyle=linestyles[diseases.index(disease) % 3])  #, c="blue")

        #print "|", disease, "|",
        #for k,v in sorted(genders.iteritems()):
        #    if(v==max_gender):
        #        print "_.",
        #    print v, "|",
        #print tot_gender, "|"

        print "|", disease, "|",
        for k, v in sorted(age_groups.iteritems()):
            if (v == max_age):
                print "_.",
            print v, "|",
        print tot, "|"


        #print "\n\n\n" + disease
        #pprint(age_groups)
        #pprint(genders)

    plt.legend(diseases, loc='upper center', bbox_to_anchor=(0.5, -0.05),
               fancybox=True, shadow=True, ncol=6)
    plt.legend(diseases, ncol=5, loc='upper left')
    plt.show()

def get_diseases_per_post():
    #all_disesase_opfile = open(os.path.join("per_disease", "all_diseases.csv"), 'w')

    print " 18-24 25-34 35-49 50-64 65-xx female male"

    lines = open(id_age_gender_fname, 'r').readlines()
    age_gender_for_id = {}

    for line in lines:
        _, uid, age, gender = line.strip().split(",")
        age_gender_for_id[uid] = [age, gender]

    for disease in diseases:
        # age_groups = {(10, 20): 0, (20, 30): 0, (30, 40): 0, (40, 50): 0, (50, 60): 0, (60, 70): 0, (70, 5000): 0}
        age_groups = {(18, 24) : 0, (25, 34) : 0, (35, 49) : 0, (50, 64) : 0, (65, 200) : 0}
        genders = {"Female": 0, "Male": 0}
        user_posts = open(os.path.join(rootdir, disease, "userPostList.txt"), 'r').readlines()
        #opfile = open(os.path.join("per_disease", disease + ".csv"), 'w')
        for post in user_posts:
            uid, _, date_posted = post.split(",")
            if (uid in age_gender_for_id):
                age, gender = age_gender_for_id[uid]
                data = uid + "," + age + "," + gender + "\n"
                update_age(age_groups, get_actual_age(age.strip(), date_posted))
                update_gender(genders, gender.strip())
        #        opfile.write(data)
        #        all_disesase_opfile.write(disease + "," + data)

        print disease, " ".join([str(age_groups[x]) for x in sorted(age_groups)]),
        print " ".join([str(genders[x]) for x in sorted(genders)])
        #opfile.close()
        # tot = sum(age_groups.values())
        # max_age = max(age_groups.values())
        # tot_gender = sum(genders.values())
        # max_gender = max(genders.values())

        #print "|", disease, "|",
        #for k,v in sorted(genders.iteritems()):
        #    if(v==max_gender):
        #        print "_.",
        #    print v, "|",
        #print tot_gender, "|"

        # print "|", disease, "|",
        # for k, v in sorted(age_groups.iteritems()):
        #     if (v == max_age):
        #         print "_.",
        #     print v, "|",
        # print tot, "|"


        #print "\n\n\n" + disease
        #pprint(age_groups)
        #pprint(genders)

##########################################################################################################
#from create_author_profile_dataset_range1

def parse_problem_or_reply(xmlobj):
    if xmlobj.find('person') is not None: #added since /mnt/docsig/storage/daily-strength/Bipolar-Disorder/XML/10056719-taken-advantage.html.xml has no data in problem or replies
        uid = xmlobj.find('person').get('id') #'/people/436265'
        uid = uid.split("/")[-1]
        date_posted = xmlobj.find('date').text
        return (uid, date_posted)
    return None

##################################################################################################

def get_user_posts(disease):
    user_posts = [] #uid: date_posted
    xmldir = os.path.join(rootdir, disease, "XML")
    files = sorted(os.listdir(xmldir))
    for doc_fname in files:
        extension = os.path.splitext(doc_fname)[-1]
        if extension != ".xml":
            continue
        tree = xml.parse(os.path.join(os.path.join(xmldir, doc_fname)))
        root = tree.getroot()
        for problem in root.iter('problem'):
            val = parse_problem_or_reply(problem)
            if val:
                user_posts.append(val)
        for reply in root.iter('reply'):
            val = parse_problem_or_reply(reply)
            if val:
                user_posts.append(val)
    return user_posts

def get_diseases_per_unique_user():
    #all_disesase_opfile = open(os.path.join("per_disease", "all_diseases.csv"), 'w')
    print " 18-24 25-34 35-49 50-64 65-xx female male"
    lines = open(id_age_gender_fname, 'r').readlines()
    age_gender_for_id = {}

    for line in lines:
        _, uid, age, gender = line.strip().split(",")
        age_gender_for_id[uid] = [age, gender]

    for disease in diseases:
        age_groups = {(18, 24) : [], (25, 34) : [], (35, 49) : [], (50, 64) : [], (65, 200) : []}
        genders = {"Female": [], "Male": []}
        # user_posts = open(os.path.join(rootdir, disease, "userPostList.txt"), 'r').readlines()
        user_posts = get_user_posts(disease)
        #opfile = open(os.path.join("per_disease", disease + ".csv"), 'w')
        for uid, date_posted in user_posts:
            if (uid in age_gender_for_id):
                age, gender = age_gender_for_id[uid]
                # update_age_unique_users(age_groups, get_actual_age(age.strip(), date_posted), uid) #update gender only if we updated age for this user
                # update_gender_unique_users(genders, gender.strip(), uid)

                if update_age_unique_users(age_groups, get_actual_age(age.strip(), date_posted), uid): #update gender only if we updated age for this user
                    update_gender_unique_users(genders, gender.strip(), uid)

        # print [set(age_groups[x]) for x in sorted(age_groups)]
        total = sum([len(set(x)) for x in genders.values()])

        # print disease, " ".join([str(len(set(age_groups[x]))) for x in sorted(age_groups)]),
        # print " ".join([str(len(set(genders[x]))) for x in sorted(genders)]), total
        print disease, " ".join([str(len(set(age_groups[x])) * 100.0 / total) for x in sorted(age_groups)]),
        print " ".join([str(len(set(genders[x])) * 100.0 / total) for x in sorted(genders)]), total


def get_correct_age_group_for_id():
    correct_age_group_for_id = {}
    files = next(os.walk(traindir))[2] #get just the files and not directories
    for fname in files:
        uid, _, age_group, _ = fname.split(".")[0].split("_")
        min_age, max_age = age_group.split("-")
        min_age = int(min_age)
        if min_age == 65:
            max_age = 200
        else:
            max_age = int(max_age)
        correct_age_group_for_id[uid] = (min_age, max_age)
    return correct_age_group_for_id


if __name__ == "__main__":
    # id_age_gender_fname = "/home/prasha/Dropbox/dailystrength/resources/username_id_age_gender_both_age_gender_combined.txt"
    id_age_gender_fname = "/home/prasha/Dropbox/dailystrength/resources/userid_user_details_age_gender_both_train.txt"
    rootdir = "/mnt/docsig/storage/daily-strength"
    traindir = "/home/prasha/Documents/dailystrength/author_profiling_data/train"
    # diseases = ["Acne"]
    # diseases = ["Bone-Cancer"]
    diseases = ["Acne", "ADHD", "Alcoholism", "Asthma", "Back-Pain", "Bipolar-Disorder", "Bone-Cancer", "COPD",
                "Diets-Weight-Maintenance", "Fibromyalgia", "Gastric-Bypass-Surgery", "Immigration-Law", "Infertility",
                "Loneliness", "Lung-Cancer", "Migraine", "Miscarriage", "Pregnancy", "Rheumatoid-Arthritis",
                "War-In-Iraq"]
    correct_age_group_for_id = get_correct_age_group_for_id()
    # get_diseases_per_post()
    get_diseases_per_unique_user()

