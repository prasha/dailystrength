__author__ = 'prasha'
from pprint import pprint
import string
import sys
import get_confusionmatrix_accuracy_liblinearop as gcal
import os

# 0  0 0 18-24_female
# 1  0 1 18-24_male
# 2  1 0 25-34_female
# 3  1 1 25-34_male
# 4  2 0 35-49_female
# 5  2 1 35-49_male
# 6  3 0 50-64_female
# 7  3 1 50-64_male
# 8  4 0 65-xx_female
# 9  4 1 65-xx_male

delim = " "

def get_predictions(age_prediction_file, gender_prediction_file):
    predictions = []
    age_predictions = [int(line.split(delim, 1)[0]) for line in open(age_prediction_file).readlines()[1:]]
    gender_predictions = [int(line.split(delim, 1)[0]) for line in open(gender_prediction_file).readlines()[1:]]
    for i, ap in enumerate(age_predictions):
        gp = gender_predictions[i]
        predictions.append(ap * 2 + gp)
    return predictions

if __name__ == "__main__":
    prediction_rootdir = sys.argv[1]
    actual_file = sys.argv[2]
    age_prediction_file = os.path.join(prediction_rootdir, "age", "out.txt")
    gender_prediction_file = os.path.join(prediction_rootdir, "gender", "out.txt")

    gcal.get_results("both", actual_file, get_predictions(age_prediction_file, gender_prediction_file))

