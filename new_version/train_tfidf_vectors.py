__author__ = 'shrprasha'

import os
import utils
from collections import Counter

# def get_tf_values(ngrams):
#     return Counter(ngrams)

# def get_idf_values():
#     pass

def get_ngrams_df(doc_ngrams):
    ngrams_df = {}
    # for


def tokenize(fpath):
    pass

def write_tfidf_vectors(dirpath):
    docs = next(os.walk(dirpath))[2] #get just the files and not directories
    doc_ngrams = {}  #{doc1: Counter({ngram1: cnt1, ngram2: cnt2, ...}, doc2: Counter(...), .... }
    all_ngrams = []  #all ngram features
    for doc in docs:
        fhandle = open(os.path.join(dirpath, doc), 'r')
        words = utils.get_words_in_file()
        ngrams = utils.create_n_gram_profile(words, 2)
        ngrams.extend(utils.create_n_gram_profile(words, 3))
        ngrams.extend(words)
        doc_ngrams[doc] = Counter(ngrams)
    ngrams_df = get_ngrams_df(doc_ngrams)

