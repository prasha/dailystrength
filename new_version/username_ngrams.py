__author__ = 'prasha'
import os
import utils
from collections import Counter
import sys


# creates tf-idf style feature files with maps. not list
#fname feature_no:feature_value feature_no:feature_value ....

def char_ngrams1(str, n):
    str_ngrams = []
    for i in range(0, len(str) - n + 1):
        str_ngrams.append(str[i:n + i])
    return str_ngrams


def create_features(username_text_dir, uid_uname_map):
    files = next(os.walk(username_text_dir))[2]  #get just the files and not directories
    fname_features = {}
    features_set = set()
    for fname in files:
        fHandle = open(os.path.join(username_text_dir, fname), 'r').read()
        # if len(fHandle) >= 250:
        #     continue
        uid = fname.split("_", 1)[0]
        username = uid_uname_map[uid]

        username_start_end_marker = "%" + username + "%"
        bigrams = char_ngrams1(username_start_end_marker, 2)
        trigrams = char_ngrams1(username_start_end_marker, 3)
        fourgrams = char_ngrams1(username_start_end_marker, 4)
        fivegrams = char_ngrams1(username_start_end_marker, 5)
        features = bigrams + trigrams + fourgrams + fivegrams
        cfeatures = Counter(features)
        # maxm = cfeatures.most_common()[0][1] * 1.0
        # for k, v in cfeatures.iteritems():
        #      cfeatures[k] /= maxm
        fname_features[fname] = cfeatures
        features_set.update(cfeatures)
    return fname_features, features_set


def load_uid_uname(uid_uname_file):
    lines = open(uid_uname_file, 'r').readlines()
    uid_uname_map = {}
    for line in lines:
        uid, uname = line.strip().split(",")
        uid_uname_map[uid] = uname
    print len(uid_uname_map)
    return uid_uname_map


def write_to_file(fname_features, features_set):
    output_fHandle = open(oppath, 'w')
    # output_fHandle = open("/home/prasha/Documents/dailystrength/author_profiling_data/features/username_char_ngrams.vectors", 'w')
    output_fHandle.write(str(len(features_set)) + "\n")
    #start_at = 10202954  # so that this can be merged with tfidf features without iterating through and changing indices
    all_features = list(features_set)  # since set does not have index
    for fname, features in fname_features.iteritems():  # fname=filename, features={ngram:count, ...}
        featureidx_featureval_dict = {}  # convert ngram to feature idx first, since the feature file need to have features sorted by idx
        for feature_name, feature_val in features.iteritems():  # feature_name=ngram, feature_val=count
            feature_idx = all_features.index(feature_name)
            featureidx_featureval_dict[feature_idx] = feature_val
        # line = utils.remove_username_from_fname(fname)
        line = fname
        for feature_idx in sorted(featureidx_featureval_dict):
            feature_val = featureidx_featureval_dict[feature_idx]
            line += " " + str(feature_idx) + ":" + str(feature_val)
        output_fHandle.write(line + "\n")

# python username_ngrams.py /Users/shrprasha/Projects/resources/dailystrength/together /Users/shrprasha/Projects/dailystrength/resources/userid_userdetails.txt /Users/shrprasha/Projects/resources/dailystrength/features/uname_char_ngram.vectors
# python username_ngrams.py /Users/shrprasha/Projects/resources/dailystrength/together /Users/shrprasha/Projects/dailystrength/resources/userid_userdetails_combined.txt /Users/shrprasha/Projects/resources/dailystrength/features/uname_char_ngram.vectors
# python username_ngrams.py /home/prasha/Documents/dailystrength/author_profiling_data/together /home/prasha/Dropbox/dailystrength/resources/userid_userdetails.txt /home/prasha/Documents/dailystrength/author_profiling_data/features/uname_char_ngram.vectors
if __name__ == "__main__":
    username_text_dir = sys.argv[1]
    uid_uname_file = sys.argv[2]
    oppath = sys.argv[3]
    # username_text_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/together"
    # uid_uname_file = "/home/prasha/Dropbox/dailystrength/resources/userid_userdetails.txt"
    # # username_text_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/username_data_pan14_train_test/small_test_familial"

    # uids = load_uid_uname(uid_uname_file).keys()
    # files = next(os.walk(username_text_dir))[2]
    # for fname in files:
    #     uid = fname.split("_", 1)[0]
    #     if uid not in uids:
    #         print fname

    write_to_file(*create_features(username_text_dir, load_uid_uname(uid_uname_file)))
