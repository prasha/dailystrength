__author__ = 'prasha'
import xml.etree.ElementTree as xml
import os
import codecs
import sys

# decisions:
#did not leave out treatments: although treatments are not indicative of a person's age or gender
#knowledge about what disease this post was under is not preserved

#TODO:
#load people_age_gender
#parse all xml in XML folder to get all <text> and <treatment>, join them by space

#create file with fname: userid_age_gender
#NA when age or gender are not available
#each file contains all the texts written by that user at that age
class CreateDataset:
    def __init__(self, age_gender_file, opfile, diseases):
        self.opfile = opfile
        self.opHandle = open(opfile, 'w')
        self.diseases = diseases
        lines = open(age_gender_file).readlines()
        self.age_gender_map = {}
        self.posts_by_user = {}  #{user1: {Acne: [# of posts, # of replies], ADHD: [# of posts, # of replies], ...}
        for line in lines:
            username_id_age_gender = line.strip()
            username, id, age, gender = username_id_age_gender.split(",")
            self.age_gender_map[id] = (username, age, gender)
            self.posts_by_user[id] = {"Acne": [0, 0], "ADHD": [0, 0], "Alcoholism": [0, 0], "Asthma": [0, 0],
                                      "Back-Pain": [0, 0], "Bipolar-Disorder": [0, 0], "Bone-Cancer": [0, 0],
                                      "COPD": [0, 0], "Diets-Weight-Maintenance": [0, 0], "Fibromyalgia": [0, 0],
                                      "Gastric-Bypass-Surgery": [0, 0], "Immigration-Law": [0, 0],
                                      "Infertility": [0, 0], "Loneliness": [0, 0], "Lung-Cancer": [0, 0],
                                      "Migraine": [0, 0], "Miscarriage": [0, 0], "Pregnancy": [0, 0],
                                      "Rheumatoid-Arthritis": [0, 0], "War-In-Iraq": [0, 0]}

    def createds(self, xmldir):
        self.xmldir = xmldir
        self.disease = xmldir.split("/")[-2]  #/mnt/docsig/storage/daily-strength/Acne/XML
        files = sorted(os.listdir(self.xmldir))
        for doc_fname in files:
            extension = os.path.splitext(doc_fname)[-1]
            if extension != ".xml":
                continue
            print self.disease, doc_fname
            self.parse_xml(os.path.join(self.xmldir, doc_fname))

    def parse_xml(self, xml_file):
        tree = xml.parse(os.path.join(xml_file))
        root = tree.getroot()
        for problem in root.iter('problem'):
            self.store_to_map(problem, 0)
        for reply in root.iter('reply'):
            self.store_to_map(reply, 1)

    def store_to_map(self, xmlobj, p_or_r):
        val = self.parse_problem_or_reply(xmlobj)
        if (val):
            id = val
            if id in self.age_gender_map:
                self.posts_by_user[id][disease][p_or_r] += 1


    def parse_problem_or_reply(self, xmlobj):
        if xmlobj.find(
                'person') is not None:  #added since /mnt/docsig/storage/daily-strength/Bipolar-Disorder/XML/10056719-taken-advantage.html.xml has no data in problem or replies
            id = xmlobj.find('person').get('id')  #'/people/436265'
            id = id.split("/")[-1]
            return id
        else:
            return None

    #all data was read on 2013; so the age is the age of the poster in 2013
    def get_actual_age_group(self, age_now, date_posted):
        year_obtained = 13
        if (age_now):
            age = int(age_now)
            year_posted = int(date_posted.split()[0].split("/")[-1])
            actual_age = age - (year_obtained - year_posted)
            return self.get_age_group(actual_age)
        else:
            self.stats["no_age"] += 1
            return "NA"

    #find out the range under which the age falls
    def get_age_group(self, age):
        for min, max in self.age_groups:
            if (min <= age <= max):
                if (min == 65):
                    return "65-xx"
                else:
                    return str(min) + "-" + str(max)
        return "NA"

    def write_to_file(self):
        for id in sorted(self.posts_by_user):
            diseases = self.posts_by_user[id]
            username, age, gender = self.age_gender_map[id]
            line = str(id) + "," + username + "," + age + "," + gender + ","
            #for disease in self.diseases:
            for disease in self.diseases:
                posts_count, replies_count = self.posts_by_user[id][disease]
                line += str(posts_count) + "," + str(replies_count) + ","
            line = line[:-1] + "\n"
            self.opHandle.write(line)


    def get_stats(self):
        return self.stats


if __name__ == "__main__":
    opfile = "/home/prasha/Dropbox/dailystrength/resources/userid_diseases.txt"

    # age_gender_file = "/home/prasha/del/username_id_age_gender_both_age_gender.txt"
    # xmlroot = "/home/prasha/Documents/dailystrength/daily-strength-data-partial"
    # #diseases = ["Acne", "ADHD"]

    age_gender_file = "/home/prasha/Dropbox/dailystrength/resources/username_id_age_gender_both_age_gender_combined.txt"
    xmlroot = "/mnt/docsig/storage/daily-strength"
    # diseases = ["Acne", "ADHD"]
    diseases = ["Acne", "ADHD", "Alcoholism", "Asthma", "Back-Pain", "Bipolar-Disorder", "Bone-Cancer", "COPD",
                "Diets-Weight-Maintenance", "Fibromyalgia", "Gastric-Bypass-Surgery", "Immigration-Law", "Infertility",
                "Loneliness", "Lung-Cancer", "Migraine", "Miscarriage", "Pregnancy", "Rheumatoid-Arthritis",
                "War-In-Iraq"]
    cds = CreateDataset(age_gender_file, opfile, diseases)
    for disease in diseases:
        xmldir = os.path.join(xmlroot, disease, "XML")
        print xmldir
        cds.createds(xmldir)
    cds.write_to_file()

    #age_gender_file = "/home/prasha/Dropbox/DailyStrength/id_age_gender.txt"
    #age_groups = [(18, 24), (25, 34), (35, 49), (50, 64), (65, 200)]
    #xmldir = "/home/prasha/Documents/dailystrength/author_profiling_data/test/test_ip"
    #opfile = "/home/prasha/Documents/dailystrength/author_profiling_data/test/test_op"
    #cds = CreateDataset(age_gender_file, opfile, age_groups)
    #cds.createds(xmldir)


    #xmldir = sys.argv[1]
    #opfile = sys.argv[2]
    #cd.parse_xml("/mnt/docsig/storage/daily-strength/Acne/XML/9142628-need-some-input-please.html.xml")


