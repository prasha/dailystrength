__author__ = 'prasha'
from pprint import pprint
import string
import sys

# predicted_file = "/home/prasha/Documents/dailystrength/author_profiling_data/processed_data/output2/ds_all_with_familial.out"
# actual_file = "/home/prasha/Documents/dailystrength/author_profiling_data/processed_data/output2/combined_features_liblinear_test"
delim = " "
#labels = ["18-24_female", "18-24_male", "25-34_female", "25-34_male", "35-49_female", "35-49_male", "50-64_female",
#          "50-64_male", "65-xx_female", "65-xx_male"]

def calculate_age_gender_separate(results, total, classes):
        #calculate age, gender, total accuracies
    corr_tot = 0
    corr_age = 0
    corr_gender = 0
    for i in range(0, classes):
        corr_tot += results[i][i]
        if (i % 2 == 0):
            corr_age += results[i][i] + results[i][i + 1]
            for j in range(0, classes):
                if (j % 2 == 0):
                    corr_gender += results[i][j]
        else:
            corr_age += results[i][i] + results[i][i - 1]
            for j in range(0, classes):
                if (j % 2 != 0):
                    corr_gender += results[i][j]
    print corr_gender
    print "age %: ", corr_age * 100.0 / total
    print "gender %: ", corr_gender * 100.0 / total
    print "total %: ", corr_tot * 100.0 / total
    print corr_gender * 100.0 / total, "\t", corr_age * 100.0 / total, "\t", corr_tot * 100.0 / total

#made separately so that get_results can be used by get_combined_age_gender_from_separate_run.py
def get_predictions():
    predictions = [int(line.split(delim, 1)[0]) for line in open(predicted_file).readlines()[1:]]
    return predictions

def get_results(age_gender_both, actual_file, predictions):
    if age_gender_both == "age":
        labels = ["18-24", "25-34", "35-49", "50-64", "65-xx"]
    elif age_gender_both == "gender":
        labels = ["female", "male"]
    elif age_gender_both == "both":
        labels = ["18-24_female", "18-24_male", "25-34_female", "25-34_male", "35-49_female", "35-49_male",
                  "50-64_female", "50-64_male", "65-xx_female", "65-xx_male"]
    classes = len(labels)
    results = []
    for i in range(0, classes):
        results.append([])
        for j in range(0, classes):
            results[i].append(0)
    allLetters = string.lowercase

    actual_classes = [int(line.split(delim, 1)[0]) for line in open(actual_file).readlines()]
    total = len(predictions)

    for i in range(0, total):
        predicted = predictions[i]
        actual = actual_classes[i]
        results[actual][predicted] += 1
    # pprint(results)

    if age_gender_both == "both":
        calculate_age_gender_separate(results, total, classes)

    #print out confusion matrix
    for i in range(0, classes):
        print allLetters[i], "\t",
    print "<---Classified as"
    for i in range(0, classes):
        tot = 0
        for j in range(0, classes):
            print results[i][j], "\t",
            tot += results[i][j]
        print "|  ", tot, "\t", allLetters[i], "\t=", labels[i]


if __name__ == "__main__":
    predicted_file = sys.argv[1]
    actual_file = sys.argv[2]
    age_gender_both = sys.argv[3]
    get_results(age_gender_both, actual_file, get_predictions())

