__author__ = 'prasha'


def remove_username_from_fname(fname):
    stats = fname.split("_")
    # gender = stats[-1].split(".")[0]
    age = stats[-2]
    lang = stats[-3]
    id = stats[-4]
    return "_".join([id, lang, age, stats[-1]])


def remove_username_from_fnames(fnames):
    new_fnames = []
    for fname in fnames:
        new_fnames.append(remove_username_from_fname(fname))
    return new_fnames


def get_userid(fname):
    stats = fname.split("_")
    id = stats[-4]
    return id