'''
Created on Sep 18, 2013

@author: bgyawali


this program downloads all the HTML files for the peoples. 
'''
import mechanize, shutil, os
from time import sleep
import sys


def purge_already_fetched(userLinks, outputDir):

    # list of threads that still need downloading
    rem_list = []
    # check each tweet to see if we have it
    for item in userLinks:
        linkId = getLinkId(item)
        thread_file = outputDir + '/' + linkId + ".html"
        if(not os.path.exists(thread_file)):
            rem_list.append(item)
    return rem_list


def getLinkId(userLink):
    link = userLink.split("\t")[0]
    linkId = link.split('/')[2]
    return linkId
        
    

def download_forums(userLink, outputDir):
    link = userLink.split("\t")[0]
    link = "http://www.dailystrength.org" + link
    try:
        linkId = getLinkId(userLink)
        outputFile =   outputDir + "/" + linkId + ".html" 
        br = mechanize.Browser()
        br.set_handle_robots(False)
        br.set_handle_equiv(False)
        br.open(link)
        html = br.response().read()
        f = open(outputFile, "w")
        f.write(html)
        f.close()
    except (mechanize.HTTPError, mechanize.URLError) as e:
        if isinstance(e,mechanize.HTTPError):
            f = open(outputFile, "w")
            f.close()
        elif isinstance(e, mechanize.URLError):
            print "error in downloading " + str(e)
            print (link)
        else:
            raise e.reason.args
    # sleep(5)

def read_total_list(inputDir, inputFile):

    # read total fetch list csv
    total_list = [line.strip() for line in open(inputDir + "/" + inputFile)]
    return total_list

   
# MYEDIT for cluster run
### start of main method
def main(userLinks, outputDir):
    if not os.path.exists(outputDir):
        os.makedirs(outputDir)

    # ## read the input file
    # for dirs in inputDirs:
    #     userLinks = read_total_list(dirs, inputFile)
    if os.path.exists(outputDir):
        fetch_list = purge_already_fetched(userLinks, outputDir)
    else:
        os.makedirs(outputDir)
        fetch_list = userLinks
    # print "condition: " + dirs
    print "Total threads: " + str(len(userLinks))
    print "Threads remaining to download: " + str(len(fetch_list))
    for i in range(0, len(fetch_list)):
        # print str(i + 1),
        download_forums(fetch_list[i], outputDir)

# ### start of main method
# def main(inputDirs, inputFile, outputDir):
#     if not os.path.exists(outputDir):
#         os.makedirs(outputDir)
#
#     # ## read the input file
#     for dirs in inputDirs:
#         userLinks = read_total_list(dirs, inputFile)
#         if os.path.exists(outputDir):
#             fetch_list = purge_already_fetched(userLinks, outputDir)
#         else:
#             os.makedirs(outputDir)
#             fetch_list = userLinks
#         print "condition: " + dirs
#         print "Total threads: " + str(len(userLinks))
#         print "Threads remaining to download: " + str(len(fetch_list))
#         for i in range(0, len(fetch_list)):
# 			print str(i + 1),
# 			download_forums(fetch_list[i], outputDir)

def is_last_process():
    return (process_no + 1) == total_processes
    
if __name__ == "__main__":
    # inputDirs = ["/Users/shrprasha/del/ds_test/Acne", "/Users/shrprasha/del/ds_test/Fructose_Intolerance",
    #              # "/Users/shrprasha/del/ds_test/Infertility", "/Users/shrprasha/del/ds_test/Miscarriage-Stillbirth",
    #              "/Users/shrprasha/del/ds_test/Physical-Emotional-Abuse", "/Users/shrprasha/del/ds_test/Widows-Widowers"]
    # outputDir = "/Users/shrprasha/del/ds_test/PeopleHTML"
    # inputFile = "peopleLists.txt"

    # inputDirs = ["/project/solorio/prasha/ds/data/all_people_list_divided"]
    # outputDir = "/project/solorio/prasha/ds/output/PeopleHTML"
    outputDir = "/Users/shrprasha/Projects/resources/dailystrength/nicolas_data/PeopleHTML"
    # inputFiles = "peopleLists.txt"
    # if os.path.isfile(os.path.join(inputDirs[0], inputFile)):
    # all_users = read_total_list("/project/solorio/prasha/ds/data/new_for_age_range_output", "peopleLists.txt")

    all_users = read_total_list("/Users/shrprasha/Projects/resources/dailystrength/nicolas_data", "peopleLists.txt")
    userLinks = all_users
    main(userLinks, outputDir)

    # # diseases = ["Anger_Management", "Asthma"]
    # # diseases = ["Autism", "Bereavement", "Bipolar_Disorder"]
    # diseases = ["Bisexuality", "Bone_Cancer", "Brain_Injury", "COPD", "Caregivers", "Chronic_Fatigue", "Chronic_Pain", "Cirrhosis", "Codependency", "Common_Variable_Immunodeficiency", "Crohn_Disease_Ulcerative_Colitis", "Deep_Vein_Thrombosis", "Depression_Teen", "Diabetes_Type_2"]
    # for disease in diseases:
    #     print disease
    #     all_users = read_total_list("/Users/shrprasha/Projects/resources/dailystrength/nicolas_data/" + disease, "peopleLists.txt")
    #     userLinks = all_users
    #     main(userLinks, outputDir)


    # all_users = read_total_list("/Users/shrprasha/Projects/resources/dailystrength/nicolas_data", "peopleLists.txt")
    #
    # #cluster
    # # process_no = int(sys.argv[1])
    # # total_processes = int(sys.argv[2])
    # # processed_by_each = len(all_users) / total_processes
    # # process_from = process_no * processed_by_each
    # # process_to = len(all_users) if is_last_process() else process_from + processed_by_each
    # # userLinks = all_users[process_from:process_to]
    # #local
    # userLinks = all_users
    #
    # main(userLinks, outputDir)
    
