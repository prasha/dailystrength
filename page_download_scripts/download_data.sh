#!/bin/bash

# $1-> disease

scripts_folder="/Users/shrprasha/Projects/dailystrength/page_download_scripts"
output_folder="/Users/shrprasha/del/DS_download_test"
#output_folder="/project/solorio/prasha/ds/data/new_for_age_range_output"
#scripts_folder="/project/solorio/prasha/ds/code/for_data_download/page_download_scripts"
current_output_folder=$output_folder/$1
mkdir -p current_output_folder

echo "========ListAllPages========"
python $scripts_folder/ListAllPages.py http://www.dailystrength.org/c/$1/forum $current_output_folder

echo "========downloadWebPages========"
python $scripts_folder/downloadWebPages.py "$current_output_folder" "$current_output_folder/FilesLists.txt"

echo "========ThreadsToXML========"
echo "python $scripts_folder/ThreadsToXML.py "$current_output_folder/HTML" $current_output_folder FilesLists.txt"
python $scripts_folder/ThreadsToXML.py "$current_output_folder/HTML" $current_output_folder "FilesLists.txt"



#================cluster=====================#

#output_folder="/project/solorio/prasha/ds/data/new_for_age_range_output"
#scripts_folder="/project/solorio/prasha/ds/code/for_data_download/page_download_scripts"
##scripts_folder="/Users/shrprasha/Projects/dailystrength/page_download_scripts"
#current_output_folder=$output_folder/$1
#mkdir -p current_output_folder
#
#echo "ListAllPages"
#python $scripts_folder/ListAllPages.py http://www.dailystrength.org/c/$1/forum $current_output_folder
#
#echo "downloadWebPages"
#python $scripts_folder/downloadWebPages.py "$current_output_folder" "$current_output_folder/FilesLists.txt"
#
#echo "ThreadsToXML"
#python $scripts_folder/ThreadsToXML.py "$current_output_folder/HTML" $current_output_folder "FilesLists.txt"