#!/usr/bin/python
__author__ = 'melquiades'

import ListAllPages
import downloadWebPages
import ThreadsToXML
import XMLtoText
import GetListUsers

from joblib import Parallel, delayed
import multiprocessing

##########################################################################################################3
# To create the HTML files
# for dir in */; do mkdir -- "$dir/HTML"; done


mainPath = '/media/melquiades/NICOLAS_3/dailyStrengthNEW3_FINAL/'

diseaseUrlList = {
    'Common_Variable_Immunodeficiency':'http://www.dailystrength.org/c/Common-Variable-Immunodeficiency/forum',
    'Acne':'http://www.dailystrength.org/c/Acne/forum',
    'ADHD':'http://www.dailystrength.org/c/ADHD-ADD/forum',
    'Agoraphobia_Social_Anxiety':'http://www.dailystrength.org/c/Agoraphobia-and-Social-Anxiety/forum',
    'Alcoholism':'http://www.dailystrength.org/c/Alcoholism/forum',
    'Alzheimer_Disease':'http://www.dailystrength.org/c/Alzheimers-Disease/forum',
    'Anger_Management':'http://www.dailystrength.org/c/Anger-Management/forum',
    'Anxiety':'http://www.dailystrength.org/c/Anxiety/forum',
    'Asperger_Syndrome':'http://www.dailystrength.org/c/Asperger-Syndrome/forum',
    'Asthma':'http://www.dailystrength.org/c/Asthma/forum',
    'Atrial_Fibrillation':'http://www.dailystrength.org/c/Atrial-Fibrillation-AFib/forum',
    'Autism':'http://www.dailystrength.org/c/Autism-Autism-Spectrum/forum',
    'Back_Pain':'http://www.dailystrength.org/c/Back-Pain/forum',
    'Bereavement':'http://www.dailystrength.org/c/Bereavement/forum',
    'Bipolar_Disorder':'http://www.dailystrength.org/c/Bipolar-Disorder/forum',
    'Bisexuality':'http://www.dailystrength.org/c/Bisexuality/forum',
    'Bone_Cancer':'http://www.dailystrength.org/c/Bone-Cancer/forum',
    'Brain_Injury':'http://www.dailystrength.org/c/Brain-Injury/forum',
    'Breakups_Divorce':'http://www.dailystrength.org/c/Breakups-Divorce/forum',
    'Caregivers':'http://www.dailystrength.org/c/Caregivers/forum',
    'Chronic_Fatigue':'http://www.dailystrength.org/c/Chronic-Fatigue-Syndrome/forum',
    'Chronic_Pain':'http://www.dailystrength.org/c/Chronic-Pain/forum',
    'Cirrhosis':'http://www.dailystrength.org/c/Cirrhosis/forum',
    'Codependency':'http://www.dailystrength.org/c/Codependency/forum',
    'COPD':'http://www.dailystrength.org/c/COPD-Emphysema/forum',
    'Crohn_Disease_Ulcerative_Colitis':'http://www.dailystrength.org/c/Crohns-Disease-Ulcerative-Colitis/forum',
    'Deep_Vein_Thrombosis':'http://www.dailystrength.org/c/Deep-Vein-Thrombosis-DVT/forum',
    'Depression':'http://www.dailystrength.org/c/Depression/forum',
    'Depression_Teen':'http://www.dailystrength.org/c/Depression-Teen/forum',
    'Diabetes_Type_2':'http://www.dailystrength.org/c/Diabetes-Type-2/forum',
    'Diets_Weight_Maintenance':'http://www.dailystrength.org/c/Diets-Weight-Maintenance/forum',
    'Eating_Disorders':'http://www.dailystrength.org/c/Eating-Disorders/forum',
    'Endometriosis':'http://www.dailystrength.org/c/Endometriosis/forum',
    'Epilepsy_Seizures':'http://www.dailystrength.org/c/Epilepsy-Seizures/forum',
    'Families_Friends_Of_Addicts':'http://www.dailystrength.org/c/Families-and-Friends-Of-Addicts/forum',
    'Families_of_Prisoners':'http://www.dailystrength.org/c/Families-of-Prisoners/forum',
    'Family_Friends_of_Bipolar':'http://www.dailystrength.org/c/Family-and-Friends-of-Bipolar/forum',
    'Family_Issues':'http://www.dailystrength.org/c/Family-Issues/forum',
    'Fibromyalgia':'http://www.dailystrength.org/c/Fibromyalgia/forum',
    'Financial_Challenges':'http://www.dailystrength.org/c/Financial-Challenges/forum',
    'Food_Addiction':'http://www.dailystrength.org/c/Food-Addiction/forum',
    'Gambling_Addiction_Recovery':'http://www.dailystrength.org/c/Gambling-Addiction/forum',
    'Gastric_Bypass_Surgery':'http://www.dailystrength.org/c/Gastric-Bypass-Surgery/forum',
    'Gay_Lesbian_Teens':'http://www.dailystrength.org/c/Gay-Lesbian-Teens/forum',
    'Grandparents_Raising_Children':'http://www.dailystrength.org/c/Grandparents-Raising-Children/forum',
    'Graves_Disease':'http://www.dailystrength.org/c/Graves-Disease/forum',
    'Healthy_Relationships':'http://www.dailystrength.org/c/Healthy-Relationships/forum',
    'Healthy_Sex':'http://www.dailystrength.org/c/Healthy-Sex/forum',
    'Hepatitis_C':'http://www.dailystrength.org/c/Hepatitis-C/forum',
    'Hidradenitis_Suppurativa':'http://www.dailystrength.org/c/Hidradenitis-Suppurativa/forum',
    'HPV':'http://www.dailystrength.org/c/HPV/forum',
    'Hypothyroidism':'http://www.dailystrength.org/c/Hypothyroidism/forum',
    'Immigration_Law':'http://www.dailystrength.org/c/Immigration-Law/forum',
    'Incest_Survivors':'http://www.dailystrength.org/c/Incest-Survivors/forum',
    'Infertility':'http://www.dailystrength.org/c/Infertility/forum',
    'Insomnia':'http://www.dailystrength.org/c/Insomnia/forum',
    'Irritable_Bowel_Syndrome':'http://www.dailystrength.org/c/Irritable-Bowel-Syndrome-IBS/forum',
    'Lesbian_Relationship_Challenges':'http://www.dailystrength.org/c/Lesbian-Relationship-Challenges/forum',
    'Loneliness':'http://www.dailystrength.org/c/Loneliness/forum',
    'Lung_Cancer':'http://www.dailystrength.org/c/Lung-Cancer/forum',
    'Lupus':'http://www.dailystrength.org/c/Lupus/forum',
    'Lyme_Disease':'http://www.dailystrength.org/c/Lyme-Disease/forum',
    'Menopause':'http://www.dailystrength.org/c/Menopause/forum',
    'Migraine':'http://www.dailystrength.org/c/Migraine-Headaches/forum',
    'Migraine_Headaches':'http://www.dailystrength.org/c/Migraine-Headaches/forum',
    'Miscarriage':'http://www.dailystrength.org/c/Miscarriage-Stillbirth/forum',
    'Multiple_Sclerosis':'http://www.dailystrength.org/c/Multiple-Sclerosis-MS/forum',
    'Myasthenia_Gravis':'http://www.dailystrength.org/c/Myasthenia-Gravis/forum',
    'Narcolepsy':'http://www.dailystrength.org/c/Narcolepsy/forum',
    'Obesity':'http://www.dailystrength.org/c/Obesity/forum',
    'Obsessive_Compulsive_Disorder':'http://www.dailystrength.org/c/Obsessive-Compulsive-Disorder-OCD/forum',
    'Pancreatitis':'http://www.dailystrength.org/c/Pancreatitis/forum',
    'Panic_Attacks':'http://www.dailystrength.org/c/Panic-Attacks/forum',
    'Parkinson_Disease':'http://www.dailystrength.org/c/Parkinsons-Disease/forum',
    'Personality_Disorders':'http://www.dailystrength.org/c/Personality-Disorders/forum',
    'Physical_Emotional_Abuse':'http://www.dailystrength.org/c/Physical-Emotional-Abuse/forum',
    'Polycystic_Ovarian_Syndrome':'http://www.dailystrength.org/c/Polycystic-Ovarian-Syndrome-PCOS/forum',
    'Post_Traumatic_Stress_Disorder':'http://www.dailystrength.org/c/Post-Traumatic-Stress-Disorder/forum',
    'Pregnancy':'http://www.dailystrength.org/c/Pregnancy/forum',
    'Pregnancy_After_Loss_Infertility':'http://www.dailystrength.org/c/Pregnancy-After-Loss-Or-Infertility/forum',
    'Pseudotumor_Cerebri':'http://www.dailystrength.org/c/Psuedotumor-Cerebri/forum',
    'Pulmonary_Embolism':'http://www.dailystrength.org/c/Pulmonary-Embolism/forum',
    'Rape':'http://www.dailystrength.org/c/Rape/forum',
    'Rheumatoid_Arthritis':'http://www.dailystrength.org/c/Rheumatoid-Arthritis/forum',
    'Schizophrenia':'http://www.dailystrength.org/c/Schizophrenia/forum',
    'Self_Injury':'http://www.dailystrength.org/c/Self-Injury/forum',
    'Sex_Pornography_Addiction':'http://www.dailystrength.org/c/Sex-Pornography-Addiction/forum',
    'Sexual_Abuse':'http://www.dailystrength.org/c/Sexual-Abuse/forum',
    'Smoking_Addiction':'http://www.dailystrength.org/c/Smoking-Addiction/forum',
    'Smoking_Addiction_Recovery':'http://www.dailystrength.org/c/Smoking-Addiction/forum',
    'Stillbirth':'http://www.dailystrength.org/c/Stillbirth/forum',
    'Tinnitus':'http://www.dailystrength.org/c/Tinnitus/forum',
    'Trichotillomania_Hair_Pulling':'http://www.dailystrength.org/c/Trichotillomania-Hair-Pulling/forum',
    'Trying_To_Conceive':'http://www.dailystrength.org/c/Trying-To-Conceive/forum',
    'War_In_Iraq':'http://www.dailystrength.org/c/War-in-Iraq/forum',
    'Widows_Widowers':'http://www.dailystrength.org/c/Widows-Widowers/forum'
}

# diseaseUrlList = {'Rheumatoid-Arthritis':'http://www.dailystrength.org/c/Rheumatoid-Arthritis/forum'}
##########################################################################################################3
print diseaseUrlList


def processInput13(nameDisease, seedUrl, num_cores, t):
    outPath = mainPath + '/' + nameDisease + '/'
    outPathHTML = outPath + 'HTML/'
    outPathXML = outPath + 'XML/'

    # 1
    # ListAllPages.command( seedUrl, outPath )
    # 2 (Dont forget to create the HTML folders)
    # for dir in */; do mkdir -- "$dir/HTML"; done
    # downloadWebPages.command( outPath, 'FilesLists.txt', num_cores, t )
    # 3
    # ThreadsToXML.command( outPathHTML, outPath, 'FilesLists.txt', num_cores, t )
    # 4
    # GetListUsers.command( outPath, outPathXML )

def processInput4(nameDisease, seedUrl):
    outPath = mainPath + '/' + nameDisease + '/'
    outPathHTML = outPath + 'HTML/'
    outPathXML = outPath + 'XML/'

    # 1
    # ListAllPages.command( seedUrl, outPath )
    # 2 (Dont forget to create the HTML folders)
    # for dir in */; do mkdir -- "$dir/HTML"; done
    # downloadWebPages.command( outPath, 'FilesLists.txt', num_cores, t )
    # 3
    # ThreadsToXML.command( outPathHTML, outPath, 'FilesLists.txt' )
    # 4
    GetListUsers.command( outPath, outPathXML )

# # 1-3
# num_cores = 4
# for nameDisease, seedUrl in diseaseUrlList.iteritems():
#     Parallel(n_jobs=num_cores)(delayed(processInput13)(nameDisease, seedUrl, num_cores, t) for t in range(0,num_cores))

# Sometimes the amount of ram consumed by 4 is too much, so it is better to run it in single thread
# # 4
# num_cores = 4
# Parallel(n_jobs=num_cores)(delayed(processInput4)(nameDisease, seedUrl) for nameDisease, seedUrl in diseaseUrlList.iteritems())







