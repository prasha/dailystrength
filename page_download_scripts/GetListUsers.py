# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import os, shutil, sys
from lxml import etree
import xml.etree.ElementTree as ET


# ## main method
def main( user_params ):

    outputDir = user_params['outputDir']
    if not os.path.exists(user_params['outputDir']):
        print "FIXME: The outpath ("+user_params['outputDir']+") does not exists. Closing"
        return

    outputDirXML = user_params['outputDirXML']
    # if os.path.exists(outputDir):
    #     shutil.rmtree(outputDir)
    # os.makedirs(outputDir)
    if not os.path.exists(user_params['outputDirXML']):
        print "FIXME: The outpath ("+user_params['outputDirXML']+") does not exists. Closing"
        return

    # ## create a dictionary for people links
    people = {}

    listOfFiles = os.listdir(user_params['outputDirXML'])
    cnt = 0
    for file in listOfFiles:
        cnt = cnt + 1
        print "File #: "+str(cnt)+", missing: "+str(len(listOfFiles))

        eachXML = open(user_params['outputDirXML']+'/'+file, "r")
        content = eachXML.readlines()

        newContent=""
        for c in content:
            for l in c:
                newContent = newContent+ l

        soup = BeautifulSoup(newContent)
        listPerson = soup.findAll('person')
        for person in listPerson:
            # print person
            # print person['id']
            # print person.string

            if person.string.endswith('&amp;hellip;'):
                person.string = person.string[:-13]

            people[person['id']] = person.string

        eachXML.close()


    peopleFileName = user_params['outputDir'] + "/peopleLists.txt"
    peopleFile = open(peopleFileName, "w")
    for temp in people:
        #print temp + "\t" + people[temp]
        peopleFile.write(temp + "\t" + people[temp] + "\n")

    return
                

def get_user_params():

    user_params = {}
    # get user input params
    user_params['outputDir'] = raw_input('\nOutput Directory [Back-Pain]: ')
    user_params['outputDirXML'] = raw_input('\nOutput Directory [Back-Pain/XML]: ')

    if user_params['outputDir'] == '' or user_params['outputDirXML'] == '':
        print "Invalid seed URL or Output directory. Try again"
        sys.exit()

    return user_params



def dump_user_params(user_params):

    # dump user params for confirmation
    print 'Output Directory:    ' + user_params['outputDir']
    print 'Output Directory XML:   ' + user_params['outputDirXML']
    return

def command( outputDir, outputDirXML ):

    user_params = {}
    # get user input params
    user_params['outputDir'] = outputDir #raw_input('\nOutput Directory [Back-Pain]: ')
    user_params['outputDirXML'] = outputDirXML #raw_input('\nOutput Directory [Back-Pain]: ')


    if user_params['outputDir'] == '' or user_params['outputDirXML'] == '':
        print "Invalid seed URL or Output directory. Try again"
        sys.exit()

    dump_user_params(user_params)
    main( user_params )
            
        
if __name__ == "__main__":

    user_params = get_user_params()
    dump_user_params(user_params)
    main( user_params )
