__author__ = 'prasha'
import xml.etree.ElementTree as xml
import os
import codecs
import sys
import shutil
import nltk
from pprint import pprint
from collections import Counter
import re


#username can have '_' in it. so, start indexing from the end after splitting by '_'

def char_ngrams(str, n):
    arr = list(str)
    return nltk.ngrams(arr, n)

class UsernameLibLinear:

    def __init__(self, username_text_dir, output_fHandle):
        self.username_text_dir = username_text_dir
        self.output_fHandle = output_fHandle
        self.label_instances_map = {}
        self.label_list = []
        self.feature_list = []
        self.create_features()
        self.write_to_file()

    def create_features(self):
        files = next(os.walk(self.username_text_dir))[2] #get just the files and not directories
        # lines = open(age_gender_file, 'r').readlines()
        # id_fname_map = {}
        for file in files:
            stats = file.split("_")
            # print file
            # print stats
            gender = stats[-1].split(".")[0]
            age = stats[-2]
            lang = stats[-3]
            id = stats[-4]
            username = "_".join(stats[:-4])
            firstnames = self.get_firstname_partitions(username)
            username_start_end_marker = "%" + username + "%"
            label = gender
            bigrams = char_ngrams(username_start_end_marker, 2)
            trigrams = char_ngrams(username_start_end_marker, 3)
            fourgrams = char_ngrams(username_start_end_marker, 4)
            fivegrams = char_ngrams(username_start_end_marker, 5)
            features = bigrams + trigrams + fourgrams + fivegrams + firstnames
            self.update_maps(Counter(features), label)

    def get_firstname_partitions(self, username):
        fp = []
        parts = re.split("[_\.-]", username)
        if not parts[0] == username:
            fp.extend(parts)
        parts1 = []
        for part in parts:
            p = re.findall('[A-Z][^A-Z]*', part)
            if(len(p) > 0):
                if not p[0] == part:
                    parts1.extend(p)
        fp.extend(parts1)
        return fp

    def update_maps(self, features, label):
        self.label_list.append(label)
        self.label_list = list(set(self.label_list))
        self.feature_list = list(set(self.feature_list+features.keys()))
        if label not in self.label_instances_map:
            self.label_instances_map[label] = [features]
        else:
            self.label_instances_map[label].append(features)

    def write_to_file(self):
        self.label_list = sorted(self.label_list)
        self.feature_list = sorted(self.feature_list)
        print self.label_list
        # print self.feature_list
        # pretty_print(self.feature_list)
        self.write_list_to_file(self.label_list, "liblinear_op/labels_gender")
        self.write_list_to_file(self.feature_list, "liblinear_op/features_gender")
        for label, instances in self.label_instances_map.iteritems(): #instances: all instances/people of certain class(label)
            # idx = str(self.label_list.index(label))
            label_idx = self.label_list.index(label) + 1
            for instance in instances:  #instance: features of an instance/person
                line = str(label_idx) + " "
                for feature in sorted(instance):
                    count = instance[feature]
                    feature_idx = self.feature_list.index(feature) +1
                    line += str(feature_idx) + ":" + str(count) + " "
                line = line.strip() + "\n"
                self.output_fHandle.write(line)

    def write_list_to_file(self, list1, fname):
        fHandle = open(fname, 'w')
        for k,v in enumerate(list1):
            fHandle.write(str(k+1) + "\t" + str(v) + "\n")

#idx already calculated before, maybe not such a good idea since list(set()) might change it
    def update_maps_idx(self, features, label):
        self.label_list.append(label)
        self.label_list = list(set(self.label_list))
        # self.feature_list.extend(features.keys())
        # self.feature_list = list(set(self.feature_list))
        self.feature_list = list(set(self.feature_list+features.keys()))
        label_idx = self.label_list.index(label)
        features_with_idx = {}
        for f, c in features.iteritems():
            features_with_idx[self.feature_list.index(f)] = c
        if label_idx not in self.label_instances_map:
            self.label_instances_map[label_idx] = [features_with_idx]
        else:
            self.label_instances_map[label_idx].append(features_with_idx)

    def write_to_file_idx(self):
        # print self.label_list
        # pretty_print(self.feature_list)
        for label_idx, instances in self.label_instances_map.iteritems(): #instances: all instances/people of certain class(label)
            # idx = str(self.label_list.index(label))
            for instance in instances:  #instance: features of an instance/person
                line = str(label_idx) + " "
                for feature_idx in sorted(instance):
                    count = instance[feature_idx]
                    line += str(feature_idx) + ":" + str(count) + " "
                line = line.strip() + "\n"
                self.output_fHandle.write(line)

#extra
def pretty_print(list1):
    for k,v in enumerate(list1):
        print k, ":", v,
    print "\n"


if __name__ == "__main__":
    # age_gender_file = "/home/prasha/Dropbox/DailyStrength/username_id_age_gender_both_age_gender.txt"
    # people_html_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/data_pan14_1"
    username_text_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/username_data_pan14_train_test/train"
    # username_text_dir = "/home/prasha/Documents/dailystrength/author_profiling_data/test_small_username_data_pan14"
    output_file = "liblinear_op/username_liblinear_train_gender"
    ull = UsernameLibLinear(username_text_dir, open(output_file, 'w'))

    # print get_firstname_partitions("Abc_def.ghFi")






