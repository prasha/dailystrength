__author__ = 'shrprasha'

import os
from dill import load, dump


def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

def create_dumps(obj, fpath):
    with open(fpath, "w") as fhandle:
        dump(obj, fhandle)

## actual create
# if __name__ == "__main__":
#     users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps"
#
#     train_users = load_dumps(os.path.join(users_dumps_rootdir, "train.dump"))
#     part_count = len(train_users) / 3
#
#     train_tuples = train_users.items()
#
#     train1 = dict(train_tuples[:part_count])
#     train2 = dict(train_tuples[part_count:part_count*2])
#     train3 = dict(train_tuples[part_count*2:])
#
#     create_dumps(train1, os.path.join(users_dumps_rootdir, "train1.dump"))
#     create_dumps(train2, os.path.join(users_dumps_rootdir, "train2.dump"))
#     create_dumps(train3, os.path.join(users_dumps_rootdir, "train3.dump"))


##small
if __name__ == "__main__":
    users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps"
    train_users = load_dumps(os.path.join(users_dumps_rootdir, "train.dump"))
    train_tuples = train_users.items()
    train_small = dict(train_tuples[:3])
    create_dumps(train_small, os.path.join(users_dumps_rootdir, "train_small.dump"))


# test
# if __name__ == "__main__":
#     users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps"
#
#     train_users = load_dumps(os.path.join(users_dumps_rootdir, "train.dump"))
#
#     train1 = load_dumps(os.path.join(users_dumps_rootdir, "train1.dump"))
#     train2 = load_dumps(os.path.join(users_dumps_rootdir, "train2.dump"))
#     train3 = load_dumps(os.path.join(users_dumps_rootdir, "train3.dump"))
#
#     print len(train_users), len(train1), len(train2), len(train3), len(train1) + len(train2) + len(train3)
#     train1.update(train2)
#     train1.update(train3)
#     print len(train1)
#     if train1 == train_users:
#         print "True"





