__author__ = 'shrprasha'

from dill import load, dump
import os
from user import User

__author__ = 'shrprasha'



def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

def get_files_in_folder(folder):
    all_files = sorted(next(os.walk(folder))[2])
    if ".DS_Store" in all_files:
        all_files.remove(".DS_Store")
    return all_files

def dump_features_non_zero(dirpath, fname, features): # for feature that do not need cleaning; already only non-zero
    with open(os.path.join(dirpath, fname), "w") as opfhandle:
        dump(features, opfhandle)

if __name__ == "__main__":
    feature_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_feature_dumps"
    users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps"

    old_familial_rootdir = os.path.join(feature_dumps_rootdir, "familial_features_no_hubby")
    familial_buckets_rootdir = os.path.join(feature_dumps_rootdir, "familial_gender_bucket_features")
    new_familial_rootdir = os.path.join(feature_dumps_rootdir, "familial_features")

    fnames = get_files_in_folder(familial_buckets_rootdir)
    count = 0
    for fname in fnames:
        old_idx_feature_val = load_dumps(os.path.join(old_familial_rootdir, fname))
        buckets_idx_feature_val = load_dumps(os.path.join(familial_buckets_rootdir, fname))
        if len(old_idx_feature_val) == 0 and len(buckets_idx_feature_val) > 0:
            new_idx_feature_val = {37: buckets_idx_feature_val[2]}
            count += 1
        else:
            new_idx_feature_val = old_idx_feature_val
        dump_features_non_zero(new_familial_rootdir, fname, new_idx_feature_val)
    print count




