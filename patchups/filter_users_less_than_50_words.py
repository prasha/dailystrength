__author__ = 'shrprasha'

from pickle import load, dump
import os


def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

def filter_users_with_less_than_x_words_total(users_dict):
    filtered_dict = {}
    for fname in users_dict:
        user_posts = load_dumps(os.path.join(user_posts_dump_rootdir, fname))
        tokens_count = 0
        for post in user_posts:
            tokens_count += len(post.token_tags)
        if tokens_count >= x:
            filtered_dict[fname] = users_dict[fname]
        else:
            print fname, tokens_count
    print len(users_dict), len(filtered_dict)
    return filtered_dict


if __name__ == "__main__":
    x = 50
    users_dumps_rootdir_old = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps/old_containing_empty_posts"
    users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps"
    user_posts_dump_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/user_post_dumps"


    train_users_old = load_dumps(os.path.join(users_dumps_rootdir_old, "train.dump"))
    dev_users_old = load_dumps(os.path.join(users_dumps_rootdir_old, "dev.dump"))
    # test_users_old = load_dumps(os.path.join(users_dumps_rootdir_old, "test.dump"))

    print "done loading"

    train_users = filter_users_with_less_than_x_words_total(train_users_old)
    dev_users = filter_users_with_less_than_x_words_total(dev_users_old)
    # test_users = filter_users_with_less_than_x_words_total(test_users_old)

    print "done filtering"

    dump(train_users, open(os.path.join(users_dumps_rootdir, "train.dump"), "w"))
    dump(dev_users, open(os.path.join(users_dumps_rootdir, "dev.dump"), "w"))
    # dump(test_users, open(os.path.join(users_dumps_rootdir, "test.dump"), "w"))
