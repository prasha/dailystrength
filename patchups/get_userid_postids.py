__author__ = 'shrprasha'

from pickle import load, dump
from user import User, Post
import os


def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

def print_obj_attrs(obj):
    attrs = vars(obj)
    print ', '.join("%s: %s" % item for item in attrs.items())

def find_posts_from_uid_in_disease(disease, uid):
    disease_post_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/disease_post_dumps"
    disease_posts = load_dumps(os.path.join(disease_post_dumps_rootdir, disease + "_posts.dump"))
    for post in disease_posts:
        if post.uid == uid:
            print_obj_attrs(post)


if __name__ == "__main__":
    find_posts_from_uid_in_disease("Anger-Management", "111873")

