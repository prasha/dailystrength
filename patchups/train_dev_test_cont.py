from user import User

__author__ = 'shrprasha'


import os
from dill import load, dump

def create_dumps(obj, fpath):
    with open(fpath, "w") as fhandle:
        dump(obj, fhandle)

def load_dumps(fpath):
    with open(fpath, "r") as fhandle:
        obj = load(fhandle)
    return obj

def update_age_groups_to_cont(type):
    all_users = load_dumps(os.path.join(users_dumps_rootdir, type + ".dump"))
    updated_count_users = {}
    for uname in sorted(all_users):
        user = all_users[uname]
        user.age_group = age_group_map[user.age_group]
        new_fname = user.uid + "_" + user.age_group
        print new_fname
        updated_count_users[new_fname] = user
    create_dumps(updated_count_users, "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps/old_cont_all/" + type + ".dump")


if __name__ == "__main__":
    users_dumps_rootdir = "/Users/shrprasha/Projects/resources/dailystrength/ds_processed_data/users_dumps/old_all_cont"
    types = ["train", "dev", "test"]
    age_group_map = {"12-16": "12-17", "19-28": "18-29", "31-48": "30-49", "51-63": "50-64", "66-xx":  "65-xx", "66-200":  "65-xx"}
    for type in types:
        update_age_groups_to_cont(type)

